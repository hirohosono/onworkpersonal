//
//  LocationListTVC.swift
//  OnWork
//
//  Created by 細野浩明 on 2016/07/26.
//  Copyright © 2016年 Hiroaki Hosono. All rights reserved.
//

import UIKit

class LocationListTVC: UITableViewController {

    
    let appDl:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得
 
    @IBAction func unwindToLocationList(_ segue: UIStoryboardSegue) {
        let controller = self.parent as! WorkPlaceVC
        controller.updateButtonState()

        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
        
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if appDl.locationLists[appDl.WPIndex].count < 1 {
            return 1
        } else {
            return appDl.locationLists[appDl.WPIndex].count
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationListCell", for: indexPath)
        
        if appDl.locationLists[appDl.WPIndex].count < 1 {
            cell.textLabel?.text = "No Location"
        } else {
            cell.textLabel?.text = appDl.locationLists[appDl.WPIndex][indexPath.row].LocationName
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("LocationList Index=",indexPath.row)
   }
    



    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if appDl.locationLists[appDl.WPIndex].count <= 1 { // 残り1つは削除できない
            return false
        }
        return true
    }

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if removeLocation(appDl.locationLists[appDl.WPIndex][indexPath.row]) {
                appDl.locationLists[appDl.WPIndex].remove(at: indexPath.row)
                let controller = self.parent as! WorkPlaceVC
                controller.updateButtonState()
                tableView.deleteRows(at: [indexPath], with: .fade)
                tableView.reloadData()
            }
            
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "削除"
    }


    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("prepareForSegue:",segue.identifier!)
        
        switch segue.identifier ?? "" {
        case "toLocationSetting":
            let controller = segue.destination as! LocationSettingVC
            let indexPath = self.tableView.indexPathForSelectedRow!
            
            if appDl.locationLists[appDl.WPIndex].count < 1 {
                controller.isNewLocation = true
                controller.SelectedIndex = 0
            } else {
                controller.selectedLocation = appDl.locationLists[appDl.WPIndex][indexPath.row]
                controller.SelectedIndex = indexPath.row
                controller.isNewLocation = false
            }
            
        default :
            break
        }
    }



}
