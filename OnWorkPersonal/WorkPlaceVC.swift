//
//  WorkPlaceVC.swift
//  OnWork
//
//  Created by 細野浩明 on 2016/07/26.
//  Copyright © 2016年 Hiroaki Hosono. All rights reserved.
//

import UIKit

class WorkPlaceVC: UIViewController {
    
    var appDl:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得

    
    @IBOutlet weak var btnSave: BackgroundEnabledButton!
    @IBOutlet weak var txWorkPlaceLabel: UITextField!
    @IBOutlet weak var txCloseDate: UITextField!
    @IBOutlet weak var txHourlyRate: UITextField!
    @IBOutlet weak var txOverRate: UITextField!
    @IBOutlet weak var txMidnightRate: UITextField!
    @IBOutlet weak var btnAddLocation: UIButton!
    @IBOutlet weak var btnAddTimeTable: UIButton!
    
    func updateButtonState() {
        if appDl.locationLists[appDl.WPIndex].count >= 3 {
            btnAddLocation.isEnabled = false
        } else {
            btnAddLocation.isEnabled = true
        }
        let timeTableMax:Int
        switch appDl.perchase.license.isBasicAvalable() {
        case .none, .expired:
            timeTableMax = 3
        case .avalable, .permanent :
            timeTableMax = 10
            print("User License is avalable, Expand timeTableMax to 10")
        }
        if appDl.timeTableLists[appDl.WPIndex].count >= timeTableMax {
            btnAddTimeTable.isEnabled = false
        } else {
            btnAddTimeTable.isEnabled = true
        }
    }
    
    func checkInputValue() -> Bool {
        var isSafe:Bool = true
        
        if txWorkPlaceLabel.text?.isEmpty ?? true {
            isSafe = false
            txWorkPlaceLabel.backgroundColor = myColorPastelLightPink
        } else {
            txWorkPlaceLabel.backgroundColor = .white
        }
        // CloseDate
        if Int(txCloseDate.text!) == nil {
            isSafe = false
            txCloseDate.textColor = NC_014_KARAKURENAI
        } else {
            let inputVal:Int = Int(txCloseDate.text!)!
            if (inputVal >= 1) && (inputVal <= 31) {
                txCloseDate.textColor = .black
            } else {
                isSafe = false
                txCloseDate.textColor = NC_014_KARAKURENAI
            }
        }
        // HourlyRate
        if Int(txHourlyRate.text!) == nil {
            isSafe = false
            txHourlyRate.textColor = NC_014_KARAKURENAI
        } else {
            let inputVal:Int = Int(txHourlyRate.text!)!
            if (inputVal >= 1) && (inputVal <= 30000) {
                txHourlyRate.textColor = .black
            } else {
                isSafe = false
                txHourlyRate.textColor = NC_014_KARAKURENAI
            }
        }
        // OverRate
        if Int(txOverRate.text!) == nil {
            isSafe = false
            txOverRate.textColor = NC_014_KARAKURENAI
        } else {
            let inputVal:Int = Int(txOverRate.text!)!
            if (inputVal >= 0) && (inputVal <= 500) {
                txOverRate.textColor = .black
            } else {
                isSafe = false
                txOverRate.textColor = NC_014_KARAKURENAI
            }
        }
        // midNightRate
        if Int(txMidnightRate.text!) == nil {
            isSafe = false
            txMidnightRate.textColor = NC_014_KARAKURENAI
        } else {
            let inputVal:Int = Int(txMidnightRate.text!)!
            if (inputVal >= 0) && (inputVal <= 500) {
                txMidnightRate.textColor = .black
            } else {
                isSafe = false
                txMidnightRate.textColor = NC_014_KARAKURENAI
            }
        }
        
        return isSafe
    }
    @IBAction func btnSaveOnClick(_ sender: BackgroundEnabledButton) {
        if checkInputValue() {
            appDl.workPlaces[appDl.WPIndex].workPlaceLabel = txWorkPlaceLabel.text ?? "勤務地１"
            appDl.workPlaces[appDl.WPIndex].closeDate = Int(txCloseDate.text ?? "31")!
            appDl.workPlaces[appDl.WPIndex].hourlyRate = Int(txHourlyRate.text ?? "1000")!
            appDl.workPlaces[appDl.WPIndex].overTimeRate = Int(txOverRate.text ?? "25")!
            appDl.workPlaces[appDl.WPIndex].midNightRate = Int(txMidnightRate.text ?? "25")!
            
            updateworkPlace(appDl.workPlaces[appDl.WPIndex])
            
            performSegue(withIdentifier: "unwindToSettingMenu", sender: nil)
        }
       
    }
    
    
    func dispWPInfo() {
        //workPlaces = getWorkPlaces(appDl.personalUser.IDNo)
        if appDl.workPlaces.count > 0 {
            txWorkPlaceLabel.text = appDl.workPlaces[appDl.WPIndex].workPlaceLabel
            txCloseDate.text = appDl.workPlaces[appDl.WPIndex].closeDate.description
            txHourlyRate.text = appDl.workPlaces[appDl.WPIndex].hourlyRate.description
            txOverRate.text = appDl.workPlaces[appDl.WPIndex].overTimeRate.description
            txMidnightRate.text = appDl.workPlaces[appDl.WPIndex].midNightRate.description
            _ = checkInputValue()
        }
        updateButtonState()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        dispWPInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("prepareForSegue:",segue.identifier!)
         switch segue.identifier ?? "" {
         case "toNewLocation":
             let controller = segue.destination as! LocationSettingVC
            controller.isNewLocation = true
            controller.SelectedIndex = appDl.locationLists[appDl.WPIndex].count
        
         case "toNewTimeTable":
            let controller = segue.destination as! TimeTableSettingVC
            controller.isNewTimeTable = true
            controller.SelectedIndex = appDl.timeTableLists[appDl.WPIndex].count
            
         default :
         break
         }

    }
    

}
