//
//  TimeTableListTVC.swift
//  OnWork
//
//  Created by 細野浩明 on 2016/07/26.
//  Copyright © 2016年 Hiroaki Hosono. All rights reserved.
//

import UIKit

class TimeTableListTVC: UITableViewController {

//    var selectedTimeTable = timeTableDef()
//    var SelectedIndex:Int = 0

    let appDl:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得
   
    let dtc = dateTimeControll()

    @IBAction func unwindToTimeTableList(_ segue: UIStoryboardSegue) {
        let controller = self.parent as! WorkPlaceVC
        controller.updateButtonState()

        tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if appDl.timeTableLists[appDl.WPIndex].count < 1 {
            return 1
        } else {
            return appDl.timeTableLists[appDl.WPIndex].count
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimeTableListCell", for: indexPath)
        
        if appDl.timeTableLists[appDl.WPIndex].count < 1 {
            let lbName = cell.viewWithTag(1) as! UILabel
            lbName.text = "No TimeTable"
            let lbClipped = cell.viewWithTag(2) as! UILabel
            lbClipped.text = "No"
            let lbStart = cell.viewWithTag(3) as! UILabel
            lbStart.text = "--"
            let lbEnd = cell.viewWithTag(4) as! UILabel
            lbEnd.text = "--"
        } else {
            let tableName = appDl.timeTableLists[appDl.WPIndex][indexPath.row].tableName
            var clippedName = tableName
            if tableName.count > 2 {
                clippedName = tableName[..<tableName.index(tableName.startIndex, offsetBy: 2)].description
            }
            
            let lbName = cell.viewWithTag(1) as! UILabel
            lbName.text = tableName
            let lbClipped = cell.viewWithTag(2) as! UILabel
            lbClipped.text = clippedName
            let lbStart = cell.viewWithTag(3) as! UILabel
            lbStart.text = appDl.timeTableLists[appDl.WPIndex][indexPath.row].startTime
            let lbEnd = cell.viewWithTag(4) as! UILabel
            lbEnd.text = appDl.timeTableLists[appDl.WPIndex][indexPath.row].endTime
        }
        
        return cell
    }
    
 /*   override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedTimeTable = appDl.timeTableLists[appDl.WPIndex][indexPath.row]
        SelectedIndex = indexPath.row
    }
*/

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if appDl.timeTableLists[appDl.WPIndex].count <= 1 { // 残り1つは削除できない
            return false
        }
        return true
    }
    

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if removeTimeTable(appDl.timeTableLists[appDl.WPIndex][indexPath.row]) {
                appDl.timeTableLists[appDl.WPIndex].remove(at: indexPath.row)
                let controller = self.parent as! WorkPlaceVC
                controller.updateButtonState()
                tableView.deleteRows(at: [indexPath], with: .fade)
                tableView.reloadData()
            }

        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
 
    
    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "削除"
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */


    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("prepareForSegue:",segue.identifier!)

         switch segue.identifier ?? "" {
         case "toTimeTableSetting":
            let controller = segue.destination as! TimeTableSettingVC
            let indexPath = self.tableView.indexPathForSelectedRow!
            
            controller.selectedTimeTable = appDl.timeTableLists[appDl.WPIndex][indexPath.row]
            controller.SelectedIndex = indexPath.row
            controller.isNewTimeTable = false
            
         default :
         break
         }
    }

}
