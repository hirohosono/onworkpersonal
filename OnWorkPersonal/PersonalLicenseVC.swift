//
//  PersonalLicenseVC.swift
//  RepeatAfterYou
//
//  Created by 細野浩明 on 2017/11/04.
//  Copyright © 2017年 Melting Pot LLC. All rights reserved.
//

import UIKit
import StoreKit

class PersonalLicenseVC: UIViewController {
    let appDl:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得

    // MARK: - Variable for interface
    
    // MARK: - Variable for internal use
    // For License
    var myProducts : [SKProduct] = []

    // MARK: - @IBOutlet
    //@IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var vLicense: UIView!
    @IBOutlet weak var lbLicense: UILabel!
    @IBOutlet weak var icLicense: UIImageView!
    @IBOutlet weak var lbBasicLicense: UILabel!
    @IBOutlet weak var lbBasicLicensePrice: UILabel!
    @IBOutlet weak var lbBasicLicenseNote: UILabel!
    @IBOutlet weak var lbBasicLicenseExpireDate: UILabel!
    @IBOutlet weak var lbBasicLicenseExpireDateNote: UILabel!
    @IBOutlet weak var swBasicLicense: UISwitch!
    @IBOutlet weak var btnRestore: BackgroundEnabledButton!
    @IBOutlet weak var btnBuy: BackgroundEnabledButton!
    @IBOutlet weak var icBasicLicenseLock: UIImageView!
    @IBOutlet weak var icBasicLicenseExpireDate: UIImageView!
    @IBOutlet weak var tvBasicLicenseExplanation: UITextView!
    @IBOutlet weak var btnPrivacyPolicy: UIButton!
    @IBOutlet weak var btnTermsOfUse: UIButton!


    // MARK: - @IBAction
    @IBAction func tapScrennVD(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    @IBAction func swBasicLicenseChanged(_ sender: UISwitch) {
        buttonEnable()
    }
    @IBAction func btnBuyOnClick(_ sender: BackgroundEnabledButton) {
        buttonDisable()
        
        switch appDl.perchase.license.isBasicAvalable() {
        case .none,.expired:
            confirmBuying(appDl.perchase.license,product:myProducts[licenseType.BASIC.rawValue],type:licenseType.BASIC)
        default:
            break ;
        }
        
        buttonEnable()
    }
    @IBAction func btnRestoreOnClick(_ sender: BackgroundEnabledButton) {
        buttonDisable()
        print("リストア開始 \(#function)[\(#line)]")
        startRestore()
        
        //buttonEnable()
    }
    @IBAction func btnPrivacyPolicyOnClick(_ sender: UIButton) {
        let url:URL = URL(string: "http://meltingpot.co.jp/privacypolicy/")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func btnTermsOfUseOnClick(_ sender: UIButton) {
        let url:URL = URL(string: "http://meltingpot.co.jp/terms-of-use/")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }

    // MARK: - functions
    func buttonDisable() {
        btnBuy.isEnabled = false
        btnRestore.isEnabled = false
    }

    func buttonEnable() {
        switch appDl.perchase.license.isBasicAvalable() {
        case .avalable, .permanent:
            btnRestore.isEnabled = false
            btnBuy.isEnabled = false
        case .none, .expired:
            btnBuy.isEnabled = true
            btnRestore.isEnabled = true
        }
    }
    
    func checkProductCode(_ code:String) -> Bool {
        let specialCode = "MP19600720HH"
        let lockcode = appDl.dtc.getDateToString(Date(), format: "##yyyyMMdd##")
        print("lockcode=",lockcode," \(#function)[\(#line)]")
        if (lockcode == code) || (specialCode == code) {
            print("code=",code, " -> Hit!"," \(#function)[\(#line)]")
            return true
        }
        print("code=",code, " ->/u{1b}]92m NG !/u{1b}]m"," \(#function)[\(#line)]")
        return false
    }
    
    func confirmUserCode() {
     
        let alertController = UIAlertController(
            title: NSLocalizedString("Confirm license key", tableName: "Message",comment: "ライセンスキー確認"),
            message: NSLocalizedString("confirm-User-Code", tableName: "Message",value: "If you have a user license key already, enter it and press \"Yes\".", comment: "既にライセンスキーをお持ちの場合はライセンスキーを入力して「はい」を押してください。\nお持ちでない場合は「いいえ」に進んでください。"),
            preferredStyle: .alert)
        
        
        let cancelAction = UIAlertAction(
            title: NSLocalizedString("Yes", tableName: "Message",comment: "はい"),
            style: .cancel,
            handler:{
                (action:UIAlertAction!) -> Void in
                let textField = alertController.textFields![0] as UITextField
                if !(textField.text?.isEmpty ?? true) {
                    if self.checkProductCode(textField.text!) {
                        self.appDl.perchase.license.permanent = true
                        self.appDl.perchase.saveLicenseLocal()
                        self.restoreStateLicensed()
                    } else {
                        self.confirmUserCode() // もう一度
                    }
                }
        })
        alertController.addAction(cancelAction)
        
        
        let defaultAction = UIAlertAction(
            title: NSLocalizedString("No", tableName: "Message",comment: "いいえ"),
            style: .destructive, handler: {
                (action:UIAlertAction!) -> Void in
                self.confirmBuying(self.appDl.perchase.license,product:self.myProducts[licenseType.BASIC.rawValue],type:licenseType.BASIC)

        })
        
        alertController.addAction(defaultAction)
        // UIAlertControllerにtextFieldを追加
        alertController.addTextField { (textField:UITextField!) -> Void in
            textField.text = ""
            textField.placeholder = NSLocalizedString("License key", tableName: "Message",comment: "ライセンスキー")
        }
        alertController.popoverPresentationController?.sourceView = view
        alertController.popoverPresentationController?.sourceRect = view.frame
        
        present(alertController, animated: true, completion: nil)

    }
    
    func setupDisplayData() {
        appDl.restoreUserInfo()
    }
    
    func saveData() {
        appDl.saveUserInfo()
    }

    
    ///購入確認
    func confirmBuying(_ license:mylicense,product:SKProduct,type:licenseType){
        var messageStr:String = ""
        
        if type == .BASIC {
            messageStr = NSLocalizedString("btnBuy-message-confirm-basic", tableName: "Message",value: "Do you buy basic license?", comment: "新規に「ベーシックライセンス」を購入しますか？")
        }
        
        let alertController = UIAlertController(
            title: NSLocalizedString("Confirm Buying", tableName: "Message", comment: "購入確認"),
            message: messageStr ,
            preferredStyle: .alert)
        
        let defaultAction:UIAlertAction = UIAlertAction(title: NSLocalizedString("Confirm !", tableName: "Message", comment: "購入する") ,
                                                        style: UIAlertActionStyle.default,
                                                        handler:{
                                                            (action:UIAlertAction!) -> Void in
                                                            self.startPurchase(product)
        })
        
        alertController.addAction(defaultAction)
        
        let cancelAction = UIAlertAction(
            title: NSLocalizedString("Cancel", tableName: "Message", comment: "キャンセル"),
            style: .cancel, handler: {
                (action:UIAlertAction!) -> Void in
                self.buttonEnable()
        })
        
        alertController.addAction(cancelAction)
        alertController.popoverPresentationController?.sourceView = view
        alertController.popoverPresentationController?.sourceRect = btnBuy.frame
        
        present(alertController, animated: true, completion: nil )
        
    }
    
    
    /// 課金開始
    func startPurchase(_ product : SKProduct) {
        //デリゲード設定
        OWPurchaseManager.sharedManager().delegate = appDl.perchase
        //課金処理開始
        OWPurchaseManager.sharedManager().startWithProduct(product)
    }
    
    /// リストア開始
    func startRestore() {
        //デリゲード設定
        OWPurchaseManager.sharedManager().delegate = appDl.perchase
        //リストア開始
        OWPurchaseManager.sharedManager().startRestore()
    }
    
    
    func synchronousRequest(_ request:URLRequest) -> (JSON:Data?,resp:URLResponse?){
        let semaphore = DispatchSemaphore(value: 0)
        var result:Data? = nil
        var response: URLResponse? = nil
        
        let configuration = URLSessionConfiguration.ephemeral
        let session = URLSession(configuration: configuration)
        let task = session.dataTask(with: request, completionHandler: {resData,resp,error in
            if error != nil {
                semaphore.signal()
                return
            } else {
                result = resData
                response = resp
                semaphore.signal()
                return
            }
        })
        
        task.resume()
        _ = semaphore.wait(timeout: .distantFuture)
        
        return (result,response)
        
    }
    // appStoreに検証を投げる処理
    func verifyFromStore(_ receipt:String,url:URL) ->(isSandboxRequired:Bool, dict:NSDictionary?) {
        
        let requestContents: NSDictionary = ["receipt-data": receipt,"password":productSecretKey] as NSDictionary
        let requestData: Data
        do {
            requestData = try JSONSerialization.data(withJSONObject: requestContents, options: JSONSerialization.WritingOptions())
        } catch  let error1  {
            print("/u{1b}]92m requestData error !/u{1b}]m:\(error1)"," \(#function)[\(#line)]")
            return (false,nil)
        }
        
        var request = URLRequest(url: url)
        
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField:"content-type")
        request.timeoutInterval = 5.0
        request.httpMethod = "POST"
        request.httpBody = requestData
        
        var resultJSON:Data? = nil
        var response: URLResponse? = nil
        
        (resultJSON,response) = synchronousRequest(request)
        if response == nil {
            return (false,nil)
        }
        
        let httpResponse = response as? HTTPURLResponse
//        print("httpResponse:",httpResponse?.description ?? "/u{1b}]92m No httpResponse!/u{1b}]m"," \(#function)[\(#line)]")
        print("httpResponse.code:\(httpResponse?.statusCode ?? 0) \(#function)[\(#line)]")
        if httpResponse?.statusCode != 200 {
            return (false,nil)
        } else {
            // 帰ってきたデータを文字列に変換.
            var resultDict:[AnyHashable:Any] = [:]
            do{
                resultDict =  try JSONSerialization.jsonObject(with: resultJSON!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary as! [AnyHashable : Any]
            } catch  let error1  {
                print("/u{1b}]92m Error !/u{1b}]m:\(error1)"," \(#function)[\(#line)]")
                return (true,nil)
            }
            let status = resultDict["status"] as? Int ?? -1
            if status == 0 { // Receipt is OK
                return (false,resultDict as NSDictionary?)
            } else if status == 21007 { // Sandbox Receipt
                return (true,nil)
                
            } else { // Other error
                return (false,nil)
            }
            
        }
        
    }
    
    func restoreStateLicensed() {
        switch appDl.perchase.license.isBasicAvalable() {
        case .avalable:
            swBasicLicense.isOn = true
            swBasicLicense.isUserInteractionEnabled = false
            lbBasicLicenseExpireDate.text = appDl.dtc.getDateToString(appDl.perchase.license.basicExpireDate, format: "yyyy/MM/dd HH:mm:ss", nillStr: "----/--/--")
            icBasicLicenseLock.image = #imageLiteral(resourceName: "icoUnLocked")
            icBasicLicenseLock.backgroundColor = ICO_COL_UNLOCKED
            icBasicLicenseExpireDate.backgroundColor = ICO_COL_UNLOCKED
            btnRestore.isEnabled = false
            btnBuy.isEnabled = false
        case .permanent:
            swBasicLicense.isOn = true
            swBasicLicense.isUserInteractionEnabled = false
            lbBasicLicenseExpireDate.text = "9999/12/31"
            icBasicLicenseLock.image = #imageLiteral(resourceName: "icoUnLocked")
            icBasicLicenseLock.backgroundColor = ICO_COL_UNLOCKED
            icBasicLicenseExpireDate.backgroundColor = ICO_COL_UNLOCKED
            btnRestore.isEnabled = false
            btnBuy.isEnabled = false
        case .none:
            swBasicLicense.isOn = false
            swBasicLicense.isUserInteractionEnabled = false
            lbBasicLicenseExpireDate.text = "----/--/--"
            icBasicLicenseLock.image = #imageLiteral(resourceName: "icoLocked")
            icBasicLicenseLock.backgroundColor = ICO_COL_LOCKED
            icBasicLicenseExpireDate.backgroundColor = ICO_COL_CLEAR
            btnRestore.isEnabled = true
            btnBuy.isEnabled = true
        case .expired:
            swBasicLicense.isOn = true
            swBasicLicense.isUserInteractionEnabled = false
            lbBasicLicenseExpireDate.text = appDl.dtc.getDateToString(appDl.perchase.license.basicExpireDate, format: "yyyy/MM/dd HH:mm:ss", nillStr: "----/--/--")
            icBasicLicenseLock.image = #imageLiteral(resourceName: "icoLocked")
            icBasicLicenseLock.backgroundColor = ICO_COL_LOCKED
            icBasicLicenseExpireDate.backgroundColor = ICO_COL_LOCKED
            btnRestore.isEnabled = true
            btnBuy.isEnabled = true
        }
        
        appDl.setValidMonth()

//        buttonEnable()

    }
    


    // MARK: - View Main

    override func viewDidLoad() {
        super.viewDidLoad()
        appDl.perchase.licenseVc = self
        tvBasicLicenseExplanation.text = NSLocalizedString("basic-license-explanation",
                                                           tableName: "Message",
                                                           value:
                    """
                    About basic license
                    • You can use following functions during the basic license validity period.
                     1)Aggregate up to the past year and see the schedule for the next year
                     2)Export the entered data in CSV format text
                     3)Maximum number of time tables up to 10
                     4)Hide ads
                    • Basic license is valid for 1 month. And it is automatically renewed every month.
                    • Please check the abobe monthly fee.
                    • Payment will be charged to your iTunes account.
                    • If you do not renew your license, please turn off automatic renewal at least 24 hours before the deadline.
                    • Account will be charged for renewal within 24-hours prior to the end of the current period, and identify the cost of the renewal.
                    • Subscriptions may be managed by the user and auto-renewal may be turned off by going to the user's Account Settings after purchase.
                    """,
                                                           comment: "ベーシックライセンスの説明文")
        setupDisplayData()
        appDl.perchase.readProductPurchase()
        OWPurchaseManager.sharedManager().clearTransactionQueue()
        restoreStateLicensed()

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        appDl.perchase.licenseVc = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("prepareForSegue:\(segue.identifier?.description)"," \(#function)[\(#line)]")
        switch segue.identifier ?? "" {
        case "unwindLicenceToSettingMenu":
            print("unwind To Setting menu. \(#function)[\(#line)]")
        default :
            break
        }
    }
    

}
