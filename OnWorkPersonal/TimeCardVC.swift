//
//  TimeCardVC.swift
//  OnWork
//
//  Created by 細野浩明 on 2016/07/31 made from CardViewController.swift
//  Copyright © 2016年 Hiroaki Hosono. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications
import Firebase
import GoogleMobileAds

class TimeCardVC: UIViewController,CLLocationManagerDelegate, UITextFieldDelegate {
    
    // appDelegateの取得
    let appDl:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得

    // MARK: - Variable for interface
    
    // MARK: - Variable for internal use

    // 現在選択されている勤務先、勤務地、タイムテーブル
    var selectedworkPlace = workPlaceInfo()
    var WPIndex:Int = 0
    var selectedTimeTable = timeTableDef()
    var TTIndex:Int = 0
    var selectedLocation = locationDef()
    var LocIndex:Int = 0
    var currentLocation = locationDef()
    var currentLocIndex:Int = 0
    var isHolyday:Bool = false
 
    let locManager = CLLocationManager()
    var activityIndicator: UIActivityIndicatorView!
    
    var scrollbase:CGPoint = CGPoint(x: 0.0,y: 0.0)
    var alreadyStartingCoordinateSet : Bool = false
    var currentLatitude:CLLocationDegrees = 0.0
    var currentLongitude:CLLocationDegrees = 0.0
    var currentAddress:String = ""
    var currentDistance:Int = -1 // meter -1:Not Set
    var dailyLog: NCMBObject?
    var timer : Timer!
    var isStartCheckLocation:Bool = false
    var isStartStamped:Bool = false
    var startStampedTime:Date? = nil
    var startStampedColor:UIColor? = UIColor.black
    var startTimeOfWork:Date? = nil
    var isEndStamped:Bool = false
    var endStampedTime:Date? = nil
    var endStampedColor:UIColor? = UIColor.black
    var endTimeOfWork:Date? = nil
    var isOnWorkLocation:Bool = false
    var isInAdjustment:Bool = false
    var todaysDateString:String = "" // 今日(firstday)の日付 yyyy/mm/dd
    var memoText:String = ""
    var isInitiated:Bool = false
    struct remindMessage {
        var message:String
        var time:Date
    }
    
    // MARK: - @IBOutlet

    @IBOutlet var vSelf: UIView!
    @IBOutlet weak var lbCurrentPosition: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbWorkPlaceLabel: UILabel!
    @IBOutlet weak var lbLocation: UILabel!
    @IBOutlet weak var lbClock: UILabel!
    @IBOutlet weak var lbTimeTable: UILabel!
    @IBOutlet weak var lbStart: UILabel!
    @IBOutlet weak var lbEnd: UILabel!
    @IBOutlet weak var lbStartTime: UILabel!
    @IBOutlet weak var lbEndTime: UILabel!
    @IBOutlet weak var btnStartRewind: UIButton!
    @IBOutlet weak var btnStartForward: UIButton!
    @IBOutlet weak var btnEndRewind: UIButton!
    @IBOutlet weak var btnEndForward: UIButton!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var btnCancelStart: UIButton!
    @IBOutlet weak var btnEnd: UIButton!
    @IBOutlet weak var btnCancelEnd: UIButton!
    @IBOutlet weak var iconStartStamp: UIImageView!
    @IBOutlet weak var iconEndStamp: UIImageView!
    @IBOutlet weak var lbStartStamp: UILabel!
    @IBOutlet weak var lbEndStamp: UILabel!
    @IBOutlet weak var txMemo: UITextField!
    @IBOutlet var scrCardView: UIScrollView!
    @IBOutlet weak var lbClockWidthCOnstraint: NSLayoutConstraint!
    @IBOutlet weak var btnEditMemo: UIButton!
    @IBOutlet weak var sgHolyday: UISegmentedControl!
    @IBOutlet weak var btnLink: UIButton!
    @IBOutlet weak var vAdMobBanner: GADBannerView!
    
    // MARK: - @IBAction

    @IBAction func btnStartRewindOnClick(_ sender: UIButton) {
        // とりあえず、自分で時間変更できなくするので何もしない
    }
    
    @IBAction func btnStartForwardOnClick(_ sender: UIButton) {
        // とりあえず、自分で時間変更できなくするので何もしない
    }
    
    @IBAction func btnEndRewindOnClick(_ sender: UIButton) {
        // とりあえず、自分で時間変更できなくするので何もしない
    }
    
    @IBAction func btnEndForwardOnClick(_ sender: UIButton) {
        // とりあえず、自分で時間変更できなくするので何もしない
    }
    
    @IBAction func btnStartOnClick(_ sender: UIButton) {
        let location = NCMBGeoPoint(latitude: currentLatitude, longitude: currentLongitude)
        let now = Date()
        let startTime = appDl.dtc.getDateLine(lbStartTime.text!)
        let manuallyFlag:Bool = !isOnWorkLocation
        let completion: ((_ dailyLogObj:NCMBObject) -> Void) = { dailyLogObj in
            dailyLogObj.setObject(self.appDl.today, forKey: "Date")
            dailyLogObj.setObject(self.appDl.personalUser.IDNo, forKey: "UserID")
            dailyLogObj.setObject(startTime, forKey: "StartTime")
            dailyLogObj.setObject(self.currentLocation.LocationName, forKey: "StartLocation")
            dailyLogObj.setObject(location, forKey: "ArrivalGeo")
            dailyLogObj.setObject(now, forKey: "ArrivalTime")
            dailyLogObj.setObject(self.currentAddress, forKey: "ArrivalAddress")
            dailyLogObj.setObject(self.appDl.todaysData.companyName, forKey: "CompanyName")
            dailyLogObj.setObject(self.appDl.todaysData.companyID, forKey: "companyID")
            dailyLogObj.setObject(self.appDl.personalUser.groupName, forKey: "GroupName")
            dailyLogObj.setObject(self.appDl.todaysData.timeTable, forKey: "TimeTableName")
            dailyLogObj.setObject(self.appDl.todaysData.timeTableID, forKey: "timeTableID")
            dailyLogObj.setObject(self.appDl.todaysData.locationID, forKey: "locationID")
            dailyLogObj.setObject(manuallyFlag, forKey: "StartManually")
            dailyLogObj.setObject(false, forKey: "isDayOff")
            dailyLogObj.setObject(dayKindType.DayOn.rawValue, forKey: "DayKind")
            print("startTime=",startTime)
            
            dailyLogObj.saveInBackground({(error) in
                if error != nil {print("Save error : ",error!)}
            })
        }
        if getDailyLogObject(&dailyLog,user: appDl.personalUser ,todaysInfo: appDl.todaysData ,closure: completion) {
            print("Logを取得",#function, #line)
            
            btnStartRewind.isHidden = true
            btnStartForward.isHidden = true
            btnCancelStart.isHidden = false
            btnCancelStart.isEnabled = !isEndStamped
            btnStart.isEnabled = false
            btnStart.alpha = 0.5
            iconStartStamp.isHidden = false
            lbStartStamp.text = appDl.dtc.getLocalDateTimeString(now)
            lbStartStamp.isHidden = false
            isStartStamped = true
            startStampedTime = now
            self.startTimeOfWork = startTime
            startStampedColor = lbStartTime.textColor
            //退勤のアラートをセット
            var messages:[remindMessage] = []
            var endTime:Date = appDl.dtc.getDateLine(selectedTimeTable.endTime,baseDate: startTime)
            endTime = endTime.addingTimeInterval(180.0) // 定時の３分後
            var message = remindMessage(message:"定時(退勤時刻)になりました！", time: endTime)
            messages.append(message)
            endTime = endTime.addingTimeInterval(3600.0) // その60分後
            message = remindMessage(message:"定時(退勤時刻)から１時間経過しました！", time: endTime)
            messages.append(message)
            endTime = endTime.addingTimeInterval(3600.0) // その60分後
            message = remindMessage(message:"定時(退勤時刻)から２時間経過しました！", time: endTime)
            messages.append(message)
            endTime = endTime.addingTimeInterval(3600.0) // その60分後
            message = remindMessage(message:"定時(退勤時刻)から３時間経過しました！", time: endTime)
            messages.append(message)
            
            setNotifications(messages)
            
            saveCurrentState()
            
            appDl.isUpdateStamp = true
            
        }
    }
    
    @IBAction func btnCancelStartOnClick(_ sender: UIButton) {
        let alertController = UIAlertController(
            title: "確認",
            message: "タイムカードの出勤入力を取り消します",
            preferredStyle: .actionSheet)
        
        let defaultAction:UIAlertAction = UIAlertAction(title: "取り消す",
                                                        style: UIAlertActionStyle.destructive,
                                                        handler:{
                                                            (action:UIAlertAction!) -> Void in
                                                            self.cancelStart()
                                                            self.appDl.isUpdateStamp = true

        })
        
        let cancelAction = UIAlertAction(
            title: "キャンセル",
            style: .cancel, handler: nil)
        
        alertController.addAction(defaultAction)
        alertController.addAction(cancelAction)
        alertController.popoverPresentationController?.sourceView = view
        alertController.popoverPresentationController?.sourceRect = sender.frame
        
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func btnEndOnClick(_ sender: UIButton) {
        let location = NCMBGeoPoint(latitude: currentLatitude, longitude: currentLongitude)
        let now = Date()
        let endTime = appDl.dtc.getDateLine(lbEndTime.text!)
        endStampedTime = now
        self.endTimeOfWork = endTime
        appDl.todaysData.endTimeOfWork = endTime
        appDl.todaysData.startTimeOfWork = self.startTimeOfWork
        appDl.todaysData = appDl.ttw.setWorkedTime(appDl.todaysData)
        let manuallyFlag:Bool = !isOnWorkLocation
        let completion: ((_ dailyLogObj:NCMBObject) -> Void ) = { dailyLogObj in
            dailyLogObj.setObject(self.appDl.today, forKey: "Date")
            dailyLogObj.setObject(self.appDl.personalUser.IDNo, forKey: "UserID")
            dailyLogObj.setObject(endTime, forKey: "EndTime")
            dailyLogObj.setObject(self.currentLocation.LocationName, forKey: "EndLocation")
            dailyLogObj.setObject(location, forKey: "LeaveGeo")
            dailyLogObj.setObject(now, forKey: "LeaveTime")
            dailyLogObj.setObject(self.currentAddress, forKey: "LeaveAddress")
            dailyLogObj.setObject(self.selectedworkPlace.workPlaceLabel, forKey: "CompanyName")
            dailyLogObj.setObject(self.selectedworkPlace.workPlaceID, forKey: "companyID")
            dailyLogObj.setObject(self.appDl.personalUser.groupName, forKey: "GroupName")
            dailyLogObj.setObject(self.appDl.todaysData.timeTable, forKey: "TimeTableName")
            dailyLogObj.setObject(self.appDl.todaysData.timeTableID, forKey: "timeTableID")
            dailyLogObj.setObject(self.appDl.todaysData.locationID, forKey: "locationID")
            dailyLogObj.setObject(manuallyFlag, forKey: "EndManually")
            dailyLogObj.setObject(self.appDl.todaysData.workHours, forKey: "WorkHours")
            dailyLogObj.setObject(self.appDl.todaysData.overtimeWorkHours, forKey: "OvertimeWorkHours")
            dailyLogObj.setObject(self.appDl.todaysData.lateNightWorkHours, forKey: "LateNightWorkHours")
            dailyLogObj.setObject(false, forKey: "isDayOff")
            dailyLogObj.setObject(dayKindType.DayOn.rawValue, forKey: "DayKind")
            
            print("endTime=",endTime)
            
            dailyLogObj.saveInBackground({(error) in
                if error != nil {print("Save error : ",error!)}
            })
        }
        if getDailyLogObject(&dailyLog,user: appDl.personalUser ,todaysInfo: appDl.todaysData ,closure: completion) {
            print("Logを取得",#function, #line)
            
            btnEndRewind.isHidden = true
            btnEndForward.isHidden = true
            btnCancelEnd.isHidden = false
            btnEnd.isEnabled = false
            btnEnd.alpha = 0.5
            iconEndStamp.isHidden = false
            lbEndStamp.text = appDl.dtc.getLocalDateTimeString(now)
            lbEndStamp.isHidden = false
            isEndStamped = true
            btnCancelStart.isEnabled = !isEndStamped
            if isOnWorkLocation { // 色を確定
                lbEndTime.textColor = UIColor.black
            } else {
                lbEndTime.textColor = UIColor.red
            }
            endStampedColor = lbEndTime.textColor
            
            //次回出勤のアラートをセット
            setNextWorkAlerm()
            saveCurrentState()
            appDl.isUpdateStamp = true

        }
    }
    
    @IBAction func btnCancelEndOnClick(_ sender: UIButton) {
        let alertController = UIAlertController(
            title: "確認",
            message: "タイムカードの退勤入力を取り消します",
            preferredStyle: .actionSheet)
        
        let defaultAction:UIAlertAction = UIAlertAction(title: "取り消す",
                                                        style: UIAlertActionStyle.destructive,
                                                        handler:{
                                                            (action:UIAlertAction!) -> Void in
                                                            self.cancelEnd()
                                                            self.appDl.isUpdateStamp = true
        })
        
        let cancelAction = UIAlertAction(
            title: "キャンセル",
            style: .cancel, handler: nil)
        
        alertController.addAction(defaultAction)
        alertController.addAction(cancelAction)
        alertController.popoverPresentationController?.sourceView = view
        alertController.popoverPresentationController?.sourceRect = sender.frame
        present(alertController, animated: true, completion: nil)
    }

    @IBAction func btnShiftOnClick(_ sender: UIButton) {
        
        let alertController = UIAlertController(
            title: "確認",
            message: "タイムテーブル(シフト名)を選択します",
            preferredStyle: .actionSheet)
        
        for i in 0 ..< appDl.timeTableLists[appDl.WPIndex].count {
            let timeTable = appDl.timeTableLists[appDl.WPIndex][i]
            let defaultAction:UIAlertAction = UIAlertAction(title: timeTable.tableName ,
                                                            style: UIAlertActionStyle.default,
                                                            handler:{
                                                                (action:UIAlertAction!) -> Void in
                                                                self.lbTimeTable.text = action.title
                                                                self.selectedTimeTable = timeTable
                                                                self.TTIndex = i
                                                                self.appDl.todaysData.timeTableID = timeTable.timeTableID
                                                                self.appDl.todaysData.timeTable = timeTable.tableName
                                                                self.appDl.ttw.createTimeTable(timeTable,oneday: self.appDl.todaysData,refresh: true)
            })
            alertController.addAction(defaultAction)
        }
        
        let cancelAction = UIAlertAction(
            title: "キャンセル",
            style: .cancel, handler: nil)
        
        alertController.addAction(cancelAction)
        alertController.popoverPresentationController?.sourceView = view
        alertController.popoverPresentationController?.sourceRect = sender.frame
        present(alertController, animated: true, completion: nil )
        
        
    }
    
    @IBAction func btnLinkOnClick(_ sender: UIButton) {
        let url:URL = URL(string: "http://meltingpot.co.jp/")!
        if #available( iOS 10.0 , *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func sgHolydayChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 { // 出勤
            isHolyday = false
            resetHoliday()
        } else { // 休日
            isHolyday = true
            setHoliday()
        }
    }
    
    
    @IBAction func txMemoChanged(_ sender: UITextField) {
        saveMemo(sender)
    }
    
    @IBAction func btnEditMemoOnClick(_ sender: UIButton) {
        
        let alertController = UIAlertController(
            title: "MEMO",
            message: "メモを記録します",
            preferredStyle: .alert)
        
        let defaultAction:UIAlertAction = UIAlertAction(title: "記録",
                                                        style: UIAlertActionStyle.destructive,
                                                        handler:{
                                                            (action:UIAlertAction!) -> Void in
                                                            // 入力したテキストをコンソールに表示
                                                            let textField = alertController.textFields![0] as UITextField
                                                            self.txMemo.text = textField.text
                                                            self.saveMemo(textField )
        })
        
        let cancelAction = UIAlertAction(
            title: "キャンセル",
            style: .cancel, handler: nil)
        
        alertController.addAction(defaultAction)
        alertController.addAction(cancelAction)
        // UIAlertControllerにtextFieldを追加
        alertController.addTextField { (textField:UITextField!) -> Void in
            //self.txMemo = textField
            textField.text = self.txMemo.text
            textField.placeholder = "メモ"
        }
        alertController.popoverPresentationController?.sourceView = view
        alertController.popoverPresentationController?.sourceRect = sender.frame
        
        present(alertController, animated: true, completion: nil)
        
        
    }
    
    // MARK: - functions

    func saveMemo(_ sender: UITextField) {
        appDl.todaysData.memo = sender.text ?? ""
        memoText = appDl.todaysData.memo
        
        let completion: ((_ dailyLogObj:NCMBObject) -> Void) = { dailyLogObj in
            dailyLogObj.setObject(self.appDl.today, forKey: "Date")
            dailyLogObj.setObject(self.appDl.personalUser.IDNo, forKey: "UserID")
            dailyLogObj.setObject(self.appDl.personalUser.companyName, forKey: "CompanyName")
            dailyLogObj.setObject(self.appDl.personalUser.companyID, forKey: "companyID")
            dailyLogObj.setObject(self.appDl.personalUser.groupName, forKey: "GroupName")
            dailyLogObj.setObject(self.appDl.todaysData.timeTable, forKey: "TimeTableName")
            dailyLogObj.setObject(self.memoText, forKey: "memo")
            
            dailyLogObj.saveInBackground(nil)
        }
        if getDailyLogObject(&dailyLog,user: appDl.personalUser ,todaysInfo: appDl.todaysData ,closure: completion) {
            print("Logを取得",#function, #line)
            saveCurrentState()
        }
    
    }

    func setNotifications(_ messages:[remindMessage]) {
        
        
        if #available(iOS 10, *) {
            // Use iOS 10 APIs on iOS
            let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
            let center = UNUserNotificationCenter.current()
            center.removeAllPendingNotificationRequests() //送信待ちの全通知を全削除
            center.removeAllDeliveredNotifications() // 送信済みの全通知を削除
            let dtc = dateTimeControll()
            let reqID = dtc.createUserID("OnWorkRequest")
            
            for i in 0 ..< messages.count {
                
                let content = UNMutableNotificationContent()
                content.title = "出勤・退勤　時刻のお知らせ"
                content.body = messages[i].message
                content.sound = UNNotificationSound(named: UILocalNotificationDefaultSoundName)
                var components = calendar.dateComponents([.year,.month,.day,.hour,.minute], from: messages[i].time)
                components.second = 0
                //print("components =",components)
                let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
                let requestIdentifier = reqID + i.description
                //print("requestIdentifier = ",requestIdentifier)
                let request = UNNotificationRequest(identifier: requestIdentifier,
                                                    content: content,
                                                    trigger: trigger)
                
                UNUserNotificationCenter.current().add(request) {
                    (error) in
                    if error != nil {print(error ?? "Any error occured in local notification!")}
                }
                //print("Set Notification:", messages[i].message, messages[i].time)
            }
            
        } else {
            // Fall back to earlier iOS and macOS APIs
            
            // 設定する前に、設定済みの通知をキャンセルする
            UIApplication.shared.cancelAllLocalNotifications()
            
            for i in 0 ..< messages.count {
                // Notificationを生成する.
                let notification: UILocalNotification = UILocalNotification()
                
                // メッセージを代入する.
                notification.soundName = UILocalNotificationDefaultSoundName // 再生サウンドを設定する.
                notification.timeZone = TimeZone.current // Timezoneを設定する.
                notification.alertBody = messages[i].message
                notification.fireDate = messages[i].time
                UIApplication.shared.scheduleLocalNotification(notification) // Notificationを表示する.
                print("Set Notification:", messages[i].message, messages[i].time)
            }
        }
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
        
    }
    
    func startTimeLabelColor()->UIColor {
        if isStartStamped { return startStampedColor! }
        
        if isOnWorkLocation { // 勤務地内
            return UIColor.black
        } else {
            return UIColor.red
        }
    }
    
    func endTimeLabelColor()->UIColor {
        if isEndStamped { return endStampedColor! }
        
        if isOnWorkLocation { // 勤務地内
            if isInAdjustment { // 調整中
                return UIColor.gray
            } else {
                return UIColor.black
                            }
        } else {
            if isInAdjustment { // 調整中
                return myColorDarkPastelPink

            } else {
                return UIColor.red
            }
        }
    }
    
    /// 日付変更線を超えた時にデータをクリアする処理
    ///
    /// - parameter None :
    /// - returns: None
    func clearForNewday(){
        dailyLog = nil
        appDl.today = appDl.todaysData.beginningOfDay
        isStartStamped = false
        isEndStamped = false
        isOnWorkLocation = false
        todaysDateString = "" // 今日(firstday)の日付 yyyy/mm/dd
        memoText = ""
        
    }
    
    /// Restore View State
    ///
    /// - parameter  無し:
    /// - returns: 無し
    func restoreViewState() {
        print("Start!",#function, #line)
        
        if isHolyday {
            btnStartRewind.isHidden = true /*false 隠しておく*/
            btnStartForward.isHidden = true /*false 隠しておく*/
            btnCancelStart.isHidden = true
            btnCancelStart.isEnabled = false
            btnStart.isEnabled = false
            btnStart.alpha = 0.5
            iconStartStamp.isHidden = true
            lbStartStamp.text = ""
            lbStartStamp.isHidden = true
            lbStartTime.text = "--:--"
            btnEndRewind.isHidden = true /*false 隠しておく*/
            btnEndForward.isHidden = true /*false 隠しておく*/
            btnCancelEnd.isHidden = true
            btnEnd.isEnabled = false
            btnEnd.alpha = 0.5
            iconEndStamp.isHidden = true
            lbEndStamp.text = ""
            lbEndStamp.isHidden = true
            lbEndTime.text = "--:--"

            vSelf.backgroundColor = NC_012_SAKURA
            sgHolyday.tintColor = NC_014_KARAKURENAI
            sgHolyday.backgroundColor = NC_012_SAKURA
            sgHolyday.selectedSegmentIndex = 1
            
        } else {
            if isStartStamped { // スタートボタンが押されてる
                btnStartRewind.isHidden = true
                btnStartForward.isHidden = true
                btnCancelStart.isHidden = false
                btnCancelStart.isEnabled = !isEndStamped
                btnStart.isEnabled = false
                btnStart.alpha = 0.5
                iconStartStamp.isHidden = false
                lbStartStamp.text = appDl.dtc.getLocalDateTimeString(startStampedTime)
                lbStartStamp.isHidden = false
                lbStartTime.text = appDl.dtc.getLocalTimeString(startTimeOfWork)
                lbStartTime.textColor = startStampedColor
            } else {
                btnStartRewind.isHidden = true /*false 隠しておく*/
                btnStartForward.isHidden = true /*false 隠しておく*/
                btnCancelStart.isHidden = true
                btnCancelStart.isEnabled = false
                btnStart.isEnabled = true
                btnStart.alpha = 1.0
                iconStartStamp.isHidden = true
                lbStartStamp.text = ""
                lbStartStamp.isHidden = true
            }
            if isEndStamped {  // エンドボタンが押されてる
                btnEndRewind.isHidden = true
                btnEndForward.isHidden = true
                btnCancelEnd.isHidden = false
                btnEnd.isEnabled = false
                btnEnd.alpha = 0.5
                iconEndStamp.isHidden = false
                lbEndStamp.text = appDl.dtc.getLocalDateTimeString(endStampedTime)
                lbEndStamp.isHidden = false
                lbEndTime.text = appDl.dtc.getLocalTimeString(endTimeOfWork)
                lbEndTime.textColor = endStampedColor
            } else {
                btnEndRewind.isHidden = true /*false 隠しておく*/
                btnEndForward.isHidden = true /*false 隠しておく*/
                btnCancelEnd.isHidden = true
                btnEnd.isEnabled = true
                btnEnd.alpha = 1.0
                iconEndStamp.isHidden = true
                lbEndStamp.text = ""
                lbEndStamp.isHidden = true
            }
            vSelf.backgroundColor = NC_233_SHIRONERI
            sgHolyday.tintColor = #colorLiteral(red: 0.6642242074, green: 0.6642400622, blue: 0.6642315388, alpha: 1)
            sgHolyday.backgroundColor = NC_233_SHIRONERI
            sgHolyday.selectedSegmentIndex = 0


        }
        txMemo.text = memoText
    }
    
    /// Save Current State to UserDefault
    ///
    /// - parameter  無し:
    /// - returns: 無し
    func saveCurrentState() {
        appDl.saveUserInfo()
        let userDefaults = UserDefaults.standard
        userDefaults.set(isStartStamped, forKey: "isStartStamped")
        userDefaults.set(startStampedTime, forKey: "startStampedTime")
        userDefaults.set(startTimeOfWork, forKey: "startTime")
        userDefaults.set(isEndStamped, forKey: "isEndStamped")
        userDefaults.set(endStampedTime, forKey: "endStampedTime")
        userDefaults.set(endTimeOfWork, forKey: "endTime")
        userDefaults.set(isOnWorkLocation, forKey: "isOnWorkLocation")
        userDefaults.set(appDl.today, forKey: "today")
        userDefaults.set(memoText, forKey: "memoText")
        userDefaults.set(selectedworkPlace.workPlaceLabel, forKey: "workPlaceLabel")
        userDefaults.set(selectedworkPlace.workPlaceID, forKey: "workPlaceID")
        userDefaults.set(WPIndex, forKey: "WPIndex")
        userDefaults.set(selectedLocation.LocationName, forKey: "LocationName")
        userDefaults.set(selectedLocation.locationID, forKey: "locationID")
        userDefaults.set(LocIndex, forKey: "LocIndex")
        userDefaults.set(selectedTimeTable.tableName, forKey: "tableName")
        userDefaults.set(selectedTimeTable.timeTableID, forKey: "timeTableID")
        userDefaults.set(TTIndex, forKey: "TTIndex")
        userDefaults.set(isHolyday, forKey: "isHolyday")
        userDefaults.setColor(startStampedColor, forKey: "startStampedColor")
        userDefaults.setColor(endStampedColor, forKey: "endStampedColor")
    }
    
    func selectTodaysInfo(_ wp:Int,loc:Int,TT:Int) {
        selectedworkPlace = appDl.workPlaces[wp]
        selectedLocation = appDl.locationLists[wp][loc]
        selectedTimeTable = appDl.timeTableLists[wp][TT]
        
        appDl.todaysData.companyName = selectedworkPlace.workPlaceLabel
        appDl.todaysData.companyID = selectedworkPlace.workPlaceID
        appDl.todaysData.location = selectedLocation.LocationName
        appDl.todaysData.locationID = selectedLocation.locationID
        appDl.todaysData.timeTable = selectedTimeTable.tableName
        appDl.todaysData.timeTableID = selectedTimeTable.timeTableID
        
        print("appDl.todaysData.companyName : ",appDl.todaysData.companyName,#function, #line)
        print("appDl.todaysData.timeTable : ",appDl.todaysData.timeTable,#function, #line)
        print("appDl.todaysData.location : ",appDl.todaysData.location,#function, #line)

    }
    
    func selectTodaysInfoByID(_ wpID:String,locID:String,TTID:String) {
        print("Start!",#function, #line)

        for i in 0 ..< appDl.workPlaces.count {
            if appDl.workPlaces[i].workPlaceID == wpID {
                selectedworkPlace = appDl.workPlaces[i]
                WPIndex = i
                break
            }
        }
        for j in 0 ..< appDl.locationLists[WPIndex].count {
            if appDl.locationLists[WPIndex][j].locationID == locID {
                selectedLocation = appDl.locationLists[WPIndex][j]
                LocIndex = j
                break
            }
        }
        for k in 0 ..< appDl.timeTableLists[WPIndex].count {
            if appDl.timeTableLists[WPIndex][k].timeTableID == TTID {
                selectedTimeTable = appDl.timeTableLists[WPIndex][k]
                TTIndex = k
                break
            }
        }
        
        appDl.todaysData.companyName = selectedworkPlace.workPlaceLabel
        appDl.todaysData.companyID = selectedworkPlace.workPlaceID
        appDl.todaysData.location = selectedLocation.LocationName
        appDl.todaysData.locationID = selectedLocation.locationID
        appDl.todaysData.timeTable = selectedTimeTable.tableName
        appDl.todaysData.timeTableID = selectedTimeTable.timeTableID
        
        print("appDl.todaysData.companyName : ",appDl.todaysData.companyName,#function, #line)
        print("appDl.todaysData.timeTable : ",appDl.todaysData.timeTable,#function, #line)
        print("appDl.todaysData.location : ",appDl.todaysData.location,#function, #line)
        
    }
    
    /// Load Current State from UserDefault
    ///
    /// - parameter  無し:
    /// - returns: 無し
    func loadSavedState() {
        print("Start!",#function, #line)
        let userDefaults = UserDefaults.standard
        let savedDate = userDefaults.object(forKey: "today") as? Date
        if savedDate == nil {
            loadFromLog()
           return
        } // データ未登録の起動はデータをロードしない
        
        // 本日1回目&日付変更線を超えた 場合の起動はデータをロードしない ログがあったらロードする
        if appDl.todaysData.beginningOfDay?.compare(savedDate!) == .orderedDescending { // startTime < savedDate
            clearForNewday()
            loadFromLog()
            return
        }
        // 2回めの起動、または、データ登録後の再起動
        //appDl.restoreUserInfo()
        isStartStamped = userDefaults.bool(forKey: "isStartStamped")
        startStampedTime = userDefaults.object(forKey: "startStampedTime") as? Date
        startTimeOfWork = userDefaults.object(forKey: "startTime") as? Date
        isEndStamped = userDefaults.bool(forKey: "isEndStamped")
        endStampedTime = userDefaults.object(forKey: "endStampedTime") as? Date
        endTimeOfWork = userDefaults.object(forKey: "endTime") as? Date
        isOnWorkLocation = userDefaults.bool(forKey: "isOnWorkLocation")
        appDl.today = appDl.todaysData.beginningOfDay
        memoText = userDefaults.string(forKey: "memoText")!
        appDl.todaysData.memo = memoText
        
        WPIndex = userDefaults.integer(forKey: "WPIndex")
        LocIndex = userDefaults.integer(forKey: "LocIndex") 
        TTIndex = userDefaults.integer(forKey: "TTIndex") 
        selectTodaysInfo(WPIndex,loc:LocIndex,TT:TTIndex)
        // colorForKeyはNSUserDefaultsのExtentionで定義してある　→ appCommon
        startStampedColor = userDefaults.colorForKey("startStampedColor") ?? UIColor.black
        endStampedColor = userDefaults.colorForKey("endStampedColor")  ?? UIColor.black
        isHolyday = userDefaults.bool(forKey: "isHolyday")

    }

    /// Load Current State from DailyLog
    ///
    /// - parameter:  無し
    /// - returns: 無し
    func loadFromLog() {
        print("Start!",#function, #line)
        let userLog = getOnesLog(appDl.personalUser, openDate: appDl.todaysData.beginningOfDay!, CloseDate: appDl.todaysData.endingOfDay!)
        if userLog == nil {
            WPIndex = 0
            LocIndex = 0
            TTIndex = 0
            selectTodaysInfo(WPIndex,loc:LocIndex,TT:TTIndex)
            return
        } // データ未登録の場合はデータをロードしない
        
        selectTodaysInfoByID((userLog?.companyID)!,locID: (userLog?.locationID)!,TTID: (userLog?.timeTableID)!)
        
        if userLog!.startStamp?.workTime == nil {
            isStartStamped = false
            startTimeOfWork = nil
        } else {
            isStartStamped = true
            startStampedTime = userLog!.startStamp?.stampTime
            startTimeOfWork = userLog!.startStamp?.workTime
        }
        appDl.todaysData.startTimeOfWork = startTimeOfWork
        if userLog!.endStamp?.workTime == nil {
            isEndStamped = false
            endTimeOfWork = nil
        } else {
            isEndStamped = true
            endStampedTime = userLog?.endStamp?.stampTime
            endTimeOfWork = userLog!.endStamp?.workTime
        }
        appDl.todaysData.endTimeOfWork = endTimeOfWork
        if (userLog!.startStamp?.workPlace == "エリア外")&&(userLog!.endStamp?.workPlace == "エリア外") {
            isOnWorkLocation = false
        } else {
            isOnWorkLocation = true
        }
        if userLog!.timeTable != "" {
            appDl.todaysData.timeTable = userLog!.timeTable
            print("appDl.todaysData.timeTable : ",appDl.todaysData.timeTable,#function, #line)
        }
        memoText = userLog!.memo
        appDl.todaysData.memo = memoText
        if userLog!.startStamp!.isManually {
            startStampedColor = UIColor.red
        } else {
            startStampedColor = UIColor.black
        }
        if userLog!.endStamp!.isManually {
            endStampedColor = UIColor.red
        } else {
            endStampedColor = UIColor.black
        }
        isHolyday = userLog!.isDayOff
        appDl.todaysData.isDayOff = isHolyday
        
    }

    // locationManagerの認証が変更された時に呼び出されるメソッド.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status{
        case .authorizedWhenInUse:
            print("locationManager:AuthorizedWhenInUse")
        case .authorizedAlways:
            print("locationManager:Authorized")
        case .denied:
            print("locationManager:Denied")
        case .restricted:
            print("locationManager:Restricted")
        case .notDetermined:
            print("locationManager:NotDetermined")
            //default:
            //    print("etc.")
        }
    }
    
    func locationManager( _ manager: CLLocationManager,
        didFailWithError error: Error) {
            print(error)
    }

    // 現在地を画面表示する処理　デバッグ用
    func setCurrentPosition() {
        lbCurrentPosition.text = "現在地：" + currentAddress + "\n(緯度:" + currentLatitude.description + ", 経度:" + currentLongitude.description + ")\n" + "距離：\(currentDistance) m"
    }

    // GPSから値を取得した際に呼び出されるメソッド.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let currentLoc:CLLocation = locations.last!
        //let centerCoordinate:CLLocationCoordinate2D = currentLocation.coordinate
        currentLatitude = currentLoc.coordinate.latitude
        currentLongitude = currentLoc.coordinate.longitude
        let result = getNearestLocation(appDl.personalUser.companyID,location:currentLoc)
        appDl.personalUser.currentLocation = result.locName
        let wpID = result.locID
        for i in 0 ..< appDl.workPlaces.count {
            for j in 0 ..< appDl.locationLists[i].count {
                if appDl.locationLists[i][j].locationID == wpID {
                    self.currentLocation = appDl.locationLists[i][j]
                    self.currentLocIndex = j
                    break
                }
            }
        }

        isOnWorkLocation = result.isOnWorkLocation
        
        lbLocation.text = /*"現在地：" +*/ appDl.personalUser.currentLocation
        if !isOnWorkLocation {
            lbLocation.textColor = UIColor.red
        } else {
            lbLocation.textColor = UIColor.black
        }
        lbStartTime.textColor = startTimeLabelColor()
        lbEndTime.textColor = endTimeLabelColor()

//
        // 距離計算
        //仕事場の位置 ** 現在地の近くの勤務地を取得
        let workPlcaeGeo:CLLocation  = result.locGeo
        
        let distance:CLLocationDistance  = currentLoc.distance(from: workPlcaeGeo)
        print("距離＝\(distance) m")
        currentDistance = Int(distance) // meter
//
        let myGeocorder = CLGeocoder()
        
        // 逆ジオコーディング開始.
        myGeocorder.reverseGeocodeLocation(currentLoc,
            completionHandler: { (placemarks, error) -> Void in
                if placemarks != nil {
                    for placemark in placemarks! {
                    /*
                        print("Name: \(placemark.name)")
                        print("Country: \(placemark.country)")
                        print("ISOcountryCode: \(placemark.ISOcountryCode)")
                        print("administrativeArea: \(placemark.administrativeArea)")
                        print("subAdministrativeArea: \(placemark.subAdministrativeArea)")
                        print("Locality: \(placemark.locality)")
                        print("PostalCode: \(placemark.postalCode)")
                        print("areaOfInterest: \(placemark.areasOfInterest)")
                        print("Ocean: \(placemark.ocean)")
                        print("thoroughfare: \(placemark.thoroughfare)")
                        print("subThoroughfare: \(placemark.subThoroughfare)")
                        print("subLocality: \(placemark.subLocality)")
                    */
                        // 現在の住所
                        self.currentAddress = ""
                        self.currentAddress += placemark.administrativeArea ?? ""
                        self.currentAddress += placemark.locality ?? ""
                        self.currentAddress += placemark.thoroughfare ?? ""
                        self.currentAddress += placemark.subThoroughfare ?? ""
                        print(self.currentAddress)
                        
                        self.setCurrentPosition()  // 現在地の表示
                    }
                }
                
        })
        
        
    }
    
    /// Serch window of period indexed from timeTableWindow for start time
    ///
    /// - parameter period : target Period number
    /// - returns: Nothing ( update starttime label & time)
    func getStartWindow(_ period:Int) {
        var startFlag:Bool = false
        var startNextFlag:Bool = false
        var startPriorFlag:Bool = false
        
        if appDl.ttw.timeTableWindow.count < 1  {
            print("No timeTableWindow!")
            return
        }
        
        if period >= appDl.ttw.timeTableWindow.count { return }
        if period < 0 { return }
        
        for i in period+1  ..< appDl.ttw.timeTableWindow.count {
            if appDl.ttw.timeTableWindow[i].isWorkTime != workTimeType.recessTime {
                if !startFlag {
                    lbStartTime.text = appDl.ttw.timeTableWindow[i].startTimeLabel
                    startFlag = true
                } else {
                    btnStartForward.setTitle(appDl.ttw.timeTableWindow[i].startTimeLabel, for: UIControlState())
                    startNextFlag = true
                    break
                }
            }
        }
        if !startNextFlag { // 次の時間帯が見つからなかったケースは最初から探す
            for i in 0  ..< period {
                if appDl.ttw.timeTableWindow[i].isWorkTime != workTimeType.recessTime {
                    if !startFlag {
                        lbStartTime.text = appDl.ttw.timeTableWindow[i].startTimeLabel
                        startFlag = true
                    } else {
                        btnStartForward.setTitle(appDl.ttw.timeTableWindow[i].startTimeLabel, for: UIControlState())
                        startNextFlag = true
                        break
                    }
                }
            }
        }
        // 前の時間帯を探す
        for i in (0 ... period).reversed() {
            if appDl.ttw.timeTableWindow[i].isWorkTime != workTimeType.recessTime {
                btnStartRewind.setTitle(appDl.ttw.timeTableWindow[i].startTimeLabel, for: UIControlState())
                startPriorFlag = true
               break
            }
        }
        if !startPriorFlag {
            for i in (period ..< appDl.ttw.timeTableWindow.count).reversed() {
                if appDl.ttw.timeTableWindow[i].isWorkTime != workTimeType.recessTime {
                    btnStartRewind.setTitle(appDl.ttw.timeTableWindow[i].startTimeLabel, for: UIControlState())
                    startPriorFlag = true
                    break
                }
            }
        }
        
    }
    
    /// Serch window of period indexed from timeTableWindow for end time
    ///
    /// - parameter period : target Period number
    /// - returns: Nothing ( update endtime label & time)
    func getEndWindow(_ period:Int) {
        var endFlag:Bool = false
        var endNextFlag:Bool = false
        var endPriorFlag:Bool = false

        if appDl.ttw.timeTableWindow.count < 1  {
            print("No timeTableWindow!")
            return
        }
        
        if period < 0 { return }
        
        for i in (0 ..< period).reversed() {
            if appDl.ttw.timeTableWindow[i].isWorkTime != workTimeType.recessTime {
                if !endFlag {
                    if (startTimeOfWork == nil) || (appDl.ttw.timeTableWindow[i].endTime.compare(startTimeOfWork!) == .orderedAscending) {
                        lbEndTime.text = lbStartTime.text // Start時刻以前の場合は単純にStart時刻に合わせる
                        isInAdjustment = true
                    } else {
                        lbEndTime.text = appDl.ttw.timeTableWindow[i].endTimeLabel
                        isInAdjustment = false
                    }
                    lbEndTime.textColor = endTimeLabelColor()
                    endFlag = true
                } else {
                    btnEndRewind.setTitle(appDl.ttw.timeTableWindow[i].endTimeLabel, for: UIControlState())
                    endPriorFlag = true
                    break
                }
            }
        }
        if !endPriorFlag { // 前の時間帯が見つからなかったケースは最後から探す
            for i in (period ..< appDl.ttw.timeTableWindow.count).reversed() {
                if appDl.ttw.timeTableWindow[i].isWorkTime != workTimeType.recessTime {
                    if !endFlag {
                        if (startTimeOfWork == nil) || (appDl.ttw.timeTableWindow[i].endTime.compare(startTimeOfWork!) == .orderedAscending) {
                            lbEndTime.text = lbStartTime.text // Start時刻以前の場合は単純にStart時刻に合わせる
                            isInAdjustment = true
                        } else {
                            lbEndTime.text = appDl.ttw.timeTableWindow[i].endTimeLabel
                            isInAdjustment = false
                        }
                        lbEndTime.textColor = endTimeLabelColor()
                        endFlag = true
                    } else {
                        btnEndRewind.setTitle(appDl.ttw.timeTableWindow[i].endTimeLabel, for: UIControlState())
                        endPriorFlag = true
                        break
                    }
                }
            }
        }
        // 次の時間帯を探す
        for i in period  ..< appDl.ttw.timeTableWindow.count  {
            if appDl.ttw.timeTableWindow[i].isWorkTime != workTimeType.recessTime {
                btnEndForward.setTitle(appDl.ttw.timeTableWindow[i].endTimeLabel, for: UIControlState())
                endNextFlag = true
                break
            }
        }
        if !endNextFlag {
            for i in 0  ..< period  {
                if appDl.ttw.timeTableWindow[i].isWorkTime != workTimeType.recessTime {
                    btnEndForward.setTitle(appDl.ttw.timeTableWindow[i].endTimeLabel, for: UIControlState())
                    endNextFlag = true
                    break
                }
            }
        }
        
    }
    
    //NSTimerIntervalで指定された秒数毎に呼び出されるメソッド
    @objc func onUpdate(_ timer : Timer){
        
        let now = Date()
 
        lbDate.text = appDl.dtc.getDateTodayYYYYMMDD_EEE()
        lbClock.text = appDl.dtc.getLocalTimeStringHHmmss(now)
        
        if appDl.todaysData.endingOfDay?.compare(now) == .orderedAscending { // Over Date Line endTime < now
            appDl.todaysData = appDl.cf.initTodaysData(selectedworkPlace)
            appDl.today = appDl.todaysData.beginningOfDay

            clearForNewday()
            appDl.ttw.onedaysData = appDl.todaysData
            appDl.ttw.refreshTimeTable()
            saveCurrentState()
            restoreViewState()
        }
        
        if (!isStartStamped || !isEndStamped) && !isHolyday {
            let timePeriod = appDl.ttw.getTimePeriod(time:now)
            //print("timePeriod=",timePeriod)
            if !isStartStamped {
                getStartWindow(timePeriod)
            }
            if !isEndStamped {
                getEndWindow(timePeriod)
            }
        }
        
      
        
    }
    
    @objc func keyboardAppeared (_ aNotification:Notification) {
        let navBarHeight = self.navigationController?.navigationBar.frame.size.height ?? 0
        let statusBarHeight: CGFloat = UIApplication.shared.statusBarFrame.height
        var keyOffset:CGFloat = 0.0
        if let userInfo = (aNotification as NSNotification).userInfo{
            if let keyboard = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue{
                let keyBoardRect = keyboard.cgRectValue
                let textFieldRect:CGRect? = txMemo.frame
                keyOffset = (textFieldRect!.maxY - keyBoardRect.minY)  + statusBarHeight - 10 /* 余白 */
                // キーボードに隠れない場合は何もしない
                if (keyOffset + navBarHeight) < 0  {
                    return
                }

            }
        }
        let scrollPoint:CGPoint = CGPoint(x: 0.0,y: keyOffset)
        scrCardView.setContentOffset(scrollPoint,animated:false)
        
    }
    
    @objc func keyboardDisappered (_ aNotification:Notification) {
        scrCardView.setContentOffset(scrollbase,animated:true) //元のオフセットに戻す
    }
    
    func cancelStart() {
        let location = NCMBGeoPoint(latitude: 0.0, longitude: 0.0)
        let completion: ((_ dailyLogObj:NCMBObject) -> Void) = { dailyLogObj in
            dailyLogObj.setObject(self.appDl.today, forKey: "Date")
            dailyLogObj.setObject(self.appDl.personalUser.IDNo, forKey: "UserID")
            dailyLogObj.setObject(nil,forKey: "StartTime")
            dailyLogObj.setObject(nil, forKey: "StartLocation")
            dailyLogObj.setObject(location, forKey: "ArrivalGeo")
            dailyLogObj.setObject(nil, forKey: "ArrivalTime")
            dailyLogObj.setObject(nil, forKey: "ArrivalAddress")
            dailyLogObj.setObject(nil, forKey: "StartManually")
            dailyLogObj.setObject(nil, forKey: "WorkHours")
            dailyLogObj.setObject(nil, forKey: "OvertimeWorkHours")
            dailyLogObj.setObject(nil, forKey: "LateNightWorkHours")
          
            dailyLogObj.saveInBackground({(error) in
                if error != nil {print("Save error : ",error!)}
            })
        }
        if getDailyLogObject(&dailyLog,user: appDl.personalUser ,todaysInfo: appDl.todaysData ,closure: completion) {
            print("Logを取得",#function, #line)
        
            btnStartRewind.isHidden = true /*false 隠しておく*/
            btnStartForward.isHidden = true /*false 隠しておく*/
            btnCancelStart.isHidden = true
            btnStart.isEnabled = true
            btnStart.alpha = 1.0
            iconStartStamp.isHidden = true
            lbStartStamp.text = ""
            lbStartStamp.isHidden = true
            isStartStamped = false
            startStampedTime = nil
            self.startTimeOfWork = nil
            startStampedColor = UIColor.black
            appDl.todaysData = appDl.ttw.setWorkedTime(appDl.todaysData)
            
            saveCurrentState()
            startCheckLocation()
        }
    }

    func cancelEnd() {
        let location = NCMBGeoPoint(latitude: 0.0, longitude: 0.0)
        endStampedTime = nil
        self.endTimeOfWork = nil
        appDl.todaysData = appDl.ttw.setWorkedTime(appDl.todaysData)
        let completion: ((_ dailyLogObj:NCMBObject) -> Void ) = { dailyLogObj in
            dailyLogObj.setObject(self.appDl.today, forKey: "Date")
            dailyLogObj.setObject(self.appDl.personalUser.IDNo, forKey: "UserID")
            dailyLogObj.setObject(nil, forKey: "EndTime")
            dailyLogObj.setObject(nil, forKey: "EndLocation")
            dailyLogObj.setObject(location, forKey: "LeaveGeo")
            dailyLogObj.setObject(nil, forKey: "LeaveTime")
            dailyLogObj.setObject(nil, forKey: "LeaveAddress")
            dailyLogObj.setObject(nil, forKey: "EndManually")
            dailyLogObj.setObject(nil, forKey: "WorkHours")
            dailyLogObj.setObject(nil, forKey: "OvertimeWorkHours")
            dailyLogObj.setObject(nil, forKey: "LateNightWorkHours")
            dailyLogObj.saveInBackground({(error) in
                if error != nil {print("Save error : ",error!)}
            })
        }
        if getDailyLogObject(&dailyLog,user: appDl.personalUser ,todaysInfo: appDl.todaysData ,closure: completion) {
            print("Logを取得",#function, #line)

            btnEndRewind.isHidden = true /*false 隠しておく*/
            btnEndForward.isHidden = true /*false 隠しておく*/
            btnCancelEnd.isHidden = true
            btnEnd.isEnabled = true
            btnEnd.alpha = 1.0
            iconEndStamp.isHidden = true
            lbEndStamp.text = ""
            lbEndStamp.isHidden = true
            isEndStamped = false
            btnCancelStart.isEnabled = !isEndStamped
            lbEndTime.textColor = UIColor.black
            endStampedColor = lbEndTime.textColor
            
            saveCurrentState()
            startCheckLocation()
        }
        
    }

    func setNextWorkAlerm() {
        appDl.createMonthlyList()
        let nextWorkDay = appDl.dtc.getNextWorkData(appDl.todaysData.beginningOfDay!, thisMonthList: appDl.thisMonthHold, nextMonthList: appDl.nextMonthHold)
        if nextWorkDay.beginningOfDay != nil { // 次の出勤日が見つからない場合はスキップ
            let startTimeStr = appDl.dtc.getStarTimeOfTimeTable(appDl.timeTableLists,ttID:nextWorkDay.timeTableID)
            var nextTime = appDl.dtc.getDateLine(startTimeStr, baseDate: nextWorkDay.beginningOfDay!)
            var messages:[remindMessage] = []
            nextTime = nextTime.addingTimeInterval(-600.0) // 定時の10分前
            var message = remindMessage(message:"あと10分で定時(出勤時刻)になります！", time: nextTime)
            messages.append(message)
            nextTime = nextTime.addingTimeInterval(420.0) // 定時の３分前
            message = remindMessage(message:"あと3分で定時(出勤時刻)になります！", time: nextTime)
            messages.append(message)
            nextTime = nextTime.addingTimeInterval(3600.0) // その60分後
            message = remindMessage(message:"定時(出勤時刻)から１時間経過します！", time: nextTime)
            messages.append(message)
            nextTime = nextTime.addingTimeInterval(3600.0) // その60分後
            message = remindMessage(message:"定時(出勤時刻)から２時間経過します！", time: nextTime)
            messages.append(message)
            nextTime = nextTime.addingTimeInterval(3600.0) // その60分後
            message = remindMessage(message:"定時(出勤時刻)から３時間経過します！", time: nextTime)
            messages.append(message)
            
            setNotifications(messages)
        }

    }
    func setHoliday() {
        appDl.todaysData.isDayOff = true
        appDl.todaysData.workHours = 0
        appDl.todaysData.overtimeWorkHours = 0
        appDl.todaysData.lateNightWorkHours = 0
        
        let completion: ((_ dailyLogObj:NCMBObject) -> Void ) = { dailyLogObj in
            dailyLogObj.setObject(self.appDl.today, forKey: "Date")
            dailyLogObj.setObject(self.appDl.personalUser.IDNo, forKey: "UserID")
            dailyLogObj.setObject(self.selectedworkPlace.workPlaceLabel, forKey: "CompanyName")
            dailyLogObj.setObject(self.selectedworkPlace.workPlaceID, forKey: "companyID")
            dailyLogObj.setObject(self.appDl.personalUser.groupName, forKey: "GroupName")
            dailyLogObj.setObject(0, forKey: "WorkHours")
            dailyLogObj.setObject(0, forKey: "OvertimeWorkHours")
            dailyLogObj.setObject(0, forKey: "LateNightWorkHours")
            dailyLogObj.setObject(true, forKey: "isDayOff")
            dailyLogObj.setObject(dayKindType.DayOff.rawValue, forKey: "DayKind")
            
            dailyLogObj.saveInBackground({(error) in
                if error != nil {print("Save error : ",error!)}
            })
        }
        if getDailyLogObject(&dailyLog,user: appDl.personalUser ,todaysInfo: appDl.todaysData ,closure: completion) {
            print("Logを取得",#function, #line)
            
            restoreViewState()
            //次回出勤のアラートをセット
            setNextWorkAlerm()
            saveCurrentState()
            
        }
    }

    func resetHoliday() {
        appDl.todaysData.isDayOff = false
        
        let completion: ((_ dailyLogObj:NCMBObject) -> Void ) = { dailyLogObj in
            dailyLogObj.setObject(self.appDl.today, forKey: "Date")
            dailyLogObj.setObject(self.appDl.personalUser.IDNo, forKey: "UserID")
            dailyLogObj.setObject(self.selectedworkPlace.workPlaceLabel, forKey: "CompanyName")
            dailyLogObj.setObject(self.selectedworkPlace.workPlaceID, forKey: "companyID")
            dailyLogObj.setObject(self.appDl.personalUser.groupName, forKey: "GroupName")
            dailyLogObj.setObject(false, forKey: "isDayOff")
            dailyLogObj.setObject(dayKindType.DayOn.rawValue, forKey: "DayKind")
            
            dailyLogObj.saveInBackground({(error) in
                if error != nil {print("Save error : ",error!)}
            })
        }
        if getDailyLogObject(&dailyLog,user: appDl.personalUser ,todaysInfo: appDl.todaysData ,closure: completion) {
            print("Logを取得",#function, #line)
            
            restoreViewState()
            saveCurrentState()
            
        }
    }

    func startCheckLocation() {
        if isStartCheckLocation {return} // Already Started
        // 位置情報取得の認証
        let authStatus = CLLocationManager.authorizationStatus()
        if (authStatus == CLAuthorizationStatus.denied) ||
            (authStatus == CLAuthorizationStatus.restricted) {
                print("Location Access Denied or Restricted")
                // 位置情報が読み取り許可になっていない場合はアラートを出してタイマのみ作成
                let alertController = UIAlertController(
                    title: "位置情報の利用が\n許可されていません！",
                    message: "「設定」-「プライバシー」-「位置情報サービス」を開き、OnWorkアプリの設定を使用中のみ許可として下さい",
                    preferredStyle: .alert)
                
                let defaultAction:UIAlertAction = UIAlertAction(title: "OK",
                    style: UIAlertActionStyle.default,
                    handler: nil)
                alertController.addAction(defaultAction)
                present(alertController, animated: true, completion: nil)
                
                // return
                
        } else {
            
            if authStatus == CLAuthorizationStatus.notDetermined {
                //許可要求
                if locManager.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization)) {
                    locManager.requestWhenInUseAuthorization()
                }
            }
            
            // 電池消費下げるため精度は低くしておく 調整必要！！
            //locManager.desiredAccuracy = kCLLocationAccuracyKilometer
            locManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters // 精度上げる
            locManager.distanceFilter = 20.0
            locManager.startUpdatingLocation() // 標準位置情報サービスの開始
            //locManager.startMonitoringSignificantLocationChanges() // 大幅変更位置情報サービスの開始
            isStartCheckLocation = true
        }
        
        if (timer == nil) {
            // タイマーを作る.
            timer = Timer.scheduledTimer( timeInterval: 1.0 /*Sec*/, target: self, selector: #selector(TimeCardVC.onUpdate(_:)), userInfo: nil, repeats: true)
        }
        
    }
    
    func stopCheckLocation() {
        if isStartCheckLocation { // Already Started
            // 位置情報取得の停止
            locManager.stopUpdatingLocation()
            isStartCheckLocation = false
        }
        
        if (timer != nil) {
            // タイマーを止めて消す.
            timer.invalidate()
            timer = nil
        }
        
    }
    
    func initiateTodaysData () {
        if isInitiated { return } // Already initiated
        
        appDl.personalUser = appDl.cf.getPersonalUserInfo(appDl.personalUser) //ユーザ情報読み出し
        appDl.getUserEnv()
        selectedworkPlace = appDl.selectedWP
        lbWorkPlaceLabel.text = selectedworkPlace.workPlaceLabel
        lbWorkPlaceLabel.sizeToFit()
        
        //　前回の状態を回復
        loadSavedState()
        //　画面を前回の状態にする
        restoreViewState()
        lbTimeTable.text = appDl.todaysData.timeTable
        
        // 位置情報取得開始
        startCheckLocation()
        
        if selectedTimeTable.companyID == "" {
            selectedTimeTable = appDl.timeTableLists[0][0] // タイムテーブル名はログから取得したもの優先
        }
        appDl.ttw = cTimeTableWindow(wp: appDl.selectedWP)
        appDl.ttw.createTimeTable(selectedTimeTable,oneday: appDl.todaysData,refresh: true)
        // 祝祭日を取得
        appDl.holidays = getHolidaysByID("", isUsePublic: true , dateFrom: appDl.todaysData.beginningOfDay!)

        isInitiated = true

    }
    
    // MARK: - View Main

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // lbClockの幅を調整
        let fWidth = CGFloat(self.view.frame.width)
        lbClockWidthCOnstraint.constant = (fWidth - 30) / fWidth
   
        // ▼▼▼▼　画面の初期化 User情報に依存しない部分
        let imgStartNormal : UIImage? = UIImage( named:"Arrival")
        let imgStartPush : UIImage? = UIImage( named:"Arrival_Push")
        btnStart.setImage(imgStartNormal, for: UIControlState())
        btnStart.setImage(imgStartPush, for: UIControlState.highlighted)
        btnStart.setImage(imgStartPush, for: UIControlState.disabled)
        let imgEndNormal : UIImage? = UIImage( named:"Leave")
        let imgEndinPush : UIImage? = UIImage( named:"Leave_Push")
        btnEnd.setImage(imgEndNormal, for: UIControlState())
        btnEnd.setImage(imgEndinPush, for: UIControlState.highlighted)
        btnEnd.setImage(imgEndinPush, for: UIControlState.disabled)
        let imgCancelNormal : UIImage? = UIImage( named:"Edit")
        let imgCancelPush : UIImage? = UIImage( named:"Edit_Push")
        btnCancelStart.setImage(imgCancelNormal, for: UIControlState())
        btnCancelStart.setImage(imgCancelPush, for: UIControlState.highlighted)
        btnCancelEnd.setImage(imgCancelNormal, for: UIControlState())
        btnCancelEnd.setImage(imgCancelPush, for: UIControlState.highlighted)

        locManager.delegate = self
        lbDate.text = appDl.dtc.getDateTodayYYYYMMDD_EEE()
        lbClock.text = appDl.dtc.getLocalTimeStringHHmmss(Date())
        isOnWorkLocation = false
        lbLocation.text = /*"現在地：圏外"*/ "エリア外"
        lbTimeTable.text = /*"時間帯：未定"*/ "未定"
        lbLocation.textColor = UIColor.red
        lbStartTime.textColor = UIColor.red
        lbEndTime.textColor = UIColor.red
        // キーボード表示/非表示の検出
        txMemo.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(TimeCardVC.keyboardAppeared(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TimeCardVC.keyboardDisappered(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // インジケータを作成する.
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.center = self.view.center
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        activityIndicator.color = myColorDarkPastelOrange
        self.view.addSubview(activityIndicator)
        // ▲▲▲▲　画面の初期化 User情報に依存しない部分

        
        // ▼▼▼▼　画面の初期化 User情報に依存する部分
        appDl.autoLogin()
        print("Check isLoggedIn!",appDl.personalUser.isLoggedIn,#function, #line)
        if !appDl.personalUser.isLoggedIn { //ログインできない＝ユーザ情報が無い場合は一旦ログイン画面に移動
            
            return
        } else { //ログインできたら処理開始
            initiateTodaysData ()
            print("Check isLoggedIn!",appDl.personalUser.isLoggedIn,#function, #line)
        }
        // ▲▲▲▲　画面の初期化 User情報に依存する部分
        
        // ▼▼▼▼　AdMob
        // 有料ユーザはAdMob表示しない
        switch appDl.perchase.license.isBasicAvalable() {
        case .none, .expired:
            vAdMobBanner.isHidden = false
            vAdMobBanner.adSize = kGADAdSizeBanner
            vAdMobBanner.rootViewController = self
            // 本番用
            vAdMobBanner.adUnitID = "ca-app-pub-4045648911133483/4136792036"
            // テスト用
            //        vAdMobBanner.adUnitID = "ca-app-pub-3940256099942544/2934735716"
            vAdMobBanner.load(GADRequest())
        case .avalable, .permanent :
            vAdMobBanner.isHidden = true
            print("User License is avalable, Skip AdMob")
        }
        // ▲▲▲▲　AdMob
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Viewに戻るときに表示を更新
        super.viewDidAppear(animated)
        activityIndicator.startAnimating()
        print("Check isLoggedIn!",appDl.personalUser.isLoggedIn,#function, #line)
        if !appDl.personalUser.isLoggedIn {
            self.tabBarController?.selectedIndex = 3 // Setting Tab
            activityIndicator.stopAnimating()
            return
        }
        initiateTodaysData ()
        startCheckLocation()
        lbTimeTable.text = appDl.todaysData.timeTable

        activityIndicator.stopAnimating()
    }
    
    
 //   override func viewWillAppear(_ animated: Bool) {
 //       super.viewWillAppear(animated)
 //
 //       print("Check isLoggedIn!",appDl.personalUser.isLoggedIn,#function, #line)
 //   }

    override func viewDidDisappear(_ animated: Bool){
        // Viewに戻るときに表示を更新
        super.viewDidDisappear(animated)
        
        // 位置情報取得終了、タイマ停止
        stopCheckLocation()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

