//
//  WorkPlanVC.swift
//  OnWork
//
//  Created by 細野浩明 on 2016/07/27.
//  Copyright © 2016年 Hiroaki Hosono. All rights reserved.
//

import UIKit

class WorkPlanVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var appDl:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得
    let calendar = Calendar(identifier: Calendar.Identifier.gregorian)

    // MARK: - Variable for interface

    // MARK: - Variable for internal use
    
    var isHolyday:Bool = false
    var isDisplayThisMonth:Bool = false
    var locations:[locationDef] = []
    var timetables:[timeTableDef] = []
    var selectedWP = workPlaceInfo()
    var selectedLoc = locationDef()
    var selectedTT = timeTableDef()

    let daysPerWeek: Int = 7
    let cellMargin : CGFloat = 1.0  //セルのマージン
    var cvHeight:CGFloat = 1.0
    var cellOffset:Int = 0
    
    var IDlabel:[String:String] = [:]  // ID:Name
    var TTNamelabel:[String:String] = [:]  // TimeTable Name:ID
    var LocNamelabel:[String:String] = [:]  // Location Name:ID
    
    var DATE_BG_COL_PAST:UIColor = .black
    var DATE_TX_COL_PAST:UIColor = .black


    // MARK: - @IBOutlet
    
    @IBOutlet weak var lbMonth: UILabel!
    @IBOutlet weak var scSelectMonth: UISegmentedControl!
    @IBOutlet weak var vPlan: UIView!
    @IBOutlet weak var sgHolyday: UISegmentedControl!
    @IBOutlet weak var lbWP: UILabel!
    @IBOutlet weak var sgLoc: UISegmentedControl!
    @IBOutlet weak var sgTT: UISegmentedControl!
    @IBOutlet weak var btnSetWeeklyPlan: BackgroundEnabledButton!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnForward: UIButton!
    @IBOutlet weak var cvCalendar: UICollectionView!

    // MARK: - @IBAction
    
    @IBAction func sgHolydayChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 { // 出勤
            vPlan.backgroundColor = NC_233_SHIRONERI
            isHolyday = false
        } else { // 休日
            vPlan.backgroundColor = NC_012_SAKURA
            isHolyday = true
        }
    }
    
    @IBAction func sgLocChanged(_ sender: UISegmentedControl) {
        selectedLoc = locations[sender.selectedSegmentIndex]
    }
    
    @IBAction func sgTTChanged(_ sender: UISegmentedControl) {
        selectedTT = timetables[sender.selectedSegmentIndex]
   }
    
    @IBAction func btnSetWeeklyPlanOnClick(_ sender: BackgroundEnabledButton) {
        
        let alertController = UIAlertController(
            title: "確認：週間スケジュールを適用します",
            message: "適用条件を選択してください",
            preferredStyle: .actionSheet)
        
        let updatetAction:UIAlertAction = UIAlertAction(title: "再設定（すべて上書き）" ,
                                                        style: UIAlertActionStyle.default,
                                                        handler:{
                                                            (action:UIAlertAction!) -> Void in
                                                            self.updateWeeklySchedule()
        })
        alertController.addAction(updatetAction)
        
        let fillAction:UIAlertAction = UIAlertAction(title: "補完（未設定の日のみ）" ,
                                                        style: UIAlertActionStyle.default,
                                                        handler:{
                                                            (action:UIAlertAction!) -> Void in
                                                            self.updateWeeklySchedule(false) // isUpdate=false
        })
        alertController.addAction(fillAction)
        
        let cancelAction = UIAlertAction(
            title: "キャンセル",
            style: .cancel, handler: nil)
        
        alertController.addAction(cancelAction)
        alertController.popoverPresentationController?.sourceView = view
        alertController.popoverPresentationController?.sourceRect = sender.frame
        present(alertController, animated: true, completion: nil )

    }
    
    @IBAction func swipeLeft(_ sender: UISwipeGestureRecognizer) {
        goToNextMonth()
    }
    
    @IBAction func btnBackOnClick(_ sender: UIButton) {
        goBackLastMonth()
    }
    @IBAction func btnForwardOnClick(_ sender: UIButton) {
        goToNextMonth()
    }
    
    @IBAction func swipeRight(_ sender: UISwipeGestureRecognizer) {
        goBackLastMonth()
    }


    // MARK: - functions
    
    func goToNextMonth() {
        // ここに開始日の判定を入れる　endMonth（最後の月の初日）と比較
        // 来月分は表示するため、翌月の処理を実施　再来月は抑止　表示中の月(thisMonth)がendMonthより前の月なら実行
        if appDl.thisMonthList.last!.beginningOfDay! >= appDl.endMonth {
            print("nextMonthList create skipped !",#function, #line)
            return
        }
        appDl.FFMonthlyList(appDl.workPlaces[appDl.WPIndex])
        createCalendar()
        cvCalendar.reloadData()
    }
    
    func goBackLastMonth() {
        if isDisplayThisMonth { return } // 予定なので過去は表示しない
        appDl.REWMonthlyList(appDl.workPlaces[appDl.WPIndex])
        createCalendar()
        cvCalendar.reloadData()

    }
    func refreshChildView(){
        if appDl.isUpdateStamp {
            appDl.thisMonthList = []
            appDl.createMonthlyList()
            createCalendar()
            appDl.isUpdateStamp = false
        }
        cvCalendar.reloadData()
    }
    
    
    func dispSetting() {
        // Month
        lbMonth.text = appDl.dtc.getDateYYYYMM(appDl.today ?? Date()) + "の予定"  // 未設定の場合は今日の日付から作る

        // WorkPlace
        lbWP.text = appDl.workPlaces[appDl.WPIndex].workPlaceLabel
        selectedWP = appDl.workPlaces[appDl.WPIndex]
        
        // Location 現状登録は３つまで
        var NoLoc = locationDef()
        NoLoc.LocationName = "--"
        locations = [ NoLoc ,NoLoc ,NoLoc ]
        for i in 0 ..< appDl.locationLists[appDl.WPIndex].count {
            if i >= locations.count {break}
            locations[i] = appDl.locationLists[appDl.WPIndex][i]
        }
        // TimeTable 現状登録は３つまで
        var NoTT = timeTableDef()
        NoTT.tableName = "--"
        timetables = [ NoTT ,NoTT ,NoTT ]
        for i in 0 ..< appDl.timeTableLists[appDl.WPIndex].count {
            if i >= timetables.count {
                timetables.append(contentsOf: [appDl.timeTableLists[appDl.WPIndex][i]])
            } else {
                timetables[i] = appDl.timeTableLists[appDl.WPIndex][i]
            }
        }
        for i in 0 ... 2 {
            // Location
            sgLoc.setTitle(locations[i].LocationName, forSegmentAt: i)
            if (locations[i].locationID ) == "" {
                sgLoc.setEnabled(false, forSegmentAt: i)
            }
        }
        
        // TimeTable
        sgTT.removeAllSegments()
        for i in 0  ..< timetables.count {
            sgTT.insertSegment(withTitle: timetables[i].tableName, at: i, animated: false)
            if (timetables[i].timeTableID ) == "" {
                sgTT.setEnabled(false, forSegmentAt: i)
            }
        }
        selectedLoc = locations[0]
        sgLoc.selectedSegmentIndex = 0
        selectedTT = timetables[0]
        sgTT.selectedSegmentIndex = 0
        
    }
    
    func createCalendar() {
        appDl.createTableCal()
        lbMonth.text = appDl.dtc.getDateYYYYMM((appDl.monthlyData[7].beginningOfDay)!) + "の予定"
    }
    
    func setThemeColor() {
        //self.view.backgroundColor = appDl.themeColor.BASE
        cvCalendar.backgroundColor = appDl.themeColor.CAL_BG
        DATE_BG_COL_PAST = appDl.cf.getLetterColor(NC_173_KAMENOZOKI)!
        DATE_TX_COL_PAST = appDl.cf.getLetterColor(DATE_BG_COL_PAST)!
    }

    func setIDlabel() {
        IDlabel = [:]
        LocNamelabel = [:]
        TTNamelabel = [:]
        // Location
        for loc in appDl.locationLists[appDl.WPIndex] {
            IDlabel[loc.locationID] = loc.LocationName
            LocNamelabel[loc.LocationName] = loc.locationID
        }
        // TimeTable
        for tt in appDl.timeTableLists[appDl.WPIndex] {
            IDlabel[tt.timeTableID] = tt.tableName
            TTNamelabel[tt.tableName] = tt.timeTableID
        }
    }
    
    func getShiftTime(_ tableID:String) -> (start:String,end:String) {
        for i in 0 ..< appDl.timeTableLists[appDl.WPIndex].count {
            if appDl.timeTableLists[appDl.WPIndex][i].timeTableID == tableID {
                return (appDl.timeTableLists[appDl.WPIndex][i].startTime,appDl.timeTableLists[appDl.WPIndex][i].endTime)
            }
        }
        return ("--:--","--:--")
    }
    
    func updateWeeklySchedule(_ isUpdate:Bool = true ) {
        let now = Date()
        setIDlabel()
        
        let monthlyList = appDl.thisMonthList
        
        for i in 0 ..< monthlyList.count {
            var oneday = monthlyList[i]
            // 過去データは更新しない
            if oneday.endingOfDay!.compare(now) == .orderedAscending {
                //print("Skipped, its past day",oneday.dateLabel,oneday.weekdayLabel)
                continue
            }
            // Fillのケース
            if !isUpdate && (oneday.timeTableID != "")  {
                //print("Skipped, Already set",oneday.dateLabel,oneday.weekdayLabel,oneday.timeTable)
                continue
            }
            
            
            // 曜日の判定 と　データの作成
            switch calendar.component(.weekday, from: oneday.beginningOfDay!) {
                
            case 1 /* 日 */:
                oneday.isDayOff = !appDl.weeklyPlan.isOn_Sun
                oneday.companyID = appDl.weeklyPlan.WP_Sun
                oneday.locationID = appDl.weeklyPlan.Loc_Sun
                oneday.timeTableID = appDl.weeklyPlan.TT_Sun
            case 2 /* 月 */:
                oneday.isDayOff = !appDl.weeklyPlan.isOn_Mon
                oneday.companyID = appDl.weeklyPlan.WP_Mon
                oneday.locationID = appDl.weeklyPlan.Loc_Mon
                oneday.timeTableID = appDl.weeklyPlan.TT_Mon
            case 3 /* 火 */:
                oneday.isDayOff = !appDl.weeklyPlan.isOn_Tue
                oneday.companyID = appDl.weeklyPlan.WP_Tue
                oneday.locationID = appDl.weeklyPlan.Loc_Tue
                oneday.timeTableID = appDl.weeklyPlan.TT_Tue
            case 4 /* 水 */:
                oneday.isDayOff = !appDl.weeklyPlan.isOn_Wed
                oneday.companyID = appDl.weeklyPlan.WP_Wed
                oneday.locationID = appDl.weeklyPlan.Loc_Wed
                oneday.timeTableID = appDl.weeklyPlan.TT_Wed
            case 5 /* 木 */:
                oneday.isDayOff = !appDl.weeklyPlan.isOn_Thu
                oneday.companyID = appDl.weeklyPlan.WP_Thu
                oneday.locationID = appDl.weeklyPlan.Loc_Thu
                oneday.timeTableID = appDl.weeklyPlan.TT_Thu
            case 6 /* 金 */ :
                oneday.isDayOff = !appDl.weeklyPlan.isOn_Fri
                oneday.companyID = appDl.weeklyPlan.WP_Fri
                oneday.locationID = appDl.weeklyPlan.Loc_Fri
                oneday.timeTableID = appDl.weeklyPlan.TT_Fri
            case 7 /* 土 */:
                oneday.isDayOff = !appDl.weeklyPlan.isOn_Sat
                oneday.companyID = appDl.weeklyPlan.WP_Sat
                oneday.locationID = appDl.weeklyPlan.Loc_Sat
                oneday.timeTableID = appDl.weeklyPlan.TT_Sat
            default:
                break
            }
            
            oneday.companyName = IDlabel[oneday.companyID] ?? ""
            oneday.location = IDlabel[oneday.locationID] ?? ""
            oneday.timeTable = IDlabel[oneday.timeTableID] ?? ""
            //print("Set data :",oneday.dateLabel,oneday.weekdayLabel,oneday.timeTable,oneday.location)
            
            
            // データの書き込み
            updateDailyLog(oneday)
            appDl.thisMonthList[i] = oneday
        }
        
        
        // 画面更新
        appDl.createTableCal()
        cvCalendar.reloadData()
        
    }

    func updateDailyLog(_ onedaysData:workDataOfDay) {
        
        var noLog: NCMBObject? = nil
        var dayKind:String = ""
        if onedaysData.isDayOff {
            dayKind = dayKindType.DayOff.rawValue
        } else {
            dayKind = dayKindType.DayOn.rawValue
        }
        
        let completion: ((_ dailyLogObj:NCMBObject) -> Void) = { dailyLogObj in
            dailyLogObj.setObject(onedaysData.beginningOfDay, forKey: "Date")
            dailyLogObj.setObject(self.appDl.personalUser.IDNo, forKey: "UserID")
            dailyLogObj.setObject("Personal", forKey: "GroupName")
            dailyLogObj.setObject(onedaysData.companyName, forKey: "CompanyName")
            dailyLogObj.setObject(onedaysData.companyID, forKey: "companyID")
            dailyLogObj.setObject(onedaysData.locationID, forKey: "locationID")
            dailyLogObj.setObject(onedaysData.timeTableID, forKey: "timeTableID")
            dailyLogObj.setObject(onedaysData.timeTable, forKey: "TimeTableName")
            dailyLogObj.setObject(onedaysData.isDayOff, forKey: "isDayOff")
            dailyLogObj.setObject(dayKind, forKey: "DayKind")
            
            
            dailyLogObj.saveInBackground({(error) in
                if error != nil {print("Save error : ",error!)}
            })
        }
        if getDailyLogObject( &noLog ,user: appDl.personalUser ,todaysInfo: onedaysData ,closure: completion) {
            print("Logを取得",#function, #line)
        }
        
        
    }



    // MARK: - View Main
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cvCalendar.delegate = self
        cvCalendar.dataSource = self
        setThemeColor()
       
        if appDl.personalUser.isLoggedIn {
            dispSetting()
            appDl.thisMonthList = []
            appDl.createMonthlyList()
            appDl.createTableCal()
            setIDlabel()
            appDl.isUpdateStamp = false
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Viewに戻るときに表示を更新
        super.viewDidAppear(animated)
        if !appDl.personalUser.isLoggedIn {
            self.tabBarController?.selectedIndex = 3 // Setting Tab
            return
        }
        dispSetting()
        refreshChildView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //cvHeight = UIScreen.main.bounds.height - ( 20.0 /*Status Bar*/ + 49.0 /*Tab Bar*/ + 184.5 /*上部*/ + 5.0 /*下部*/)
        cvHeight = cvCalendar.frame.height

    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - For CollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2 //　曜日と日にち
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // Section毎にCellの総数を変える.
        if section == 0 {
            return 7
        } else {
            isDisplayThisMonth = false
            cellOffset = appDl.monthlyData.count - 1
            return appDl.monthlyData.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "planCalCell", for: indexPath) as! PersonalMainCalCell
        if appDl.monthlyData.count <= 0 {
            return cell
        }
        let oneDay = appDl.monthlyData[indexPath.row]
        let now = Date()
        
        // セルを初期化
        cell.vTime.isHidden = true
        cell.lbTriangle.isHidden = true
        cell.lbStart.isHidden = true
        cell.lbEnd.isHidden = true
        cell.lbLocation.isHidden = true
        cell.icoShiftBase.isHidden = true
        cell.lbShift.isHidden = true
        cell.backgroundColor = appDl.themeColor.CAL_BG_WEEKDAY
        cell.lbDate.textColor = appDl.themeColor.CAL_TX_WEEKDAY
        cell.lbDate.backgroundColor = UIColor.clear
        cell.icoShiftBase.backgroundColor = NC_173_KAMENOZOKI
        cell.lbShift.textColor = NC_194_KUROTSURUBAMI
        cell.lbTriangle.textColor = UIColor.black
        cell.lbStart.textColor = UIColor.black
        cell.lbEnd.textColor = UIColor.black
        
        
        //テキスト配置
        if indexPath.section == 0 {
            cell.lbDate.text = oneDay.weekdayLabel
            cell.lbDate.textAlignment = .center
            if appDl.weeklyPlan.isWeekdayOff[oneDay.weekdayNum] {
                cell.backgroundColor = appDl.themeColor.CAL_BG_HOLIDAY
                cell.lbDate.textColor = appDl.themeColor.CAL_TX_HOLIDAY
            } else {
                cell.backgroundColor = appDl.themeColor.CAL_BG_WEEKDAY
                cell.lbDate.textColor = appDl.themeColor.CAL_TX_WEEKDAY
            }
            cell.lbDate.backgroundColor = UIColor.clear
            
            cell.vTime.isHidden = true
            cell.lbTriangle.isHidden = true
            cell.lbStart.isHidden = true
            cell.lbEnd.isHidden = true
            cell.lbLocation.isHidden = true
            cell.icoShiftBase.isHidden = true
            cell.lbShift.isHidden = true
            
        } else {
            cell.lbDate.text = " " + oneDay.dateLabel[oneDay.dateLabel.index(oneDay.dateLabel.endIndex, offsetBy: -2)...].description
            cell.lbDate.textAlignment = .left
            //実績と予定を分ける
            if oneDay.beginningOfDay!.compare(now) == .orderedDescending { // 予定
                //セルの色
                if oneDay.isDayOff {
                    cell.icoShiftBase.isHidden = false
                    cell.lbShift.isHidden = false
                    
                    cell.lbDate.backgroundColor = appDl.themeColor.CAL_BG_HOLIDAY
                    cell.lbDate.textColor = appDl.themeColor.CAL_TX_HOLIDAY
                    cell.icoShiftBase.backgroundColor = appDl.themeColor.CAL_TX_HOLIDAY
                    cell.lbShift.textColor = appDl.themeColor.CAL_BG_HOLIDAY
                    cell.lbShift.text = "休日"
                    cell.lbLocation.text = oneDay.holidayName
                    cell.vTime.isHidden = true
                    cell.lbTriangle.isHidden = true
                    cell.lbStart.text = ""
                    cell.lbEnd.text = ""
                    
                    cell.backgroundColor =  myColorPastelLightPink
                    
                } else {
                    cell.vTime.isHidden = false
                    cell.lbTriangle.isHidden = false
                    cell.lbStart.isHidden = false
                    cell.lbEnd.isHidden = false
                    cell.lbLocation.isHidden = false
                    
                    cell.lbDate.backgroundColor = appDl.themeColor.CAL_BG_WEEKDAY
                    cell.lbDate.textColor = appDl.themeColor.CAL_TX_WEEKDAY
                    let timeTableName:String = oneDay.timeTable
                    if timeTableName == "" {
                        cell.icoShiftBase.isHidden = false
                        cell.lbShift.isHidden = false
                        cell.icoShiftBase.backgroundColor = NC_235_SHIRONEZUMI
                        cell.lbShift.textColor = NC_233_SHIRONERI
                        cell.lbShift.text = "未定"
                    } else {
                        cell.icoShiftBase.isHidden = false
                        cell.lbShift.isHidden = false
                        cell.icoShiftBase.backgroundColor = NC_173_KAMENOZOKI
                        cell.lbShift.textColor = NC_194_KUROTSURUBAMI
                        
                        if timeTableName.count > 2 {
                            cell.lbShift.text = timeTableName[..<timeTableName.index(timeTableName.startIndex, offsetBy: 2)].description
                        } else {
                            cell.lbShift.text = timeTableName
                        }
                    }
                    // 勤務地
                    cell.lbLocation.text = IDlabel[ appDl.monthlyData[indexPath.row].locationID]
                    // 勤務開始時刻 timetableの開始時刻を入れる
                    // 勤務終了時刻   timetableの開始時刻を入れる
                    var TTID = appDl.monthlyData[indexPath.row].timeTableID
                    if TTID == "" {
                        TTID = TTNamelabel[ timeTableName ] ?? ""
                    }
                    let result = getShiftTime(TTID)
                    cell.vTime.isHidden = false
                    cell.lbTriangle.isHidden = false
                    cell.lbStart.textColor = UIColor.black
                    cell.lbEnd.textColor = UIColor.black
                    cell.lbStart.text = result.start
                    cell.lbEnd.text = result.end
                    
                    cell.backgroundColor =  NC_233_SHIRONERI
                    
                }
                
            } else { //実績
                //セルの色
                if oneDay.isDayOff {
                    cell.icoShiftBase.isHidden = false
                    cell.lbShift.isHidden = false
                    cell.lbDate.backgroundColor = appDl.themeColor.CAL_BG_HOLIDAY_PAST
                    cell.lbDate.textColor = appDl.themeColor.CAL_TX_HOLIDAY
                    cell.icoShiftBase.backgroundColor = appDl.themeColor.CAL_BG_HOLIDAY
                    cell.lbShift.textColor = appDl.themeColor.CAL_TX_HOLIDAY
                    cell.lbShift.text = "休日"
                    cell.lbLocation.text = oneDay.holidayName
                    cell.vTime.isHidden = true
                    cell.lbTriangle.isHidden = true
                    cell.lbStart.text = ""
                    cell.lbEnd.text = ""
                    
                    cell.backgroundColor =  myColorPastelLightPink
                    
                } else {
                    cell.vTime.isHidden = false
                    cell.lbTriangle.isHidden = false
                    cell.lbStart.isHidden = false
                    cell.lbEnd.isHidden = false
                    cell.lbLocation.isHidden = false
                    
                    cell.lbDate.backgroundColor = DATE_BG_COL_PAST
                    cell.lbDate.textColor = DATE_TX_COL_PAST
                    let timeTableName:String = oneDay.timeTable
                    if timeTableName == "" {
                        cell.icoShiftBase.isHidden = false
                        cell.lbShift.isHidden = false
                        cell.icoShiftBase.backgroundColor = NC_235_SHIRONEZUMI
                        cell.lbShift.textColor = NC_233_SHIRONERI
                        cell.lbShift.text = "なし"
                    } else {
                        cell.icoShiftBase.isHidden = false
                        cell.lbShift.isHidden = false
                        cell.icoShiftBase.backgroundColor = NC_173_KAMENOZOKI
                        cell.lbShift.textColor = NC_194_KUROTSURUBAMI
                        
                        if timeTableName.count > 2 {
                            cell.lbShift.text = timeTableName[..<timeTableName.index(timeTableName.startIndex, offsetBy: 2)].description

                        } else {
                            cell.lbShift.text = timeTableName
                        }
                    }
                    // 勤務地
                    cell.lbLocation.text = IDlabel[ oneDay.locationID]
                    // 出勤時刻
                    cell.lbStart.text = appDl.dtc.getLocalTimeString(oneDay.startTimeOfWork)
                    if oneDay.isStartManually {
                        cell.lbStart.textColor = NC_014_KARAKURENAI
                    } else {
                        cell.lbStart.textColor = UIColor.black
                    }
                    
                    // 退勤時刻
                    cell.lbEnd.text = appDl.dtc.getLocalTimeString(oneDay.endTimeOfWork)
                    if oneDay.isEndManually {
                        cell.lbEnd.textColor = NC_014_KARAKURENAI
                    } else {
                        cell.lbEnd.textColor = UIColor.black
                    }
                    
                    cell.backgroundColor =  NC_233_SHIRONERI
                    
                }
                if calendar.isDate(oneDay.beginningOfDay!, inSameDayAs: Date()) { // 今日と同じ日付
                    cell.lbDate.backgroundColor = NC_014_KARAKURENAI
                    cell.lbDate.textColor = NC_233_SHIRONERI
                    if oneDay.isThisMonth {
                        isDisplayThisMonth = true
                    }
                }
                
            }
            
            if !oneDay.isThisMonth {
                cell.lbDate.backgroundColor = appDl.themeColor.CAL_BG_OUT_OF_RANGE
                cell.lbDate.textColor = appDl.themeColor.CAL_TX_OUT_OF_RANGE
                cell.backgroundColor = appDl.themeColor.CAL_BG_OUT_OF_RANGE
                cell.lbTriangle.textColor = appDl.themeColor.CAL_TX_OUT_OF_RANGE
                cell.lbStart.textColor = appDl.themeColor.CAL_TX_OUT_OF_RANGE
                cell.lbEnd.textColor = appDl.themeColor.CAL_TX_OUT_OF_RANGE
                cell.icoShiftBase.backgroundColor = appDl.themeColor.CAL_TX_OUT_OF_RANGE
            } else {
                if cellOffset > indexPath.row {
                    cellOffset = indexPath.row
                }
            }
            
        }
        return cell
    }
    
    //セルのサイズを設定
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfMargin: CGFloat = 8.0
        let width: CGFloat = (collectionView.frame.width - cellMargin * numberOfMargin) / CGFloat(daysPerWeek)
        let height: CGFloat
        if indexPath.section == 0 {
            height = 20.0
        } else {
            let columns:Int = appDl.monthlyData.count / 7
            height = (cvHeight - 20.0 /*曜日*/ - (cellMargin * CGFloat(columns))) / CGFloat(columns)
        }
        return CGSize(width:width, height:height)
    }
    
    //セルの垂直方向のマージンを設定
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return cellMargin
    }
    
    //セルの水平方向のマージンを設定
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return cellMargin
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            return
        }
        if appDl.monthlyData[indexPath.row].endingOfDay!.compare(Date()) == .orderedAscending { // endingOfDay < now
            return // 過去の日は変更不可とする
        }
        if !appDl.monthlyData[indexPath.row].isThisMonth {
            return // 今月分以外は更新不可とする
        }
        
        appDl.monthlyData[indexPath.row].isDayOff = isHolyday
        appDl.monthlyData[indexPath.row].companyID = selectedWP.workPlaceID
        appDl.monthlyData[indexPath.row].companyName = selectedWP.workPlaceLabel
        appDl.monthlyData[indexPath.row].locationID = selectedLoc.locationID
        appDl.monthlyData[indexPath.row].location = selectedLoc.LocationName
        appDl.monthlyData[indexPath.row].timeTableID = selectedTT.timeTableID
        appDl.monthlyData[indexPath.row].timeTable = selectedTT.tableName
        
        appDl.thisMonthList[indexPath.row - cellOffset] = appDl.monthlyData[indexPath.row]
        
        updateDailyLog(appDl.monthlyData[indexPath.row])
        
        cvCalendar.reloadData()

    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
//        print("shouldPerformSegueWithIdentifier:",identifier)
//        return true
//    }

//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        print("prepareForSegue:",segue.identifier!)
//        
//        switch segue.identifier ?? "" {
//        case "toScheduleList": 
//            listCtr = segue.destination as? WorkPlanListTVC
//            listCtr?.monthPointer = 0
//            
//        default :
//            break
//        }
//    }


}
