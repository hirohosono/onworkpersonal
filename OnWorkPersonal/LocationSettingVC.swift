//
//  LocationSettingVC.swift
//  OnWork
//
//  Created by 細野浩明 on 2016/07/26.
//  Copyright © 2016年 Hiroaki Hosono. All rights reserved.
//

import UIKit
import MapKit

class LocationSettingVC: UIViewController,MKMapViewDelegate {
    
    var isNewLocation:Bool = false
    //var selectedWorkPlace = workPlaceInfo()
    //var locationLists:[locationDef] = []
    var selectedLocation = locationDef()
    var SelectedIndex:Int = 0
    var oldPin:MKPointAnnotation? = nil

    
    var appDl:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得

    @IBOutlet weak var lbWorkPlaceLabel: UILabel!
 
    @IBOutlet weak var txLocation: UITextField!
    @IBOutlet weak var txAddress: UITextField!
    @IBOutlet weak var txLatitude: UITextField!
    @IBOutlet weak var txLongitude: UITextField!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var btnSave: BackgroundEnabledButton!
    @IBOutlet weak var btnUpdateMap: UIButton!

    @IBAction func dataChanged(_ sender: UITextField) {
        setButtonState()
    }
    
    func setPin(_ coordinate:CLLocationCoordinate2D) {
        if oldPin != nil {
            map.removeAnnotation(oldPin!)
            oldPin = nil
        }
        
        let pin = MKPointAnnotation()
        pin.coordinate = coordinate
        
        map.addAnnotation(pin)
        oldPin = pin
        
    }
    
    func setMapCentre(_ centerCoordinate:CLLocationCoordinate2D){
        let coordinateSpan:MKCoordinateSpan = MKCoordinateSpanMake(0.01,0.01)
        let newRegion:MKCoordinateRegion = MKCoordinateRegionMake(centerCoordinate, coordinateSpan)
        map.setRegion(newRegion, animated: true)

    }
    
    func setLocationFromGeoCoding(_ placemark:CLPlacemark,address:String){
        txLatitude.text = placemark.location?.coordinate.latitude.description
        txLongitude.text = placemark.location?.coordinate.longitude.description
        txAddress.text = address  // 現在地の表示

        selectedLocation.address = address
        selectedLocation.geo.latitude = (placemark.location?.coordinate.latitude)!
        selectedLocation.geo.longitude = (placemark.location?.coordinate.longitude)!

        setMapCentre((placemark.location?.coordinate)!)
        setPin((placemark.location?.coordinate)!)

    }
    
    func reverseGeoCoding(_ location:CLLocation) {
        print("location:",location.description)
        let myGeocorder = CLGeocoder()
        // 逆ジオコーディング開始.
        myGeocorder.reverseGeocodeLocation(location,
            completionHandler: { (placemarks, error) -> Void in
                if placemarks != nil {
                    for placemark in placemarks! {
                        // 現在の住所
                        var currentAddress = ""
                        currentAddress += placemark.administrativeArea ?? ""
                        currentAddress += placemark.locality ?? ""
                        currentAddress += placemark.thoroughfare ?? ""
                        currentAddress += placemark.subThoroughfare ?? ""
                        
                        self.txAddress.text = currentAddress  // 現在地の表示
                        self.selectedLocation.address = currentAddress
                        self.selectedLocation.geo.latitude = (placemark.location?.coordinate.latitude)!
                        self.selectedLocation.geo.longitude = (placemark.location?.coordinate.longitude)!
                        self.setMapCentre((placemark.location?.coordinate)!)
                        self.setPin((placemark.location?.coordinate)!)
                        print("Latitude:",self.txLatitude.text ?? "??","Longitude:",self.txLongitude.text ?? "??","Address:",currentAddress)
                   }
                }
        })
    }
    
    func geoCoding(_ address:String) {
        let myGeocorder = CLGeocoder()
        // ジオコーディング開始.
        myGeocorder.geocodeAddressString(address,
            completionHandler:  { (placemarks, error) -> Void in
                //print(error)
                if placemarks != nil {
                    //print("placemarks.count=",placemarks!.count)
                    for placemark in placemarks! {
                        // 現在の緯度経度
                        var currentAddress = ""
                        currentAddress += placemark.administrativeArea ?? ""
                        currentAddress += placemark.locality ?? ""
                        currentAddress += placemark.thoroughfare ?? ""
                        currentAddress += placemark.subThoroughfare ?? ""
                        if currentAddress != address {
                            print(currentAddress)
                            let message:String = "下記の住所がヒットしました。この住所を使用しますか？\n" + currentAddress
                            let alertController = UIAlertController(
                                title: "確認",
                                message: message,
                                preferredStyle: .actionSheet)
                            
                            let defaultAction:UIAlertAction = UIAlertAction(title: "使用する",
                                style: UIAlertActionStyle.destructive,
                                handler:{
                                    (action:UIAlertAction!) -> Void in
                                    self.setLocationFromGeoCoding(placemark,address: currentAddress)
                            })
                            
                            let cancelAction = UIAlertAction(
                                title: "キャンセル",
                                style: .cancel, handler: nil)
                            
                            alertController.addAction(defaultAction)
                            alertController.addAction(cancelAction)
                            alertController.popoverPresentationController?.sourceView = self.view
                            alertController.popoverPresentationController?.sourceRect = self.txAddress.frame
                            
                            self.present(alertController, animated: true, completion: nil)

                            
                        } else {
                            self.setLocationFromGeoCoding(placemark,address: currentAddress)
                        }
                    }
                }
        })
        
    }


    func diaplayCompanyName(){
        if appDl.workPlaces.count > appDl.WPIndex {
            lbWorkPlaceLabel.text = appDl.workPlaces[appDl.WPIndex].workPlaceLabel
        } else {
            lbWorkPlaceLabel.text = "No Place" 
        }
    }
    
    func displayLocationData(_ location :locationDef){
        txLocation.text = location.LocationName
        txAddress.text = location.address
        txLatitude.text = location.geo.latitude.description
        txLongitude.text = location.geo.longitude.description
        let currentLocation:CLLocation = CLLocation(latitude: location.geo.latitude, longitude: location.geo.longitude)
        setMapCentre(currentLocation.coordinate)
        
        setPin(currentLocation.coordinate)

    }

    @IBAction func btnUpdateMapOnClick(_ sender: UIButton) {
        self.view.endEditing(true)


/* この画面では緯度経度の入力での更新はなし！
         if (txLatitude.text != selectedLocation.geo.latitude.description)
            || (txLongitude.text != selectedLocation.geo.longitude.description) { //緯度経度更新されていたらそちらが優先
                if (txLatitude.text != "" ) && (txLongitude.text != "") {
                    let location = CLLocation(latitude: Double(txLatitude.text!)!, longitude: Double(txLongitude.text!)!)
                    reverseGeoCoding(location)
                    return
                }
        }
 */
        if txAddress.text != selectedLocation.address { //アドレス更新されていたらそちらが優先
            if txAddress.text != "" {
                geoCoding(txAddress.text!)
            }
        }
    }

    @IBAction func mapLongPressed(_ sender: UILongPressGestureRecognizer) {
        if (sender.state == UIGestureRecognizerState.began) {  // 長押し検出開始時のみ動作
            
            //senderから長押しした地図上の座標を取得
            let location = sender.location(in: self.map)
            //CLLocationCoordinate2Dに変換
            let mapPoint:CLLocationCoordinate2D = self.map.convert(location, toCoordinateFrom: self.map)
            setPin(mapPoint)
            txLatitude.text = mapPoint.latitude.description
            txLongitude.text = mapPoint.longitude.description
            
            let myGeocorder = CLGeocoder()
            let pressedLocation = CLLocation(latitude: mapPoint.latitude, longitude: mapPoint.longitude)
            
            // 逆ジオコーディング開始.
            myGeocorder.reverseGeocodeLocation(pressedLocation,
                completionHandler: { (placemarks, error) -> Void in
                    if placemarks != nil {
                        for placemark in placemarks! {
                            // 現在の住所
                            var currentAddress = ""
                            currentAddress += placemark.administrativeArea ?? ""
                            currentAddress += placemark.locality ?? ""
                            currentAddress += placemark.thoroughfare ?? ""
                            currentAddress += placemark.subThoroughfare ?? ""
                            
                            self.txAddress.text = currentAddress  // 現在地の表示
                        }
                    }
                    
            })
          
        }

        
    }
    
    @IBAction func btnSaveOnclick(_ sender: BackgroundEnabledButton) {
        
        selectedLocation.LocationName = txLocation.text!
        selectedLocation.address = txAddress.text!
        selectedLocation.geo.latitude = Double(txLatitude.text!)!
        selectedLocation.geo.longitude = Double(txLongitude.text!)!
        selectedLocation.isUseBeacon = false  // 未サポートのため
        selectedLocation.companyName = appDl.workPlaces[appDl.WPIndex].workPlaceLabel
        selectedLocation.companyID = appDl.workPlaces[appDl.WPIndex].workPlaceID
        if isNewLocation {
            var maxID:Int = 0
            for loc in appDl.locationLists[appDl.WPIndex] {
                let ei = loc.locationID.range(of: "-L")!.upperBound
                let numStr = loc.locationID[ei...].description
                
                let locNo:Int = Int(numStr )!
                if locNo >= maxID { maxID = locNo }
            }
            maxID = maxID + 1
            selectedLocation.locationID = appDl.workPlaces[appDl.WPIndex].workPlaceID + "-L" + maxID.description
        }
        
        if !updateLocation(selectedLocation) {
            addLocation(selectedLocation) // データが見つからない場合は追加する
            appDl.locationLists[appDl.WPIndex].append(selectedLocation)
        } else {
            appDl.locationLists[appDl.WPIndex][SelectedIndex] = selectedLocation
        }
        
        performSegue(withIdentifier: "unwindToLocationList", sender: nil)

    }

    
    // Regionが変更された時に呼び出されるメソッド.
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        print("regionDidChangeAnimated")
        setButtonState()

    }
    
    // ピンにアニメーション
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        let reuseId = "pin"
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView?.animatesDrop = true
        }
        else {
            pinView?.annotation = annotation
        }
        
        return pinView
    }

    func setButtonState() {
        btnSave.isEnabled = false
        if ((txLocation.text ?? "") != "")
            && ((txLatitude.text ?? "0.0") != "0.0") {
            btnSave.isEnabled = true
        }
    
    }
    
    override func loadView() {
        super.loadView()
        
        // 勤務地取得
        if isNewLocation {
            selectedLocation = locationDef()
        } else { // 渡されたデータを使う
            
        }
        setButtonState()
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        map.delegate = self
        
        diaplayCompanyName()
        displayLocationData(selectedLocation)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("prepareForSegue:",segue.identifier!)
        /*
        switch segue.identifier ?? "" {
        case "unwindToLocationList":
            let controller = segue.destinationViewController as! LocationListTVC
            controller.tableView.reloadData()
            
        default :
            break
        }
         */
    }

}
