//
//  PersonalHelpVC.swift
//  OnWork
//
//  Created by 細野浩明 on 2016/08/08.
//  Copyright © 2016年 Hiroaki Hosono. All rights reserved.
//

import UIKit

class PersonalHelpVC: UIViewController {

    @IBOutlet weak var btnLinkProduct: UIButton!
    @IBOutlet weak var btnLinkContact: UIButton!
    @IBOutlet weak var btnHowToStart: UIButton!
    @IBAction func btnLinkProductOnClick(_ sender: AnyObject) {
        let url:URL = URL(string: "http://meltingpot.co.jp/onwork-personal/")!
        UIApplication.shared.openURL(url)
    }
    
    @IBAction func btnLinkContactOnClick(_ sender: AnyObject) {
        let url:URL = URL(string: "http://meltingpot.co.jp/contact/")!
        UIApplication.shared.openURL(url)
    }
    @IBAction func btnHowToStartOnClick(_ sender: AnyObject) {
        let url:URL = URL(string: "http://meltingpot.co.jp/start_onwork-personal/")!
        UIApplication.shared.openURL(url)
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imgLinkNormal : UIImage? = UIImage( named:"Link")
        let imgLinkPush : UIImage? = UIImage( named:"Link_Push")
        btnLinkProduct.setImage(imgLinkNormal, for: UIControlState())
        btnLinkProduct.setImage(imgLinkPush, for: UIControlState.highlighted)
        btnLinkContact.setImage(imgLinkNormal, for: UIControlState())
        btnLinkContact.setImage(imgLinkPush, for: UIControlState.highlighted)
        btnHowToStart.titleLabel?.numberOfLines = 2

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
