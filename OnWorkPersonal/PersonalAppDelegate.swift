//
//  AppDelegate.swift
//  OnWork
//
//  Created by 細野浩明 on 2015/08/20.
//  Copyright © 2015年 Hiroaki Hosono. All rights reserved.
//

import UIKit
import StoreKit
import UserNotifications
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,OWPurchaseManagerDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
    let dtc = dateTimeControll()
    let cf = commonFunction()
    var ttw = cTimeTableWindow(company: companyInfo())

    // ▼▼ここから▼▼　Refactor for Personal
    
    // データよみ出し完了
    var isInitialized:Bool = false
    
    // 個人で必要な情報
    // ユーザ情報：当人の情報のみあれば良い
    var personalUser = personalUserInfo()
    // 会社情報：複数対応可能としておき、とりあえず１社のみ登録可能とする
    var workPlaces:[workPlaceInfo] = []
    var selectedWP = workPlaceInfo()
    var WPIndex:Int = 0
    
    // 給与情報：会社に紐付いた情報
    
    // 勤務地情報：複数対応可能としておき、とりあえず上限は３個までとする
    var locationLists:[[locationDef]] = [[]] // [WP_No][LocNo]
    // タイムテーブル情報：複数対応可能としておき、とりあえず上限は３個までとする
    var timeTableLists:[[timeTableDef]] = [[]] // [WP_No][TTNo]
    // 会社と勤務地、会社とタイムテーブルを紐付けておき、選択条件とする
    // 勤務地とタイムテーブルは日付を経由して紐づく
    var todaysData = workDataOfDay()
    var today:Date? = nil
   
    // 本日の情報：
    var isUpdateStamp:Bool = false ;
    // 選択日の情報：
    // 週間予定
    var weeklyPlan = weeklySchedule()
    // 先月の実績：
    var lastMonthList:[workDataOfDay] = []
    // 今月の実績と予定：
    var thisMonthList:[workDataOfDay] = []
    var thisMonthHold:[workDataOfDay] = []
    // 来月の予定：
    var nextMonthList:[workDataOfDay] = []
    var nextMonthHold:[workDataOfDay] = []

    // 表示する月のデータ カレンダー表示のため前後の月のデータも含む
    var monthlyData:[workDataOfDay] = []
    var isStartMonday:Bool = true

    // 一般的な休日と土日はカレンダー表示で色を変えるためだけに使う
    var holidays:[holidayInfo] = [] // 先月から来月まで３ヶ月間の休日とする
    
    var themeColor = baseColor()

    
    // ライセンス情報：別途
    var perchase = PerchaseProduct()

    // カレンダーに表示できる月
    // 表示できる最初の月：先月　→　ライセンス購入したら　ライセンス登録した月 または １年前
    var startMonth:Date = Date()
    // 表示できる最後の月：３ヶ月後　→　ライセンス購入したら　１年後まで
    var endMonth:Date = Date()

    // ▲▲ここまで▲▲　Refactor for Personal

    
    var weeklyCal:weeklyCalendar = weeklyCalendar()
    var isUserChanged:Bool = false
    var currentMailAddress:String = ""
    var timeTables:[String:timeTableDef] = [:]


    // ▼▼ここから▼▼　ソース共有化のためだけに残すもの

    var imageCache:[String:Data] = [:] // Dictionary -> String:Filename,UIImage:casehed image

    // ▲▲ここまで▲▲　ソース共有化のためだけに残すもの

    func saveUserInfo() {
        let userDefaults = UserDefaults.standard
        userDefaults.set(personalUser.mail, forKey: "UserMailAddress")
        userDefaults.set(personalUser.password, forKey: "UserPassword")
        userDefaults.set(personalUser.name, forKey: "UserName")
        userDefaults.set(personalUser.nameKana, forKey: "UserNameKana")
        userDefaults.set(personalUser.sex, forKey: "UserSex")
        userDefaults.set(personalUser.IDNo, forKey: "UserID")
        userDefaults.set(personalUser.isRegistered, forKey: "UseRegistered")
        userDefaults.set(personalUser.isAuthorized, forKey: "UserAuthorized")
        
        userDefaults.set(perchase.license.basic, forKey: LICENSE_BASIC)
        userDefaults.set(perchase.license.basicExpireDate, forKey: LICENSE_BASIC_EXPIRE)
        userDefaults.set(perchase.license.permanent, forKey: LICENSE_PERMANENT)

    }

    func restoreUserInfo() {
        let userDefaults = UserDefaults.standard
        personalUser.mail = userDefaults.string(forKey: "UserMailAddress") ?? ""
        personalUser.password = userDefaults.string(forKey: "UserPassword") ?? ""
        personalUser.name = userDefaults.string(forKey: "UserName") ?? ""
        personalUser.nameKana = userDefaults.string(forKey: "UserNameKana") ?? ""
        personalUser.sex = userDefaults.string(forKey: "UserSex") ?? ""
        personalUser.IDNo = userDefaults.string(forKey: "UserID") ?? ""
        personalUser.isRegistered = userDefaults.bool(forKey: "UseRegistered")
        personalUser.isAuthorized = userDefaults.bool(forKey: "UserAuthorized")
        
        // ライセンス情報を読み出す
        perchase.license.permanent = userDefaults.bool(forKey: LICENSE_PERMANENT)
        if perchase.license.permanent {
            perchase.license.basic = true
            perchase.license.basicExpireDate = dtc.getDateFromStr("9999/12/31 23:59:59")
        } else {
            perchase.license.basic = userDefaults.bool(forKey: LICENSE_BASIC)
            perchase.license.basicExpireDate = userDefaults.object(forKey: LICENSE_BASIC_EXPIRE) as? Date
            if perchase.license.basic {
                if perchase.license.basicExpireDate == nil {
                    perchase.license.basicExpired = true
                } else if perchase.license.basicExpireDate! < Date() { // 期限切れ
                    perchase.license.basicExpired = true
                } else { // 期限内
                    perchase.license.basicExpired = false
                }
                print("basicExpireDate:\(perchase.license.basicExpireDate!.description) \(#function)[\(#line)]")
            } else {
                perchase.license.basicExpireDate = nil
                perchase.license.basicExpired = false // default値
            }
        }

    }
    
    /// Read date of today and initiate todaysData
    ///
    /// - parameter workPlaceInfo:
    /// - returns: workDataOfDay
    
    func initTodaysData(_ wp:workPlaceInfo) -> workDataOfDay {
        var todaysData = workDataOfDay()
        let now = Date()
        let dateLineWk = dtc.getDateLine(wp.dateLine)
        if dateLineWk.compare(now) == .orderedDescending { // dateLineWk > now
            todaysData.beginningOfDay = Date(timeInterval: -24*60*60, since: dateLineWk) //1日前.
            todaysData.endingOfDay = dateLineWk
        } else {
            todaysData.beginningOfDay = dateLineWk
            todaysData.endingOfDay = Date(timeInterval: 24*60*60, since: dateLineWk) //1日後.
        }
        let date =  dtc.getDateMMDD(todaysData.beginningOfDay!) // MM/DD
        todaysData.dateLabel = date.date
        todaysData.weekdayLabel = date.weekday // (月)
        todaysData.weekdayNum = date.weekdayNum // 曜日の数値

        todaysData.startTimeOfWork = nil
        todaysData.endTimeOfWork = nil
        todaysData.isDayOff = false
        todaysData.location = ""
        todaysData.timeTable = ""
        todaysData.workHours = 0  // minutes
        todaysData.overtimeWorkHours = 0  // minutes
        todaysData.lateNightWorkHours = 0 // minutes
        todaysData.memo = ""
        todaysData.isStartManually = false
        todaysData.isEndManually = false
        todaysData.startLocation = ""
        todaysData.startAddress = ""
        todaysData.endLocation = ""
        todaysData.endAddress = ""
        return todaysData
    }
    
    
    func getUserEnv() {
        if isInitialized { return } // イニシャライズ済み
        if personalUser.IDNo == "" { return } // ユーザ情報なし
        
        workPlaces = getWorkPlaces(personalUser.IDNo) //WorkPlace情報読み出し

        if workPlaces.count > 0 {
            print("workPlaces read complete !",#function, #line)
            for i in 0 ..< workPlaces.count {
                timeTableLists[i] = getTimeTableListArray(workPlaces[i].workPlaceID)
                locationLists[i] = getLocationListAllwithDef(workPlaces[i].workPlaceID)
            }
            selectedWP = workPlaces[0]
            todaysData = initTodaysData(selectedWP)
            today = todaysData.beginningOfDay as Date?
            todaysData.timeTable =  personalUser.defaultTimeTable
            print("todaysData.timeTable : ",todaysData.timeTable,#function, #line)

        } else {
            print("workPlaces read fail !",#function, #line)
            return
        }
        weeklyPlan = getWeeklySchedule(personalUser.IDNo)
        print("weeklyPlan read Complete ! ",#function, #line)

        isInitialized = true
    }
   
    func setValidMonth() {
        if lastMonthList.count == 0 { return }

        switch perchase.license.isBasicAvalable() {
        case .none, .expired:
            startMonth = lastMonthList[0].beginningOfDay!
            endMonth = nextMonthList.last!.beginningOfDay!
            endMonth = dtc.getNextDate(endMonth) // 翌々月の初日
        case .avalable, .permanent :
            startMonth = dtc.getLastYearStart(thisMonthList.first!.beginningOfDay!)
            endMonth = dtc.getNextYearEnd(thisMonthList.last!.beginningOfDay!) // 1年後
            print("User License is avalable, Enable 1 year view")
        }

    }
    func createMonthlyList() {
        if thisMonthList.count < 1 {
            (thisMonthList,lastMonthList,nextMonthList) = cf.createWorkLists(workPlaces[WPIndex])
            getMonthlyLogList(personalUser,openDate:(thisMonthList.first?.beginningOfDay!)! ,CloseDate:(thisMonthList.last?.beginningOfDay!)!,monthlyList:&thisMonthList)
            print("thisMonthList create complete !",#function, #line)
            getMonthlyLogList(personalUser,openDate:(lastMonthList.first?.beginningOfDay!)! ,CloseDate:(lastMonthList.last?.beginningOfDay!)!,monthlyList:&lastMonthList)
            print("lastMonthList create complete !",#function, #line)
            getMonthlyLogList(personalUser,openDate:(nextMonthList.first?.beginningOfDay!)! ,CloseDate:(nextMonthList.last?.beginningOfDay!)!,monthlyList:&nextMonthList)
            print("nextMonthList create complete !",#function, #line)
            // カレンダー表示で月を切り替えると変わってしまうため、保存しておく
            thisMonthHold = thisMonthList
            nextMonthHold = nextMonthList
            setValidMonth()
        }
    }
    
    func FFMonthlyList(_ workPlace:workPlaceInfo) {
        lastMonthList = thisMonthList
        thisMonthList = nextMonthList
        // 翌月分を作る
        nextMonthList = cf.createNextMonthLists(workPlace, LastDay:thisMonthList.last!.beginningOfDay!)
        getMonthlyLogList(personalUser,openDate:nextMonthList.first!.beginningOfDay! ,CloseDate:(nextMonthList.last?.beginningOfDay!)!,monthlyList:&nextMonthList)
        print("nextMonthList create complete !",#function, #line)
    }
    func REWMonthlyList(_ workPlace:workPlaceInfo) {
        nextMonthList = thisMonthList
        thisMonthList = lastMonthList
        // 前月分を作る
        lastMonthList = cf.createLastMonthLists(workPlace, firstDay:thisMonthList.first!.beginningOfDay!)
        getMonthlyLogList(personalUser,openDate:(lastMonthList.first?.beginningOfDay!)! ,CloseDate:(lastMonthList.last?.beginningOfDay!)!,monthlyList:&lastMonthList)
        print("lastMonthList create complete !",#function, #line)

    }
    
    /// automatic login
    ///
    /// - parameter 無し
    /// - returns: 無し personalUser.isLoggedInを更新
    func autoLogin() {
        personalUser.isLoggedIn = false
        print("Auto Login Start!",#function, #line," pw:",personalUser.password)


        let mailAddress = personalUser.mail
        let password = personalUser.password
        let authorized = personalUser.isAuthorized
        
        if !authorized || (mailAddress=="") || (password=="") { return }
        do {
            try NCMBUser.logIn(withMailAddress: mailAddress, password: password)
        } catch  let error1 as NSError  {
            print("Login failed: \(error1)",#function, #line)
            return
        }
        let groupName = NCMBUser.current().object(forKey: "GroupName") as? String ?? ""
        if groupName != "Personal" { // Personalユーザ？
            print("Login failed: Not Personal user")
            personalUser.isRegistered = false
            personalUser.isAuthorized = false
            personalUser.isLoggedIn = false
            NCMBUser.logOut()
            return
        }

        currentMailAddress = mailAddress
        personalUser.isLoggedIn = true
        print("Auto Login Complete!",#function, #line)

    }
    
    /// 取得した画像をメモリ[imageCache]にキャッシュする
    ///
    func cacheImageLocaly(_ imageData:Data,fileName:String) {
        
        imageCache[fileName] = imageData
        
    }
    
    func restoreImageFromCache(_ fileName:String) -> Data? {
        
        return imageCache[fileName]
        
    }
 
    // カレンダー表示で使用するデータ
    func createTableCal() {
        monthlyData = []
        var targetDate = thisMonthList.first?.beginningOfDay ?? Date() // 1日目＝今月の初日
        var weekday:Int = calendar.component(.weekday, from: targetDate)
        var offsetPre:Int = 0
        
        if isStartMonday {
            weekday -= 1
            if weekday < 1 { weekday = 7}
        }
        
        if weekday != 1 { // 日曜日 or 月曜日まで巻き戻す
            for _ in 1 ..< weekday {
                targetDate = dtc.getPriorDate(targetDate)
                offsetPre += 1
            }
        }
        // 先頭から先月分のデータを埋める
        for i in (lastMonthList.count - offsetPre) ..< lastMonthList.count {
            var oneDay = lastMonthList[i]
            oneDay.isThisMonth = false
            monthlyData.append(oneDay)
        }
        // 今月のデータを埋める
        for i in 0 ..< thisMonthList.count {
            var oneDay = thisMonthList[i]
            oneDay.isThisMonth = true
            monthlyData.append(oneDay)
        }
        // 末尾に来月のデータを埋める
        weekday = calendar.component(.weekday, from: nextMonthList.first!.beginningOfDay!)
        if isStartMonday {
            weekday -= 1
            if weekday < 1 { weekday = 7}
        }
        if weekday != 1 { // 土曜日　or 日曜日まで埋める
            for i in 0 ... (7 - weekday)  {
                var oneDay = nextMonthList[i]
                oneDay.isThisMonth = false
                monthlyData.append(oneDay)
            }
        }
        
        print("Create monthlyData complete !",#function, #line)
    }
    

  

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
 
        NCMB.setApplicationKey("57fe7004735efcb4d84cface8df0e26df0eb984ff64c9f27515ed722a735dbb1",
            clientKey: "fd685b6844a1f329d7c5f4f451564b428aa94e9d3b4e1c5772bfe23dffe3675d")
        
        // AdMob
        // Use Firebase library to configure APIs.
        FirebaseApp.configure()
        
        // Initialize the Google Mobile Ads SDK.
        // 本番用
        GADMobileAds.configure(withApplicationID: "ca-app-pub-4045648911133483~3721859067")
        // テスト用
//        GADMobileAds.configure(withApplicationID: "ca-app-pub-3940256099942544~1458002511")


        
        // 課金関係処理 保留
        // デリゲート設定
        OWPurchaseManager.sharedManager().delegate = perchase

        // オブザーバー登録
        SKPaymentQueue.default().add(OWPurchaseManager.sharedManager())

        
        if #available(iOS 10, *) {
            // Notiificationの許可
            // current() メソッドを使用してシングルトンオブジェクトを取得
            let center = UNUserNotificationCenter.current()
            
            // 通知の使用許可をリクエスト
            center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
                // Enable or disable features based on authorization.
            }
            center.delegate = self
        } else {
            //iOS8以降でのデバイストークン取得 Push通知の取得
            let settings = UIUserNotificationSettings(types: [.alert, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        // Remote Notificationは一旦保留
        //application.registerForRemoteNotifications()
        // アプリ起動時のプッシュ通知
        // if let remoteNotification = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] as? NSDictionary {
        //    print("Remote Notification \(remoteNotification)")
        //}
        
        // 記録済みユーザ情報の読み込み
        restoreUserInfo()
        personalUser.isLoggedIn = false
        if perchase.license.basicExpired {
            // レシートのチェック
            print("期限切れ：レシートの再チェック \(#function)[\(#line)]")
            perchase.fetchReceipt()
        }

        
        return true
    }
    
// Notification
    // アプリが foreground の時に通知を受け取った時に呼ばれるメソッド
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("fire! notification ID:\(notification.request.identifier)")
        
        let soundIdRing:SystemSoundID = 1005  // alarm.caf
        AudioServicesPlaySystemSound(soundIdRing)
        
        let alertController = UIAlertController(
            title: "確認",
            message: notification.request.content.body ,
            preferredStyle: .alert)
        
        let defaultAction:UIAlertAction = UIAlertAction(title: "OK",
                                                        style: UIAlertActionStyle.default, handler: nil)
        
        alertController.addAction(defaultAction)
        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications() // 送信済みの全通知を削除
        
        
    }

    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        // アプリ起動中(フォアグラウンド)にLocal通知が届いた場合
        if application.applicationState == .active {
            
            let soundIdRing:SystemSoundID = 1005  // alarm.caf
            AudioServicesPlaySystemSound(soundIdRing)

            let alertController = UIAlertController(
                title: "確認",
                message: notification.alertBody ?? "時間です！" ,
                preferredStyle: .alert)
            
            let defaultAction:UIAlertAction = UIAlertAction(title: "OK",
                style: UIAlertActionStyle.default, handler: nil)
            
            alertController.addAction(defaultAction)
            //let activeView = self.window?.rootViewController?.presentedViewController
            //alertController.popoverPresentationController?.sourceView = activeView?.view
            //alertController.popoverPresentationController?.sourceRect = (activeView?.view.frame)!
            self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
            
            application.cancelAllLocalNotifications() // 通知を削除

        }
    }

// Remote Notificationは一旦保留
/*
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let installation = NCMBInstallation.currentInstallation()
        installation.setDeviceTokenFromData(deviceToken)
        //installation.saveInBackgroundWithBlock(nil) //必要があればBlock内でエラー処理を行う
        installation.saveInBackgroundWithBlock { (error: NSError?) -> Void in
            if let e = error {
                // 端末情報の登録が失敗した場合の処理
                print(e.description)
                if (e.code == 409001){
                    // 失敗した原因がdeviceTokenの重複だった場合
                    self.updateExistInstallation(installation)
                } else {
                    // deviceTokenの重複以外のエラーが返ってきた場合
                }
            }
        }
    }

    // deviceTokenの重複で端末情報の登録に失敗した場合に上書き処理を行う
    func updateExistInstallation(currentInstallation: NCMBInstallation) {
        let installationQuery = NCMBInstallation.query()
        installationQuery.whereKey("deviceToken", equalTo: currentInstallation.deviceToken)
        
        do {
            let searchDevice = try installationQuery.getFirstObject()
            
            currentInstallation.objectId = searchDevice.objectId
            currentInstallation.saveInBackgroundWithBlock { (error: NSError?) -> Void in
                if error == nil {
                    // 端末情報更新に成功したときの処理
                } else {
                    // 端末情報更新に失敗したときの処理
                }
            }
        } catch let searchError as NSError {
            // 端末情報の検索に失敗した場合の処理
            print(searchError.description)
        }
    }
    
    // Push通知が利用不可であればerrorが返ってくる
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        NSLog("#### didFailToRegisterForRemoteNotificationsWithError: " + "\(error.description)")
    }
    
    // 起動中に受け取ったプッシュ通知
    func application(application: UIApplication, didReceiveRemoteNotification userInfo:[NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void){
        print("#### didReceiveRemoteNotification")
        print("User Info \(userInfo)")
    }
*/


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // 課金オブザーバー登録解除　保留
        //SKPaymentQueue.defaultQueue().removeTransactionObserver(OWPurchaseManager.sharedManager())
    }


}

