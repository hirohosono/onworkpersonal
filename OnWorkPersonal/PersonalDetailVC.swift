//
//  PersonalDetailVC.swift
//  OnWork
//
//  Created by 細野浩明 on 2016/08/01.
//  Copyright © 2016年 Hiroaki Hosono. All rights reserved.
//

import UIKit

class PersonalDetailVC: UIViewController {
    // appDelegateの取得
    let appDl:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得
 
    // MARK: - Variable for interface
    // 以下のデータはLoadされるときに渡される
    var onedaysData = workDataOfDay()
    // 現在選択されている勤務先、勤務地、タイムテーブル
    var selectedworkPlace = workPlaceInfo()
    var WPIndex:Int = 0
    var selectedTimeTable = timeTableDef()
    var TTIndex:Int = 0
    var selectedLocation = locationDef()
    var LocIndex:Int = 0
    
    // MARK: - Variable for internal use

    var isEditting:Bool = false
    var isChanged:Bool = false
    var isPast:Bool = false
    var isSaved:Bool = false
    var isHolyday:Bool = false
    var isHolydayOld:Bool = false
   
    var startOrigin:String = ""
    var endOrigin:String = ""
    var timeTableOrigin:String = ""
    var memoOrigin:String = ""
    
    var targetField:UITextField? = nil

    
   
    // MARK: - @IBOutlet
    
    @IBOutlet var vSelf: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbTimeTable: UILabel!
    @IBOutlet weak var lbStartTime: UILabel!
    @IBOutlet weak var txStartTime: UITextField!
    @IBOutlet weak var lbStartLocation: UILabel!
    @IBOutlet weak var lbEndTime: UILabel!
    @IBOutlet weak var txEndTime: UITextField!
    @IBOutlet weak var lbEndLocation: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnShift: UIButton!
    @IBOutlet weak var txMemo: UITextField!
    @IBOutlet weak var btnREWStart: UIButton!
    @IBOutlet weak var btnFFStart: UIButton!
    @IBOutlet weak var btnREWEnd: UIButton!
    @IBOutlet weak var btnFFEnd: UIButton!
    @IBOutlet weak var lbWorkTimeValue: UILabel!
    @IBOutlet weak var lbOvertimeWorkValue: UILabel!
    @IBOutlet weak var lbLateNightWorkValue: UILabel!
    @IBOutlet weak var lbTotalTimeValue: UILabel!
    @IBOutlet weak var sgHolyday: UISegmentedControl!
    @IBOutlet weak var btnEditMemo: UIButton!

    
    // MARK: - @IBAction
    
    @IBAction func sgHolydayChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 { // 出勤
            vSelf.backgroundColor = NC_233_SHIRONERI
            sgHolyday.tintColor = NC_166_TETSU
            sgHolyday.backgroundColor = NC_233_SHIRONERI
            isHolyday = false
        } else { // 休日
            vSelf.backgroundColor = NC_012_SAKURA
            sgHolyday.tintColor = NC_014_KARAKURENAI
            sgHolyday.backgroundColor = NC_012_SAKURA
            isHolyday = true
        }
        onedaysData.isDayOff = isHolyday
        setDisplayData()
        setButtonState(isEditting)
        setSaveButtonState()
    }
    
    @IBAction func btnEditMemoOnClick(_ sender: UIButton) {
        
        let alertController = UIAlertController(
            title: "MEMO",
            message: "メモを記録します",
            preferredStyle: .alert)
        
        let defaultAction:UIAlertAction = UIAlertAction(title: "記録",
                                                        style: UIAlertActionStyle.destructive,
                                                        handler:{
                                                            (action:UIAlertAction!) -> Void in
                                                            // 入力したテキストをコンソールに表示
                                                            let textField = alertController.textFields![0] as UITextField
                                                            self.txMemo.text = textField.text ?? ""
                                                            self.onedaysData.memo = textField.text ?? ""
                                                            self.setSaveButtonState()

        })
        
        let cancelAction = UIAlertAction(
            title: "キャンセル",
            style: .cancel, handler: nil)
        
        alertController.addAction(defaultAction)
        alertController.addAction(cancelAction)
        // UIAlertControllerにtextFieldを追加
        alertController.addTextField { (textField:UITextField!) -> Void in
            textField.text = self.txMemo.text
            textField.placeholder = "メモ"
        }
        alertController.popoverPresentationController?.sourceView = view
        alertController.popoverPresentationController?.sourceRect = sender.frame
        
        present(alertController, animated: true, completion: nil)
        
    }

    @IBAction func btnEditOnClick(_ sender: UIButton) {
        isEditting = true
        txStartTime.text = lbStartTime.text
        txEndTime.text = lbEndTime.text
        setButtonState(isEditting)
    }
    
    @IBAction func btnSaveOnClick(_ sender: UIButton) {
        isEditting = false
        isChanged = false
        setButtonState(isEditting)
        startOrigin = lbStartTime.text ?? ""
        endOrigin = lbEndTime.text ?? ""
        timeTableOrigin = lbTimeTable.text ?? ""
        memoOrigin = txMemo.text ?? ""
        isHolydayOld = isHolyday
        
        if isPast { // 未来の日付の場合は呼び出されないようにガード済み
            onedaysData.isDayOff = isHolyday
            updateDailyLog()
        }
        performSegue(withIdentifier: "unwindToSummary", sender: nil)
        
    }
    
    @IBAction func btnShiftOnClick(_ sender: UIButton) {
        
        let alertController = UIAlertController(
            title: "確認",
            message: "タイムテーブル(シフト名)を選択します",
            preferredStyle: .actionSheet)
        
        for i in 0 ..< appDl.timeTableLists[appDl.WPIndex].count {
            let timeTable = appDl.timeTableLists[appDl.WPIndex][i]
            let defaultAction:UIAlertAction = UIAlertAction(title: timeTable.tableName ,
                                                            style: UIAlertActionStyle.default,
                                                            handler:{
                                                                (action:UIAlertAction!) -> Void in
                                                                self.lbTimeTable.text = action.title
                                                                self.selectedTimeTable = timeTable
                                                                self.TTIndex = i
                                                                self.onedaysData.timeTableID = timeTable.timeTableID
                                                                self.onedaysData.timeTable = timeTable.tableName
                                                                self.appDl.ttw.createTimeTable(timeTable,oneday: self.onedaysData,refresh:true)
            })
            alertController.addAction(defaultAction)
        }
        
        let cancelAction = UIAlertAction(
            title: "キャンセル",
            style: .cancel, handler: nil)
        
        alertController.addAction(cancelAction)
        alertController.popoverPresentationController?.sourceView = view
        alertController.popoverPresentationController?.sourceRect = sender.frame
        present(alertController, animated: true, completion: {
            self.reCalcWorkHours()
            self.setSaveButtonState()
        }
        )
        
        
    }
    
    @IBAction func btnStartREWOnClick(_ sender: UIButton) {
        if lbStartTime.text == "--:--" {
            calcStartTime()
        } else {
            if onedaysData.startTimeOfWork?.compare(onedaysData.beginningOfDay! as Date) == .orderedDescending { // 今日の始まりよりも後
                lbStartTime.text = appDl.cf.updateTimeLabel(lbStartTime.text!,addMinute: -appDl.workPlaces[appDl.WPIndex].unitTime).label
                let unitSec:TimeInterval = -Double(appDl.ttw.unitTime) * 60.0 // sec
                onedaysData.startTimeOfWork = Date(timeInterval: unitSec , since: onedaysData.startTimeOfWork! as Date)
            }
        }
        checkStartLocation()
        reCalcWorkHours()
    }
    
    @IBAction func btnStartFFOnClick(_ sender: UIButton) {
        if lbStartTime.text == "--:--" {
            calcStartTime()
        } else {
            var timeLimit = onedaysData.endingOfDay
            if lbEndTime.text != "--:--" { timeLimit = onedaysData.endTimeOfWork }
            
            if onedaysData.startTimeOfWork?.compare(timeLimit! as Date)  == .orderedAscending { // 今日の終わりまたはEndTimeより前
                lbStartTime.text = appDl.cf.updateTimeLabel(lbStartTime.text!,addMinute: appDl.workPlaces[appDl.WPIndex].unitTime).label
                let unitSec:TimeInterval = Double(appDl.ttw.unitTime) * 60.0 // sec
                onedaysData.startTimeOfWork = Date(timeInterval: unitSec, since: onedaysData.startTimeOfWork! as Date)
            }
        }
        checkStartLocation()
        reCalcWorkHours()
    }
    
    @IBAction func btnEndREWOnClick(_ sender: UIButton) {
        if lbEndTime.text == "--:--" {
            calcEndTime()
        } else {
            var timeLimit = onedaysData.beginningOfDay
            if lbStartTime.text != "--:--" { timeLimit = onedaysData.startTimeOfWork }
            
            if onedaysData.endTimeOfWork?.compare(timeLimit! as Date)  == .orderedDescending { // 今日の始まりまたはStartTimeより後
                lbEndTime.text = appDl.cf.updateTimeLabel(lbEndTime.text!,addMinute: -appDl.workPlaces[appDl.WPIndex].unitTime).label
                let unitSec:TimeInterval = -Double(appDl.ttw.unitTime) * 60.0 // sec
                onedaysData.endTimeOfWork = Date(timeInterval: unitSec, since: onedaysData.endTimeOfWork! as Date)
            }
        }
        checkEndLocation()
        reCalcWorkHours()
    }
    
    @IBAction func btnEndFFOnClick(_ sender: UIButton) {
        if lbEndTime.text == "--:--" {
            calcEndTime()
        } else {
            if onedaysData.endTimeOfWork?.compare(onedaysData.endingOfDay! as Date) == .orderedAscending { // 今日の終わりよりも前
                lbEndTime.text = appDl.cf.updateTimeLabel(lbEndTime.text!,addMinute: appDl.workPlaces[appDl.WPIndex].unitTime).label
                let unitSec:TimeInterval = Double(appDl.ttw.unitTime) * 60.0 // sec
                onedaysData.endTimeOfWork = Date(timeInterval: unitSec, since: onedaysData.endTimeOfWork! as Date)
            }
        }
        checkEndLocation()
        reCalcWorkHours()
    }
    
    @IBAction func StartTimeFieldInEditing(_ sender: UITextField) {
        targetField = sender
        if sender.text! == "--:--" {
            calcStartTime()
        }
        sender.text = lbStartTime.text
        setDatePicker(sender,date: onedaysData.startTimeOfWork! ,mode: .time,interval: appDl.workPlaces[0].unitTime)
    }

    @IBAction func EndTimeFieldInEditing(_ sender: UITextField) {
        targetField = sender
        if sender.text! == "--:--" {
            calcEndTime()
        }
        sender.text = lbEndTime.text
        setDatePicker(sender,date: onedaysData.endTimeOfWork! ,mode: .time,interval: appDl.workPlaces[0].unitTime)
    }

    // MARK: - functions
    
    func setButtonImage() {
        let imgFFNormal : UIImage? = UIImage( named:"FF")
        let imgFFPush : UIImage? = UIImage( named:"FF_Push")
        let imgREWNormal : UIImage? = UIImage( named:"REW")
        let imgREWPush : UIImage? = UIImage( named:"REW_Push")
        let imgEditNormal : UIImage? = UIImage( named:"Edit")
        let imgEditPush : UIImage? = UIImage( named:"Edit_Push")
        let imgSaveNormal : UIImage? = UIImage( named:"Save")
        let imgSavePush : UIImage? = UIImage( named:"Save_Push")
        let imgShiftNormal : UIImage? = UIImage( named:"Shift")
        let imgShiftPush : UIImage? = UIImage( named:"Shift_Push")
        let imgBackNormal : UIImage? = UIImage( named:"Close")
        let imgBackPush : UIImage? = UIImage( named:"Close_Push")
        
        btnREWStart.setImage(imgREWNormal, for: UIControlState())
        btnREWStart.setImage(imgREWPush, for: UIControlState.highlighted)
        btnREWEnd.setImage(imgREWNormal, for: UIControlState())
        btnREWEnd.setImage(imgREWPush, for: UIControlState.highlighted)
        
        btnFFStart.setImage(imgFFNormal, for: UIControlState())
        btnFFStart.setImage(imgFFPush, for: UIControlState.highlighted)
        btnFFEnd.setImage(imgFFNormal, for: UIControlState())
        btnFFEnd.setImage(imgFFPush, for: UIControlState.highlighted)
        
        btnEdit.setImage(imgEditNormal, for: UIControlState())
        btnEdit.setImage(imgEditPush, for: UIControlState.highlighted)
        btnSave.setImage(imgSaveNormal, for: UIControlState())
        btnSave.setImage(imgSavePush, for: UIControlState.highlighted)
        btnShift.setImage(imgShiftNormal, for: UIControlState())
        btnShift.setImage(imgShiftPush, for: UIControlState.highlighted)
        
        btnBack.setImage(imgBackNormal, for: UIControlState())
        btnBack.setImage(imgBackPush, for: UIControlState.highlighted)
    }
    
    func setSaveButtonState() {
        
        isChanged = (startOrigin != lbStartTime.text ?? "") ||
            (endOrigin != lbEndTime.text ?? "") ||
            (timeTableOrigin != lbTimeTable.text ?? "" ) ||
            (memoOrigin != txMemo.text ?? "" ) ||
           (isHolyday != isHolydayOld )
        btnSave.isEnabled = isChanged
    }
    
    func setButtonState(_ isEditting:Bool) {
        if isEditting {
            sgHolyday.isEnabled = true
            btnEdit.isEnabled = false
            btnSave.isEnabled = isChanged
            txMemo.isEnabled = false
            if !isHolyday {
                btnShift.isHidden = false
//                btnREWStart.isHidden = false
//                btnFFStart.isHidden = false
//                btnREWEnd.isHidden = false
//                btnFFEnd.isHidden = false
                txStartTime.isHidden = false
                txEndTime.isHidden = false
                btnEditMemo.isHidden = false
            } else {
                btnShift.isHidden = true
//                btnREWStart.isHidden = true
//                btnFFStart.isHidden = true
//                btnREWEnd.isHidden = true
//                btnFFEnd.isHidden = true
                txStartTime.isHidden = true
                txEndTime.isHidden = true
                btnEditMemo.isHidden = true
            }
        } else {
            sgHolyday.isEnabled = false
            btnEdit.isEnabled = true
            btnSave.isEnabled = false
            btnShift.isHidden = true
//            btnREWStart.isHidden = true
//            btnFFStart.isHidden = true
//            btnREWEnd.isHidden = true
//            btnFFEnd.isHidden = true
            txStartTime.isHidden = true
            txEndTime.isHidden = true
            txMemo.isEnabled = false
            btnEditMemo.isHidden = true
        }
        
    }
    
    func setWorkHours() {
        lbWorkTimeValue.text = appDl.dtc.transMin2Hour(onedaysData.workHours)
        lbOvertimeWorkValue.text = appDl.dtc.transMin2Hour(onedaysData.overtimeWorkHours)
        lbLateNightWorkValue.text = appDl.dtc.transMin2Hour(onedaysData.lateNightWorkHours)
        let totalTime = onedaysData.workHours + onedaysData.overtimeWorkHours
        lbTotalTimeValue.text = appDl.dtc.transMin2Hour(totalTime)
    }
    
    func setDisplayData() {
        let beginingOfToday = appDl.dtc.getDateLine(appDl.workPlaces[appDl.WPIndex].dateLine)
        lbDate.text = appDl.dtc.getDateYYYYMMDD_EEE(onedaysData.beginningOfDay!)
        isHolyday = onedaysData.isDayOff
        
        isPast = (beginingOfToday.compare(onedaysData.beginningOfDay!) != .orderedAscending) // 今日より前か？
        if isPast {
            if isHolyday {
                vSelf.backgroundColor = NC_012_SAKURA
                sgHolyday.tintColor = NC_014_KARAKURENAI
                sgHolyday.backgroundColor = NC_012_SAKURA
                sgHolyday.selectedSegmentIndex = 1
                lbTimeTable.text = "休日"
            } else {
                vSelf.backgroundColor = NC_233_SHIRONERI
                sgHolyday.tintColor = NC_166_TETSU
                sgHolyday.backgroundColor = NC_233_SHIRONERI
                sgHolyday.selectedSegmentIndex = 0
                lbTimeTable.text = selectedTimeTable.tableName
           }
        } else {
            vSelf.backgroundColor = myColorPastelYellow // ここには来ない
        }
        onedaysData.timeTable = selectedTimeTable.tableName
        // Start
        lbStartTime.textColor = UIColor.black
        lbStartLocation.textColor = UIColor.black
        if (onedaysData.startTimeOfWork == nil) || isHolyday {
            lbStartTime.text = "--:--"
            lbStartLocation.text = ""
        } else {
            lbStartTime.text = appDl.dtc.getLocalTimeString(onedaysData.startTimeOfWork)
            lbStartLocation.text = onedaysData.startLocation
            if onedaysData.isStartManually {
                lbStartTime.textColor = UIColor.red
                lbStartLocation.textColor = UIColor.red
                lbStartLocation.text?.append("\n" + onedaysData.startAddress)
            }
        }
        // End
        lbEndTime.textColor = UIColor.black
        lbEndLocation.textColor = UIColor.black
        if (onedaysData.endTimeOfWork == nil) || isHolyday {
            lbEndTime.text = "--:--"
            lbEndLocation.text = ""
        } else {
            lbEndTime.text = appDl.dtc.getLocalTimeString(onedaysData.endTimeOfWork)
            lbEndLocation.text = onedaysData.endLocation
            if onedaysData.isEndManually {
                lbEndTime.textColor = UIColor.red
                lbEndLocation.textColor = UIColor.red
                lbEndLocation.text?.append("\n" + onedaysData.endAddress)
            }
        }
        txMemo.text = onedaysData.memo
        
        setWorkHours()
        
    }
    
    func reCalcWorkHours() {
        
        
        onedaysData = appDl.ttw.setWorkedTime(onedaysData)
        
        setWorkHours()
        
        setSaveButtonState()
    }
    
    func updateDailyLog() {
        
        var noLog: NCMBObject? = nil
        var dayKind:String = ""
        if onedaysData.isDayOff {
            dayKind = dayKindType.DayOff.rawValue
        } else {
            dayKind = dayKindType.DayOn.rawValue
        }

        
        let completion: ((_ dailyLogObj:NCMBObject) -> Void) = { dailyLogObj in
            dailyLogObj.setObject(self.onedaysData.beginningOfDay, forKey: "Date")
            dailyLogObj.setObject(self.appDl.personalUser.IDNo, forKey: "UserID")
            dailyLogObj.setObject("Personal", forKey: "GroupName")
            dailyLogObj.setObject(self.onedaysData.companyName, forKey: "CompanyName")
            dailyLogObj.setObject(self.onedaysData.companyID, forKey: "companyID")
            dailyLogObj.setObject(self.onedaysData.locationID, forKey: "locationID")
            dailyLogObj.setObject(self.onedaysData.timeTableID, forKey: "timeTableID")
            dailyLogObj.setObject(self.onedaysData.timeTable, forKey: "TimeTableName")
            dailyLogObj.setObject(self.onedaysData.startTimeOfWork, forKey: "StartTime")
            dailyLogObj.setObject(self.onedaysData.isStartManually, forKey: "StartManually")
            dailyLogObj.setObject(self.onedaysData.startLocation, forKey: "StartLocation")
            dailyLogObj.setObject(self.onedaysData.endTimeOfWork, forKey: "EndTime")
            dailyLogObj.setObject(self.onedaysData.isEndManually, forKey: "EndManually")
            dailyLogObj.setObject(self.onedaysData.endLocation, forKey: "EndLocation")
            dailyLogObj.setObject(self.onedaysData.workHours, forKey: "WorkHours")
            dailyLogObj.setObject(self.onedaysData.overtimeWorkHours, forKey: "OvertimeWorkHours")
            dailyLogObj.setObject(self.onedaysData.lateNightWorkHours, forKey: "LateNightWorkHours")
            dailyLogObj.setObject(self.onedaysData.isDayOff, forKey: "isDayOff")
            dailyLogObj.setObject(dayKind, forKey: "DayKind")
            dailyLogObj.setObject(self.onedaysData.memo, forKey: "memo")
            
            dailyLogObj.saveInBackground({(error) in
                if error != nil {print("Save error : ",error!)}
            })
        }
        
        isSaved = getDailyLogObject( &noLog ,user: appDl.personalUser ,todaysInfo: onedaysData ,closure: completion)
        print("Logを取得",#function, #line)
        
        
    }

    func calcStartTime() {
        let fixedStartTime = appDl.dtc.getDateLine(selectedTimeTable.startTime,baseDate: onedaysData.beginningOfDay!)
        if lbEndTime.text == "--:--" {
            lbStartTime.text = selectedTimeTable.startTime
            onedaysData.startTimeOfWork = fixedStartTime
        } else {
            onedaysData.startTimeOfWork = (fixedStartTime as NSDate).earlierDate(onedaysData.endTimeOfWork!)
            lbStartTime.text = appDl.dtc.getLocalTimeString(onedaysData.startTimeOfWork)
        }
        
    }
    
    func calcEndTime() {
        var fixedEndTime = appDl.dtc.getDateLine(selectedTimeTable.endTime,baseDate: onedaysData.beginningOfDay!)
        if fixedEndTime.compare(onedaysData.beginningOfDay!) == .orderedAscending { //前日の時刻になってしまったら1日足す
            fixedEndTime = appDl.dtc.getNextDate(fixedEndTime)
        }
        if lbStartTime.text == "--:--" {
            lbEndTime.text = selectedTimeTable.endTime
            onedaysData.endTimeOfWork = fixedEndTime
        } else {
            onedaysData.endTimeOfWork = (fixedEndTime as NSDate).laterDate(onedaysData.startTimeOfWork!)
            lbEndTime.text = appDl.dtc.getLocalTimeString(onedaysData.endTimeOfWork)
        }
        
    }
    
    func checkStartLocation() {
        if onedaysData.startLocation == "" {
            onedaysData.startLocation = "エリア外"
            onedaysData.isStartManually = true
            lbStartTime.textColor = UIColor.red
            lbStartLocation.textColor = UIColor.red
            lbStartLocation.text = "エリア外"
        }
        
    }
    
    func checkEndLocation() {
        if onedaysData.endLocation == "" {
            onedaysData.endLocation = "エリア外"
            onedaysData.isEndManually = true
            lbEndTime.textColor = UIColor.red
            lbEndLocation.textColor = UIColor.red
            lbEndLocation.text = "エリア外"

        }
        
    }
    
    func selectOnedayInfo(_ oneday: inout workDataOfDay) {
        
        oneday.name = appDl.personalUser.name
        oneday.UserID = appDl.personalUser.IDNo
        oneday.groupName = appDl.personalUser.groupName
        
        
        if oneday.companyID == "" {
            selectedworkPlace = appDl.workPlaces[0]
            WPIndex = 0
        } else {
            for i in 0 ..< appDl.workPlaces.count {
                if appDl.workPlaces[i].workPlaceID == oneday.companyID {
                    selectedworkPlace = appDl.workPlaces[i]
                    WPIndex = i
                    break
                }
            }
        }
        oneday.companyID = selectedworkPlace.workPlaceID
        oneday.companyName = selectedworkPlace.workPlaceLabel
        
        if oneday.locationID == "" {
            selectedLocation = appDl.locationLists[WPIndex][0]
            LocIndex = 0
        } else {
            for j in 0 ..< appDl.locationLists[WPIndex].count {
                if appDl.locationLists[WPIndex][j].locationID == oneday.locationID {
                    selectedLocation = appDl.locationLists[WPIndex][j]
                    LocIndex = j
                    break
                }
            }
        }
        oneday.locationID = selectedLocation.locationID
        oneday.location = selectedLocation.LocationName
        
        if oneday.timeTableID == "" {
            selectedTimeTable = appDl.timeTableLists[WPIndex][0]
            TTIndex = 0
        } else {
            for k in 0 ..< appDl.timeTableLists[WPIndex].count {
                if appDl.timeTableLists[WPIndex][k].timeTableID == oneday.timeTableID {
                    selectedTimeTable = appDl.timeTableLists[WPIndex][k]
                    TTIndex = k
                    break
                }
            }
        }
        oneday.timeTableID = selectedTimeTable.timeTableID
        oneday.timeTable = selectedTimeTable.tableName
    
        print("selectedworkPlace :[\(WPIndex)] ",selectedworkPlace.workPlaceLabel,#function, #line)
        print("selectedLocation :[\(LocIndex)] ",selectedLocation.LocationName,#function, #line)
        print("selectedTimeTable :[\(TTIndex)] ",selectedTimeTable.tableName,#function, #line)
        
    }

    // MARK: - DatePicker
    @objc func inputDone() {
        self.view.endEditing(true)
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "HH:mm";
        if targetField != nil {
            targetField!.text = dateFormatter.string(from: sender.date)
            if targetField!.tag == 1 { // Start
                lbStartTime.text = targetField!.text
                onedaysData.startTimeOfWork = sender.date
                checkStartLocation()
            } else { // End
                lbEndTime.text = targetField!.text
                onedaysData.endTimeOfWork = sender.date
                checkEndLocation()
            }
            reCalcWorkHours()

        }
    }
    
    func setDatePicker(_ sender: UITextField,date:Date = Date(),mode:UIDatePickerMode = .dateAndTime,interval:Int = 5) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = mode
        datePickerView.minuteInterval = interval
        datePickerView.date = date
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 35))
        let doneItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.inputDone))
        toolbar.setItems([doneItem], animated: true)
        
        sender.inputView = datePickerView
        sender.inputAccessoryView = toolbar
        targetField = sender
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged(sender: )), for: .valueChanged)
    }

    
    // MARK: - View Main
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonImage()
        setButtonState(isEditting)
        selectOnedayInfo(&onedaysData)
        setDisplayData()
        appDl.ttw.createTimeTable(selectedTimeTable,oneday: onedaysData,refresh: true)
        // 初期データを保存
        startOrigin = lbStartTime.text ?? ""
        endOrigin = lbEndTime.text ?? ""
        timeTableOrigin = lbTimeTable.text ?? ""
        memoOrigin = txMemo.text ?? ""
        isHolydayOld = isHolyday
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "unwindToSummary" {
            let controller = segue.destination as! PersonalSummaryVC
            if isSaved {
                controller.oneDaysDataReturned = self.onedaysData
            } else {
                controller.oneDaysDataReturned = nil 
            }
        }
    }
}
