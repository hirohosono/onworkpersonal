//
//  SettingMenuVC.swift
//  OnWork
//
//  Created by 細野浩明 on 2016/07/21.
//  Copyright © 2016年 Hiroaki Hosono. All rights reserved.
//

import UIKit

class SettingMenuVC: UIViewController {

    //let userDefaults = NSUserDefaults.standardUserDefaults()
    
    var appDl:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得

    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnWorkPlace: UIButton!
    @IBOutlet weak var btnWeeklySchedule: UIButton!
    @IBOutlet weak var btnLicense: UIButton!

    @IBOutlet weak var lbUserInfo: UILabel!

    @IBAction func unwindToSettingMenu(_ segue: UIStoryboardSegue) {
        if lbUserInfo == nil {return}
        if (appDl.personalUser.name ) != "" {
            lbUserInfo.text = appDl.personalUser.name + "(" + appDl.personalUser.nameKana + ")"
        }
        setButtonState()
    }


    @IBAction func btnLoginOnClick(_ sender: UIButton) {
        self.view.endEditing(true)
        setButtonDisable()
        
        if (appDl.personalUser.mail == "")  || ( appDl.personalUser.password == "") {
            self.appDl.personalUser.isRegistered = true
            self.appDl.personalUser.isAuthorized = true
            self.appDl.personalUser.isLoggedIn = false
            setButtonState()
            return
        }
        
        NCMBUser.logInWithMailAddress(inBackground: appDl.personalUser.mail, password: appDl.personalUser.password, block: {(user, error ) -> Void in
            if error != nil { // login fail
                print("Login failed: \(String(describing: error))")
                if !self.appDl.personalUser.isRegistered { // 未認証
                    self.lbUserInfo.text = "未認証:\n" + self.appDl.personalUser.name + "(" + self.appDl.personalUser.nameKana + ")"

                } else {
                    self.lbUserInfo.text = "ログイン失敗:\n" + self.appDl.personalUser.name + "(" + self.appDl.personalUser.nameKana + ")"
                }
                self.appDl.personalUser.isLoggedIn = false
            } else {
                print("Login successed!")
                self.appDl.personalUser.isRegistered = true
                self.appDl.personalUser.isAuthorized = true
                self.appDl.personalUser.isLoggedIn = true
                self.appDl.saveUserInfo()
                self.appDl.personalUser = self.appDl.cf.getPersonalUserInfo(self.appDl.personalUser) //ユーザ情報読み出し

                self.appDl.getUserEnv()

                self.setButtonState()
            }
        })
        
        setButtonState()
    }
    
    @IBAction func btnLogoutOnClick(_ sender: UIButton) {
        self.view.endEditing(true)
        setButtonDisable()
        NCMBUser.logOut()
        print("Logged out!")
        lbUserInfo.text = "ログアウト:\n" + self.appDl.personalUser.name + "(" + self.appDl.personalUser.nameKana + ")"
        appDl.personalUser.isLoggedIn = false
        setButtonState()
    }
    
    
    func setButtonState() {
        //未登録状態の場合は登録ボタンを表示
        if !appDl.personalUser.isRegistered { // 未登録
            print("User isn't registered.")
            lbUserInfo.text = "未登録"
            btnLogin.isHidden = true
            btnLogout.isHidden = true
            btnRegister.isHidden = false
            btnRegister.isEnabled = true
            btnWorkPlace.isEnabled = false
            btnWeeklySchedule.isEnabled = false
            btnLicense.isEnabled = false
        } else if !appDl.personalUser.isAuthorized {  // 未認証
            print("User isn't authorized.")

            lbUserInfo.text = "未認証:\n" + appDl.personalUser.name + "(" + appDl.personalUser.nameKana + ")"
            btnLogin.isHidden = false
            btnLogin.isEnabled = true
            btnLogout.isHidden = false
            btnLogout.isEnabled = false
            btnRegister.isHidden = true
            btnRegister.isEnabled = false
            btnWorkPlace.isEnabled = false
            btnWeeklySchedule.isEnabled = false
            btnLicense.isEnabled = false
        } else if !appDl.personalUser.isLoggedIn { //　未ログイン
            print("User isn't logged in.")

            lbUserInfo.text = "未ログイン:\n" + appDl.personalUser.name + "(" + appDl.personalUser.nameKana + ")"

            btnLogin.isHidden = false
            btnLogin.isEnabled = true
            btnLogout.isHidden = false
            btnLogout.isEnabled = false
            btnRegister.isHidden = false
            btnRegister.isEnabled = true
            btnWorkPlace.isEnabled = false
            btnWeeklySchedule.isEnabled = false
            btnLicense.isEnabled = false
        } else {  // ログイン済み
            print("User is logged in.")

            lbUserInfo.text = "ログイン済:\n" + appDl.personalUser.name + "(" + appDl.personalUser.nameKana + ")"
            btnLogin.isHidden = false
            btnLogin.isEnabled = false
            btnLogout.isHidden = false
            btnLogout.isEnabled = true
            btnRegister.isHidden = true
            btnRegister.isEnabled = false
            btnWorkPlace.isEnabled = true
            btnWeeklySchedule.isEnabled = true
            btnLicense.isEnabled = true
        }
    }

    func setButtonDisable() {
        btnLogin.isHidden = false
        btnLogin.isEnabled = false
        btnLogout.isHidden = false
        btnLogout.isEnabled = false
        btnRegister.isHidden = true
        btnRegister.isEnabled = false
        btnWorkPlace.isEnabled = false
        btnWeeklySchedule.isEnabled = false
        btnLicense.isEnabled = false
    }

    func initializeEnv() {
        if !appDl.isInitialized {
            appDl.getUserEnv()
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeEnv()
        switch appDl.perchase.license.isBasicAvalable() {
        case .none,.expired:
            btnLicense.setImage(#imageLiteral(resourceName: "icoLocked"), for: .normal)
        case .avalable,.permanent:
            btnLicense.setImage(#imageLiteral(resourceName: "icoUnLocked"), for: .normal)
        }

        setButtonState()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("prepareForSegue:",segue.identifier!)
/*
        switch segue.identifier ?? "" {
        case "toPersonalUser":
            let controller = segue.destinationViewController as! PersonalUserVC
            //controller. = selectedCompany
        case "toWeeklySchedule":
            let controller = segue.destinationViewController as! WeeklyPlanVC
            //controller.selectedCompany = selectedCompany
        case "toWorkPlace":
            let controller = segue.destinationViewController as! WorkPlaceVC
            //controller.person = personalUser!
        case "toMonthlySchedule":
            let controller = segue.destinationViewController as! WorkPlanVC
            //controller.selectedCompany = selectedCompany
        default :
            break
        }
 */
    }

}
