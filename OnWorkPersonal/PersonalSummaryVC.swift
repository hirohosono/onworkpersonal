//
//  PersonalSummaryVC.swift
//  OnWork
//
//  Created by 細野浩明 on 2016/07/31.
//  Copyright © 2016年 Hiroaki Hosono. All rights reserved.
//

import UIKit
import MessageUI

class PersonalSummaryVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, MFMailComposeViewControllerDelegate  {

    var appDl:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得

    // MARK: - Variable for interface
    var displayMonth = Date()

    // 戻り値
    var oneDaysDataReturned:workDataOfDay? = nil

    var selectedUser = userInfo()

    // MARK: - Variable for internal use
    let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
    let daysPerWeek: Int = 7
    let cellMargin : CGFloat = 1.0  //セルのマージン
    var cvHeight:CGFloat = 1.0

    var IDlabel:[String:String] = [:]  // ID:Name
    var TTNamelabel:[String:String] = [:]  // TimeTable Name:ID
    var LocNamelabel:[String:String] = [:]  // Location Name:ID

    var monthlySummary = userSummary()
    
    var DATE_BG_COL_PAST:UIColor = .black
    var DATE_TX_COL_PAST:UIColor = .black
    
    var CSVDatafileName = ""
    
    var CSVHeader1:String = "集計開始日,集計終了日,総労働日数,所定労働時間,時間外労働時間,深夜労働時間,総労働時間,概算給与" // タイトル行1
    var CSVHeader2:String = "日付,曜日,シフト,勤務開始時刻,勤務終了時刻,所定労働時間,時間外労働時間,深夜労働時間,メモ" // タイトル行2

    var sumCSV:[String] = []

    
    
    // MARK: - @IBOutlet
    
    @IBOutlet weak var lbMonth: UILabel!

    // summary
    @IBOutlet weak var lbWorkDays: UILabel!
    @IBOutlet weak var lbWorkTime: UILabel!
    @IBOutlet weak var lbOvertimeWork: UILabel!
    @IBOutlet weak var lbLateNightWork: UILabel!
    @IBOutlet weak var lbTotalTime: UILabel!
    @IBOutlet weak var lbWorkDaysValue: UILabel!
    @IBOutlet weak var lbWorkTimeValue: UILabel!
    @IBOutlet weak var lbOvertimeWorkValue: UILabel!
    @IBOutlet weak var lbLateNightWorkValue: UILabel!
    @IBOutlet weak var lbTotalTimeValue: UILabel!
    @IBOutlet weak var lbTotalSaraly: UILabel!
   
    @IBOutlet weak var btnPrint: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnToday: UIButton!
    @IBOutlet weak var btnForward: UIButton!
    @IBOutlet weak var cvCalendar: UICollectionView!
    @IBOutlet weak var btnExport: UIButton!

    // MARK: - @IBAction
    
    @IBAction func unwindToSummary(_ segue: UIStoryboardSegue) {
        if oneDaysDataReturned != nil {
            if let indexPath = self.cvCalendar.indexPathsForSelectedItems?[0] {
                appDl.monthlyData[indexPath.row] = oneDaysDataReturned!
                cvCalendar.reloadData()
                sumMonthlyData()
            }
        }
    }
    
    
    @IBAction func swipeLeft(_ sender: UISwipeGestureRecognizer) {
        goToNextMonth()
    }
    
    @IBAction func btnBackOnClick(_ sender: UIButton) {
        goBackLastMonth()
    }
    
    @IBAction func btnTodayOnClick(_ sender: UIButton) {
        appDl.thisMonthList = []
        appDl.createMonthlyList()
        createCalendar()
        cvCalendar.reloadData()
        sumMonthlyData()
    }
    
    @IBAction func btnForwardOnClick(_ sender: UIButton) {
        goToNextMonth()
    }
    
    @IBAction func swipeRight(_ sender: UISwipeGestureRecognizer) {
        goBackLastMonth()
    }

    
    @IBAction func btnPrintOnClick(_ sender: UIButton) {
        showPrinterPicker()
    }
    
    @IBAction func btnExportOnClick(_ sender: UIButton) {
        if checkLicense() {
            exportCSV()
        }
    }

    // MARK: - functions
    
    func checkLicense() -> Bool {
        switch appDl.perchase.license.isBasicAvalable() {
        case .avalable:
            return true
        case .permanent:
            return true
        case .none:
            // ここで確認ダイアログを出す
            let alert = UIAlertController(
                title: NSLocalizedString("Alert!", tableName: "Message",comment: "注意！"),
                message: NSLocalizedString("buy-license-message", tableName: "Message",value: "Please buy basic license to export. in setting menu.", comment: "エキスポートにはライセンスが必要です。\n設定画面から購入をお願いします。"),
                preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {
                (action:UIAlertAction!) -> Void in
                self.performSegue(withIdentifier: "toLicenseFromSummary", sender: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            return false
        case .expired:
            // ここで確認ダイアログを出す
            let alert = UIAlertController(
                title: NSLocalizedString("Alert!", tableName: "Message",comment: "注意！"),
                message: NSLocalizedString("rebuy-license-message", tableName: "Message",value: "Licence is expired! Please buy basic license to export. in setting menu.", comment: "ベーシックライセンスが期限切れです。エキスポートするにはライセンスの延長が必要です。\n設定画面から購入をお願いします。"),
                preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {
                (action:UIAlertAction!) -> Void in
                self.performSegue(withIdentifier: "toLicenseFromSummary", sender: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            return false
        }
    }

    func exportCSV() {
        if !MFMailComposeViewController.canSendMail() { // メール送信不可
            print("Mail can't be send")
            return
        }

        let mailVC = MFMailComposeViewController()
        let toRecipients:[String] = [appDl.currentMailAddress] //Toのアドレス指定
        
        mailVC.mailComposeDelegate = self
        mailVC.setSubject("OnWork Personal: 勤怠データエクスポート" + appDl.dtc.getDateTodayYYYYMMDD_EEE())
        mailVC.setToRecipients(toRecipients)
        let mailMessage = "勤怠データをCSV形式で添付します。\n　\n -- OnWork Personal --"
        mailVC.setMessageBody(mailMessage, isHTML: false)
        let csvData = outputCSV().data(using: .shiftJIS)
        if csvData != nil {
            mailVC.addAttachmentData(csvData!, mimeType: "text/csv", fileName: CSVDatafileName )
            self.present(mailVC, animated: true, completion:nil)
        } else {
            print("outputCSV convert error")
        }

    }
    
    
    func outputCSV() -> String {
        var DataCSV : String = ""
        if sumCSV.count > 0 {
            DataCSV = sumCSV[0]
        }
        for i in 1 ..< sumCSV.count {
            DataCSV += "\r\n" + sumCSV[i]
        }
        DataCSV += "\r\n"
        return DataCSV
    }

    
    func goToNextMonth(){
        // ここに開始日の判定を入れる　endMonth（最後の月の初日）と比較
        // 来月分は表示するため、翌月の処理を実施　再来月は抑止　表示中の月(thisMonth)がendMonthより前の月なら実行
        if appDl.nextMonthList.last!.beginningOfDay! >= appDl.endMonth {
            print("nextMonthList create skipped !",#function, #line)
            return
        }
        appDl.FFMonthlyList(appDl.workPlaces[appDl.WPIndex])
        createCalendar()
        cvCalendar.reloadData()
        sumMonthlyData()

    }
    
    func goBackLastMonth(){
        // ここに開始日の判定を入れる　startMonth（最初の月の初日）と比較
        // 先月分は表示し、その前は抑止 表示中の月(thisMonth)がstartMonthより後の月なら実行
        if appDl.thisMonthList.first!.beginningOfDay! <= appDl.startMonth {
            print("lastMonthList create skipped !",#function, #line)
            return
        }
        appDl.REWMonthlyList(appDl.workPlaces[appDl.WPIndex])
        createCalendar()
        cvCalendar.reloadData()
        sumMonthlyData()

    }
    
    func createCalendar() {
        appDl.createTableCal()
        lbMonth.text = appDl.dtc.getDateYYYYMM((appDl.monthlyData[7].beginningOfDay)!)
    }
    
    func setThemeColor() {
        self.view.backgroundColor = appDl.themeColor.BASE
        cvCalendar.backgroundColor = appDl.themeColor.CAL_BG
        DATE_BG_COL_PAST = appDl.cf.getLetterColor(NC_173_KAMENOZOKI)!
        DATE_TX_COL_PAST = appDl.cf.getLetterColor(DATE_BG_COL_PAST)!
    }


    
    func showPrinterPicker() {
        // UIPrinterPickerControllerのインスタンス化
        let printerPicker = UIPrinterPickerController(initiallySelectedPrinter: nil)
        
        // UIPrinterPickerControllerをモーダル表示する
        switch traitCollection.userInterfaceIdiom {
        case .phone:
            printerPicker.present(animated: true, completionHandler:
                {
                    [unowned self] printerPickerController, userDidSelect, error in
                    if (error != nil) {
                        // エラー
                        //print("Error : \(error)")
                    } else {
                        // 選択したUIPrinterを取得する
                        if let printer: UIPrinter = printerPickerController.selectedPrinter {
                            //print("Printer's URL : \(printer.URL)")
                            self.printToPrinter(printer: printer)
                        } else {
                            //print("Printer is not selected")
                        }
                    }
                }
            )
            break
        case .pad:
            printerPicker.present(from: self.btnPrint.frame, in: self.view, animated: true , completionHandler: {
                [unowned self] printerPickerController, userDidSelect, error in
                if (error != nil) {
                    // エラー
                    //print("Error : \(error)")
                } else {
                    // 選択したUIPrinterを取得する
                    if let printer: UIPrinter = printerPickerController.selectedPrinter {
                        //print("Printer's URL : \(printer.URL)")
                        self.printToPrinter(printer: printer)
                    } else {
                        //print("Printer is not selected")
                    }
                }
            })
            break
        default:
            break
            
        }
    }
    
    func printToPrinter(printer: UIPrinter) {
        // 印刷してみる
        let printIntaractionController = UIPrintInteractionController.shared
        let info = UIPrintInfo(dictionary: nil)
        info.jobName = "Sample Print"
        info.orientation = .portrait
        printIntaractionController.printInfo = info
        //印刷する内容
        printIntaractionController.printingItem = getScreenShot()
        printIntaractionController.print(to: printer, completionHandler: {
            controller, completed, error in
        })
    }
    
    func getScreenShot() -> UIImage {
        let rect = self.view.bounds
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        let context: CGContext = UIGraphicsGetCurrentContext()!
        self.view.layer.render(in: context)
        let capturedImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return capturedImage
    }


    
    func refreshChildView(){
        // データを作り直す？
        if appDl.isUpdateStamp {
            appDl.thisMonthList = []
            appDl.createMonthlyList()
            createCalendar()
            appDl.isUpdateStamp = false
        }
        
        cvCalendar.reloadData()
        sumMonthlyData()
    }
    
    func sumMonthlyData(){
        let monthlyList = appDl.thisMonthList
        monthlySummary = userSummary()
        monthlySummary.user = selectedUser
        CSVDatafileName = "Export_" + selectedUser.name + "_" + appDl.dtc.getDateToString(monthlyList[0].beginningOfDay, format: "yyyy_MM") + ".csv"
        sumCSV = [CSVHeader1,"",CSVHeader2] // 初期化

        for oneDay in monthlyList {
            var oneDayCSV:String = ""
            if oneDay.isDayOff {
                let workHours:Int = oneDay.workHours + oneDay.overtimeWorkHours
                // 出勤したかどうかの判定はworkHours>０
                if workHours > 0 {
                    monthlySummary.workDaysOnDaysOff += 1
                    monthlySummary.workHoursOnDaysOff += workHours // サマリ上は休日分ける
                }
                oneDayCSV = oneDay.dateLabel + "," + oneDay.weekdayLabel + ",休日,00:00,00:00,0.0,0.0,0.0"
                oneDayCSV += "," + oneDay.memo
                sumCSV += [oneDayCSV]
            } else {
                let workHours:Int = oneDay.workHours + oneDay.overtimeWorkHours
                // 出勤したかどうかの判定はworkHours>０
                if workHours > 0 {
                    monthlySummary.workDays += 1
                    monthlySummary.workHours += oneDay.workHours
                    monthlySummary.overtimeWorkHours += oneDay.overtimeWorkHours
                    oneDayCSV = oneDay.dateLabel + "," + oneDay.weekdayLabel
                    oneDayCSV += "," + oneDay.timeTable
                    oneDayCSV += "," + appDl.dtc.getDateToString(oneDay.startTimeOfWork,format:"HH:mm")
                    oneDayCSV += "," + appDl.dtc.getDateToString(oneDay.endTimeOfWork,format:"HH:mm")
                    oneDayCSV += "," + appDl.dtc.transMin2Hour(oneDay.workHours)
                    oneDayCSV += "," + appDl.dtc.transMin2Hour(oneDay.overtimeWorkHours)
                    oneDayCSV += "," + appDl.dtc.transMin2Hour(oneDay.lateNightWorkHours)
                    oneDayCSV += "," + oneDay.memo
                } else {
                    oneDayCSV = oneDay.dateLabel + "," + oneDay.weekdayLabel + ",--,00:00,00:00,0.0,0.0,0.0"
                    oneDayCSV += ",\"" + oneDay.memo + "\""
                }
                sumCSV += [oneDayCSV]
            }
            monthlySummary.lateNightWorkHours += oneDay.lateNightWorkHours
        }
        
        lbWorkDaysValue.text = (monthlySummary.workDays + monthlySummary.workDaysOnDaysOff).description
        lbWorkTimeValue.text = appDl.dtc.transMin2Hour(monthlySummary.workHours)
        lbOvertimeWorkValue.text = appDl.dtc.transMin2Hour(monthlySummary.overtimeWorkHours + monthlySummary.workHoursOnDaysOff)
        lbLateNightWorkValue.text = appDl.dtc.transMin2Hour(monthlySummary.lateNightWorkHours)
        let totalTime = monthlySummary.workHours + monthlySummary.overtimeWorkHours + monthlySummary.workHoursOnDaysOff
        lbTotalTimeValue.text = appDl.dtc.transMin2Hour(totalTime)

        var totalSaraly:Float = (Float(totalTime)/60.0) * Float(appDl.workPlaces[appDl.WPIndex].hourlyRate)
        totalSaraly = totalSaraly + (Float(monthlySummary.overtimeWorkHours) / 60.0 ) * Float(appDl.workPlaces[appDl.WPIndex].hourlyRate) * ( Float(appDl.workPlaces[appDl.WPIndex].overTimeRate) / 100.0 ) // 残業割り増し
        totalSaraly = totalSaraly + (Float(monthlySummary.lateNightWorkHours) / 60.0 ) * Float(appDl.workPlaces[appDl.WPIndex].hourlyRate) * ( Float(appDl.workPlaces[appDl.WPIndex].midNightRate) / 100.0 ) // 残業割り増し
        // ３桁でカンマ区切り
        let num = NSNumber(value: Int(totalSaraly) as Int)
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        let resultStr = formatter.string(from: num)
        
        lbTotalSaraly.text = "¥ " + (resultStr ?? "0")
        
        var monthlyCSV:String = ""
        monthlyCSV += appDl.dtc.getDateToString(monthlyList.first!.beginningOfDay, format: "yyyy年MM月dd日")
        monthlyCSV += "," + appDl.dtc.getDateToString(monthlyList.last!.beginningOfDay, format: "yyyy年MM月dd日")
        monthlyCSV += "," + lbWorkDaysValue.text!
        monthlyCSV += "," + lbWorkTimeValue.text!
        monthlyCSV += "," + lbOvertimeWorkValue.text!
        monthlyCSV += "," + lbLateNightWorkValue.text!
        monthlyCSV += "," + lbTotalTimeValue.text!
        monthlyCSV += "," + Int(totalSaraly).description

        sumCSV[1] = monthlyCSV

    }
    
    func setIDlabel() {
        IDlabel = [:]
        LocNamelabel = [:]
        TTNamelabel = [:]
        // Location
        for loc in appDl.locationLists[appDl.WPIndex] {
            IDlabel[loc.locationID] = loc.LocationName
            LocNamelabel[loc.LocationName] = loc.locationID
        }
        // TimeTable
        for tt in appDl.timeTableLists[appDl.WPIndex] {
            IDlabel[tt.timeTableID] = tt.tableName
            TTNamelabel[tt.tableName] = tt.timeTableID
        }
    }
    
    func getShiftTime(_ tableID:String) -> (start:String,end:String) {
        for i in 0 ..< appDl.timeTableLists[appDl.WPIndex].count {
            if appDl.timeTableLists[appDl.WPIndex][i].timeTableID == tableID {
                return (appDl.timeTableLists[appDl.WPIndex][i].startTime,appDl.timeTableLists[appDl.WPIndex][i].endTime)
            }
        }
        return ("--:--","--:--")
    }

    // MARK: - MFMailComposeViewControllerDelegate

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result {
        case MFMailComposeResult.cancelled:
            print("Email Send Cancelled")
            break
        case MFMailComposeResult.saved:
            print("Email Saved as a Draft")
            break
        case MFMailComposeResult.sent:
            print("Email Sent Successfully")
            break
        case MFMailComposeResult.failed:
            print("Email Send Failed")
            break
            //default:
            //    break
        }
        
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: - View Main
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cvCalendar.delegate = self
        cvCalendar.dataSource = self
        setThemeColor()
      
        if appDl.personalUser.isLoggedIn {
            lbMonth.text = appDl.dtc.getDateYYYYMM(appDl.today ?? Date()) // 未設定の場合は今日の日付から作る
            appDl.createMonthlyList()
            appDl.createTableCal()
            sumMonthlyData()
            setIDlabel()
            refreshChildView()
            appDl.isUpdateStamp = false
        }

      
    }
    override func viewDidAppear(_ animated: Bool) {
        // Viewに戻るときに表示を更新
        super.viewDidAppear(animated)
        if !appDl.personalUser.isLoggedIn {
            self.tabBarController?.selectedIndex = 3 // Setting Tab
            return
        }
        refreshChildView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        cvHeight = cvCalendar.frame.height
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    
    // MARK: - For CollectionView
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2 //　曜日と日にち
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // Section毎にCellの総数を変える.
        if section == 0 {
            return 7
        } else {
            return appDl.monthlyData.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mainCalCell", for: indexPath) as! PersonalMainCalCell
        if appDl.monthlyData.count <= 0 {
            return cell
        }
        let oneDay = appDl.monthlyData[indexPath.row]
        let now = Date()
        
        // セルを初期化
        cell.vTime.isHidden = true
        cell.lbTriangle.isHidden = true
        cell.lbStart.isHidden = true
        cell.lbEnd.isHidden = true
        cell.lbLocation.isHidden = true
        cell.lbLocation.textColor = DATE_BG_COL_PAST
        cell.icoShiftBase.isHidden = true
        cell.lbShift.isHidden = true
        cell.backgroundColor = appDl.themeColor.CAL_BG_WEEKDAY
        cell.lbDate.textColor = appDl.themeColor.CAL_TX_WEEKDAY
        cell.lbDate.backgroundColor = UIColor.clear
        cell.icoShiftBase.backgroundColor = NC_173_KAMENOZOKI
        cell.lbShift.textColor = NC_194_KUROTSURUBAMI
        cell.lbTriangle.textColor = UIColor.black
        cell.lbStart.textColor = UIColor.black
        cell.lbEnd.textColor = UIColor.black


        //テキスト配置
        if indexPath.section == 0 {
            cell.lbDate.text = oneDay.weekdayLabel
            cell.lbDate.textAlignment = .center
            if appDl.weeklyPlan.isWeekdayOff[oneDay.weekdayNum] {
                cell.backgroundColor = appDl.themeColor.CAL_BG_HOLIDAY
                cell.lbDate.textColor = appDl.themeColor.CAL_TX_HOLIDAY
            } else {
                cell.backgroundColor = appDl.themeColor.CAL_BG_WEEKDAY
                cell.lbDate.textColor = appDl.themeColor.CAL_TX_WEEKDAY
            }
            cell.lbDate.backgroundColor = UIColor.clear
            
            cell.vTime.isHidden = true
            cell.lbTriangle.isHidden = true
            cell.lbStart.isHidden = true
            cell.lbEnd.isHidden = true
            cell.lbLocation.isHidden = true
            cell.icoShiftBase.isHidden = true
            cell.lbShift.isHidden = true
            
        } else {
            cell.lbDate.text = " " + oneDay.dateLabel[oneDay.dateLabel.index(oneDay.dateLabel.endIndex, offsetBy: -2)...].description
            cell.lbDate.textAlignment = .left
            //実績と予定を分ける
            if oneDay.beginningOfDay!.compare(now) == .orderedDescending { // 予定
                //セルの色
                if oneDay.isDayOff {
                    cell.icoShiftBase.isHidden = false
                    cell.lbShift.isHidden = false
                    cell.lbDate.backgroundColor = appDl.themeColor.CAL_BG_HOLIDAY
                    cell.lbDate.textColor = appDl.themeColor.CAL_TX_HOLIDAY
                    cell.icoShiftBase.backgroundColor = appDl.themeColor.CAL_TX_HOLIDAY
                    cell.lbShift.textColor = appDl.themeColor.CAL_BG_HOLIDAY
                    cell.lbShift.text = "休日"
                    cell.lbLocation.text = oneDay.holidayName
                    cell.lbLocation.isHidden = false
                    cell.lbLocation.textColor = appDl.themeColor.CAL_TX_HOLIDAY
                    cell.vTime.isHidden = true
                    cell.lbTriangle.isHidden = true
                    cell.lbStart.text = ""
                    cell.lbEnd.text = ""
                    
                    cell.backgroundColor =  myColorPastelLightPink
                    
                } else {
                    cell.vTime.isHidden = false
                    cell.lbTriangle.isHidden = false
                    cell.lbStart.isHidden = false
                    cell.lbEnd.isHidden = false
                    cell.lbLocation.isHidden = false

                    cell.lbDate.backgroundColor = appDl.themeColor.CAL_BG_WEEKDAY
                    cell.lbDate.textColor = appDl.themeColor.CAL_TX_WEEKDAY
                    let timeTableName:String = oneDay.timeTable
                    if timeTableName == "" {
                        cell.icoShiftBase.isHidden = false
                        cell.lbShift.isHidden = false
                        cell.icoShiftBase.backgroundColor = NC_235_SHIRONEZUMI
                        cell.lbShift.textColor = NC_233_SHIRONERI
                        cell.lbShift.text = "未定"
                    } else {
                        cell.icoShiftBase.isHidden = false
                        cell.lbShift.isHidden = false
                        cell.icoShiftBase.backgroundColor = NC_173_KAMENOZOKI
                        cell.lbShift.textColor = NC_194_KUROTSURUBAMI
                        
                        if timeTableName.count > 2 {
                            cell.lbShift.text = timeTableName[..<timeTableName.index(timeTableName.startIndex, offsetBy: 2)].description
                        } else {
                            cell.lbShift.text = timeTableName
                        }
                    }
                    // 勤務地
                    cell.lbLocation.text = IDlabel[ appDl.monthlyData[indexPath.row].locationID]
                    // 勤務開始時刻 timetableの開始時刻を入れる
                    // 勤務終了時刻 timetableの開始時刻を入れる
                    var TTID = appDl.monthlyData[indexPath.row].timeTableID
                    if TTID == "" {
                        TTID = TTNamelabel[ timeTableName ] ?? ""
                    }
                    let result = getShiftTime(TTID)
                    cell.vTime.isHidden = false
                    cell.lbTriangle.isHidden = false
                    cell.lbStart.textColor = UIColor.gray
                    cell.lbEnd.textColor = UIColor.gray
                    cell.lbStart.text = result.start
                    cell.lbEnd.text = result.end
                    
                    cell.backgroundColor =  NC_233_SHIRONERI
                    
                }
                
            } else { //実績
                //セルの色
                if oneDay.isDayOff {
                    cell.icoShiftBase.isHidden = false
                    cell.lbShift.isHidden = false
                    cell.lbDate.backgroundColor = appDl.themeColor.CAL_BG_HOLIDAY_PAST
                    cell.lbDate.textColor = appDl.themeColor.CAL_TX_HOLIDAY
                    cell.icoShiftBase.backgroundColor = appDl.themeColor.CAL_BG_HOLIDAY
                    cell.lbShift.textColor = appDl.themeColor.CAL_TX_HOLIDAY
                    cell.lbShift.text = "休日"
                    cell.lbLocation.text = oneDay.holidayName
                    cell.lbLocation.isHidden = false
                    cell.lbLocation.textColor = appDl.themeColor.CAL_TX_HOLIDAY
                    cell.vTime.isHidden = true
                    cell.lbTriangle.isHidden = true
                    cell.lbStart.text = ""
                    cell.lbEnd.text = ""
                    
                    cell.backgroundColor =  myColorPastelLightPink
                    
                } else {
                    cell.vTime.isHidden = false
                    cell.lbTriangle.isHidden = false
                    cell.lbStart.isHidden = false
                    cell.lbEnd.isHidden = false
                    cell.lbLocation.isHidden = false

                    cell.lbDate.backgroundColor = DATE_BG_COL_PAST
                    cell.lbDate.textColor = DATE_TX_COL_PAST
                    let timeTableName:String = oneDay.timeTable
                    if timeTableName == "" {
                        cell.icoShiftBase.isHidden = false
                        cell.lbShift.isHidden = false
                        cell.icoShiftBase.backgroundColor = NC_235_SHIRONEZUMI
                        cell.lbShift.textColor = NC_233_SHIRONERI
                        cell.lbShift.text = "なし"
                    } else {
                        cell.icoShiftBase.isHidden = false
                        cell.lbShift.isHidden = false
                        cell.icoShiftBase.backgroundColor = NC_173_KAMENOZOKI
                        cell.lbShift.textColor = NC_194_KUROTSURUBAMI
                        
                        if timeTableName.count > 2 {
                            cell.lbShift.text = timeTableName[..<timeTableName.index(timeTableName.startIndex, offsetBy: 2)].description
                        } else {
                            cell.lbShift.text = timeTableName
                        }
                    }
                    // 勤務地
                    cell.lbLocation.text = IDlabel[ oneDay.locationID]
                    // 出勤時刻
                    cell.lbStart.text = appDl.dtc.getLocalTimeString(oneDay.startTimeOfWork)
                    if oneDay.isStartManually {
                        cell.lbStart.textColor = NC_014_KARAKURENAI
                    } else {
                        cell.lbStart.textColor = UIColor.black
                    }
                    
                    // 退勤時刻
                    cell.lbEnd.text = appDl.dtc.getLocalTimeString(oneDay.endTimeOfWork)
                    if oneDay.isEndManually {
                        cell.lbEnd.textColor = NC_014_KARAKURENAI
                    } else {
                        cell.lbEnd.textColor = UIColor.black
                    }
                    
                    cell.backgroundColor =  NC_233_SHIRONERI
                    
                }
                if calendar.isDate(oneDay.beginningOfDay!, inSameDayAs: Date()) { // 今日と同じ日付
                    cell.lbDate.backgroundColor = NC_014_KARAKURENAI
                    cell.lbDate.textColor = NC_233_SHIRONERI
                    
                }
                
            }
            
            if !oneDay.isThisMonth {
                cell.lbDate.backgroundColor = appDl.themeColor.CAL_BG_OUT_OF_RANGE
                cell.lbDate.textColor = NC_057_KOGECHA
                cell.backgroundColor = appDl.themeColor.CAL_BG_OUT_OF_RANGE
                cell.lbTriangle.textColor = appDl.themeColor.CAL_TX_OUT_OF_RANGE
                cell.lbStart.textColor = appDl.themeColor.CAL_TX_OUT_OF_RANGE
                cell.lbEnd.textColor = appDl.themeColor.CAL_TX_OUT_OF_RANGE
                cell.icoShiftBase.backgroundColor = appDl.themeColor.CAL_TX_OUT_OF_RANGE
            }
            
        }
        return cell
    }
    
    //セルのサイズを設定
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfMargin: CGFloat = 8.0
        let width: CGFloat = (collectionView.frame.width - cellMargin * numberOfMargin) / CGFloat(daysPerWeek)
        let height: CGFloat
        if indexPath.section == 0 {
            height = 20.0
        } else {
            let columns:Int = appDl.monthlyData.count / 7
            height = (cvHeight - 20.0 /*曜日*/ - (cellMargin * CGFloat(columns))) / CGFloat(columns)
        }
        return CGSize(width:width, height:height)
    }
    
    //セルの垂直方向のマージンを設定
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return cellMargin
    }
    
    //セルの水平方向のマージンを設定
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return cellMargin
    }

    // MARK: - Navigation

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        print("shouldPerformSegueWithIdentifier:",identifier)
        if identifier == "toDetail" {
            if let indexPath = cvCalendar.indexPathsForSelectedItems?[0] {
                let now = Date()
                if appDl.monthlyData[indexPath.row].beginningOfDay!.compare(now) == .orderedDescending { // beginningOfDay > now
                    print("Rejected, so it's a future day")
                    return false
                }
            }
        }
        return true
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("prepareForSegue:",segue.identifier!)
        if segue.identifier == "toDetail" {
            if let indexPath = cvCalendar.indexPathsForSelectedItems?[0] {
                let controller = segue.destination as! PersonalDetailVC
                controller.onedaysData = appDl.monthlyData[indexPath.row]
            }
        }
    }
}
