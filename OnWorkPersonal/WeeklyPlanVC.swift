//
//  WeeklyPlanVC.swift
//  OnWork
//
//  Created by 細野浩明 on 2016/07/27.
//  Copyright © 2016年 Hiroaki Hosono. All rights reserved.
//

import UIKit

class WeeklyPlanVC: UIViewController {
 
    var appDl:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得

    @IBOutlet weak var scrWeeklyPlan: UIScrollView!

    // Monday
    @IBOutlet weak var vMonday: UIView!
    @IBOutlet weak var lbMonday: UILabel!
    @IBOutlet weak var swMonday: UISwitch!
    @IBOutlet weak var lbWPMonday: UILabel!
    @IBOutlet weak var sgLocMonday: UISegmentedControl!
    @IBOutlet weak var sgTTMonday: UISegmentedControl!
    // Tuesday
    @IBOutlet weak var vTuesday: UIView!
    @IBOutlet weak var lbTuesday: UILabel!
    @IBOutlet weak var swTuesday: UISwitch!
    @IBOutlet weak var lbWPTuesday: UILabel!
    @IBOutlet weak var sgLocTuesday: UISegmentedControl!
    @IBOutlet weak var sgTTTuesday: UISegmentedControl!
    // Wendesday
    @IBOutlet weak var vWednesday: UIView!
    @IBOutlet weak var lbWednesday: UILabel!
    @IBOutlet weak var swWednesday: UISwitch!
    @IBOutlet weak var lbWPWednesday: UILabel!
    @IBOutlet weak var sgLocWednesday: UISegmentedControl!
    @IBOutlet weak var sgTTWednesday: UISegmentedControl!
    // Thursday
    @IBOutlet weak var vThursday: UIView!
    @IBOutlet weak var lbThursday: UILabel!
    @IBOutlet weak var swThursday: UISwitch!
    @IBOutlet weak var lbWPThursday: UILabel!
    @IBOutlet weak var sgLocThursday: UISegmentedControl!
    @IBOutlet weak var sgTTThursday: UISegmentedControl!
    // Friday
    @IBOutlet weak var vFriday: UIView!
    @IBOutlet weak var lbFriday: UILabel!
    @IBOutlet weak var swFriday: UISwitch!
    @IBOutlet weak var lbWPFriday: UILabel!
    @IBOutlet weak var sgLocFriday: UISegmentedControl!
    @IBOutlet weak var sgTTFriday: UISegmentedControl!
    // Saturday
    @IBOutlet weak var vSaturday: UIView!
    @IBOutlet weak var lbSaturday: UILabel!
    @IBOutlet weak var swSaturday: UISwitch!
    @IBOutlet weak var lbWPSaturday: UILabel!
    @IBOutlet weak var sgLocSaturday: UISegmentedControl!
    @IBOutlet weak var sgTTSaturday: UISegmentedControl!
    // Sunday
    @IBOutlet weak var vSunday: UIView!
    @IBOutlet weak var lbSunday: UILabel!
    @IBOutlet weak var swSunday: UISwitch!
    @IBOutlet weak var lbWPSunday: UILabel!
    @IBOutlet weak var sgLocSunday: UISegmentedControl!
    @IBOutlet weak var sgTTSunday: UISegmentedControl!
    
    @IBAction func swChanged(_ sender: UISwitch) {
        switch sender.tag {
        case 1: // Sunday
            appDl.weeklyPlan.isOn_Sun = sender.isOn
        case 2: // Sunday
            appDl.weeklyPlan.isOn_Mon = sender.isOn
        case 3: // Sunday
            appDl.weeklyPlan.isOn_Tue = sender.isOn
        case 4: // Sunday
            appDl.weeklyPlan.isOn_Wed = sender.isOn
        case 5: // Sunday
            appDl.weeklyPlan.isOn_Thu = sender.isOn
        case 6: // Sunday
            appDl.weeklyPlan.isOn_Fri = sender.isOn
        case 7: // Sunday
            appDl.weeklyPlan.isOn_Sat = sender.isOn
        default:
            return
        }
        dispSw()
    }
    
    @IBAction func btnSaveOnClick(_ sender: BackgroundEnabledButton) {
        //appDl.weeklyPlan.scheduleID =
            
        appDl.weeklyPlan.personID = appDl.personalUser.IDNo
        appDl.weeklyPlan.workPlaceID = appDl.personalUser.companyID
        
        appDl.weeklyPlan.isOn_Mon = swMonday.isOn
        appDl.weeklyPlan.WP_Mon = appDl.workPlaces[appDl.WPIndex].workPlaceID
        appDl.weeklyPlan.Loc_Mon = appDl.locationLists[appDl.WPIndex][sgLocMonday.selectedSegmentIndex].locationID
        appDl.weeklyPlan.TT_Mon = appDl.timeTableLists[appDl.WPIndex][sgTTMonday.selectedSegmentIndex].timeTableID
        appDl.weeklyPlan.isOn_Tue = swTuesday.isOn
        appDl.weeklyPlan.WP_Tue = appDl.workPlaces[appDl.WPIndex].workPlaceID
        appDl.weeklyPlan.Loc_Tue = appDl.locationLists[appDl.WPIndex][sgLocTuesday.selectedSegmentIndex].locationID
        appDl.weeklyPlan.TT_Tue = appDl.timeTableLists[appDl.WPIndex][sgTTTuesday.selectedSegmentIndex].timeTableID
        appDl.weeklyPlan.isOn_Wed = swWednesday.isOn
        appDl.weeklyPlan.WP_Wed = appDl.workPlaces[appDl.WPIndex].workPlaceID
        appDl.weeklyPlan.Loc_Wed = appDl.locationLists[appDl.WPIndex][sgLocWednesday.selectedSegmentIndex].locationID
        appDl.weeklyPlan.TT_Wed = appDl.timeTableLists[appDl.WPIndex][sgTTWednesday.selectedSegmentIndex].timeTableID
        appDl.weeklyPlan.isOn_Thu = swThursday.isOn
        appDl.weeklyPlan.WP_Thu = appDl.workPlaces[appDl.WPIndex].workPlaceID
        appDl.weeklyPlan.Loc_Thu = appDl.locationLists[appDl.WPIndex][sgLocThursday.selectedSegmentIndex].locationID
        appDl.weeklyPlan.TT_Thu = appDl.timeTableLists[appDl.WPIndex][sgTTThursday.selectedSegmentIndex].timeTableID
        appDl.weeklyPlan.isOn_Fri = swFriday.isOn
        appDl.weeklyPlan.WP_Fri = appDl.workPlaces[appDl.WPIndex].workPlaceID
        appDl.weeklyPlan.Loc_Fri = appDl.locationLists[appDl.WPIndex][sgLocFriday.selectedSegmentIndex].locationID
        appDl.weeklyPlan.TT_Fri = appDl.timeTableLists[appDl.WPIndex][sgTTFriday.selectedSegmentIndex].timeTableID
        appDl.weeklyPlan.isOn_Sat = swSaturday.isOn
        appDl.weeklyPlan.WP_Sat = appDl.workPlaces[appDl.WPIndex].workPlaceID
        appDl.weeklyPlan.Loc_Sat = appDl.locationLists[appDl.WPIndex][sgLocSaturday.selectedSegmentIndex].locationID
        appDl.weeklyPlan.TT_Sat = appDl.timeTableLists[appDl.WPIndex][sgTTSaturday.selectedSegmentIndex].timeTableID
        appDl.weeklyPlan.isOn_Sun = swSunday.isOn
        appDl.weeklyPlan.WP_Sun = appDl.workPlaces[appDl.WPIndex].workPlaceID
        appDl.weeklyPlan.Loc_Sun = appDl.locationLists[appDl.WPIndex][sgLocSunday.selectedSegmentIndex].locationID
        appDl.weeklyPlan.TT_Sun = appDl.timeTableLists[appDl.WPIndex][sgTTSunday.selectedSegmentIndex].timeTableID
        
        appDl.weeklyPlan.upDateDayOff()
        updateWeeklySchedule(appDl.weeklyPlan)
        
        performSegue(withIdentifier: "unwindToSettingMenu", sender: nil)
        
    }

    
    func dispSw() {
        // Switch
        if appDl.weeklyPlan.isOn_Mon {
            vMonday.backgroundColor = NC_233_SHIRONERI
            lbMonday.textColor = UIColor.black
            swMonday.isOn = true
        } else {
            vMonday.backgroundColor = NC_012_SAKURA
            lbMonday.textColor = NC_014_KARAKURENAI
            swMonday.isOn = false
        }
        if appDl.weeklyPlan.isOn_Tue {
            vTuesday.backgroundColor = NC_233_SHIRONERI
            lbTuesday.textColor = UIColor.black
            swTuesday.isOn = true
        } else {
            vTuesday.backgroundColor = NC_012_SAKURA
            lbTuesday.textColor = NC_014_KARAKURENAI
            swTuesday.isOn = false
        }
        if appDl.weeklyPlan.isOn_Wed {
            vWednesday.backgroundColor = NC_233_SHIRONERI
            lbWednesday.textColor = UIColor.black
            swWednesday.isOn = true
        } else {
            vWednesday.backgroundColor = NC_012_SAKURA
            lbWednesday.textColor = NC_014_KARAKURENAI
            swWednesday.isOn = false
        }
        if appDl.weeklyPlan.isOn_Thu {
            vThursday.backgroundColor = NC_233_SHIRONERI
            lbThursday.textColor = UIColor.black
            swThursday.isOn = true
        } else {
            vThursday.backgroundColor = NC_012_SAKURA
            lbThursday.textColor = NC_014_KARAKURENAI
            swThursday.isOn = false
        }
        if appDl.weeklyPlan.isOn_Fri {
            vFriday.backgroundColor = NC_233_SHIRONERI
            lbFriday.textColor = UIColor.black
            swFriday.isOn = true
        } else {
            vFriday.backgroundColor = NC_012_SAKURA
            lbFriday.textColor = NC_014_KARAKURENAI
            swFriday.isOn = false
        }
        if appDl.weeklyPlan.isOn_Sat {
            vSaturday.backgroundColor = NC_233_SHIRONERI
            lbSaturday.textColor = UIColor.black
            swSaturday.isOn = true
        } else {
            vSaturday.backgroundColor = NC_012_SAKURA
            lbSaturday.textColor = NC_014_KARAKURENAI
            swSaturday.isOn = false
        }
        if appDl.weeklyPlan.isOn_Sun {
            vSunday.backgroundColor = NC_233_SHIRONERI
            lbSunday.textColor = UIColor.black
            swSunday.isOn = true
        } else {
            vSunday.backgroundColor = NC_012_SAKURA
            lbSunday.textColor = NC_014_KARAKURENAI
            swSunday.isOn = false
        }

    }
    
    
    
    func dispSetting() {
        // WorkPlace
        lbWPMonday.text = appDl.workPlaces[appDl.WPIndex].workPlaceLabel
        lbWPTuesday.text = appDl.workPlaces[appDl.WPIndex].workPlaceLabel
        lbWPWednesday.text = appDl.workPlaces[appDl.WPIndex].workPlaceLabel
        lbWPThursday.text = appDl.workPlaces[appDl.WPIndex].workPlaceLabel
        lbWPFriday.text = appDl.workPlaces[appDl.WPIndex].workPlaceLabel
        lbWPSaturday.text = appDl.workPlaces[appDl.WPIndex].workPlaceLabel
        lbWPSunday.text = appDl.workPlaces[appDl.WPIndex].workPlaceLabel
        
        // Switch
        dispSw()
        // Location 現状登録は３つまで
        var NoLoc = locationDef()
        NoLoc.LocationName = "--"
        var loc = [ NoLoc ,NoLoc ,NoLoc ]
        for i in 0 ..< appDl.locationLists[appDl.WPIndex].count {
            if i >= loc.count {break}
            loc[i] = appDl.locationLists[appDl.WPIndex][i]

        }
        // TimeTable 現状登録は３つまで
        var NoTT = timeTableDef()
        NoTT.tableName = "--"
        var tt = [ NoTT ,NoTT ,NoTT ]
        for i in 0 ..< appDl.timeTableLists[appDl.WPIndex].count {
            if i >= tt.count {
                tt.append(contentsOf: [appDl.timeTableLists[appDl.WPIndex][i]])
            } else {
                tt[i] = appDl.timeTableLists[appDl.WPIndex][i]
            }
        }
        
        for i in 0 ... 2 {
            // Location
            sgLocMonday.setTitle(loc[i].LocationName, forSegmentAt: i)
            sgLocTuesday.setTitle(loc[i].LocationName, forSegmentAt: i)
            sgLocWednesday.setTitle(loc[i].LocationName, forSegmentAt: i)
            sgLocThursday.setTitle(loc[i].LocationName, forSegmentAt: i)
            sgLocFriday.setTitle(loc[i].LocationName, forSegmentAt: i)
            sgLocSaturday.setTitle(loc[i].LocationName, forSegmentAt: i)
            sgLocSunday.setTitle(loc[i].LocationName, forSegmentAt: i)
            if (loc[i].locationID ) == "" {
                sgLocMonday.setEnabled(false, forSegmentAt: i)
                sgLocTuesday.setEnabled(false, forSegmentAt: i)
                sgLocWednesday.setEnabled(false, forSegmentAt: i)
                sgLocThursday.setEnabled(false, forSegmentAt: i)
                sgLocFriday.setEnabled(false, forSegmentAt: i)
                sgLocSaturday.setEnabled(false, forSegmentAt: i)
                sgLocSunday.setEnabled(false, forSegmentAt: i)
            } else {
                if loc[i].locationID == appDl.weeklyPlan.Loc_Mon { sgLocMonday.selectedSegmentIndex = i }
                if loc[i].locationID == appDl.weeklyPlan.Loc_Tue { sgLocTuesday.selectedSegmentIndex = i }
                if loc[i].locationID == appDl.weeklyPlan.Loc_Wed { sgLocWednesday.selectedSegmentIndex = i }
                if loc[i].locationID == appDl.weeklyPlan.Loc_Thu { sgLocThursday.selectedSegmentIndex = i }
                if loc[i].locationID == appDl.weeklyPlan.Loc_Fri { sgLocFriday.selectedSegmentIndex = i }
                if loc[i].locationID == appDl.weeklyPlan.Loc_Sat { sgLocSaturday.selectedSegmentIndex = i }
                if loc[i].locationID == appDl.weeklyPlan.Loc_Sun { sgLocSunday.selectedSegmentIndex = i }
            }
        }
        
        sgTTMonday.removeAllSegments()
        sgTTTuesday.removeAllSegments()
        sgTTWednesday.removeAllSegments()
        sgTTThursday.removeAllSegments()
        sgTTFriday.removeAllSegments()
        sgTTSaturday.removeAllSegments()
        sgTTSunday.removeAllSegments()
        for i in 0 ..< tt.count {
            // TimeTable
            sgTTMonday.insertSegment(withTitle: tt[i].tableName, at: i, animated: false)
            sgTTTuesday.insertSegment(withTitle: tt[i].tableName, at: i, animated: false)
            sgTTWednesday.insertSegment(withTitle: tt[i].tableName, at: i, animated: false)
            sgTTThursday.insertSegment(withTitle: tt[i].tableName, at: i, animated: false)
            sgTTFriday.insertSegment(withTitle: tt[i].tableName, at: i, animated: false)
            sgTTSaturday.insertSegment(withTitle: tt[i].tableName, at: i, animated: false)
            sgTTSunday.insertSegment(withTitle: tt[i].tableName, at: i, animated: false)
            if (tt[i].timeTableID ) == "" {
                sgLocMonday.setEnabled(false, forSegmentAt: i)
                sgLocTuesday.setEnabled(false, forSegmentAt: i)
                sgLocWednesday.setEnabled(false, forSegmentAt: i)
                sgLocThursday.setEnabled(false, forSegmentAt: i)
                sgLocFriday.setEnabled(false, forSegmentAt: i)
                sgLocSaturday.setEnabled(false, forSegmentAt: i)
                sgLocSunday.setEnabled(false, forSegmentAt: i)
            } else {
                if tt[i].timeTableID == appDl.weeklyPlan.TT_Mon { sgTTMonday.selectedSegmentIndex = i }
                if tt[i].timeTableID == appDl.weeklyPlan.TT_Tue { sgTTTuesday.selectedSegmentIndex = i }
                if tt[i].timeTableID == appDl.weeklyPlan.TT_Wed { sgTTWednesday.selectedSegmentIndex = i }
                if tt[i].timeTableID == appDl.weeklyPlan.TT_Thu { sgTTThursday.selectedSegmentIndex = i }
                if tt[i].timeTableID == appDl.weeklyPlan.TT_Fri { sgTTFriday.selectedSegmentIndex = i }
                if tt[i].timeTableID == appDl.weeklyPlan.TT_Sat { sgTTSaturday.selectedSegmentIndex = i }
                if tt[i].timeTableID == appDl.weeklyPlan.TT_Sun { sgTTSunday.selectedSegmentIndex = i }               
            }

        }
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dispSetting()
    }
 
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrWeeklyPlan.contentSize.height = 800.0
        print(scrWeeklyPlan.contentSize)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
