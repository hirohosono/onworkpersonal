//
//  PersonalUserVC.swift
//  OnWork Personal
//
//  Created by 細野浩明 on 2016/07/21. modified from TriaManagerVC
//  Copyright © 2016 Hiroaki Hosono. All rights reserved.
//

import UIKit

class PersonalUserVC: UIViewController, UITextFieldDelegate {
    var appDl:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得

    // MARK: - Variable for interface

    // MARK: - Variable for internal use

    var activeTextField:UITextField! = nil

    // MARK: - @IBOutlet

    @IBOutlet weak var txNameKanji: UITextField!
    @IBOutlet weak var txNameKana: UITextField!
    @IBOutlet weak var txMailAddress: UITextField!
    @IBOutlet weak var txPassword: UITextField!
    @IBOutlet weak var sgSex: UISegmentedControl!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnJoin: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnResetPW: UIButton!

    
    // MARK: - @IBAction

    @IBAction func txInEditting(_ sender: UITextField) {
        activeTextField = sender
    }
    
    @IBAction func checkInput(_ sender: UITextField) {
        activeTextField = nil
        setButtonState()
    }
    
    @IBAction func tapScrennVD(_ sender: UITapGestureRecognizer) {
        print("Trial Manager:Screen tapped")
        self.view.endEditing(true)
    }
    
    @IBAction func btnJoinOnClick(_ sender: UIButton) {
        var messageStr : String = ""
        messageStr += "下記のメールアドレスに確認メールを送信します。\n\n"
        messageStr += txMailAddress.text! + "\n\n"
        messageStr += "確認メールに記載されたリンクをクリックすることで使用可能になります。"
        messageStr += "メールアドレスは正しいですか？"
        
        let alertController = UIAlertController(
            title: "登録確認",
            message: messageStr,
            preferredStyle: .actionSheet)
        
        let defaultAction:UIAlertAction = UIAlertAction(title: "このメールアドレスを登録する",
                                                        style: UIAlertActionStyle.default,
                                                        handler:{
                                                            (action:UIAlertAction!) -> Void in
                                                            self.joinMember()
        })
        
        let cancelAction = UIAlertAction(
            title: "キャンセル",
            style: .cancel, handler: nil)
        
        alertController.addAction(defaultAction)
        alertController.addAction(cancelAction)
        alertController.popoverPresentationController?.sourceView = view
        alertController.popoverPresentationController?.sourceRect = sender.frame
        present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func btnLoginOnClick(_ sender: UIButton) {
        self.view.endEditing(true)
        
        appDl.personalUser.mail = txMailAddress.text ?? ""
        appDl.personalUser.password = txPassword.text ?? ""
        
        NCMBUser.logInWithMailAddress(inBackground: appDl.personalUser.mail, password: appDl.personalUser.password, block: {(user, error ) -> Void in
            if error != nil { // login fail
                print("Login failed: \(String(describing: error))")
                self.appDl.personalUser.isLoggedIn = false
                self.errorMessage(sender)
            } else {
                self.appDl.personalUser.isRegistered = true
                self.appDl.personalUser.isAuthorized = true
                self.appDl.personalUser.isLoggedIn = true
                self.appDl.saveUserInfo()
                self.appDl.personalUser = self.appDl.cf.getPersonalUserInfo(self.appDl.personalUser) //ユーザ情報読み出し
                if self.appDl.personalUser.groupName == "Personal" { // Personalユーザ？
                    self.appDl.getUserEnv()
                    self.returnToMenu()
                } else {
                    print("Login failed: Not Personal user")
                    self.appDl.personalUser.isRegistered = false
                    self.appDl.personalUser.isAuthorized = false
                    self.appDl.personalUser.isLoggedIn = false
                    self.errorMessage(sender)
                }
            }
        })
        
    }
    
    @IBAction func btnResetPWOnClick(_ sender: UIButton) {
        if txMailAddress.text == NOT_AUTH_YET {
            unableResetMail()
            return
        }
        var messageStr : String = ""
        messageStr += "下記のメールアドレス宛にパスワードリセットメールを送信します。\n\n"
        messageStr += txMailAddress.text! + "\n\n"
        messageStr += "確認メールには「仮パスワード」「パスワード変更ページのURL」が記載されています。\n"
        messageStr += "メールを受信した従業員ご本人が「パスワード変更ページのURL」からパスワードの変更が可能です。\n"
        messageStr += "メールアドレスは正しいですか？"
        
        let alertController = UIAlertController(
            title: "パスワードリセットメールの送信確認",
            message: messageStr,
            preferredStyle: .actionSheet)
        
        let defaultAction:UIAlertAction = UIAlertAction(title: "このアドレスにメールを送信する",
                                                        style: UIAlertActionStyle.default,
                                                        handler:{
                                                            (action:UIAlertAction!) -> Void in
                                                            self.sendResetMail(self.txMailAddress.text!)
        })
        
        let cancelAction = UIAlertAction(
            title: "キャンセル",
            style: .cancel, handler: nil)
        
        alertController.addAction(defaultAction)
        alertController.addAction(cancelAction)
        alertController.popoverPresentationController?.sourceView = view
        alertController.popoverPresentationController?.sourceRect = sender.frame
        present(alertController, animated: true, completion: nil)
        
    }
    
    
    // MARK: - functions
    
    func setButtonState() {
        if !appDl.cf.isValidEmail(txMailAddress.text ?? "") {
            txMailAddress.textColor = UIColor.red
            btnJoin.isEnabled = false
            btnLogin.isEnabled = false
            btnResetPW.isEnabled = false
            return
        }
        btnJoin.isEnabled = (txNameKanji.text != "")
                && (txNameKana.text != "")
                && (txPassword.text != "") // 両方入力済みならボタンを押せる
        btnLogin.isEnabled = (txPassword.text != "") // 両方入力済みならボタンを押せる
        btnResetPW.isEnabled = true // 両方入力済みならボタンを押せる

        txMailAddress.textColor = UIColor.black
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
        
    }
    
    
    @objc func keyboardAppeared (_ aNotification:Notification) {
        let navBarHeight = self.navigationController?.navigationBar.frame.size.height ?? 0
        let statusBarHeight: CGFloat = UIApplication.shared.statusBarFrame.height
        var keyOffset:CGFloat = 0.0
        //scrollbase = scrCardView.contentOffset // キーボード表示前のオフセットを記憶
        if let userInfo = (aNotification as NSNotification).userInfo{
            if let keyboard = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue{
                let keyBoardRect = keyboard.cgRectValue
                if activeTextField != nil {
                    let textFieldRect:CGRect? = activeTextField.frame
                    keyOffset = (textFieldRect!.maxY - keyBoardRect.minY)  + statusBarHeight - 10 /* 余白 */
                    //print("Scroll size = \(keyOffset)")
                    // キーボードに隠れない場合は何もしない
                    if (keyOffset + navBarHeight) < 0  {
                        //println("No Scroll size = \(keyOffset + navBarHeight! + statusBarHeight)")
                        return
                    }
                }
            }
        }
        
    }
    
    @objc func keyboardDisappered (_ aNotification:Notification) {
        //scrTrialManagerView.setContentOffset(scrollbase,animated:true) //元のオフセットに戻す
    }

    func returnToMenu() {
        performSegue(withIdentifier: "unwindToSettingMenu", sender: nil)
    }
    
    func restoreUserInfo() {

        txNameKanji.text = appDl.personalUser.name
        txNameKana.text = appDl.personalUser.nameKana
        txMailAddress.text = appDl.personalUser.mail
        txPassword.text = appDl.personalUser.password
        for i in 0 ..< sgSex.numberOfSegments {
            if appDl.personalUser.sex == sgSex.titleForSegment(at: i) {
                sgSex.selectedSegmentIndex = i
                break
            }
        }
        if appDl.personalUser.isRegistered { // 登録済みならログインする
            btnJoin.isEnabled = false
            btnLogin.isEnabled = true
        } else {
            //btnJoin.enabled = true // 登録済みでない場合はデータ入力状態に従う
            btnLogin.isEnabled = false
        }
    }
    
    func joinMember(){
        appDl.personalUser.name = txNameKanji.text!
        appDl.personalUser.nameKana = txNameKana.text!
        appDl.personalUser.sex = sgSex.titleForSegment(at: sgSex.selectedSegmentIndex)!
        appDl.personalUser.mail = txMailAddress.text ?? ""
        appDl.personalUser.password = txPassword.text ?? ""
        if appDl.personalUser.IDNo == "" {
            let dtc = dateTimeControll()
            appDl.personalUser.IDNo = dtc.createUserID("P") // Personal user prefix = "P"
            appDl.personalUser.companyID = appDl.personalUser.IDNo + "-C0"
        }

        
        if !checkUserMail(appDl.personalUser.mail) {
            print("Already Same User Address was used")
            let alertController = UIAlertController(
                title: "確認",
                message: "入力されたメールアドレスは既に登録されています。ログインしてください。",
                preferredStyle: .alert)
            
            let defaultAction:UIAlertAction = UIAlertAction(title: "OK",
                style: UIAlertActionStyle.default, handler: nil)
            
            alertController.addAction(defaultAction)
            alertController.popoverPresentationController?.sourceView = view
            alertController.popoverPresentationController?.sourceRect = CGRect(x: 100.0, y: 100.0, width: 20.0, height: 20.0)
            present(alertController, animated: true, completion: nil)
            txMailAddress.textColor = UIColor.red
            btnLogin.isEnabled = true
            appDl.personalUser.isRegistered = true

            return
        }
        
        // 個人用の会社情報は別管理とする　→ workPlaceを作る
       
        var workPlace = workPlaceInfo()
        workPlace.workPlaceLabel = appDl.personalUser.companyLabel
        workPlace.workPlaceID = appDl.personalUser.companyID
        workPlace.personID = appDl.personalUser.IDNo
       
        addWorkPlace(workPlace)
        
        createPersonalUser(appDl.personalUser)
        
        appDl.personalUser.isRegistered = true
        
        appDl.saveUserInfo()
        
        /*
        // ライセンス情報追加
        var user = userInfo()
        user.UserID = appDl.personalUser.IDNo
        user.companyName = appDl.personalUser.companyName
        user.companyID = appDl.personalUser.companyID
        addLicense(user)
        */
        
        performSegue(withIdentifier: "unwindToSettingMenu", sender: nil)
    
    }
    
    func errorMessage(_ sender: UIButton) {
        var messageStr : String = ""
        messageStr += "メールアドレスとパスワードは正しいですか？\n\n"
        messageStr += "再確認してログインするか、\n新規登録をお願いします"
        
        let alertController = UIAlertController(
            title: "ログインできません",
            message: messageStr,
            preferredStyle: .actionSheet)
        
        let defaultAction:UIAlertAction = UIAlertAction(title: "OK",
                                                        style: UIAlertActionStyle.default,
                                                        handler:nil)
        
        alertController.addAction(defaultAction)
        alertController.popoverPresentationController?.sourceView = view
        alertController.popoverPresentationController?.sourceRect = sender.frame
        present(alertController, animated: true, completion: nil)
       
    }
    func sendResetMail(_ addr:String) {
        NCMBUser.requestPasswordReset(forEmail: txMailAddress.text , error: nil)
        var messageStr : String = ""
        messageStr += "パスワード変更のメールが送信されました。\n"
        messageStr += "メールに従いパスワードを変更して下さい。"
        
        let alertController = UIAlertController(
            title: "パスワードリセットメールの送信確認",
            message: messageStr,
            preferredStyle: .actionSheet)
        
        let defaultAction:UIAlertAction = UIAlertAction(title: "OK",
                                                        style: UIAlertActionStyle.default,
                                                        handler:nil)
        
        alertController.addAction(defaultAction)
        alertController.popoverPresentationController?.sourceView = view
        alertController.popoverPresentationController?.sourceRect = view.frame
        present(alertController, animated: true, completion: nil)
    }
    
    func unableResetMail() {
        var messageStr : String = ""
        messageStr += "認証待ちの状態です。\n"
        messageStr += "認証完了するまでリセットは出来ません。"
        
        let alertController = UIAlertController(
            title: "パスワードリセットメールの送信確認",
            message: messageStr,
            preferredStyle: .actionSheet)
        
        let defaultAction:UIAlertAction = UIAlertAction(title: "OK",
                                                        style: UIAlertActionStyle.default,
                                                        handler:nil)
        
        alertController.addAction(defaultAction)
        alertController.popoverPresentationController?.sourceView = view
        alertController.popoverPresentationController?.sourceRect = view.frame
        present(alertController, animated: true, completion: nil)
        
    }
    
    
    // MARK: - View Main
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imgJoinNormal : UIImage? = UIImage( named:"Join")
        let imgJoinPush : UIImage? = UIImage( named:"Join_Push")
        btnJoin.setImage(imgJoinNormal, for: UIControlState())
        btnJoin.setImage(imgJoinPush, for: UIControlState.highlighted)
        let imgCancelNormal : UIImage? = UIImage( named:"Close")
        let imgCancelPush : UIImage? = UIImage( named:"Close_Push")
        btnCancel.setImage(imgCancelNormal, for: UIControlState())
        btnCancel.setImage(imgCancelPush, for: UIControlState.highlighted)
    

        txMailAddress.delegate = self
        txNameKana.delegate = self
        txNameKanji.delegate = self
        txPassword.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(PersonalUserVC.keyboardAppeared(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PersonalUserVC.keyboardDisappered(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        restoreUserInfo()
        
        btnLogin.isEnabled = appDl.personalUser.isRegistered
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        appDl.saveUserInfo()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("prepareForSegue:",segue.identifier!)
        // MARK: - 変更要

        if segue.identifier == "unwindToSettingMenu" {
            appDl.saveUserInfo()
        }
 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
