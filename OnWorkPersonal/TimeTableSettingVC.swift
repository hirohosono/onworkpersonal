//
//  TimeTableSettingVC.swift
//  OnWork
//
//  Created by 細野浩明 on 2016/07/26.
//  Copyright © 2016年 Hiroaki Hosono. All rights reserved.
//

import UIKit

class TimeTableSettingVC: UIViewController {

    var appDl:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得

    // MARK: - Variable for interface
    var isNewTimeTable:Bool = false
    var selectedTimeTable = timeTableDef()
    var SelectedIndex:Int = 0
 
    // MARK: - Variable for internal use
    var targetField:UITextField? = nil
    var targetPairField:UITextField? = nil
    var fieldPair:[Int:UITextField] = [:]

 
    // MARK: - @IBOutlet
    
    @IBOutlet weak var lbWorkPlaceLabel: UILabel!
    @IBOutlet weak var txTimeTableName: UITextField!
    @IBOutlet weak var lbClippeddName: UILabel!
    @IBOutlet weak var txWorkTimeStart: UITextField!
    @IBOutlet weak var txWorkTimeEnd: UITextField!
    @IBOutlet weak var txReccessTime_1_Start: UITextField!
    @IBOutlet weak var txReccessTime_1_End: UITextField!
    @IBOutlet weak var txReccessTime_2_Start: UITextField!
    @IBOutlet weak var txReccessTime_2_End: UITextField!
    @IBOutlet weak var txReccessTime_3_Start: UITextField!
    @IBOutlet weak var txReccessTime_3_End: UITextField!
    @IBOutlet weak var txReccessTime_4_Start: UITextField!
    @IBOutlet weak var txReccessTime_4_End: UITextField!
    @IBOutlet weak var txReccessTime_5_Start: UITextField!
    @IBOutlet weak var txReccessTime_5_End: UITextField!
    @IBOutlet weak var btnREW: UIButton!
    @IBOutlet weak var btnFF: UIButton!
    @IBOutlet weak var btnErase: UIButton!
    @IBOutlet weak var btnSave: BackgroundEnabledButton!

    // MARK: - @IBAction
    
    @IBAction func dataChanged(_ sender: UITextField) {
        setButtonState()
    }
    
    @IBAction func TimeFieldInEditing(_ sender: UITextField) {
        targetField = sender
        sender.backgroundColor = myColorPastelLightPink
        targetPairField = fieldPair[sender.tag]
        let timeStr:String
        if (sender.text?.isEmpty) ?? true {
            timeStr = "12:00"
            sender.text = timeStr
            if targetPairField?.text?.isEmpty ?? false {
                targetPairField!.text = timeStr
            }
        } else {
            timeStr = sender.text!
            if targetPairField?.text?.isEmpty ?? false {
                targetPairField!.text = sender.text!
            }
        }
        let timeValue:Date = appDl.dtc.getDateLine(timeStr , baseDate: Date())
        setDatePicker(sender,date: timeValue ,mode: .time,interval: appDl.workPlaces[0].unitTime)
    }
    
    @IBAction func TimeFieldOutEditing(_ sender: UITextField) {
        sender.backgroundColor = UIColor.white
    }
    
    @IBAction func btnREWOnClick(_ sender: UIButton) {
        if targetField != nil {
            targetField!.text = appDl.cf.updateTimeLabel(targetField!.text ?? "00:00",addMinute: -15).label
            if (targetField!.tag % 2) == 0 { // 偶数？ Start側のフィールド
                let result = checkTimePair(start:(targetField!.text ?? "00:00") ,end:(targetPairField!.text ?? "") )
                targetPairField!.text = result.end
            } else { // 奇数　End側のフィールド
                let result = checkTimePair(start:(targetPairField!.text)! ,end:(targetField!.text ?? "00:00"),isStartFirst: false )
                targetPairField!.text = result.start
            }
            
        }
        
    }
    @IBAction func btnFFOnClick(_ sender: UIButton) {
        if targetField != nil {
            targetField!.text = appDl.cf.updateTimeLabel(targetField!.text ?? "00:00",addMinute: 15).label
            if (targetField!.tag % 2) == 0 { // 偶数？ Start側のフィールド
                let result = checkTimePair(start:(targetField!.text ?? "00:00") ,end:(targetPairField!.text ?? "") )
                targetPairField!.text = result.end
            } else { // 奇数　End側のフィールド
                let result = checkTimePair(start:(targetPairField!.text)! ,end:(targetField!.text ?? "00:00"),isStartFirst: false )
                targetPairField!.text = result.start
            }
            
        }
    }
    
    @IBAction func btnEraseOnclick(_ sender: UIButton) {
        if targetField != nil {
            if targetField!.tag > 1 { // 開始と終了は削除できない
                targetField!.text = ""
            }
        }
        if targetPairField != nil {
            if targetPairField!.tag > 1 { // 開始と終了は削除できない
                targetPairField!.text = ""
            }
        }
    }
    @IBAction func btnSaveOnclick(_ sender: BackgroundEnabledButton) {
        
        selectedTimeTable.tableName = txTimeTableName.text ?? ""
        
        (selectedTimeTable.startTime,selectedTimeTable.endTime) = checkTimePair(start:(txWorkTimeStart.text ?? "09:00") ,end:(txWorkTimeEnd.text ?? "17:00") )
        (selectedTimeTable.recessStart1,selectedTimeTable.recessEnd1) = checkTimePair(start:(txReccessTime_1_Start.text ?? "") ,end:(txReccessTime_1_End.text ?? "") )
        (selectedTimeTable.recessStart2,selectedTimeTable.recessEnd2) = checkTimePair(start:(txReccessTime_2_Start.text ?? "") ,end:(txReccessTime_2_End.text ?? "") )
        (selectedTimeTable.recessStart3,selectedTimeTable.recessEnd3) = checkTimePair(start:(txReccessTime_3_Start.text ?? "") ,end:(txReccessTime_3_End.text ?? "") )
        (selectedTimeTable.recessStart4,selectedTimeTable.recessEnd4) = checkTimePair(start:(txReccessTime_4_Start.text ?? "") ,end:(txReccessTime_4_End.text ?? "") )
        (selectedTimeTable.recessStart5,selectedTimeTable.recessEnd5) = checkTimePair(start:(txReccessTime_5_Start.text ?? "") ,end:(txReccessTime_5_End.text ?? "") )
        
        selectedTimeTable.companyName = appDl.workPlaces[appDl.WPIndex].workPlaceLabel
        selectedTimeTable.companyID = appDl.workPlaces[appDl.WPIndex].workPlaceID
        if isNewTimeTable {
            var maxID:Int = 0
            for tt in appDl.timeTableLists[appDl.WPIndex] {
                let ei = tt.timeTableID.range(of: "-TT")!.upperBound
                let numStr = tt.timeTableID[ei...].description
                let ttNo:Int = Int(numStr )!
                if ttNo >= maxID { maxID = ttNo }
            }
            maxID = maxID + 1
            
            selectedTimeTable.timeTableID = appDl.workPlaces[appDl.WPIndex].workPlaceID + "-TT" + maxID.description
        }
        
        
        if !updateTimeTable(selectedTimeTable) {
            addTimeTable(selectedTimeTable) // データが見つからない場合は追加する
            appDl.timeTableLists[appDl.WPIndex].append(selectedTimeTable)
        } else {
            appDl.timeTableLists[appDl.WPIndex][SelectedIndex] = selectedTimeTable
        }
        performSegue(withIdentifier: "unwindToTimeTableList", sender: nil)
        
        
    }
   
   
    // MARK: - functions
 
    // MARK: - DatePicker
    @objc func inputDone() {
        self.view.endEditing(true)
    }

    @objc func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "HH:mm";
        if targetField != nil {
            targetField!.text = dateFormatter.string(from: sender.date)
        }
        if (targetField!.tag % 2) == 0 { // 偶数？ Start側のフィールド
            let result = checkTimePair(start:(targetField!.text ?? "00:00") ,end:(targetPairField!.text ?? "") )
            targetPairField!.text = result.end
        } else { // 奇数　End側のフィールド
            let result = checkTimePair(start:(targetPairField!.text)! ,end:(targetField!.text ?? "00:00"),isStartFirst: false )
            targetPairField!.text = result.start
        }

    }
    
    func setDatePicker(_ sender: UITextField,date:Date = Date(),mode:UIDatePickerMode = .dateAndTime,interval:Int = 5) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = mode
        datePickerView.minuteInterval = interval
        datePickerView.date = date
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 0, height: 35))
        let doneItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.inputDone))
        toolbar.setItems([doneItem], animated: true)
        
        sender.inputView = datePickerView
        sender.inputAccessoryView = toolbar
        targetField = sender
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged(sender: )), for: .valueChanged)
    }

    func diaplayCompanyName(){
        lbWorkPlaceLabel.text = appDl.workPlaces[appDl.WPIndex].workPlaceLabel
    }
    
    func displayTimeTableData(_ timeTable :timeTableDef){
        
        txTimeTableName.text = timeTable.tableName
        if timeTable.tableName.count > 2 {
            lbClippeddName.text = String(timeTable.tableName[..<timeTable.tableName.index(timeTable.tableName.startIndex, offsetBy: 2)])
        } else {
            lbClippeddName.text = timeTable.tableName
        }
        txWorkTimeStart.text = timeTable.startTime
        txWorkTimeEnd.text = timeTable.endTime
        txReccessTime_1_Start.text = timeTable.recessStart1
        txReccessTime_1_End.text = timeTable.recessEnd1
        txReccessTime_2_Start.text = timeTable.recessStart2
        txReccessTime_2_End.text = timeTable.recessEnd2
        txReccessTime_3_Start.text = timeTable.recessStart3
        txReccessTime_3_End.text = timeTable.recessEnd3
        txReccessTime_4_Start.text = timeTable.recessStart4
        txReccessTime_4_End.text = timeTable.recessEnd4
        txReccessTime_5_Start.text = timeTable.recessStart5
        txReccessTime_5_End.text = timeTable.recessEnd5
        
    }
    func checkTimePair(start:String, end:String, isStartFirst:Bool = true) -> (start:String,end:String) {
        
        let startTime = appDl.cf.adjustTimeLabel(start)
        let endTime = appDl.cf.adjustTimeLabel(end)
        if (startTime.timeStr == "") || (endTime.timeStr == "") {
            return ("","")
        } else {
            if startTime.timeVal >= endTime.timeVal {
                if isStartFirst {
                    return ( startTime.timeStr,
                             appDl.cf.updateTimeLabel(startTime.timeStr,addMinute: 15).label )
                } else {
                    return ( appDl.cf.updateTimeLabel(endTime.timeStr,addMinute: -15).label,
                             endTime.timeStr )
                }
            } else {
                return ( startTime.timeStr, endTime.timeStr )
            }
        }
    }

    
    func setButtonState() {
        btnSave.isEnabled = false
        if (txTimeTableName.text ?? "") != "" {
            btnSave.isEnabled = true
        }
        
    }

    func setFiledPair() {
        fieldPair[txWorkTimeStart.tag] = txWorkTimeEnd
        fieldPair[txWorkTimeEnd.tag]   = txWorkTimeStart
        fieldPair[txReccessTime_1_Start.tag] = txReccessTime_1_End
        fieldPair[txReccessTime_1_End.tag]   = txReccessTime_1_Start
        fieldPair[txReccessTime_2_Start.tag] = txReccessTime_2_End
        fieldPair[txReccessTime_2_End.tag]   = txReccessTime_2_Start
        fieldPair[txReccessTime_3_Start.tag] = txReccessTime_3_End
        fieldPair[txReccessTime_3_End.tag]   = txReccessTime_3_Start
        fieldPair[txReccessTime_4_Start.tag] = txReccessTime_4_End
        fieldPair[txReccessTime_4_End.tag]   = txReccessTime_4_Start
        fieldPair[txReccessTime_5_Start.tag] = txReccessTime_5_End
        fieldPair[txReccessTime_5_End.tag]   = txReccessTime_5_Start
    }

    // MARK: - View Main
    

    override func loadView() {
        super.loadView()

        displayTimeTableData(selectedTimeTable)
        setButtonState()
            
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setFiledPair()
        diaplayCompanyName()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("prepareForSegue:",segue.identifier!)
/*
        switch segue.identifier ?? "" {
        case "toTimeTableList":
            //let controller = segue.destinationViewController as! TimeTableListTVC
            
        default :
            break
        }
*/
    }

}
