//
//  PersonalMainCalCell.swift
//  OnWork
//
//  Created by 細野浩明 on 2017/07/17.
//  Copyright © 2017年 Hiroaki Hosono. All rights reserved.
//

import UIKit

class PersonalMainCalCell: UICollectionViewCell {
    
    @IBOutlet var lbDate: UILabel!
    @IBOutlet weak var icoShiftBase: UIImageView!
    @IBOutlet weak var lbShift: UILabel!
    @IBOutlet weak var lbLocation: UILabel!
    @IBOutlet weak var lbStart: UILabel!
    @IBOutlet weak var lbTriangle: UILabel!
    @IBOutlet weak var lbEnd: UILabel!
    @IBOutlet weak var vTime: UIView!
   

}
