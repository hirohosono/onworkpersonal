//
//  DataTimeControl.swift
//
//  Created by 細野浩明 on 2015/12/24.
//  Copyright © 2015年 Hiroaki Hosono. All rights reserved.
//

import Foundation
import UIKit

class dateTimeControll {
    let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
    
    /// Return Date string as format String
    ///
    /// - parameter Date : 日付
    /// - returns: String as format String
    func getDateToString(_ date:Date?,format:String,nillStr:String = "")->String {
        if date == nil {return nillStr}
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date!) as String
    }

    /// Return Date of String as "YYYY/MM/DD HH:MM:SS"
    ///
    /// - parameter dateString :  date as String
    /// - returns :  date as Date
    func getDateFromStr(_ dateString:String)->Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        let date = dateFormatter.date(from: dateString)
        return date!
    }

    /// Return Date string as "YYYY年MM月" format
    ///
    /// - parameter Date : 日付
    /// - returns: String as "YYYY年MM月"
    func getDateYYYYMM(_ date:Date)->String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.dateFormat = "yyyy年MM月"
        return dateFormatter.string(from: date) as String
    }
    
    /// Return Date string as "MM/DD" format
    ///
    /// - parameter NSDate : 日付
    /// - returns: tupple date & weekday - String as "MM/DD" & "(月)"
    func getDateMMDD(_ date:Date)->( date:String,weekday:String,weekdayNum:Int) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.dateFormat = "MM/dd"
        let dateString = dateFormatter.string(from: date) as String
        dateFormatter.dateFormat = "EEE"
        let weekdayString = dateFormatter.string(from: date) as String
        let weekday:Int = calendar.component(.weekday, from: date)
        return (dateString,weekdayString,weekday)
    }
    
    /// Return Date string of today as "YYYY年MM月DD日(曜日)" format
    ///
    /// - parameter Nothing:
    /// - returns: String as "YYYY年MM月DD日(曜日)"
    func getDateTodayYYYYMMDD_EEE(_ space:Bool = false)->String {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        if space {
            dateFormatter.dateFormat = "yyyy年 MM月dd日 (EEE)"
        } else {
            dateFormatter.dateFormat = "yyyy年MM月dd日(EEE)"
        }
        return dateFormatter.string(from: now) as String
    }
    
    /// Return Date string of date as "YYYY/MM/DD(曜日)" format
    ///
    /// - parameter Nothing:
    /// - returns: String as "YYYY/MM/DD(曜日)"
    func getDateYYYYMMDD_EEE(_ date:Date)->String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.dateFormat = "yyyy/MM/dd (EEE)"
        return dateFormatter.string(from: date) as String
    }
    
    /// Return Date string of date as "YYYY/MM/DD" format
    ///
    /// - parameter date: NSDate
    /// - returns: String as "YYYY/MM/DD"
    func getDateYYYYMMDD(_ date:Date?)->String {
        if date == nil {return "----/--/--"}
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.dateFormat = "yyyy/MM/dd"
        return dateFormatter.string(from: date!) as String
    }
    
    /// Return Date string of date as "YYYY-MM-DD" format
    ///
    /// - parameter date: NSDate
    /// - returns: String as "YYYY-MM-DD"
    func getDateYYYYMMDDSeparatedByMinus(_ date:Date?)->String {
        if date == nil {return "----------"}
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: date!) as String
    }
    
    /// Return Date string as "yyyy/MM/dd hh:mm:ss" format
    ///
    /// - parameter : date as NSDate
    /// - returns: String as "yyyy/MM/dd hh:mm:ss"
    func getLocalDateTimeString(_ date:Date?)->String {
        if date == nil {return "----/--/-- --:--:--"}
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        return dateFormatter.string(from: date!) as String
    }
    
    /// Return Local Time string as "HH:mm" format
    ///
    /// - parameter : date as NSDate
    /// - returns: String as "HH:mm"
    func getLocalTimeString(_ date:Date?)->String {
        if date == nil { return "--:--" }
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: date!) as String
    }
    
    /// Return Local Time string as "HH:mm:ss" format
    ///
    /// - parameter : date as NSDate
    /// - returns: String as "HH:mm:ss"
    func getLocalTimeStringHHmmss(_ date:Date?)->String {
        if date == nil { return "--:--:--" }
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.dateFormat = "HH:mm:ss"
        return dateFormatter.string(from: date!) as String
    }
    /// Return Local Date & Time string as "yyyy/MM/dd" & "HH:mm:ss" format
    ///
    /// - parameter : date as NSDate
    /// - returns: String as "yyyy/MM/dd" & "HH:mm:ss"
    func getLocalDateTimeStrings(_ date:Date?)->(date:String,time:String) {
        if date == nil { return ("--/--/--","--:--:--") }
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.dateFormat = "yyyy/MM/dd"
        let dateStr = dateFormatter.string(from: date!) as String
        dateFormatter.dateFormat = "HH:mm:ss"
        let time = dateFormatter.string(from: date!) as String
        return (dateStr,time)
    }
    
    /// Return Date as NSDate format with GMT
    ///
    /// - parameter : String as "yyyy/MM/dd hh:mm:ss"
    /// - returns :  date as NSDate
    func getLocalDateTime(_ dateString:String)->Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        let date = dateFormatter.date(from: dateString)
        return date!
    }
    
    /// Return Date as NSDate format with GMT
    ///
    /// - parameter : Time string as "hh:mm"
    /// - returns :  date as NSDate
    func getDateLine(_ timeString:String,baseDate:Date = Date())->Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.dateFormat = "yyyy/MM/dd "
        var dateString =  dateFormatter.string(from: baseDate) as String
        dateString += timeString + ":00"
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        let date = dateFormatter.date(from: dateString)
        return date!
    }
    
    /// Return Prior Date as NSDate format with GMT
    ///
    /// - parameter referenceDate : date as NSDate
    /// - returns :  date as NSDate
    func getPriorDate(_ referenceDate:Date)->Date {
        return Date(timeInterval: -24*60*60, since: referenceDate) // referenceDateから１日前
    }
    /// Return Next Date as NSDate format with GMT
    ///
    /// - parameter referenceDate : date as NSDate
    /// - returns :  date as NSDate
    func getNextDate(_ referenceDate:Date)->Date {
        return Date(timeInterval: 24*60*60, since: referenceDate) // referenceDateから１日後
    }
    
    /// Return Next Date as NSDate & Day String
    ///
    /// - parameter referenceDate : date as NSDate
    /// - returns :  date as NSDate & day String
    func getNextDateEx(_ referenceDate:Date)->(date:Date,dateStr:String) {
        let date = Date(timeInterval: 24*60*60, since: referenceDate) // referenceDateから１日後
        let dateNum = calendar.component(.day, from: date)
        var dateStr:String = "0"
        if dateNum < 10 {
            dateStr += dateNum.description
        } else {
            dateStr = dateNum.description
        }
        return (date,dateStr)
    }
    
    /// Return end day Next Month as NSDate format with GMT
    ///
    /// - parameter referenceDate : date as NSDate
    /// - returns :  date as NSDate
    func getNextMonthEnd(_ referenceDate:Date)->Date {
        var date = getLastDay(referenceDate) // 今月末
        date = getNextDate(date) // 来月１日
        date = getLastDay(date) // 来月末
        return date
    }
    
    /// Return end day Next Year as Date format with GMT
    ///
    /// - parameter referenceDate : date as Date
    /// - returns :  date as NSDate
    func getNextYearEnd(_ referenceDate:Date)->Date {
        var date = getLastDay(referenceDate) // 月末
        date = calendar.date(byAdding: .year, value: 1, to: date)!
        return date
    }
    /// Return end day Next Year as NSDate format with GMT
    ///
    /// - parameter referenceDate : date as NSDate
    /// - returns :  date as NSDate
    func getLastYearStart(_ referenceDate:Date)->Date {
        var date = get1stDay(referenceDate) // 1日
        date = calendar.date(byAdding: .year, value: -1, to: date)!
        return date
    }

    /// Return First Date of month as NSDate format with GMT
    ///
    /// - parameter referenceDate :  date as NSDate
    /// - returns :  date as NSDate
    func get1stDay(_ referenceDate:Date)->Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.dateFormat = "yyyy/MM/"
        var dateString =  dateFormatter.string(from: referenceDate) as String
        dateFormatter.dateFormat = " HH:mm:ss"
        let timeString =  dateFormatter.string(from: referenceDate) as String
        dateString += "01" + timeString
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        let date = dateFormatter.date(from: dateString)
        return date!
        
    }
    
    /// Return Last Date of month as NSDate format with GMT
    ///
    /// - parameter referenceDate :  date as NSDate
    /// - returns :  date as NSDate
    func getLastDay(_ referenceDate:Date)->Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.dateFormat = "yyyy/MM/"
        var dateString =  dateFormatter.string(from: referenceDate) as String
        dateFormatter.dateFormat = " HH:mm:ss"
        let timeString =  dateFormatter.string(from: referenceDate) as String
        let days = calendar.range(of: .day, in: .month, for: referenceDate )
        //dateString += days.length.description + timeString
        let lastDay:String = (days?.count.description)!
        dateString += lastDay + timeString
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        let date = dateFormatter.date(from: dateString)
        return date!
        
    }
    
    /// Return Close Date of month as NSDate format with GMT
    ///
    /// - parameter referenceDate :  date as NSDate
    /// - returns :  date as NSDate
    func getCloseDay(_ referenceDate:Date,closeDay:Int)->Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.dateFormat = "yyyy/MM/"
        var dateString =  dateFormatter.string(from: referenceDate) as String
        dateFormatter.dateFormat = " HH:mm:ss"
        let timeString =  dateFormatter.string(from: referenceDate) as String
        let days = calendar.range(of: .day, in: .month, for: referenceDate )!
        print("日数＝",days.count)
        if days.count < closeDay {
            dateString += days.count.description + timeString
        } else {
            var closeDayString : String = closeDay.description
            if closeDay < 10 {
                closeDayString = "0" + closeDayString
            }
            dateString += closeDayString + timeString
        }
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        let date = dateFormatter.date(from: dateString)
        return date!
        
    }
    
    /// Return hours as "0.00" format String
    ///
    /// - parameter minuteValue : Hours value with minute
    /// - returns : hours as  "0.00" format String
    func transMin2Hour(_ minuteValue:Int)->String {
        let hours:Float = Float(minuteValue)/60.0
        return NSString(format: "%.2f", hours) as String  // 桁丸め小数点以下2桁
    }
    
    /// Return UserID made from Date&Time
    ///
    /// - parameter  : prefix string
    /// - returns :  ID as String of YYMMDDHHMMSS
    func createUserID(_ pre:String = "T")->String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.dateFormat = "yyMMddHHmmss"
        let dateString =  dateFormatter.string(from: Date()) as String
        return pre + dateString
    }
    
    /// get date of next workday
    ///
    /// - parameter today & cal: NSDate & weeklyCalendar & Holidays
    /// - returns: NSDate
    func getNextWorkDay(_ today:Date,cal:weeklyCalendar,holidays:[holidayInfo]) -> Date? {
        let workday:[Bool] = [cal.isDayOffSunday,cal.isDayOffMonday,cal.isDayOffTuesday,cal.isDayOffWednesday,cal.isDayOffThursday,cal.isDayOffFriday,cal.isDayOffSaturday,cal.isDayOffSunday,cal.isDayOffMonday,cal.isDayOffTuesday,cal.isDayOffWednesday,cal.isDayOffThursday,cal.isDayOffFriday,cal.isDayOffSaturday]
        let calender = Calendar.current
        let components = calender.dateComponents([.weekday], from: today)
        let weekday = components.weekday
        var date = getNextDate(today) // 明日
        
        for_days: for i in 0 ... 6 {
            // 1.特定日か？　   → 　Yes:↓,No:→4
            
            for ptr in  0 ..< holidays.count {
                if calendar.isDate(holidays[ptr].date as Date, inSameDayAs: date ) {
                    if holidays[ptr].isOnWork { // 2.特定出勤日→出勤日(Hit)
                        return date
                    } else { // 3.特定休日か→休み(NotHit)
                        date = getNextDate(date)
                        continue for_days
                    }
                }
            }
            // 4.休日の曜日か？　→ 　Yes:休み(NotHit),No:出勤日(Hit)
            if !workday[weekday! + i] {
                return date
            }
            
            // 5.休み(NotHit)なら次の日を判定
            date = getNextDate(date)
            
        }
        
        return nil
    }

    func getStarTimeOfTimeTable(_ timeTableLists:[[timeTableDef]],ttID:String) -> String {
        for i in 0 ..< timeTableLists.count {
            for j in  0 ..< timeTableLists[i].count {
                if timeTableLists[i][j].timeTableID == ttID {
                    return timeTableLists[i][j].startTime
                }
            }
        }
        return "05:00"
    }
    
    /// get Start time of next workday
    ///
    /// - parameter today &thisMonthList & nextMonthList
    /// - returns: TimeTableID as String
    func getNextWorkData(_ today:Date,thisMonthList:[workDataOfDay],nextMonthList:[workDataOfDay]) -> workDataOfDay {
        let tommorow = getNextDate(today)
        

        for i in 0 ..< thisMonthList.count {
            if thisMonthList[i].endingOfDay!.compare(tommorow) != .orderedDescending { // endingOfDay <= tommorow
                print(thisMonthList[i].endingOfDay!.compare(tommorow))
                continue // 今日より前は無視
            }
            if thisMonthList[i].isDayOff {
                continue // 休日は無視
            }
            // 休日でなければ　timeTableID を返す
            return thisMonthList[i]
            
        }
        // 今月もう出勤日がない場合は来月を検索
        for i in 0 ..< nextMonthList.count {
            if nextMonthList[i].isDayOff {
                continue // 休日は無視
            }
            // 休日でなければ　timeTableID を返す
            return nextMonthList[i]
        }
        // 来月も出勤日がない場合は
        return workDataOfDay()

    }
    
}
