//
//  NCMBModel.swift
//  OnWork
//
//  Created by 細野浩明 on 2015/08/28.
//  Copyright © 2015年 Hiroaki Hosono. All rights reserved.
//

import Foundation
import UIKit

/// define
let NOT_AUTH_YET :String = "認証待ち"

/// Trim head & tale space character of String
///
/// - parameter String: target String
/// - returns: Trimed String
func trimSpace(_ str:String)->String {
    let trimedStr:String = str.trimmingCharacters(in: CharacterSet.whitespaces)
    return trimedStr
}

// MARK: - Functions for Location

/// get Nearest Location(WorkPlace) from here
///
/// - parameter companyID: company ID with String
/// - parameter location: Location of here with CLLocation
/// - returns: workPlaceName:WorkPlace Name with String , 
///            workPlaceGeo: Geo with CLLocation , at workplace or not
func getNearestLocationByID(_ companyID:String,location:CLLocation)->(workPlaceName :String, workPlaceGeo:CLLocation,isOnWorkLocation:Bool) {
    
    let geoPoint:NCMBGeoPoint = NCMBGeoPoint(location: location)
    let geoQuery:NCMBQuery = NCMBQuery(className: "Location")
    geoQuery.whereKey("companyID", equalTo: companyID)
    geoQuery.whereKey("geo", nearGeoPoint: geoPoint, withinKilometers: 0.1 ) // 100m以内
    var workPlaceGeo = CLLocation(latitude: 0.0, longitude: 0.0)
    var results:[AnyObject] = []
    do {
        results = try geoQuery.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return ("エリア外",workPlaceGeo,false)
    }
    if results.count > 0 {
        let result = results[0] as? NCMBObject
        let resultGeoPoint:NCMBGeoPoint = result?.object(forKey: "geo") as! NCMBGeoPoint
        workPlaceGeo = CLLocation(latitude: resultGeoPoint.longitude, longitude: resultGeoPoint.longitude)
        return (result?.object(forKey: "LocationName") as? String ?? "エリア外",workPlaceGeo,true)
    } else {
        return ("エリア外",workPlaceGeo,false)
    }
}

/// get Nearest Location(WorkPlace) from here for PersonalUser
///
/// - parameter workPlaceID: workPlace ID with String
/// - parameter location: Location of here with CLLocation
/// - returns: workPlaceName:WorkPlace Name with String ,
///            workPlaceGeo: Geo with CLLocation , at workplace or not
func getNearestLocation(_ workPlaceID:String,location:CLLocation)->(locName :String, locID :String,locGeo:CLLocation,isOnWorkLocation:Bool) {
    
    let geoPoint:NCMBGeoPoint = NCMBGeoPoint(location: location)
    let geoQuery:NCMBQuery = NCMBQuery(className: "Location")
    geoQuery.whereKey("companyID", equalTo: workPlaceID)
    geoQuery.whereKey("geo", nearGeoPoint: geoPoint, withinKilometers: 0.1 ) // 100m以内
    var workPlaceGeo = CLLocation(latitude: 0.0, longitude: 0.0)
    var results:[AnyObject] = []
    do {
        results = try geoQuery.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return ("エリア外","",workPlaceGeo,false)
    }
    if results.count > 0 {
        let result = results[0] as? NCMBObject
        let resultGeoPoint:NCMBGeoPoint = result?.object(forKey: "geo") as! NCMBGeoPoint
        let wpID = result?.object(forKey: "locationID") as? String ?? ""
        workPlaceGeo = CLLocation(latitude: resultGeoPoint.longitude, longitude: resultGeoPoint.longitude)
        return (result?.object(forKey: "LocationName") as? String ?? "エリア外",wpID,workPlaceGeo,true)
    } else {
        return ("エリア外","",workPlaceGeo,false)
    }
}


/// Get Location List
///
/// - parameter companyID: company ID with String
/// - returns: locations as [locationInfo]
func getLocationListAllByID(_ companyID:String) -> [locationInfo] {
    
    var locations:[locationInfo] = []
    
    let query = NCMBQuery(className: "Location")
    query?.whereKey("companyID", equalTo: companyID)
    query?.order(byAscending: "LocationName") // LocationNameで昇順にソート
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return locations
    }
    if results.count > 0 {
        for i in 0 ..< results.count {
            let locObj = results[i] as? NCMBObject
            var location = locationInfo()
            
            location.name = locObj?.object(forKey: "LocationName")  as? String ?? ""
            location.address = locObj?.object(forKey: "Address")  as? String ?? ""
            location.id = locObj?.object(forKey: "locationID")  as? String ?? ""
           let locNCMB = locObj?.object(forKey: "geo")  as! NCMBGeoPoint
            location.geo = locNCMB
            locations += [location]
        }
    }
    // エリア外を追加
    var outOfArea = locationInfo()
    
    outOfArea.name = "エリア外"
    outOfArea.address = ""
    
    locations += [outOfArea]
    
    return locations
    
}

/// Get Location List
///
/// - parameter companyID: company ID with String
/// - returns: locations as [locationDef]
func getLocationListAllwithDef(_ companyID:String) -> [locationDef] {
    
    var locations:[locationDef] = []
    
    let query = NCMBQuery(className: "Location")
    query?.whereKey("companyID", equalTo: companyID)
    query?.order(byAscending: "LocationName") // LocationNameで昇順にソート
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return locations
    }
    if results.count > 0 {
        for i in 0 ..< results.count {
            let locObj = results[i] as? NCMBObject
            var location = locationDef()
            
            location.LocationName = locObj?.object(forKey: "LocationName")  as? String ?? ""
            location.address = locObj?.object(forKey: "Address")  as? String ?? ""
            let locNCMB = locObj?.object(forKey: "geo")  as! NCMBGeoPoint
            location.isUseBeacon = locObj?.object(forKey: "UseBeacon")  as? Bool ?? false
            location.beaconUUID = locObj?.object(forKey: "BeaconUUID")  as? String ?? ""
            location.beaconMajor = locObj?.object(forKey: "BeaconMajor")  as? String ?? ""
            location.beaconMinor = locObj?.object(forKey: "BeaconMinor")  as? String ?? ""
            location.companyID = locObj?.object(forKey: "companyID")  as? String ?? ""
            location.companyName = locObj?.object(forKey: "CompanyName")  as? String ?? ""
            location.locationID = locObj?.object(forKey: "locationID")  as? String ?? ""
            location.geo = locNCMB
            locations += [location]
        }
    }
    
    return locations
    
}

/// add location record to Location Class
///
/// - parameter : locationDef
/// - returns:  nothing
func addLocation(_ location:locationDef) {
    
    let obj = NCMBObject(className: "Location")
    
    obj?.setObject(location.companyName,forKey:"CompanyName")
    obj?.setObject(location.companyID,forKey:"companyID")
    obj?.setObject(location.address, forKey: "Address")
    obj?.setObject(location.LocationName, forKey: "LocationName")
    obj?.setObject(location.geo, forKey: "geo")
    obj?.setObject(location.isUseBeacon, forKey: "UseBeacon")
    obj?.setObject(location.beaconUUID, forKey: "BeaconUUID")
    obj?.setObject(location.beaconMajor, forKey: "BeaconMajor")
    obj?.setObject(location.beaconMinor, forKey: "BeaconMinor")
    obj?.setObject(location.locationID, forKey: "locationID")
    
    obj?.saveInBackground({(error) in
        if error != nil {print("Location data save error : ",error!)}
    })
    
}

/// update location record of Location Class
///
/// - parameter : locationDef
/// - returns:  nothing
func updateLocation(_ location:locationDef) -> Bool {
    
    let query = NCMBQuery(className: "Location")
    query?.whereKey("companyID", equalTo: location.companyID)
    query?.whereKey("locationID", equalTo: location.locationID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return false
    }
    if results.count > 0 {
        let obj = results[0] as! NCMBObject
        
        obj.setObject(location.companyName,forKey:"CompanyName")
        obj.setObject(location.companyID,forKey:"companyID")
        obj.setObject(location.address, forKey: "Address")
        obj.setObject(location.LocationName, forKey: "LocationName")
        obj.setObject(location.geo, forKey: "geo")
        obj.setObject(location.isUseBeacon, forKey: "UseBeacon")
        obj.setObject(location.beaconUUID, forKey: "BeaconUUID")
        obj.setObject(location.beaconMajor, forKey: "BeaconMajor")
        obj.setObject(location.beaconMinor, forKey: "BeaconMinor")
        obj.setObject(location.locationID, forKey: "locationID")
        
        obj.saveInBackground({(error) in
            if error != nil {print("Save error : ",error!)}
        })
        return true
        
    }
    return false
}

/// upgrade location record of Location Class
///
/// - parameter : locationDef
/// - returns:  OK/NG
func upgradeLocation(_ loc:locationDef) -> Bool {
    
    let query = NCMBQuery(className: "Location")
    query?.whereKey("companyID", equalTo: loc.companyID)
    query?.whereKey("LocationName", equalTo: loc.LocationName)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return false
    }
    if results.count > 0 {
        let obj = results[0] as! NCMBObject
        
        obj.setObject(loc.locationID,forKey:"locationID")
        
        obj.saveInBackground({(error) in
            if error != nil {print("Save error : ",error!)}
        })
        
        return true
        
    }
    return false
}

/// remove location record of Location Class
///
/// - parameter : locationDef
/// - returns:  result
func removeLocation(_ location:locationDef) -> Bool {
    
    let query = NCMBQuery(className: "Location")
    query?.whereKey("companyID", equalTo: location.companyID)
    query?.whereKey("locationID", equalTo: location.locationID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return false
    }
    if results.count > 0 {
        let obj = results[0] as! NCMBObject
        
        obj.deleteInBackground({(error) in
            if error != nil {print("Save error : ",error!)}
        })
        return true
    }
    return false
}


// MARK: - Functions for Weekly Calendar

/// get Weekly Calendar of the company
///
/// - parameter companyID: company ID with String
/// - returns: calender object & dictionary
func getWeeklyCalendarByID(_ companyID:String) -> (cal:weeklyCalendar,dict:[Bool]) {
    var weeklyCal = weeklyCalendar()
    var isWeekdayOff:[Bool] = [true,true,true,true,true,true,true,true]
    
    weeklyCal.companyID = companyID
    let query = NCMBQuery(className: "WeeklyCalendar")
    query?.whereKey("companyID", equalTo: companyID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return (weeklyCal,isWeekdayOff)
    }
    if results.count > 0 {
        let result = results[0] as? NCMBObject
        weeklyCal.calendarName = result?.object(forKey: "CalendarName")  as? String ?? ""
        weeklyCal.isDayOffSunday = result?.object(forKey: "Sunday")  as? Bool ?? false
        weeklyCal.isDayOffMonday = result?.object(forKey: "Monday")  as? Bool ?? false
        weeklyCal.isDayOffTuesday = result?.object(forKey: "Tuesday")  as? Bool ?? false
        weeklyCal.isDayOffWednesday = result?.object(forKey: "Wednesday")  as? Bool ?? false
        weeklyCal.isDayOffThursday = result?.object(forKey: "Thursday")  as? Bool ?? false
        weeklyCal.isDayOffFriday = result?.object(forKey: "Friday")  as? Bool ?? false
        weeklyCal.isDayOffSaturday = result?.object(forKey: "Saturday")  as? Bool ?? false
        isWeekdayOff = [true
            ,weeklyCal.isDayOffSunday
            ,weeklyCal.isDayOffMonday
            ,weeklyCal.isDayOffTuesday
            ,weeklyCal.isDayOffWednesday
            ,weeklyCal.isDayOffThursday
            ,weeklyCal.isDayOffFriday
            ,weeklyCal.isDayOffSaturday]
    }
    return (weeklyCal,isWeekdayOff)
    
}

/// add weekly calendar record to WeeklyCalendar Class
///
/// - parameter : weeklyCalendar
/// - returns:  nothing
func addWeeklyCalendar(_ calendar:weeklyCalendar) {
    
    let obj = NCMBObject(className: "WeeklyCalendar")
    
    obj?.setObject(calendar.companyName,forKey:"CompanyName")
    obj?.setObject(calendar.companyID,forKey:"companyID")
    obj?.setObject(calendar.calendarName, forKey: "CalendarName")
    obj?.setObject(calendar.isDayOffMonday, forKey: "Monday")
    obj?.setObject(calendar.isDayOffTuesday, forKey: "Tuesday")
    obj?.setObject(calendar.isDayOffWednesday, forKey: "Wednesday")
    obj?.setObject(calendar.isDayOffThursday, forKey: "Thursday")
    obj?.setObject(calendar.isDayOffFriday, forKey: "Friday")
    obj?.setObject(calendar.isDayOffSaturday, forKey: "Saturday")
    obj?.setObject(calendar.isDayOffSunday, forKey: "Sunday")
    
    obj?.saveInBackground({(error) in
        if error != nil {print("WeeklyCalendar data save error : ",error!)}
    })
    
}

/// update weekly calendar record of WeeklyCalendar Class
///
/// - parameter : companyInfo
/// - returns:  nothing
func updateWeeklyCalendar(_ company:companyInfo,calendar:weeklyCalendar) {
    
    let query = NCMBQuery(className: "WeeklyCalendar")
    query?.whereKey("companyID", equalTo: company.companyID)
    query?.whereKey("CalendarName", equalTo: calendar.calendarName)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return
    }
    if results.count > 0 {
        let obj = results[0] as! NCMBObject
        
        obj.setObject(calendar.isDayOffMonday, forKey: "Monday")
        obj.setObject(calendar.isDayOffTuesday, forKey: "Tuesday")
        obj.setObject(calendar.isDayOffWednesday, forKey: "Wednesday")
        obj.setObject(calendar.isDayOffThursday, forKey: "Thursday")
        obj.setObject(calendar.isDayOffFriday, forKey: "Friday")
        obj.setObject(calendar.isDayOffSaturday, forKey: "Saturday")
        obj.setObject(calendar.isDayOffSunday, forKey: "Sunday")
        
        obj.saveInBackground({(error) in
            if error != nil {print("Save error : ",error!)}
        })
        
    }
}

// MARK: - Functions for Company

/// get Information of the company
///
/// - parameter companyID: company ID with String
/// - returns: companyInfo
func getCompanyInfoByID(_ companyID:String) -> companyInfo {
    var myCompany = companyInfo()
    
    myCompany.companyID = companyID
    let query = NCMBQuery(className: "Company")
    query?.whereKey("companyID", equalTo: companyID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return myCompany
    }
    if results.count > 0 {
        let result = results[0] as? NCMBObject
        myCompany.companyName = result?.object(forKey: "CompanyName")  as? String ?? "T99999"
        myCompany.closeDate = result?.object(forKey: "CloseDate")  as? Int ?? 31
        myCompany.dateLine = result?.object(forKey: "DateLine")  as? String ?? "05:00"
        myCompany.unitTime = result?.object(forKey: "unitTime")  as? Int ?? 15
        myCompany.isUseNationalHolidays = result?.object(forKey: "UseNationalHolidays")  as? Bool ?? true
        myCompany.companyLabel = result?.object(forKey: "CompanyLabel")  as? String ?? "Otameshi"
    }
    return myCompany
    
}

/// get Information List of all companys
///
/// - parameter nothing:
/// - returns: [companyInfo]
func getCompanyList() -> [companyInfo] {
    var companys:[companyInfo] = []
    
    let query = NCMBQuery(className: "Company")
    //query.orderByAscending("companyID") // companyIDで昇順にソート
    query?.order(byDescending: "companyID") // companyIDで降順にソート
    query?.limit = 1000

    
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return companys
    }
    if results.count > 0 {
        for result in results {
            var oneCompany = companyInfo()
            oneCompany.companyID = result.object(forKey: "companyID")  as? String ?? "T99999"
            oneCompany.companyName = result.object(forKey: "CompanyName")  as? String ?? "T99999"
            oneCompany.closeDate = result.object(forKey: "CloseDate")  as? Int ?? 31
            oneCompany.dateLine = result.object(forKey: "DateLine")  as? String ?? "05:00"
            oneCompany.unitTime = result.object(forKey: "unitTime")  as? Int ?? 15
            oneCompany.isUseNationalHolidays = result.object(forKey: "UseNationalHolidays")  as? Bool ?? true
            oneCompany.companyLabel = result.object(forKey: "CompanyLabel")  as? String ?? "Otameshi"
            companys += [oneCompany]
        }
    }
    return companys
    
}

/// check trial User'S company name
///
/// - parameter : conmapy name
/// - returns: true = no same name ,false = already same company name was used
func checkCompanyName(_ companyName:String) -> Bool {
    
    let query = NCMBQuery(className: "Company")
    query?.whereKey("CompanyName", equalTo: companyName)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return false
    }
    if results.count > 0 {
        return false // 同じ社名有り
    }
    // OK:Not Used
    return true
    
}


/// check company count & create ID
///
/// - parameter nothing :
/// - returns:  Company ID
func createCompanyID() -> String {
    
    let query = NCMBQuery(className: "Company")
    var error:NSError? = nil
    let companyCount = query?.countObjects(&error)
    if error != nil {
        print(error.debugDescription)
        return ""
    }
    let companyID:String = "C" + (NSString(format: "%05d", companyCount!) as String)
    return companyID
    
}

/// add company record to Company Class
///
/// - parameter : companyInfo
/// - returns:  nothing
func addCompany(_ company:companyInfo) {
    
    let companyObject = NCMBObject(className: "Company")
    
    companyObject?.setObject(company.companyLabel,forKey:"CompanyLabel")
    companyObject?.setObject(company.companyName,forKey:"CompanyName")
    companyObject?.setObject(company.companyID,forKey:"companyID")
    companyObject?.setObject(company.dateLine,forKey:"DateLine")
    companyObject?.setObject(company.closeDate,forKey:"CloseDate")
    companyObject?.setObject(company.unitTime,forKey:"unitTime")
    companyObject?.setObject(company.isUseNationalHolidays,forKey:"UseNationalHolidays")
    
    companyObject?.saveInBackground({(error) in
        if error != nil {print("Save error : ",error!)}
    })
    
}

/// update Company record of Company Class
///
/// - parameter : companyInfo
/// - returns:  nothing
func updateCompany(_ company:companyInfo) {
    
    let query = NCMBQuery(className: "Company")
    query?.whereKey("companyID", equalTo: company.companyID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return
    }
    if results.count > 0 {
        let obj = results[0] as! NCMBObject
        
        obj.setObject(company.companyLabel,forKey:"CompanyLabel")
        obj.setObject(company.companyName,forKey:"CompanyName")
        obj.setObject(company.dateLine,forKey:"DateLine")
        obj.setObject(company.closeDate,forKey:"CloseDate")
        obj.setObject(company.unitTime,forKey:"unitTime")
        obj.setObject(company.isUseNationalHolidays,forKey:"UseNationalHolidays")
        
        obj.saveInBackground({(error) in
            if error != nil {print("Save error : ",error!)}
        })
        
    }
}

// MARK: - Functions for WorkPlace

/// get Information of the company
///
/// - parameter companyID: company ID with String
/// - returns: companyInfo
func getWorkPlaces(_ personID:String) -> [workPlaceInfo] {
    var workPlaces:[workPlaceInfo] = []
    
    let query = NCMBQuery(className: "WorkPlace")
    query?.whereKey("personID", equalTo: personID)
    query?.order(byAscending: "workPlaceID") // workPlaceIDで昇順にソート
    
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return workPlaces
    }
    if results.count > 0 {
        for result in results {
            var obj = workPlaceInfo()
            obj.workPlaceLabel = result.object(forKey: "workPlaceLabel")  as? String ?? "未設定"
            obj.workPlaceID = result.object(forKey: "workPlaceID")  as? String ?? "未設定"
            obj.closeDate = result.object(forKey: "CloseDate")  as? Int ?? 31
            obj.dateLine = result.object(forKey: "DateLine")  as? String ?? "05:00"
            obj.unitTime = result.object(forKey: "unitTime")  as? Int ?? 15
            obj.isUseNationalHolidays = result.object(forKey: "UseNationalHolidays")  as? Bool ?? false
            obj.personID = result.object(forKey: "personID")  as? String ?? "未設定"
            obj.hourlyRate = result.object(forKey: "hourlyRate")  as? Int ?? 1000
            obj.midNightRate = result.object(forKey: "midNightRate")  as? Int ?? 25
            obj.overTimeRate = result.object(forKey: "overTimeRate")  as? Int ?? 25
           workPlaces += [obj]
        }
    }
    return workPlaces
   
}


/// add workPlace record to WorkPlace Class
///
/// - parameter : workPlaceInfo
/// - returns:  nothing
func addWorkPlace(_ workPlace:workPlaceInfo) {
    
    let wpObject = NCMBObject(className: "WorkPlace")
    
    wpObject?.setObject(workPlace.workPlaceLabel,forKey:"workPlaceLabel")
    wpObject?.setObject(workPlace.workPlaceID,forKey:"workPlaceID")
    wpObject?.setObject(workPlace.dateLine,forKey:"DateLine")
    wpObject?.setObject(workPlace.closeDate,forKey:"CloseDate")
    wpObject?.setObject(workPlace.unitTime,forKey:"unitTime")
    wpObject?.setObject(workPlace.isUseNationalHolidays,forKey:"UseNationalHolidays")
    wpObject?.setObject(workPlace.personID,forKey:"personID")
    wpObject?.setObject(workPlace.hourlyRate,forKey:"hourlyRate")
    wpObject?.setObject(workPlace.midNightRate,forKey:"midNightRate")
    wpObject?.setObject(workPlace.overTimeRate,forKey:"overTimeRate")
    
    wpObject?.saveInBackground({(error) in
        if error != nil {print("Save error : ",error!)}
    })
    
}

/// update workPlace record of Company Class
///
/// - parameter : workPlaceInfo
/// - returns:  nothing
func updateworkPlace(_ workPlace:workPlaceInfo) {
    
    let query = NCMBQuery(className: "WorkPlace")
    query?.whereKey("workPlaceID", equalTo: workPlace.workPlaceID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return
    }
    if results.count > 0 {
        let obj = results[0] as! NCMBObject

        obj.setObject(workPlace.workPlaceLabel,forKey:"workPlaceLabel")
        obj.setObject(workPlace.workPlaceID,forKey:"workPlaceID")
        obj.setObject(workPlace.dateLine,forKey:"DateLine")
        obj.setObject(workPlace.closeDate,forKey:"CloseDate")
        obj.setObject(workPlace.unitTime,forKey:"unitTime")
        obj.setObject(workPlace.isUseNationalHolidays,forKey:"UseNationalHolidays")
        obj.setObject(workPlace.personID,forKey:"personID")
        obj.setObject(workPlace.hourlyRate,forKey:"hourlyRate")
        obj.setObject(workPlace.midNightRate,forKey:"midNightRate")
        obj.setObject(workPlace.overTimeRate,forKey:"overTimeRate")
        
        obj.saveInBackground({(error) in
            if error != nil {print("Save error : ",error!)}
        })
        
    }
}


// MARK: - Functions for Holidays

/// get Holidays in one month
///
/// - parameter : companyID , use public holiday or not, date(from)
/// - returns: holidays object
func getHolidaysByID(_ companyID:String,isUsePublic:Bool,dateFrom:Date) -> [holidayInfo] {
    let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
    var holidays:[holidayInfo] = []
    let dateTo = Date(timeInterval: 92*24*60*60, since: dateFrom) // 3ヶ月後
    
    let localQuery = NCMBQuery(className: "DayOffCalendar")
    localQuery?.whereKey("companyID", equalTo: companyID)
    localQuery?.whereKey("DayOff", greaterThanOrEqualTo: dateFrom)
    localQuery?.whereKey("DayOff", lessThan: dateTo)
    let publicQuery = NCMBQuery(className: "DayOffCalendar")
    publicQuery?.whereKey("isPublic", equalTo: true)
    publicQuery?.whereKey("DayOff", greaterThanOrEqualTo: dateFrom)
    publicQuery?.whereKey("DayOff", lessThan: dateTo)
    let query:NCMBQuery
    if isUsePublic {
        let querys:[NCMBQuery] = [localQuery!,publicQuery!]
        query = NCMBQuery.orQuery(withSubqueries: querys)
        query.order(byAscending: "DayOff") // Dateで昇順にソート
    } else {
        query = localQuery!
    }
    var results:[AnyObject] = []
    do {
        results = try query.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return holidays
    }
    if results.count > 0 {
        for i in 0 ..< results.count {
            let holidayObj = results[i] as? NCMBObject
            var holiday = holidayInfo()
            
            holiday.date = holidayObj?.object(forKey: "DayOff")  as! Date
            holiday.holidayName = holidayObj?.object(forKey: "Name")  as? String ?? ""
            holiday.memo = holidayObj?.object(forKey: "Memo")  as? String ?? ""
            holiday.isPublic = holidayObj?.object(forKey: "isPublic")  as? Bool ?? false
            holiday.isOnWork = holidayObj?.object(forKey: "isOnWork")  as? Bool ?? false
            if holidays.count > 0 {
                if calendar.isDate(holiday.date, inSameDayAs: (holidays.last?.date)!) { // 前のデータと同じ日付
                    if holiday.isPublic {
                        // 公休は優先順位低いので捨てる
                    } else {
                        // 独自設定はあと勝ちで上書き
                        holidays.removeLast()
                        holidays += [holiday]
                    }
                } else { //　違う日付は追加
                    holidays += [holiday]
                }
            } else { // 最初の一つは無条件に追加
                holidays += [holiday]
            }
        }
        
    }
    return holidays
    
}

/// get Holidays of selected company
///
/// - parameter : companyID
/// - returns: holidays object array as [holidayDef]
func getCompanyHolidays(_ companyID:String) -> [holidayDef] {
    var holidays:[holidayDef] = []
    
    let query = NCMBQuery(className: "DayOffCalendar")
    query?.whereKey("companyID", equalTo: companyID)
    query?.whereKey("isPublic", equalTo: false)
    query?.order(byAscending: "DayOff") // Dateで昇順にソート
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return holidays
    }
    if results.count > 0 {
        for i in 0 ..< results.count {
        //for var i=0 ; i < results.count; i++ {
            let holidayObj = results[i] as? NCMBObject
            var holiday = holidayDef()
            
            holiday.date = holidayObj?.object(forKey: "DayOff")  as! Date
            holiday.companyName = holidayObj?.object(forKey: "CompanyName")  as? String ?? ""
            holiday.companyID = holidayObj?.object(forKey: "companyID")  as? String ?? ""
            holiday.calendarName = holidayObj?.object(forKey: "CalendarName")  as? String ?? ""
            holiday.holidayName = holidayObj?.object(forKey: "Name")  as? String ?? ""
            holiday.memo = holidayObj?.object(forKey: "Memo")  as? String ?? ""
            holiday.isPublic = holidayObj?.object(forKey: "isPublic")  as? Bool ?? false
            holiday.isOnWork = holidayObj?.object(forKey: "isOnWork")  as? Bool ?? false
            holidays += [holiday]
        }
        
    }
    return holidays
    
}

/// add holiday record to Holiday Class
///
/// - parameter : holidayDef
/// - returns:  nothing
func addHoliday(_ holiday:holidayDef) {
    
    let obj = NCMBObject(className: "DayOffCalendar")
    
    obj?.setObject(holiday.companyName,forKey:"CompanyName")
    obj?.setObject(holiday.companyID,forKey:"companyID")
    obj?.setObject(holiday.calendarName, forKey: "CalendarName")
    obj?.setObject(holiday.date, forKey: "DayOff")
    obj?.setObject(holiday.holidayName, forKey: "Name")
    obj?.setObject(holiday.memo, forKey: "Memo")
    obj?.setObject(holiday.isPublic, forKey: "isPublic")
    obj?.setObject(holiday.isOnWork, forKey: "isOnWork")
    
    obj?.saveInBackground({(error) in
        if error != nil {print("Holiday data save error : ",error!)}
    })
    
}

/// update holiday record of DayOffCalendar Class
///
/// - parameter : holidayDef
/// - returns:  nothing
func updateHoliday(_ holiday:holidayDef) -> Bool {
    
    let query = NCMBQuery(className: "DayOffCalendar")
    query?.whereKey("companyID", equalTo: holiday.companyID)
    query?.whereKey("DayOff", equalTo: holiday.date)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return false
    }
    if results.count > 0 {
        let obj = results[0] as! NCMBObject
        
        obj.setObject(holiday.calendarName, forKey: "CalendarName")
        obj.setObject(holiday.holidayName, forKey: "Name")
        obj.setObject(holiday.memo, forKey: "Memo")
        obj.setObject(holiday.isPublic, forKey: "isPublic")
        obj.setObject(holiday.isOnWork, forKey: "isOnWork")
        
        obj.saveInBackground({(error) in
            if error != nil {print("Save error : ",error!)}
        })
        return true
        
    }
    return false
}

/// update holiday record of DayOffCalendar Class
///
/// - parameter : holidayDef
/// - returns:  nothing
func updateCompanyHoliday(_ holiday:holidayDef) -> Bool {
    
    let query = NCMBQuery(className: "DayOffCalendar")
    query?.whereKey("companyID", equalTo: holiday.companyID)
    query?.whereKey("DayOff", equalTo: holiday.date)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return false
    }
    let obj:NCMBObject
    if results.count > 0 {
        obj = results[0] as! NCMBObject
    } else {
        obj = NCMBObject(className: "DayOffCalendar")
    }
    obj.setObject(holiday.companyName,forKey:"CompanyName")
    obj.setObject(holiday.companyID,forKey:"companyID")
    obj.setObject(holiday.calendarName, forKey: "CalendarName")
    obj.setObject(holiday.date, forKey: "DayOff")
    obj.setObject(holiday.holidayName, forKey: "Name")
    obj.setObject(holiday.memo, forKey: "Memo")
    obj.setObject(false, forKey: "isPublic")
    obj.setObject(holiday.isOnWork, forKey: "isOnWork")
    
    obj.saveInBackground({(error) in
        if error != nil {print("Save error : ",error!)}
    })
    return true
}

// MARK: - Functions for DailyLog

/// Get todays DailyLog object
///
/// - parameter Nothing:
/// - returns: Void
func getDailyLogObject(_ dailyLog:inout NCMBObject?,user:userInfo,todaysInfo:workDataOfDay,closure: (_ dailyLogObj:NCMBObject) -> Void) -> Bool {
    
    if dailyLog != nil {
        closure(dailyLog!)
        print("Update dailyLogObj ",todaysInfo.beginningOfDay!)
        return true
    }
    
    // 今日のログがすでにあるか検索し、無ければ新しいオブジェクトを作成
    let query = NCMBQuery(className: "DailyLog")
    query?.whereKey("companyID", equalTo: user.companyID)
    query?.whereKey("UserID", equalTo: user.UserID)
    query?.whereKey("Date", greaterThanOrEqualTo: todaysInfo.beginningOfDay)
    query?.whereKey("Date", lessThan: todaysInfo.endingOfDay)
    query?.order(byAscending: "Date") // Dateで昇順にソート
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("Query error =\(error1)")
        return false
        
    }
    if results.count > 0 { // Hit
        print("Update dailyLog:",todaysInfo.beginningOfDay!)
        dailyLog = results.last as? NCMBObject
    } else { // Not Hit -> Create Object
        print("Create dailyLog:",todaysInfo.beginningOfDay!)
        dailyLog = NCMBObject(className: "DailyLog")
    }
    closure(dailyLog!)
    return true
    
}

/// Get todays DailyLog object
///
/// - parameter :
/// - returns: Void
func getDailyLogObject(_ dailyLog:inout NCMBObject?,user:personalUserInfo,todaysInfo:workDataOfDay,closure: (_ dailyLogObj:NCMBObject) -> Void) -> Bool {
    
    if dailyLog != nil {
        closure(dailyLog!)
        print("Update dailyLogObj ",todaysInfo.beginningOfDay!)
        return true
    }
    
    // 今日のログがすでにあるか検索し、無ければ新しいオブジェクトを作成
    let query = NCMBQuery(className: "DailyLog")
    query?.whereKey("UserID", equalTo: user.IDNo)
    query?.whereKey("Date", greaterThanOrEqualTo: todaysInfo.beginningOfDay)
    query?.whereKey("Date", lessThan: todaysInfo.endingOfDay)
    query?.order(byAscending: "Date") // Dateで昇順にソート
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("Query error =\(error1)")
        return false
        
    }
    if results.count > 0 { // Hit
        print("Update dailyLog:",todaysInfo.beginningOfDay!)
        dailyLog = results.last as? NCMBObject
    } else { // Not Hit -> Create Object
        print("Create dailyLog:",todaysInfo.beginningOfDay!)
        dailyLog = NCMBObject(className: "DailyLog")
    }
    closure(dailyLog!)
    return true
    
}

/// Get Ones DailyLog
///
/// - parameter user,openDate,closeDate:
/// - returns: Void
func getOnesLog(_ user:userInfo,openDate:Date,CloseDate:Date) -> dailyLogInfo? {

    let query = NCMBQuery(className: "DailyLog")
    query?.whereKey("companyID", equalTo: user.companyID)
    query?.whereKey("UserID", equalTo: user.UserID)
    query?.whereKey("Date", greaterThanOrEqualTo: openDate)
    query?.whereKey("Date", lessThan: CloseDate)
    query?.order(byAscending: "Date") // Dateで昇順にソート
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return nil
    }
    if results.count > 0 {
        let dailyLog = results[0] as? NCMBObject
        var Dailydata = dailyLogInfo()
        
        Dailydata.date = dailyLog?.object(forKey: "Date") as? Date
        Dailydata.dayKind = dailyLog?.object(forKey: "DayKind") as? String ?? ""
        Dailydata.isDayOff = dailyLog?.object(forKey: "isDayOff")  as? Bool ?? false
        Dailydata.companyName = dailyLog?.object(forKey: "CompanyName") as? String ?? ""
        Dailydata.companyID = dailyLog?.object(forKey: "companyID") as? String ?? ""
        Dailydata.groupName = dailyLog?.object(forKey: "GroupName") as? String ?? ""
        Dailydata.UserID = dailyLog?.object(forKey: "UserID") as? String ?? ""
        Dailydata.timeTable = dailyLog?.object(forKey: "TimeTableName") as? String ?? ""
        Dailydata.timeTableID = dailyLog?.object(forKey: "timeTableID") as? String ?? ""
        Dailydata.locationID = dailyLog?.object(forKey: "locationID") as? String ?? ""
        Dailydata.startStamp = StampInfo()
        Dailydata.startStamp!.workTime = dailyLog?.object(forKey: "StartTime") as? Date
        Dailydata.startStamp!.isManually = dailyLog?.object(forKey: "StartManually")  as? Bool ?? false
        Dailydata.startStamp!.workPlace = dailyLog?.object(forKey: "StartLocation") as? String ?? ""
        Dailydata.startStamp!.stampAddress = dailyLog?.object(forKey: "ArrivalAddress") as? String ?? ""
        let locStart = dailyLog?.object(forKey: "ArrivalGeo") as? NCMBGeoPoint ?? NCMBGeoPoint(latitude: 0.0, longitude: 0.0)
        Dailydata.startStamp!.stampGeo = CLLocation(latitude: locStart!.latitude, longitude: locStart!.longitude)
        Dailydata.startStamp!.stampTime = dailyLog?.object(forKey: "ArrivalTime") as? Date
        Dailydata.endStamp = StampInfo()
        Dailydata.endStamp!.workTime = dailyLog?.object(forKey: "EndTime") as? Date
        Dailydata.endStamp!.isManually = dailyLog?.object(forKey: "EndManually")  as? Bool ?? false
        Dailydata.endStamp!.workPlace = dailyLog?.object(forKey: "EndLocation") as? String ?? ""
        Dailydata.endStamp!.stampAddress = dailyLog?.object(forKey: "LeaveAddress") as? String ?? ""
        let locEnd = dailyLog?.object(forKey: "LeaveGeo") as? NCMBGeoPoint ?? NCMBGeoPoint(latitude: 0.0, longitude: 0.0)
        Dailydata.endStamp!.stampGeo = CLLocation(latitude: locEnd!.latitude, longitude: locEnd!.longitude)
        Dailydata.endStamp!.stampTime = dailyLog?.object(forKey: "LeaveTime") as? Date
        
        Dailydata.workHours = dailyLog?.object(forKey: "WorkHours")  as? Int ?? 0
        Dailydata.overtimeWorkHours = dailyLog?.object(forKey: "OvertimeWorkHours")  as? Int ?? 0
        Dailydata.lateNightWorkHours = dailyLog?.object(forKey: "LateNightWorkHours")  as? Int ?? 0
        Dailydata.memo = dailyLog?.object(forKey: "memo")  as? String ?? ""
        
        return Dailydata
    }
    
    return nil
    
}

/// Get Ones DailyLog
///
/// - parameter user,openDate,closeDate:
/// - returns: Void
func getOnesLog(_ user:personalUserInfo,openDate:Date,CloseDate:Date) -> dailyLogInfo? {
    
    let query = NCMBQuery(className: "DailyLog")
    query?.whereKey("UserID", equalTo: user.IDNo)
    query?.whereKey("Date", greaterThanOrEqualTo: openDate)
    query?.whereKey("Date", lessThan: CloseDate)
    query?.order(byAscending: "Date") // Dateで昇順にソート
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return nil
    }
    if results.count > 0 {
        let dailyLog = results[0] as? NCMBObject
        var Dailydata = dailyLogInfo()
        
        Dailydata.date = dailyLog?.object(forKey: "Date") as? Date
        Dailydata.dayKind = dailyLog?.object(forKey: "DayKind") as? String ?? ""
        Dailydata.companyName = dailyLog?.object(forKey: "CompanyName") as? String ?? ""
        Dailydata.companyID = dailyLog?.object(forKey: "companyID") as? String ?? ""
        Dailydata.groupName = dailyLog?.object(forKey: "GroupName") as? String ?? ""
        Dailydata.UserID = dailyLog?.object(forKey: "UserID") as? String ?? ""
        Dailydata.timeTable = dailyLog?.object(forKey: "TimeTableName") as? String ?? ""
        Dailydata.timeTableID = dailyLog?.object(forKey: "timeTableID") as? String ?? ""
        Dailydata.location = dailyLog?.object(forKey: "location") as? String ?? ""
        Dailydata.locationID = dailyLog?.object(forKey: "locationID") as? String ?? ""
        Dailydata.isDayOff = dailyLog?.object(forKey: "isDayOff") as? Bool ?? false
        Dailydata.startStamp = StampInfo()
        Dailydata.startStamp!.workTime = dailyLog?.object(forKey: "StartTime") as? Date
        Dailydata.startStamp!.isManually = dailyLog?.object(forKey: "StartManually")  as? Bool ?? false
        Dailydata.startStamp!.workPlace = dailyLog?.object(forKey: "StartLocation") as? String ?? ""
        Dailydata.startStamp!.stampAddress = dailyLog?.object(forKey: "ArrivalAddress") as? String ?? ""
        let locStart = dailyLog?.object(forKey: "ArrivalGeo") as? NCMBGeoPoint ?? NCMBGeoPoint(latitude: 0.0, longitude: 0.0)
        Dailydata.startStamp!.stampGeo = CLLocation(latitude: locStart!.latitude, longitude: locStart!.longitude)
        Dailydata.startStamp!.stampTime = dailyLog?.object(forKey: "ArrivalTime") as? Date
        Dailydata.endStamp = StampInfo()
        Dailydata.endStamp!.workTime = dailyLog?.object(forKey: "EndTime") as? Date
        Dailydata.endStamp!.isManually = dailyLog?.object(forKey: "EndManually")  as? Bool ?? false
        Dailydata.endStamp!.workPlace = dailyLog?.object(forKey: "EndLocation") as? String ?? ""
        Dailydata.endStamp!.stampAddress = dailyLog?.object(forKey: "LeaveAddress") as? String ?? ""
        let locEnd = dailyLog?.object(forKey: "LeaveGeo") as? NCMBGeoPoint ?? NCMBGeoPoint(latitude: 0.0, longitude: 0.0)
        Dailydata.endStamp!.stampGeo = CLLocation(latitude: locEnd!.latitude, longitude: locEnd!.longitude)
        Dailydata.endStamp!.stampTime = dailyLog?.object(forKey: "LeaveTime") as? Date
        
        Dailydata.workHours = dailyLog?.object(forKey: "WorkHours")  as? Int ?? 0
        Dailydata.overtimeWorkHours = dailyLog?.object(forKey: "OvertimeWorkHours")  as? Int ?? 0
        Dailydata.lateNightWorkHours = dailyLog?.object(forKey: "LateNightWorkHours")  as? Int ?? 0
        Dailydata.memo = dailyLog?.object(forKey: "memo")  as? String ?? ""
        
        return Dailydata
    }
    
    return nil
    
}

/// Get Monthly DailyLog List
///
/// - parameter Nothing:
/// - returns: Void
func getMonthlyLogList(_ user:userInfo,monthlyCal:[workDataOfDay]) -> [workDataOfDay] {
    let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
    let openDate:Date = (monthlyCal.first?.beginningOfDay!)!
    let CloseDate:Date = (monthlyCal.last?.endingOfDay!)!
    var monthlyList = monthlyCal

    let query = NCMBQuery(className: "DailyLog")
    query?.whereKey("companyID", equalTo: user.companyID)
    query?.whereKey("UserID", equalTo: user.UserID)
    query?.whereKey("Date", greaterThanOrEqualTo: openDate)
    query?.whereKey("Date", lessThan: CloseDate)
    query?.order(byAscending: "Date") // Dateで昇順にソート
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return monthlyList
    }
    if results.count > 0 {
        for i in 0 ..< results.count {
        //for var i=0 ; i < results.count; i++ {
            let dailyLog = results[i] as? NCMBObject
            var Dailydata = workDataOfDay()
            Dailydata.beginningOfDay = dailyLog?.object(forKey: "Date") as? Date
            Dailydata.isStartManually = dailyLog?.object(forKey: "StartManually")  as? Bool ?? false
            Dailydata.startTimeOfWork = dailyLog?.object(forKey: "StartTime") as? Date
            Dailydata.startLocation = dailyLog?.object(forKey: "StartLocation")  as? String ?? ""
            Dailydata.startAddress = dailyLog?.object(forKey: "ArrivalAddress")  as? String ?? ""
            Dailydata.isEndManually = dailyLog?.object(forKey: "EndManually")  as? Bool ?? false
            Dailydata.endTimeOfWork = dailyLog?.object(forKey: "EndTime") as? Date
            Dailydata.endLocation = dailyLog?.object(forKey: "EndLocation")  as? String ?? ""
            Dailydata.endAddress = dailyLog?.object(forKey: "LeaveAddress")  as? String ?? ""
            Dailydata.location = dailyLog?.object(forKey: "Location")  as? String ?? ""
            Dailydata.locationID = dailyLog?.object(forKey: "locationID")  as? String ?? ""
            Dailydata.timeTable = dailyLog?.object(forKey: "TimeTableName")  as? String ?? ""
            Dailydata.timeTableID = dailyLog?.object(forKey: "timeTableID")  as? String ?? ""
            Dailydata.workHours = dailyLog?.object(forKey: "WorkHours")  as? Int ?? 0
            Dailydata.overtimeWorkHours = dailyLog?.object(forKey: "OvertimeWorkHours")  as? Int ?? 0
            Dailydata.lateNightWorkHours = dailyLog?.object(forKey: "LateNightWorkHours")  as? Int ?? 0
            Dailydata.memo = dailyLog?.object(forKey: "memo")  as? String ?? ""
            Dailydata.isDayOff = dailyLog?.object(forKey: "isDayOff")  as? Bool ?? false
            //print(Dailydata)
            
            for j in 0 ..< monthlyList.count {
            //for  var i=0; i < monthlyList.count;i++ {
                if calendar.isDate(monthlyList[j].beginningOfDay! as Date, inSameDayAs: Dailydata.beginningOfDay! as Date) {
                    monthlyList[j].UserID = user.UserID
                    monthlyList[j].companyID = user.companyID
                    monthlyList[j].isStartManually = Dailydata.isStartManually
                    monthlyList[j].startTimeOfWork = Dailydata.startTimeOfWork
                    monthlyList[j].startLocation = Dailydata.startLocation
                    monthlyList[j].startAddress = Dailydata.startAddress
                    monthlyList[j].isEndManually = Dailydata.isEndManually
                    monthlyList[j].endTimeOfWork = Dailydata.endTimeOfWork
                    monthlyList[j].endLocation = Dailydata.endLocation
                    monthlyList[j].endAddress = Dailydata.endAddress
                    monthlyList[j].location = Dailydata.location
                    monthlyList[j].locationID = Dailydata.locationID
                    monthlyList[j].timeTable = Dailydata.timeTable
                    monthlyList[j].timeTableID = Dailydata.timeTableID
                    monthlyList[j].workHours = Dailydata.workHours
                    monthlyList[j].overtimeWorkHours = Dailydata.overtimeWorkHours
                    monthlyList[j].lateNightWorkHours = Dailydata.lateNightWorkHours
                    monthlyList[j].memo = Dailydata.memo
                    monthlyList[j].isDayOff = Dailydata.isDayOff
                    
                }
            }
            
        }
    }
    return monthlyList

}

/// Get Monthly DailyLog List
///
/// - parameter Nothing:
/// - returns: Void
func getMonthlyLogList(_ user:personalUserInfo,openDate:Date,CloseDate:Date,monthlyList:inout [workDataOfDay]) {
    let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
    
    let query = NCMBQuery(className: "DailyLog")
    query?.whereKey("companyID", equalTo: user.companyID)
    query?.whereKey("UserID", equalTo: user.IDNo)
    query?.whereKey("Date", greaterThanOrEqualTo: openDate)
    query?.whereKey("Date", lessThanOrEqualTo: CloseDate)
    query?.order(byAscending: "Date") // Dateで昇順にソート
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return
    }
    if results.count > 0 {
        for i in 0 ..< results.count {
            //for var i=0 ; i < results.count; i++ {
            let dailyLog = results[i] as? NCMBObject
            var Dailydata = workDataOfDay()
            
            Dailydata.beginningOfDay = dailyLog?.object(forKey: "Date") as? Date
            Dailydata.timeTable = dailyLog?.object(forKey: "TimeTableName")  as? String ?? ""
            Dailydata.timeTableID = dailyLog?.object(forKey: "timeTableID")  as? String ?? ""
            Dailydata.startTimeOfWork = dailyLog?.object(forKey: "StartTime") as? Date
            Dailydata.startLocation = dailyLog?.object(forKey: "StartLocation")  as? String ?? ""
            Dailydata.endTimeOfWork = dailyLog?.object(forKey: "EndTime") as? Date
            Dailydata.endLocation = dailyLog?.object(forKey: "EndLocation")  as? String ?? ""
            Dailydata.location = dailyLog?.object(forKey: "Location")  as? String ?? ""
            Dailydata.locationID = dailyLog?.object(forKey: "locationID")  as? String ?? ""
            Dailydata.workHours = dailyLog?.object(forKey: "WorkHours")  as? Int ?? 0
            Dailydata.overtimeWorkHours = dailyLog?.object(forKey: "OvertimeWorkHours")  as? Int ?? 0
            Dailydata.lateNightWorkHours = dailyLog?.object(forKey: "LateNightWorkHours")  as? Int ?? 0
            Dailydata.memo = dailyLog?.object(forKey: "memo")  as? String ?? ""
            Dailydata.isStartManually = dailyLog?.object(forKey: "StartManually")  as? Bool ?? false
            Dailydata.isEndManually = dailyLog?.object(forKey: "EndManually")  as? Bool ?? false
            Dailydata.isDayOff = dailyLog?.object(forKey: "isDayOff")  as? Bool ?? false
            //print(Dailydata)
            for j in 0 ..< monthlyList.count {
                //for  var i=0; i < monthlyList.count;i++ {
                if calendar.isDate(monthlyList[j].beginningOfDay! as Date, inSameDayAs: Dailydata.beginningOfDay! as Date) {
                    monthlyList[j].startTimeOfWork = Dailydata.startTimeOfWork
                    monthlyList[j].startLocation = Dailydata.startLocation
                    monthlyList[j].endTimeOfWork = Dailydata.endTimeOfWork
                    monthlyList[j].endLocation = Dailydata.endLocation
                    monthlyList[j].location = Dailydata.location
                    monthlyList[j].locationID = Dailydata.locationID
                    monthlyList[j].timeTable = Dailydata.timeTable
                    monthlyList[j].timeTableID = Dailydata.timeTableID
                    monthlyList[j].workHours = Dailydata.workHours
                    monthlyList[j].overtimeWorkHours = Dailydata.overtimeWorkHours
                    monthlyList[j].lateNightWorkHours = Dailydata.lateNightWorkHours
                    monthlyList[j].memo = Dailydata.memo
                    monthlyList[j].isStartManually = Dailydata.isStartManually
                    monthlyList[j].isEndManually = Dailydata.isEndManually
                    monthlyList[j].isDayOff = Dailydata.isDayOff
                   
                }
            }
            
        }
    }
    
}
/// Get Monthly DailyLog List of All Users
///
/// - parameter Nothing:
/// - returns: Void
func getMonthlyLogListOfAllUsersByID(_ companyID:String,openDate:Date,CloseDate:Date) -> [workDataOfDay] {
    
    var logList:[workDataOfDay] = []
    
    let query = NCMBQuery(className: "DailyLog")
    query?.whereKey("companyID", equalTo: companyID)
    query?.whereKey("Date", greaterThanOrEqualTo: openDate)
    query?.whereKey("Date", lessThan: CloseDate)
    query?.order(byAscending: "Date") // Dateで昇順にソート
    query?.limit = 1000
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return logList
    }
    if results.count > 0 {
        for i in 0 ..< results.count {
            let dailyLog = results[i] as? NCMBObject
            var Dailydata = workDataOfDay()
            
            Dailydata.UserID = dailyLog?.object(forKey: "UserID")  as? String ?? ""
            Dailydata.isDayOff = dailyLog?.object(forKey: "isDayOff")  as? Bool ?? false
            Dailydata.timeTable = dailyLog?.object(forKey: "TimeTableName")  as? String ?? ""
            Dailydata.timeTableID = dailyLog?.object(forKey: "timeTableID")  as? String ?? ""
            Dailydata.startTimeOfWork = dailyLog?.object(forKey: "StartTime") as? Date
            Dailydata.endTimeOfWork = dailyLog?.object(forKey: "EndTime") as? Date
            Dailydata.location = dailyLog?.object(forKey: "Location")  as? String ?? ""
            Dailydata.locationID = dailyLog?.object(forKey: "locationID")  as? String ?? ""
            Dailydata.workHours = dailyLog?.object(forKey: "WorkHours")  as? Int ?? 0
            Dailydata.overtimeWorkHours = dailyLog?.object(forKey: "OvertimeWorkHours")  as? Int ?? 0
            Dailydata.lateNightWorkHours = dailyLog?.object(forKey: "LateNightWorkHours")  as? Int ?? 0
            Dailydata.memo = dailyLog?.object(forKey: "memo")  as? String ?? ""
            Dailydata.isStartManually = dailyLog?.object(forKey: "StartManually")  as? Bool ?? false
            Dailydata.isEndManually = dailyLog?.object(forKey: "EndManually")  as? Bool ?? false
            Dailydata.beginningOfDay = dailyLog?.object(forKey: "Date") as? Date
            Dailydata.endingOfDay = Date(timeInterval: 24*60*60, since: Dailydata.beginningOfDay! as Date)
            Dailydata.startLocation = dailyLog?.object(forKey: "StartLocation")  as? String ?? ""
            Dailydata.startAddress = dailyLog?.object(forKey: "ArrivalAddress")  as? String ?? ""
            Dailydata.endLocation = dailyLog?.object(forKey: "EndLocation")  as? String ?? ""
            Dailydata.endAddress = dailyLog?.object(forKey: "LeaveAddress")  as? String ?? ""
            
            logList += [Dailydata]
        }
    }
    return logList
}

// MARK: - Functions for User & Member

/// Select User data from users
///
/// - parameter userID:
/// - returns: userInfo
func selectUser(_ users:[userInfo],userID:String)-> userInfo {
    
    for user in users {
        if user.UserID == userID {
            return user
        }
    }
    print( "Not Hit in selectUser userID:",userID)
    return userInfo() // Not Hit
}

/// Get Todays Member List
///
/// - parameter Nothing:
/// - returns: Void
func getTodaysMemberListAll(_ company:companyInfo,users:[userInfo]) -> [workDataOfDay] {
    var members:[workDataOfDay] = []
    let dtc = dateTimeControll()
    
    // 今日の始まりの時間を作る
    let beginingOfToday = dtc.getDateLine(company.dateLine)
    let openDate:Date
    let closeDate:Date
    if beginingOfToday.compare(Date()) == .orderedDescending/*比較対象より未来*/{ // 日付を戻す
        openDate = dtc.getPriorDate(beginingOfToday)
        closeDate = beginingOfToday
    } else {
        openDate = beginingOfToday
        closeDate = dtc.getNextDate(beginingOfToday)
    }
    
    let query = NCMBQuery(className: "DailyLog")
    query?.whereKey("companyID", equalTo: company.companyID)
    query?.whereKey("Date", greaterThanOrEqualTo: openDate)
    query?.whereKey("Date", lessThan: closeDate)
    query?.order(byAscending: "userName") // userNameで昇順にソート
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return members
    }
    if results.count > 0 {
        for i in 0  ..< results.count {
            let dailyLog = results[i] as? NCMBObject
            var memberData = workDataOfDay()

            memberData.companyName = dailyLog?.object(forKey: "CompanyName")  as? String ?? ""
            memberData.companyID = dailyLog?.object(forKey: "companyID")  as? String ?? ""
            memberData.groupName = dailyLog?.object(forKey: "GroupName")  as? String ?? ""
            memberData.UserID = dailyLog?.object(forKey: "UserID")  as? String ?? ""
            memberData.timeTable = dailyLog?.object(forKey: "TimeTableName")  as? String ?? ""
            memberData.isDayOff = dailyLog?.object(forKey: "isDayOff")    as? Bool ?? false
            memberData.timeTableID = dailyLog?.object(forKey: "timeTableID")  as? String ?? ""
            memberData.location = dailyLog?.object(forKey: "Location")  as? String ?? ""
            memberData.locationID = dailyLog?.object(forKey: "locationID")  as? String ?? ""
            memberData.startTimeOfWork = dailyLog?.object(forKey: "StartTime") as? Date
            memberData.endTimeOfWork = dailyLog?.object(forKey: "EndTime") as? Date
            memberData.workHours = dailyLog?.object(forKey: "WorkHours")  as? Int ?? 0
            memberData.overtimeWorkHours = dailyLog?.object(forKey: "OvertimeWorkHours")  as? Int ?? 0
            memberData.lateNightWorkHours = dailyLog?.object(forKey: "LateNightWorkHours")  as? Int ?? 0
            memberData.memo = dailyLog?.object(forKey: "memo")  as? String ?? ""
            memberData.isStartManually = dailyLog?.object(forKey: "StartManually")  as? Bool ?? false
            memberData.isEndManually = dailyLog?.object(forKey: "EndManually")  as? Bool ?? false
            memberData.startLocation = dailyLog?.object(forKey: "StartLocation")  as? String ?? ""
            memberData.startAddress = dailyLog?.object(forKey: "ArrivalAddress")  as? String ?? ""
            memberData.endLocation = dailyLog?.object(forKey: "EndLocation")  as? String ?? ""
            memberData.endAddress = dailyLog?.object(forKey: "LeaveAddress")  as? String ?? ""
            let startLocation = dailyLog?.object(forKey: "StartLocation")  as? String ?? ""
            let endLocation = dailyLog?.object(forKey: "EndLocation")  as? String ?? ""
            memberData.location = startLocation
            if (startLocation == "エリア外") || (startLocation == "") {
                if endLocation != "" {
                    memberData.location = endLocation
                }
                
            }
            
            let user = selectUser(users,userID: memberData.UserID)
            memberData.name = user.name
            
            members += [memberData]
        }
    }
    
    return members
    
}

/// Get User
///
/// - parameter Nothing:
/// - returns: User
func getUserDataByMail(_ usermail:String) -> userInfo {
    var user = userInfo()
    
    let query1 = NCMBQuery(className: "user")
    query1?.whereKey("mailAddress", equalTo: usermail)
    let query2 = NCMBQuery(className: "user")
    query2?.whereKey("mailBK", equalTo: usermail)
    let query = NCMBQuery.orQuery(withSubqueries: [query1!,query2!])

    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return user
    }
    if results.count > 0 {
        let userObject = results[0] as! NCMBUser
        
        user.MBuserName = userObject.userName
        user.mailBK = userObject.object(forKey: "mailBK") as? String ?? ""
        user.isAuthenticated = userObject.object(forKey: "mailAddressConfirm") as? Bool ?? false
        if user.isAuthenticated {
            user.mail = userObject.mailAddress ?? ""
            user.password = userObject.password ?? ""
            if user.mailBK.isEmpty { //古い登録ユーザで mailBK がない場合は mailをコピーしておく
               user.mailBK = user.mail
            }
        } else {
            user.mail = NOT_AUTH_YET
        }

        user.name = userObject.object(forKey: "userNameKanji") as? String ?? ""
        user.companyName = userObject.object(forKey: "CompanyName") as? String ?? ""
        user.companyID = userObject.object(forKey: "companyID") as? String ?? ""
        user.groupName = userObject.object(forKey: "GroupName") as? String ?? ""
        user.UserID = userObject.object(forKey: "UserID") as? String ?? ""
        user.nameKana = userObject.object(forKey: "userNameKana") as? String ?? ""
        user.sex = userObject.object(forKey: "Sex") as? String ?? ""
        user.defaultLocation = userObject.object(forKey: "DefaultLocation") as? String ?? ""
        user.defaultTimeTable = userObject.object(forKey: "DefaultTimeTable") as? String ?? ""
        user.isActiveUser = userObject.object(forKey: "isActiveUser") as? Bool ?? false
        user.isManager = userObject.object(forKey: "isManager") as? Bool ?? false
        
    }
    return user
}

/// Match UserList to Log
///
/// - parameter : users & logs
/// - returns: [workDataOfDay]
func matchUserList(_ users:[userInfo],worklogs:[workDataOfDay])-> [workDataOfDay] {
    var members:[workDataOfDay] = []
    
    allUsers: for user in users {
        for var log in worklogs {
            if user.UserID == log.UserID {
                log.name = user.name
                members += [log]
                continue allUsers
            }
        }
        // 今日のデータがない場合
        var memberData = workDataOfDay()
        memberData.companyName = user.companyName
        memberData.companyID = user.companyID
        memberData.groupName = user.groupName
        memberData.UserID = user.UserID
        memberData.name = user.name
        memberData.timeTable = user.defaultTimeTable
        members += [memberData]

    }
    return members
}

/// Get All Member List with todays Log
///
/// - parameter Nothing:
/// - returns: Void
func getAllMemberListWithWorkLog(_ company:companyInfo,users:[userInfo]) -> [workDataOfDay] {
    var members:[workDataOfDay] = []
    let dtc = dateTimeControll()
    
    // 今日の始まりの時間を作る
    let beginingOfToday = dtc.getDateLine(company.dateLine)
    let openDate:Date
    let closeDate:Date
    if beginingOfToday.compare(Date()) == .orderedDescending/*比較対象より未来*/{ // 日付を戻す
        openDate = dtc.getPriorDate(beginingOfToday)
        closeDate = beginingOfToday
    } else {
        openDate = beginingOfToday
        closeDate = dtc.getNextDate(beginingOfToday)
    }
    
    let query = NCMBQuery(className: "DailyLog")
    query?.whereKey("companyID", equalTo: company.companyID)
    query?.whereKey("Date", greaterThanOrEqualTo: openDate)
    query?.whereKey("Date", lessThan: closeDate)
    query?.order(byAscending: "userName") // userNameで昇順にソート
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return members
    }
    if results.count > 0 {
        for i in 0  ..< results.count {
            let dailyLog = results[i] as? NCMBObject
            var memberData = workDataOfDay()
            
            memberData.companyName = dailyLog?.object(forKey: "CompanyName")  as? String ?? ""
            memberData.companyID = dailyLog?.object(forKey: "companyID")  as? String ?? ""
            memberData.groupName = dailyLog?.object(forKey: "GroupName")  as? String ?? ""
            memberData.UserID = dailyLog?.object(forKey: "UserID")  as? String ?? ""
            memberData.isDayOff = dailyLog?.object(forKey: "isDayOff")  as? Bool ?? false
            memberData.timeTable = dailyLog?.object(forKey: "TimeTableName")  as? String ?? ""
            memberData.startTimeOfWork = dailyLog?.object(forKey: "StartTime") as? Date
            memberData.endTimeOfWork = dailyLog?.object(forKey: "EndTime") as? Date
            memberData.workHours = dailyLog?.object(forKey: "WorkHours")  as? Int ?? 0
            memberData.overtimeWorkHours = dailyLog?.object(forKey: "OvertimeWorkHours")  as? Int ?? 0
            memberData.lateNightWorkHours = dailyLog?.object(forKey: "LateNightWorkHours")  as? Int ?? 0
            memberData.memo = dailyLog?.object(forKey: "memo")  as? String ?? ""
            memberData.isStartManually = dailyLog?.object(forKey: "StartManually")  as? Bool ?? false
            memberData.isEndManually = dailyLog?.object(forKey: "EndManually")  as? Bool ?? false
            memberData.startLocation = dailyLog?.object(forKey: "StartLocation")  as? String ?? ""
            memberData.startAddress = dailyLog?.object(forKey: "ArrivalAddress")  as? String ?? ""
            memberData.endLocation = dailyLog?.object(forKey: "EndLocation")  as? String ?? ""
            memberData.endAddress = dailyLog?.object(forKey: "LeaveAddress")  as? String ?? ""
            let startLocation = dailyLog?.object(forKey: "StartLocation")  as? String ?? ""
            let endLocation = dailyLog?.object(forKey: "EndLocation")  as? String ?? ""
            memberData.location = startLocation
            if (startLocation == "エリア外") || (startLocation == "") {
                if endLocation != "" {
                    memberData.location = endLocation
                }
                
            }
            
            //let user = selectUser(users,userID: memberData.UserID)
            //memberData.name = user.name
            
            members += [memberData]
        }
    }
    members = matchUserList(users,worklogs:members)
    
    return members
    
}


/// Get User List
///
/// - parameter companyID: company ID with String
/// - returns: users as [userInfo]
func getUserListAllByID(_ companyID:String) -> [userInfo] {
    
    var users:[userInfo] = []

    let query = NCMBQuery(className: "user")
    query?.whereKey("companyID", equalTo: companyID)
    query?.order(byAscending: "UserID") // userIDで昇順にソート
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return users
    }
    if results.count > 0 {
        for i in 0  ..< results.count {
            let userObject = results[i] as! NCMBUser
            var user = userInfo()
            
            user.MBuserName = userObject.userName
            user.mailBK = userObject.object(forKey: "mailBK") as? String ?? ""
            user.isAuthenticated = userObject.object(forKey: "mailAddressConfirm") as? Bool ?? false
            if user.isAuthenticated {
                user.mail = userObject.mailAddress ?? ""
                user.password = userObject.password ?? ""
                if user.mailBK.isEmpty { //古い登録ユーザで mailBK がない場合は mailをコピーしておく
                    user.mailBK = user.mail
                }
            } else {
                user.mail = NOT_AUTH_YET
            }

            user.name = userObject.object(forKey: "userNameKanji") as? String ?? ""
            user.companyName = userObject.object(forKey: "CompanyName") as? String ?? ""
            user.companyID = userObject.object(forKey: "companyID") as? String ?? ""
            user.groupName = userObject.object(forKey: "GroupName") as? String ?? ""
            user.UserID = userObject.object(forKey: "UserID") as? String ?? ""
            user.nameKana = userObject.object(forKey: "userNameKana") as? String ?? ""
            user.sex = userObject.object(forKey: "Sex") as? String ?? ""
            user.defaultLocation = userObject.object(forKey: "DefaultLocation") as? String ?? ""
            user.defaultTimeTable = userObject.object(forKey: "DefaultTimeTable") as? String ?? ""
            user.isActiveUser = userObject.object(forKey: "isActiveUser") as? Bool ?? false
            user.isManager = userObject.object(forKey: "isManager") as? Bool ?? false
            //user.isLoggedIn = true
            
            users += [user]
        }
    }
    return users
    
}

/// check User'S mail address
///
/// - parameter : mail addr String
/// - returns: true = no user ,false = already same mail address was used
func checkUserMail(_ usermail:String) -> Bool {
    
    let query1 = NCMBQuery(className: "user")
    query1?.whereKey("mailAddress", equalTo: usermail)
    let query2 = NCMBQuery(className: "user")
    query2?.whereKey("mailBK", equalTo: usermail)
    let userQuery = NCMBQuery.orQuery(withSubqueries: [query1!,query2!])
    var users:[AnyObject] = []
    do {
        users = try userQuery!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return false
    }
    if users.count > 0 {
        return false // 同じメアド有り
    }
    // OK:Not Used
    return true
    
}

/// add User
///
/// - parameter : companyInfo,userInfo
/// - returns: nothing
func addUser( _ company:companyInfo,user:userInfo) {
    // ユーザデータ登録
    
    let acl = NCMBACL()
    // 全員に読み書き可とする
    acl.setPublicReadAccess(true)
    acl.setPublicWriteAccess(true)
    
    let obj = NCMBUser()
    obj.userName = user.MBuserName
    obj.password = user.password
    obj.mailAddress = user.mail
    
    // 任意フィールドに値を設定
    obj.setObject(company.companyName,forKey:"CompanyName")
    obj.setObject(company.companyID,forKey:"companyID")
    obj.setObject(user.groupName,forKey:"GroupName")
    obj.setObject(user.UserID,forKey:"UserID")
    obj.setObject(user.name,forKey:"userNameKanji")
    obj.setObject(user.nameKana,forKey:"userNameKana")
    obj.setObject(user.sex,forKey:"Sex")
    obj.setObject(user.defaultLocation,forKey:"DefaultLocation")
    obj.setObject(user.defaultTimeTable,forKey:"DefaultTimeTable")
    obj.setObject(user.isActiveUser,forKey:"isActiveUser")
    obj.setObject(user.isManager,forKey:"isManager") // Manager
    obj.setObject(user.mail,forKey:"mailBK")
    obj.acl = acl // 全員に読み書き可とする
    // 登録実行
    obj.signUpInBackground({(error) in
        if error != nil {print("Add User save error : ",error!)}
    })
    
}


/// update user record
///
/// - parameter : companyInfo,userInfo
/// - returns:  nothing
func updateUser( _ company:companyInfo,user:userInfo) -> Bool {
    
    let query = NCMBQuery(className: "user")
    query?.whereKey("companyID", equalTo: company.companyID)
    query?.whereKey("UserID", equalTo: user.UserID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return false
    }
    if results.count > 0 {
        let acl = NCMBACL()
        // 全員に読み書き可とする
        acl.setPublicReadAccess(true)
        acl.setPublicWriteAccess(true)
        
        let obj = results[0] as! NCMBUser
        obj.userName = user.MBuserName
        if (user.mail != "") && (user.mail != NOT_AUTH_YET) { // メールアドレスが入力されている
            let addr = obj.object(forKey: "mailAddress") as? String ?? ""
            if (addr == "") || (addr != user.mail ) { // アドレス変更あり　または　認証中
                obj.mailAddress = user.mail
                obj.setObject(user.mail,forKey:"mailBK")
            }
        }
        
        // 任意フィールドに値を設定
        obj.setObject(company.companyName,forKey:"CompanyName")
        obj.setObject(company.companyID,forKey:"companyID")
        obj.setObject(user.groupName,forKey:"GroupName")
        obj.setObject(user.UserID,forKey:"UserID")
        obj.setObject(user.name,forKey:"userNameKanji")
        obj.setObject(user.nameKana,forKey:"userNameKana")
        obj.setObject(user.sex,forKey:"Sex")
        obj.setObject(user.defaultLocation,forKey:"DefaultLocation")
        obj.setObject(user.defaultTimeTable,forKey:"DefaultTimeTable")
        obj.setObject(user.isActiveUser,forKey:"isActiveUser")
        obj.setObject(user.isManager,forKey:"isManager") // Manager
        obj.acl = acl // 全員に読み書き可とする

        var error:NSError? = nil
        obj.save(&error)
        if error != nil {
            print(error.debugDescription)
            return false
        }

        return true
        
    }
    return false
}

/// send authentication mail again
///
/// - parameter : companyInfo,userInfo
/// - returns:  nothing
func requestConfirmAgain( _ company:companyInfo,user:userInfo) -> Bool {
    
    let query = NCMBQuery(className: "user")
    query?.whereKey("companyID", equalTo: company.companyID)
    query?.whereKey("UserID", equalTo: user.UserID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return false
    }
    if results.count > 0 {
        let acl = NCMBACL()
        // 全員に読み書き可とする
        acl.setPublicReadAccess(true)
        acl.setPublicWriteAccess(true)
        
        let obj = results[0] as! NCMBUser
        let addr = obj.object(forKey: "mailBK") as? String ?? ""
        if !addr.isEmpty {
            obj.mailAddress = addr
        } else {
            obj.mailAddress = user.mail
        }
        obj.acl = acl // 全員に読み書き可とする
        
        var error:NSError? = nil
        obj.save(&error)
        if error != nil {
            print(error.debugDescription)
            return false
        }
        
        return true
        
    }
    return false
}
/// remove user record
///
/// - parameter : companyInfo,userInfo
/// - returns:  nothing
func removeUser( _ user:userInfo) -> Bool {
    
    let query = NCMBQuery(className: "user")
    query?.whereKey("companyID", equalTo: user.companyID)
    query?.whereKey("UserID", equalTo: user.UserID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return false
    }
    if results.count > 0 {
        let obj = results[0] as! NCMBUser
        var error:NSError? = nil
        obj.delete(&error)
        if error != nil {
            print(error.debugDescription)
            return false
        }
        
        return true
        
    }
    return false
}

// MARK: - Functions for Personal User

/// set Personal User Information
///
/// - parameter : PersonalUserInfo
/// - returns: nothing
func createPersonalUser(_ userinfo:personalUserInfo) {
    let user = userinfo
    // ユーザデータ登録
    
    let workPlaceName = user.companyName
    let workPlaceID = user.companyID
    let MBUserName = user.IDNo + "_" + user.name
    let UserID = user.IDNo
    let UserName = user.name
    let UserNameKana = user.nameKana
    let UserSex = user.sex
    let groupName = "Personal"
    let defaultLocation = "勤務地１"
    let defaultTimeTable = "通常勤務"
    
    // 個人用Groupは作らない
    // WeeklyCalendarは作らない
    
    // WeeklySchedule 全て休日として仮登録
    var defaultWS = weeklySchedule()
    defaultWS.scheduleID = UserID + "-WS0"
    defaultWS.personID = UserID
    defaultWS.workPlaceID = workPlaceID
    addWeeklySchedule(defaultWS)
    
    
    // Time Table 通常勤務 作る
    var defaultTT = timeTableDef()
    defaultTT.companyName = user.companyName
    defaultTT.companyID = user.companyID
    defaultTT.tableName = defaultTimeTable
    defaultTT.timeTableID = user.companyID + "-TT0"
    // 勤務時間9:00−17:00 昼休み12:00-13:00のデフォルト設定
    addTimeTable(defaultTT)
    
    // Location 本社 作る 本社の場所は鶴岡八幡宮
    var defaultLoc = locationDef()
    defaultLoc.LocationName = defaultLocation
    defaultLoc.address = "神奈川県鎌倉市雪ノ下２丁目"
    defaultLoc.geo.latitude = 35.323826
    defaultLoc.geo.longitude = 139.554980
    defaultLoc.isUseBeacon = false  // 未サポートのため
    defaultLoc.companyName = user.companyName
    defaultLoc.companyID = user.companyID
    defaultLoc.locationID = user.companyID + "-L0"
    addLocation(defaultLoc)
    
    let acl = NCMBACL()
    // 全員に読み書き可とする
    acl.setPublicReadAccess(true)
    acl.setPublicWriteAccess(true)
    
    let newUserObj = NCMBUser()
    newUserObj.userName = MBUserName
    newUserObj.password = user.password
    newUserObj.mailAddress = user.mail
    
    // 任意フィールドに値を設定
    newUserObj.setObject(workPlaceName,forKey:"CompanyName")
    newUserObj.setObject(workPlaceID,forKey:"companyID")
    newUserObj.setObject(groupName,forKey:"GroupName")
    newUserObj.setObject(UserID,forKey:"UserID")
    newUserObj.setObject(UserName,forKey:"userNameKanji")
    newUserObj.setObject(UserNameKana,forKey:"userNameKana")
    newUserObj.setObject(UserSex,forKey:"Sex")
    newUserObj.setObject(defaultLocation,forKey:"DefaultLocation")
    newUserObj.setObject(defaultTimeTable,forKey:"DefaultTimeTable")
    newUserObj.setObject(true,forKey:"isActiveUser")
    newUserObj.setObject(true,forKey:"isManager")
    newUserObj.setObject(user.mail,forKey:"mailBK")
    newUserObj.acl = acl // 全員に読み書き可とする
    // 登録実行
    newUserObj.signUpInBackground({(error) in
        if error != nil {
            print("New User save error : ",error!)
            //print("error.code=",error!.code)
        }
        
    })
    
    return
}


/// Create Manager Information & Group etc
///
/// - parameter : companyData
/// - returns: nothing
func createManager( _ cd:companyData) {
    // ユーザデータ登録
    
    let companyName = cd.company.companyLabel
    let companyID = cd.company.companyID
    let UserID = cd.user.UserID
    let UserName = cd.user.name
    let MBUserName = companyName + "_" + UserID + "_" + UserName
    let UserNameKana = cd.user.nameKana
    let groupName = "管理者"
    let defaultLocation = "本社"
    let defaultTimeTable = "通常勤務"
    
    // Groupその他を作る必要あり
    // 管理者Groupを作る
    var group = groupInfo()
    group.name = groupName
    group.ID = companyID + "-G0"
    addGroup(cd.company, group: group)
    
    // WeeklyCalendar
    var defaultCalendar = weeklyCalendar()
    defaultCalendar.companyName = companyName
    defaultCalendar.companyID = companyID
    defaultCalendar.calendarName = "規定休日"
    defaultCalendar.isDayOffSaturday = true
    defaultCalendar.isDayOffSunday = true
    // その他の曜日はfalseのまま
    addWeeklyCalendar(defaultCalendar)
    
    // Time Table 通常勤務 作る
    var defaultTT = timeTableDef()
    defaultTT.companyName = companyName
    defaultTT.companyID = companyID
    defaultTT.tableName = defaultTimeTable
    defaultTT.timeTableID = companyID + "-TT0"
    // 勤務時間9:00−17:00 昼休み12:00-13:00のデフォルト設定
    addTimeTable(defaultTT)
    
    let acl = NCMBACL()
    // 全員に読み書き可とする
    acl.setPublicReadAccess(true)
    acl.setPublicWriteAccess(true)
    
    // Group,Location,WeeklyCalendar
    let newUserObj = NCMBUser()
    newUserObj.userName = MBUserName
    newUserObj.password = cd.user.password
    newUserObj.mailAddress = cd.user.mail
    
    // 任意フィールドに値を設定
    newUserObj.setObject(companyName,forKey:"CompanyName")
    newUserObj.setObject(companyID,forKey:"companyID")
    newUserObj.setObject(groupName,forKey:"GroupName")
    newUserObj.setObject(UserID,forKey:"UserID")
    newUserObj.setObject(UserName,forKey:"userNameKanji")
    newUserObj.setObject(UserNameKana,forKey:"userNameKana")
    newUserObj.setObject(cd.user.sex,forKey:"Sex")
    newUserObj.setObject(defaultLocation,forKey:"DefaultLocation")
    newUserObj.setObject(defaultTimeTable,forKey:"DefaultTimeTable")
    newUserObj.setObject(true,forKey:"isActiveUser")
    newUserObj.setObject(true,forKey:"isManager") // Manager
    newUserObj.setObject(cd.user.mail,forKey:"mailBK")
    newUserObj.acl = acl // 全員に読み書き可とする
    // 登録実行
    newUserObj.signUpInBackground({(error) in
        if error != nil {print("New User save error : ",error!)}
    })
    
}



// MARK: - Functions for TimeTable

/// Get TimeTable List with Dictionary
///
/// - parameter companyID: company ID with String & unitTime with Int
/// - returns: timeTables as [tablename:timeTableDef]
func getTimeTableListByID(_ companyID:String) -> [String:timeTableDef] {
    
    var timeTables:[String:timeTableDef] = [:]
    
    let query = NCMBQuery(className: "TimeTable")
    query?.whereKey("companyID", equalTo: companyID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return timeTables
    }
    if results.count > 0 {
        for i in 0 ..< results.count {
            let tblObj = results[i] as? NCMBObject
            var timeTable = timeTableDef()
            
            timeTable.companyID = tblObj?.object(forKey: "companyID")  as? String ?? ""
            timeTable.companyName = tblObj?.object(forKey: "CompanyName")  as? String ?? ""
            timeTable.tableName = tblObj?.object(forKey: "TimeTableName")  as? String ?? ""
            timeTable.timeTableID = tblObj?.object(forKey: "timeTableID")  as? String ?? ""
            timeTable.startTime =  trimSpace(tblObj?.object(forKey: "StartTime") as? String ?? "09:00")
            timeTable.endTime =  trimSpace(tblObj?.object(forKey: "EndTime") as? String ?? "17:00")
            timeTable.recessStart1 =  trimSpace(tblObj?.object(forKey: "recessStart1") as? String ?? "12:00")
            timeTable.recessEnd1 =  trimSpace(tblObj?.object(forKey: "recessEnd1") as? String ?? "13:00")
            timeTable.recessStart2 =  trimSpace(tblObj?.object(forKey: "recessStart2") as? String ?? "")
            timeTable.recessEnd2 =  trimSpace(tblObj?.object(forKey: "recessEnd2") as? String ?? "")
            timeTable.recessStart3 =  trimSpace(tblObj?.object(forKey: "recessStart3") as? String ?? "")
            timeTable.recessEnd3 =  trimSpace(tblObj?.object(forKey: "recessEnd3") as? String ?? "")
            timeTable.recessStart4 =  trimSpace(tblObj?.object(forKey: "recessStart4") as? String ?? "")
            timeTable.recessEnd4 =  trimSpace(tblObj?.object(forKey: "recessEnd4") as? String ?? "")
            timeTable.recessStart5 =  trimSpace(tblObj?.object(forKey: "recessStart5") as? String ?? "")
            timeTable.recessEnd5 =  trimSpace(tblObj?.object(forKey: "recessEnd5") as? String ?? "")
           
            timeTables[timeTable.tableName] = timeTable
        }
    }
    
    return timeTables
    
}

/// Get TimeTable List with Array
///
/// - parameter companyID: company ID with String & unitTime with Int
/// - returns: timeTables as [timeTableDef]
func getTimeTableListArray(_ companyID:String) -> [timeTableDef] {
    
    var timeTables:[timeTableDef] = []
    
    let query = NCMBQuery(className: "TimeTable")
    query?.whereKey("companyID", equalTo: companyID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return timeTables
    }
    if results.count > 0 {
        for i in 0 ..< results.count {
            let tblObj = results[i] as? NCMBObject
            var timeTable = timeTableDef()
            
            timeTable.companyID = tblObj?.object(forKey: "companyID")  as? String ?? ""
            timeTable.companyName = tblObj?.object(forKey: "CompanyName")  as? String ?? ""
            timeTable.tableName = tblObj?.object(forKey: "TimeTableName")  as? String ?? ""
            timeTable.timeTableID = tblObj?.object(forKey: "timeTableID")  as? String ?? ""
            timeTable.startTime =  trimSpace(tblObj?.object(forKey: "StartTime") as? String ?? "09:00")
            timeTable.endTime =  trimSpace(tblObj?.object(forKey: "EndTime") as? String ?? "17:00")
            timeTable.recessStart1 =  trimSpace(tblObj?.object(forKey: "recessStart1") as? String ?? "12:00")
            timeTable.recessEnd1 =  trimSpace(tblObj?.object(forKey: "recessEnd1") as? String ?? "13:00")
            timeTable.recessStart2 =  trimSpace(tblObj?.object(forKey: "recessStart2") as? String ?? "")
            timeTable.recessEnd2 =  trimSpace(tblObj?.object(forKey: "recessEnd2") as? String ?? "")
            timeTable.recessStart3 =  trimSpace(tblObj?.object(forKey: "recessStart3") as? String ?? "")
            timeTable.recessEnd3 =  trimSpace(tblObj?.object(forKey: "recessEnd3") as? String ?? "")
            timeTable.recessStart4 =  trimSpace(tblObj?.object(forKey: "recessStart4") as? String ?? "")
            timeTable.recessEnd4 =  trimSpace(tblObj?.object(forKey: "recessEnd4") as? String ?? "")
            timeTable.recessStart5 =  trimSpace(tblObj?.object(forKey: "recessStart5") as? String ?? "")
            timeTable.recessEnd5 =  trimSpace(tblObj?.object(forKey: "recessEnd5") as? String ?? "")
            
            timeTables += [timeTable]
        }
    }
    
    return timeTables
    
}

/// add timetable record to TimeTable Class
///
/// - parameter : timeTableDef
/// - returns:  nothing
func addTimeTable(_ timetable:timeTableDef) {
    
    let obj = NCMBObject(className: "TimeTable")
    
    obj?.setObject(timetable.companyName,forKey:"CompanyName")
    obj?.setObject(timetable.companyID,forKey:"companyID")
    obj?.setObject(timetable.tableName,forKey:"TimeTableName")
    obj?.setObject(timetable.timeTableID,forKey:"timeTableID")
    obj?.setObject(timetable.startTime, forKey: "StartTime")
    obj?.setObject(timetable.endTime, forKey: "EndTime")
    obj?.setObject(timetable.recessStart1, forKey: "recessStart1")
    obj?.setObject(timetable.recessEnd1, forKey: "recessEnd1")
    obj?.setObject(timetable.recessStart2, forKey: "recessStart2")
    obj?.setObject(timetable.recessEnd2, forKey: "recessEnd2")
    obj?.setObject(timetable.recessStart3, forKey: "recessStart3")
    obj?.setObject(timetable.recessEnd3, forKey: "recessEnd3")
    obj?.setObject(timetable.recessStart4, forKey: "recessStart4")
    obj?.setObject(timetable.recessEnd4, forKey: "recessEnd4")
    obj?.setObject(timetable.recessStart5, forKey: "recessStart5")
    obj?.setObject(timetable.recessEnd5, forKey: "recessEnd5")
    
    obj?.saveInBackground({(error) in
        if error != nil {print("TimeTable data save error : ",error!)}
    })
    
}

/// update timetable record of TimeTable Class
///
/// - parameter : timeTableDef
/// - returns:  nothing
func updateTimeTable(_ timetable:timeTableDef) -> Bool {
    
    let query = NCMBQuery(className: "TimeTable")
    query?.whereKey("companyID", equalTo: timetable.companyID)
    query?.whereKey("timeTableID", equalTo: timetable.timeTableID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return false
    }
    if results.count > 0 {
        let obj = results[0] as! NCMBObject
        
        obj.setObject(timetable.companyName,forKey:"CompanyName")
        obj.setObject(timetable.companyID,forKey:"companyID")
        obj.setObject(timetable.tableName,forKey:"TimeTableName")
        obj.setObject(timetable.timeTableID,forKey:"timeTableID")
        obj.setObject(timetable.startTime, forKey: "StartTime")
        obj.setObject(timetable.endTime, forKey: "EndTime")
        obj.setObject(timetable.recessStart1, forKey: "recessStart1")
        obj.setObject(timetable.recessEnd1, forKey: "recessEnd1")
        obj.setObject(timetable.recessStart2, forKey: "recessStart2")
        obj.setObject(timetable.recessEnd2, forKey: "recessEnd2")
        obj.setObject(timetable.recessStart3, forKey: "recessStart3")
        obj.setObject(timetable.recessEnd3, forKey: "recessEnd3")
        obj.setObject(timetable.recessStart4, forKey: "recessStart4")
        obj.setObject(timetable.recessEnd4, forKey: "recessEnd4")
        obj.setObject(timetable.recessStart5, forKey: "recessStart5")
        obj.setObject(timetable.recessEnd5, forKey: "recessEnd5")
        
        obj.saveInBackground({(error) in
            if error != nil {print("Save error : ",error!)}
        })
        
        return true
        
    }
    return false
}

/// upgrade timetable record of TimeTable Class
///
/// - parameter : timeTableDef
/// - returns:  nothing
func upgradeTimeTable(_ timetable:timeTableDef) -> Bool {
    
    let query = NCMBQuery(className: "TimeTable")
    query?.whereKey("companyID", equalTo: timetable.companyID)
    query?.whereKey("TimeTableName", equalTo: timetable.tableName)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return false
    }
    if results.count > 0 {
        let obj = results[0] as! NCMBObject
        
        obj.setObject(timetable.timeTableID,forKey:"timeTableID")
        
        obj.saveInBackground({(error) in
            if error != nil {print("Save error : ",error!)}
        })
        
        return true
        
    }
    return false
}


/// remove timetable record of TimeTable Class
///
/// - parameter : timeTableDef
/// - returns:  nothing
func removeTimeTable(_ timetable:timeTableDef) -> Bool {
    
    let query = NCMBQuery(className: "TimeTable")
    query?.whereKey("companyID", equalTo: timetable.companyID)
    query?.whereKey("timeTableID", equalTo: timetable.timeTableID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return false
    }
    if results.count > 0 {
        let obj = results[0] as! NCMBObject
        
        obj.deleteInBackground({(error) in
            if error != nil {print("Delete object error : ",error!)}
        })
        
        return true
        
    }
    return false
}



// MARK: - Functions for License

/// add License record to License Class
///
/// - parameter : userInfo
/// - returns:  nothing
func addLicense(_ user:userInfo) {
    
    let licObject = NCMBObject(className: "License")
    
    licObject?.setObject(user.companyID,forKey:"companyID")
    licObject?.setObject(user.companyName,forKey:"CompanyName")
    licObject?.setObject(user.UserID,forKey:"owner")
    licObject?.setObject(false,forKey:"isLicenseAvailableSmall")
    licObject?.setObject(false,forKey:"isLicenseAvailableMedium")
    licObject?.setObject(false,forKey:"isLicenseAvailableLarge")
    
    licObject?.saveInBackground({(error) in
        if error != nil {print("Save error : ",error!)}
    })
    
}

/// get License record from License Class
///
/// - parameter : companyInfo
/// - returns:  licenseDef
func getLicense(_ company:companyInfo) -> licenseDef {
    var license = licenseDef()
    
    let query = NCMBQuery(className: "License")
    query?.whereKey("companyID", equalTo: company.companyID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return license
    }
    if results.count > 0 {
        let licenseData = results[0] as? NCMBObject
        license.companyID = licenseData?.object(forKey: "companyID")  as? String ?? ""
        license.companyName = licenseData?.object(forKey: "CompanyName")  as? String ?? ""
        license.owner = licenseData?.object(forKey: "owner")  as? String ?? ""
        license.smallLicense.isLicenseAvailable = licenseData?.object(forKey: "isLicenseAvailableSmall")  as? Bool ?? false
        license.smallLicense.term = licenseData?.object(forKey: "termSmall")  as? Int ?? 0
        license.smallLicense.ExpireDate = licenseData?.object(forKey: "ExpireDateSmall")  as? Date
        license.smallLicense.isRenewAutomatically = licenseData?.object(forKey: "isRenewAutomaticallySmall")  as? Bool
        license.smallLicense.LicenseCount = licenseData?.object(forKey: "LicenseCountSmall")  as? Int ?? smallLicenseCount
        license.mediumLicense.isLicenseAvailable = licenseData?.object(forKey: "isLicenseAvailableMedium")  as? Bool ?? false
        license.mediumLicense.term = licenseData?.object(forKey: "termMedium")  as? Int ?? 0
        license.mediumLicense.ExpireDate = licenseData?.object(forKey: "ExpireDateMedium")  as? Date
        license.mediumLicense.isRenewAutomatically = licenseData?.object(forKey: "isRenewAutomaticallyMedium")  as? Bool
        license.mediumLicense.LicenseCount = licenseData?.object(forKey: "LicenseCountMedium")  as? Int ?? mediumLicenseCount
        license.largeLicense.isLicenseAvailable = licenseData?.object(forKey: "isLicenseAvailableLarge")  as? Bool ?? false
        license.largeLicense.term = licenseData?.object(forKey: "termLarge")  as? Int ?? 0
        license.largeLicense.ExpireDate = licenseData?.object(forKey: "ExpireDateLarge")  as? Date
        license.largeLicense.isRenewAutomatically = licenseData?.object(forKey: "isRenewAutomaticallyLarge")  as? Bool
        license.largeLicense.LicenseCount = licenseData?.object(forKey: "LicenseCountLarge")  as? Int ?? largeLicenseCount
        license.isLicensed = license.smallLicense.isLicenseAvailable || license.mediumLicense.isLicenseAvailable || license.largeLicense.isLicenseAvailable
        license.limitOfGroup = licenseData?.object(forKey: "limitOfGroup")  as? Int ?? limitCountOfGroup
        license.limitOfLocation = licenseData?.object(forKey: "limitOfLocation")  as? Int ?? limitCountOfLocation
        license.limitOfTimetable = licenseData?.object(forKey: "limitOfTimetable")  as? Int ?? limitCountOfTimetable

        
    }
    return license
   
}

/// update License record of License Class
///
/// - parameter : companyInfo , licenseDef
/// - returns:  nothing
func updateLicense(_ company:companyInfo,license:licenseDef) {
    
    let query = NCMBQuery(className: "License")
    query?.whereKey("companyID", equalTo: company.companyID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return
    }
    if results.count > 0 {
        let licObject = results[0] as! NCMBObject
        
        licObject.setObject(license.smallLicense.isLicenseAvailable,forKey:"isLicenseAvailableSmall")
        licObject.setObject(license.smallLicense.term,forKey:"termSmall")
        licObject.setObject(license.smallLicense.ExpireDate,forKey:"ExpireDateSmall")
        licObject.setObject(license.smallLicense.isRenewAutomatically,forKey:"isRenewAutomaticallySmall")
        licObject.setObject(license.smallLicense.LicenseCount,forKey:"LicenseCountSmall")
        licObject.setObject(license.mediumLicense.isLicenseAvailable,forKey:"isLicenseAvailableMedium")
        licObject.setObject(license.mediumLicense.term,forKey:"termMedium")
        licObject.setObject(license.mediumLicense.ExpireDate,forKey:"ExpireDateMedium")
        licObject.setObject(license.mediumLicense.isRenewAutomatically,forKey:"isRenewAutomaticallyMedium")
        licObject.setObject(license.mediumLicense.LicenseCount,forKey:"LicenseCountMedium")
        licObject.setObject(license.largeLicense.isLicenseAvailable,forKey:"isLicenseAvailableLarge")
        licObject.setObject(license.largeLicense.term,forKey:"termLarge")
        licObject.setObject(license.largeLicense.ExpireDate,forKey:"ExpireDateLarge")
        licObject.setObject(license.largeLicense.isRenewAutomatically,forKey:"isRenewAutomaticallyLarge")
        licObject.setObject(license.largeLicense.LicenseCount,forKey:"LicenseCountLarge")
        
        licObject.saveInBackground({(error) in
            if error != nil {print("Save error : ",error!)}
        })
       
    }
}

// MARK: - Functions for Group

/// Get Group List
///
/// - parameter companyID:
/// - returns: users as [groupInfo]
func getGroupListAllByID(_ companyID:String) -> [groupInfo] {
    
    var groups:[groupInfo] = []
    
    let query = NCMBQuery(className: "Group")
    query?.whereKey("companyID", equalTo: companyID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return groups
    }
    if results.count > 0 {
        for i in 0  ..< results.count {
            let groupData = results[i] as? NCMBObject
            var group = groupInfo()
            
            group.name = groupData?.object(forKey: "GroupName")  as? String ?? ""
            group.ID = groupData?.object(forKey: "groupID")  as? String ?? ""
            
            groups += [group]
        }
    }
    return groups
    
}


/// add group record to Group Class
///
/// - parameter : companyInfo,Group name
/// - returns:  nothing
func addGroup(_ company:companyInfo,group:groupInfo) {
    
    let obj = NCMBObject(className: "Group")
    
    obj?.setObject(company.companyName,forKey:"CompanyName")
    obj?.setObject(company.companyID,forKey:"companyID")
    obj?.setObject(group.name,forKey:"GroupName")
    obj?.setObject(group.ID,forKey:"groupID")
   
    obj?.saveInBackground({(error) in
        if error != nil {print("Save error : ",error!)}
    })
    
}

/// update group of Group Class
///
/// - parameter : companyInfo & groupName
/// - returns:  true: success,false :error
func updateGroup(_ company:companyInfo,group:groupInfo) -> Bool {
    
    let query = NCMBQuery(className: "Group")
    query?.whereKey("companyID", equalTo: company.companyID)
    query?.whereKey("groupID", equalTo: group.ID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return false
    }
    if results.count > 0 {
        let obj = results[0] as! NCMBObject
        
        obj.setObject(group.name,forKey:"GroupName")
        
        obj.saveInBackground({(error) in
            if error != nil {print("Save error : ",error!)}
        })
        return true
        
    }
    return false
}
/// remove group of Group Class
///
/// - parameter : companyInfo & groupName
/// - returns:  true: success,false :error
func removeGroup(_ companyID:String,group:groupInfo) -> Bool {
    
    let query = NCMBQuery(className: "Group")
    query?.whereKey("companyID", equalTo: companyID)
    query?.whereKey("groupID", equalTo: group.ID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return false
    }
    if results.count > 0 {
        let obj = results[0] as! NCMBObject
        
        var error:NSError? = nil
        obj.delete(&error)
        if error != nil {
            print(error.debugDescription)
            return false
        }
        return true
        
    }
    return false
}
/// upgrade group of Group Class
///
/// - parameter : companyInfo & groupName
/// - returns:  true: success,false :error
func upgradeGroup(_ company:companyInfo,group:groupInfo) -> Bool {
    
    let query = NCMBQuery(className: "Group")
    query?.whereKey("companyID", equalTo: company.companyID)
    query?.whereKey("GroupName", equalTo: group.name)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return false
    }
    if results.count > 0 {
        let obj = results[0] as! NCMBObject
        
        obj.setObject(group.ID,forKey:"groupID")
        
        obj.saveInBackground({(error) in
            if error != nil {print("Save error : ",error!)}
        })
        return true
        
    }
    return false
}

// MARK: - Functions for WeeklySchedule

/// get weeklySchedule record from weeklySchedule Class
///
/// - parameter : PersonalUserID
/// - returns:  weeklySchedule
func getWeeklySchedule(_ userID:String) -> weeklySchedule {
    var schedule = weeklySchedule()
    
    let query = NCMBQuery(className: "WeeklySchedule")
    query?.whereKey("personID", equalTo: userID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return schedule
    }
    if results.count > 0 {
        let schData = results[0] as? NCMBObject

        schedule.scheduleID = schData?.object(forKey: "scheduleID")  as? String ?? ""
        schedule.workPlaceID = schData?.object(forKey: "workPlaceID")  as? String ?? ""
        schedule.personID = schData?.object( forKey: "personID")  as? String ?? ""
        schedule.isOn_Mon = schData?.object( forKey: "isOn_Mon")  as? Bool ?? false
        schedule.WP_Mon = schData?.object( forKey: "WP_Mon")  as? String ?? ""
        schedule.Loc_Mon = schData?.object( forKey: "Loc_Mon")  as? String ?? ""
        schedule.TT_Mon = schData?.object( forKey: "TT_Mon")  as? String ?? ""
        schedule.isOn_Tue = schData?.object( forKey: "isOn_Tue")  as? Bool ?? false
        schedule.WP_Tue = schData?.object( forKey: "WP_Tue")  as? String ?? ""
        schedule.Loc_Tue = schData?.object( forKey: "Loc_Tue")  as? String ?? ""
        schedule.TT_Tue = schData?.object( forKey: "TT_Tue")  as? String ?? ""
        schedule.isOn_Wed = schData?.object( forKey: "isOn_Wed")  as? Bool ?? false
        schedule.WP_Wed = schData?.object( forKey: "WP_Wed")  as? String ?? ""
        schedule.Loc_Wed = schData?.object( forKey: "Loc_Wed")  as? String ?? ""
        schedule.TT_Wed = schData?.object( forKey: "TT_Wed")  as? String ?? ""
        schedule.isOn_Thu = schData?.object( forKey: "isOn_Thu")  as? Bool ?? false
        schedule.WP_Thu = schData?.object( forKey: "WP_Thu")  as? String ?? ""
        schedule.Loc_Thu = schData?.object( forKey: "Loc_Thu")  as? String ?? ""
        schedule.TT_Thu = schData?.object( forKey: "TT_Thu")  as? String ?? ""
        schedule.isOn_Fri = schData?.object( forKey: "isOn_Fri")  as? Bool ?? false
        schedule.WP_Fri = schData?.object( forKey: "WP_Fri")  as? String ?? ""
        schedule.Loc_Fri = schData?.object( forKey: "Loc_Fri")  as? String ?? ""
        schedule.TT_Fri = schData?.object( forKey: "TT_Fri")  as? String ?? ""
        schedule.isOn_Sat = schData?.object( forKey: "isOn_Sat")  as? Bool ?? false
        schedule.WP_Sat = schData?.object( forKey: "WP_Sat")  as? String ?? ""
        schedule.Loc_Sat = schData?.object( forKey: "Loc_Sat")  as? String ?? ""
        schedule.TT_Sat = schData?.object( forKey: "TT_Sat")  as? String ?? ""
        schedule.isOn_Sun = schData?.object( forKey: "isOn_Sun")  as? Bool ?? false
        schedule.WP_Sun = schData?.object( forKey: "WP_Sun")  as? String ?? ""
        schedule.Loc_Sun = schData?.object( forKey: "Loc_Sun")  as? String ?? ""
        schedule.TT_Sun = schData?.object( forKey: "TT_Sun")  as? String ?? ""
        schedule.upDateDayOff()

    }
    return schedule
    
}

/// add weekly shedule record to WeeklySchedule Class
///
/// - parameter : weeklySchedule
/// - returns:  nothing
func addWeeklySchedule(_ schedule:weeklySchedule) {
    
    let obj = NCMBObject(className: "WeeklySchedule")
    
    obj?.setObject(schedule.scheduleID,forKey:"scheduleID")
    obj?.setObject(schedule.workPlaceID,forKey:"workPlaceID")
    obj?.setObject(schedule.personID, forKey: "personID")
    obj?.setObject(schedule.isOn_Mon, forKey: "isOn_Mon")
    obj?.setObject(schedule.WP_Mon, forKey: "WP_Mon")
    obj?.setObject(schedule.Loc_Mon, forKey: "Loc_Mon")
    obj?.setObject(schedule.TT_Mon, forKey: "TT_Mon")
    obj?.setObject(schedule.isOn_Tue, forKey: "isOn_Tue")
    obj?.setObject(schedule.WP_Tue, forKey: "WP_Tue")
    obj?.setObject(schedule.Loc_Tue, forKey: "Loc_Tue")
    obj?.setObject(schedule.TT_Tue, forKey: "TT_Tue")
    obj?.setObject(schedule.isOn_Wed, forKey: "isOn_Wed")
    obj?.setObject(schedule.WP_Wed, forKey: "WP_Wed")
    obj?.setObject(schedule.Loc_Wed, forKey: "Loc_Wed")
    obj?.setObject(schedule.TT_Wed, forKey: "TT_Wed")
    obj?.setObject(schedule.isOn_Thu, forKey: "isOn_Thu")
    obj?.setObject(schedule.WP_Thu, forKey: "WP_Thu")
    obj?.setObject(schedule.Loc_Thu, forKey: "Loc_Thu")
    obj?.setObject(schedule.TT_Thu, forKey: "TT_Thu")
    obj?.setObject(schedule.isOn_Fri, forKey: "isOn_Fri")
    obj?.setObject(schedule.WP_Fri, forKey: "WP_Fri")
    obj?.setObject(schedule.Loc_Fri, forKey: "Loc_Fri")
    obj?.setObject(schedule.TT_Fri, forKey: "TT_Fri")
    obj?.setObject(schedule.isOn_Sat, forKey: "isOn_Sat")
    obj?.setObject(schedule.WP_Sat, forKey: "WP_Sat")
    obj?.setObject(schedule.Loc_Sat, forKey: "Loc_Sat")
    obj?.setObject(schedule.TT_Sat, forKey: "TT_Sat")
    obj?.setObject(schedule.isOn_Sun, forKey: "isOn_Sun")
    obj?.setObject(schedule.WP_Sun, forKey: "WP_Sun")
    obj?.setObject(schedule.Loc_Sun, forKey: "Loc_Sun")
    obj?.setObject(schedule.TT_Sun, forKey: "TT_Sun")
    
    obj?.saveInBackground({(error) in
        if error != nil {print("WeeklySchedule data save error : ",error!)}
    })
    
}

/// update weekly Schedule record of WeeklySchedule Class
///
/// - parameter : weeklySchedule
/// - returns:  nothing
func updateWeeklySchedule(_ schedule:weeklySchedule) {
    
    let query = NCMBQuery(className: "WeeklySchedule")
    query?.whereKey("scheduleID", equalTo: schedule.scheduleID)
    var results:[AnyObject] = []
    do {
        results = try query!.findObjects() as [AnyObject]
    } catch  let error1 as NSError  {
        print("\(error1)")
        return
    }
    if results.count > 0 {
        let obj = results[0] as! NCMBObject
        
        obj.setObject(schedule.workPlaceID,forKey:"workPlaceID")
        obj.setObject(schedule.personID, forKey: "personID")
        obj.setObject(schedule.isOn_Mon, forKey: "isOn_Mon")
        obj.setObject(schedule.WP_Mon, forKey: "WP_Mon")
        obj.setObject(schedule.Loc_Mon, forKey: "Loc_Mon")
        obj.setObject(schedule.TT_Mon, forKey: "TT_Mon")
        obj.setObject(schedule.isOn_Tue, forKey: "isOn_Tue")
        obj.setObject(schedule.WP_Tue, forKey: "WP_Tue")
        obj.setObject(schedule.Loc_Tue, forKey: "Loc_Tue")
        obj.setObject(schedule.TT_Tue, forKey: "TT_Tue")
        obj.setObject(schedule.isOn_Wed, forKey: "isOn_Wed")
        obj.setObject(schedule.WP_Wed, forKey: "WP_Wed")
        obj.setObject(schedule.Loc_Wed, forKey: "Loc_Wed")
        obj.setObject(schedule.TT_Wed, forKey: "TT_Wed")
        obj.setObject(schedule.isOn_Thu, forKey: "isOn_Thu")
        obj.setObject(schedule.WP_Thu, forKey: "WP_Thu")
        obj.setObject(schedule.Loc_Thu, forKey: "Loc_Thu")
        obj.setObject(schedule.TT_Thu, forKey: "TT_Thu")
        obj.setObject(schedule.isOn_Fri, forKey: "isOn_Fri")
        obj.setObject(schedule.WP_Fri, forKey: "WP_Fri")
        obj.setObject(schedule.Loc_Fri, forKey: "Loc_Fri")
        obj.setObject(schedule.TT_Fri, forKey: "TT_Fri")
        obj.setObject(schedule.isOn_Sat, forKey: "isOn_Sat")
        obj.setObject(schedule.WP_Sat, forKey: "WP_Sat")
        obj.setObject(schedule.Loc_Sat, forKey: "Loc_Sat")
        obj.setObject(schedule.TT_Sat, forKey: "TT_Sat")
        obj.setObject(schedule.isOn_Sun, forKey: "isOn_Sun")
        obj.setObject(schedule.WP_Sun, forKey: "WP_Sun")
        obj.setObject(schedule.Loc_Sun, forKey: "Loc_Sun")
        obj.setObject(schedule.TT_Sun, forKey: "TT_Sun")
        
        obj.saveInBackground({(error) in
            if error != nil {print("Save error : ",error!)}
        })
        
    }
}


// MARK: - Functions for ImageData

/// add ImageData to File Store
///
/// - parameter : image as UIImage, name of data as String
/// - returns:  nothing
func setStampImage(_ image:UIImage,name:String) {
    let appDl:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得
    
    // imageをNSDataに変換
    let imageData:Data = NSData(data: UIImagePNGRepresentation(image)!) as Data
    appDl.cacheImageLocaly(imageData,fileName:name)
    let fileName = name + ".png"
    let file:NCMBFile = NCMBFile.file(withName: fileName ,data: imageData) as! NCMBFile
    let acl = NCMBACL()
    // 全員に読み書き可とする
    acl.setPublicReadAccess(true)
    acl.setPublicWriteAccess(true)
    file.acl = acl
    
    var error1 : NSError?
    file.save(&error1)
    if error1 != nil  {
        print("Image data save error : ",error1!)
    }
}

/// Read ImageData from File Store
///
/// - parameter : name of data as String
/// - returns:  image with UIImage
func getStampImage(_ name:String) -> UIImage? {
    
    var image:UIImage? = nil
    var imageData:Data?
    let fileName = name + ".png"
    let appDl:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得
   
    imageData = appDl.restoreImageFromCache(name)
    if imageData == nil {
        print("ImageData read Error from cache :",fileName)
        let file:NCMBFile = NCMBFile.file(withName: fileName ,data: nil ) as! NCMBFile
        
        do {
            imageData = try file.getData()
        } catch  let error1 as NSError  {
            print("Image data read error from NCMB : ",error1)
            return image
        }
        appDl.cacheImageLocaly(imageData!,fileName:name)
    }
    image = UIImage(data: imageData!)
    
    return image
    
}



/*
/// Read today's ImageDatas from File Store
///
/// - parameter : name of data as String
/// - returns:  image with UIImage
func loadStampImageForCache(companyID:String,date:String) -> [String:NSData] {
    
    
    //let stampPictName = companyID + "_" + selectedMemberData.UserID + "_" +  + "_S"
    
    let query = NCMBFile.query()
    
    query.whereKey("filename", equalTo: companyID)
    query.whereKey("filename", equalTo: companyID)
    var results:[AnyObject] = []
    do {
        results = try query.findObjects()
    } catch  let error1 as NSError  {
        print("\(error1)")
        return false
    }
    if results.count > 0 {
        let obj = results[0] as! NCMBObject
        
        obj.setObject(holiday.calendarName, forKey: "CalendarName")
        obj.setObject(holiday.holidayName, forKey: "Name")
        obj.setObject(holiday.memo, forKey: "Memo")
        obj.setObject(holiday.isPublic, forKey: "isPublic")
        obj.setObject(holiday.isOnWork, forKey: "isOnWork")
        
        obj.saveInBackgroundWithBlock({(error) in
            if error != nil {print("Save error : ",error!)}
        })
        return true
        
    }
    return false


    
    var image:UIImage? = nil
    var imageData:NSData?
    let fileName = name + ".png"
    
    imageData = commonFunction().restoreImageFromCache(name)
    if imageData == nil {
        print("ImageData read Error")
        let file:NCMBFile = NCMBFile.fileWithName(fileName ,data: nil ) as! NCMBFile
        
        do {
            imageData = try file.getData()
        } catch  let error1 as NSError  {
            print("Image data read error : ",error!1)
            return image
        }
        
    }
    
    image = UIImage(data: imageData!)
    
    return image
    
}
*/






