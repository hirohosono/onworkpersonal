//
//  StateManager.swift
//  OnWork
//
//  Created by 細野浩明 on 2016/11/06.
//  Copyright © 2016年 Hiroaki Hosono. All rights reserved.
//

import Foundation

class StateManager {
    struct userState {
        var isReaded:Bool = false           // ユーザ情報がUserDefaultsから読み込み済みか？
        var isLoaded:Bool = false           // ユーザ情報がUserObjectから読み込み済みか？
        var isInitiated:Bool = false        // 入力済みか？（ローカルに）
        var isRegistered:Bool = false       // 登録済みか？
        var isAuthorized:Bool = false       // メール認証済みか？
        var isLoggedIn:Bool = false         // ログイン済みか？
        var isUserChanged:Bool = false      // ユーザが切り替わった？

    }
    struct loadedState {
        var basic:Bool = false              // 会社基本情報が読み込み済みか？
        var today:Bool = false              // 本日の日付情報が読み込み済みか？
        var license:Bool = false            // ライセンス情報が読み込み済みか？
        var groups:Bool = false             // グループ一覧情報が読み込み済みか？
        var locations:Bool = false          // 勤務地一覧が情報が読み込み済みか？
        var members:Bool = false            // メンバー一覧情報が読み込み済みか？
        var timetables:Bool = false         // タイムテーブル一覧情報が読み込み済みか？
        var weeklyCalendar:Bool = false     // 週間カレンダーが読み込み済みか？
        var lastMonthCal:Bool = false       // 先月の月間カレンダーが読み込み済みか？
        var thisMonthCal:Bool = false       // 今月の月間カレンダーが読み込み済みか？
        var nextMonthCal:Bool = false       // 来月の月間カレンダーが読み込み済みか？
        var todaysLog:Bool = false          // 今日のログ情報が読み込み済みか？
        var lastMonthLog:Bool = false       // 先月のログ情報が読み込み済みか？
        var thisMonthLog:Bool = false       // 今月のログ情報が読み込み済みか？
        var nextMonthLog:Bool = false       // 来月のログ情報が読み込み済みか？
    }
    struct assortState {
        var groups:Bool = false             // グループの箱が作成済みか？
        var locations:Bool = false          // 勤務地の箱が作成済みか？
        var shifts:Bool = false             // シフトの箱が作成済みか？
    }
    struct fillState {
        var groups:Bool = false             // グループへの振り分けが実施済みか？
        var locations:Bool = false          // 勤務地への振り分けが実施済みか？
        var shifts:Bool = false             // タイムテーブルへの振り分けが実施済みか？
    }

    struct companyState {
        var isLoaded = loadedState()
        var isAssorted = assortState()
        var isLicenseChecked:Bool = false

    }
    
    var user = userState()
    var company = companyState()
    
}
