//
//  commonFunction.swift
//  OnWork
//
//  Created by 細野浩明 on 2015/12/24.
//  Copyright © 2015年 Hiroaki Hosono. All rights reserved.
//

import Foundation
import UIKit


class commonFunction {
    let dtc = dateTimeControll()
    let calendar = Calendar(identifier: Calendar.Identifier.gregorian)

    /// Read user imformation from NCMBUser object to appDl.user
    ///
    /// - parameter 無し
    /// - returns: userInfo
    func getUserInfo() -> userInfo{
        // ログインユーザの情報を取得
        var user = userInfo()
        user.MBuserName = NCMBUser.current().userName
        user.mail = NCMBUser.current().mailAddress ?? ""
        user.name = NCMBUser.current().object(forKey: "userNameKanji") as? String ?? ""
        user.companyName = NCMBUser.current().object(forKey: "CompanyName") as? String ?? ""
        user.companyID = NCMBUser.current().object(forKey: "companyID") as? String ?? ""
        user.groupName = NCMBUser.current().object(forKey: "GroupName") as? String ?? ""
        user.UserID = NCMBUser.current().object(forKey: "UserID") as? String ?? ""
        user.nameKana = NCMBUser.current().object(forKey: "userNameKana") as? String ?? ""
        user.sex = NCMBUser.current().object(forKey: "Sex") as? String ?? ""
        user.defaultLocation = NCMBUser.current().object(forKey: "DefaultLocation") as? String ?? ""
        user.defaultTimeTable = NCMBUser.current().object(forKey: "DefaultTimeTable") as? String ?? ""
        user.isActiveUser = NCMBUser.current().object(forKey: "isActiveUser") as? Bool ?? false
        user.isManager = NCMBUser.current().object(forKey: "isManager") as? Bool ?? false
        //user.isLoggedIn = true
        user.isAuthenticated = NCMBUser.current().object(forKey: "mailAddressConfirm") as? Bool ?? false
        user.mailBK = NCMBUser.current().object(forKey: "mailBK") as? String ?? ""

        return user
        
    }
    /// Read user imformation from NCMBUser object to appDl.user
    ///
    /// - parameter 無し
    /// - returns: userInfo
    func suppleUserInfo(_ suppleUser:userInfo) -> userInfo{
        // ログインユーザの情報を取得
        var user = suppleUser
        user.MBuserName = NCMBUser.current().userName
        user.mail = NCMBUser.current().mailAddress ?? suppleUser.mail
        user.name = NCMBUser.current().object(forKey: "userNameKanji") as? String ?? suppleUser.name
        user.companyName = NCMBUser.current().object(forKey: "CompanyName") as? String ?? suppleUser.companyName
        user.companyID = NCMBUser.current().object(forKey: "companyID") as? String ?? suppleUser.companyID
        user.groupName = NCMBUser.current().object(forKey: "GroupName") as? String ?? suppleUser.groupName
        user.UserID = NCMBUser.current().object(forKey: "UserID") as? String ?? suppleUser.UserID
        user.nameKana = NCMBUser.current().object(forKey: "userNameKana") as? String ?? suppleUser.nameKana
        user.sex = NCMBUser.current().object(forKey: "Sex") as? String ?? suppleUser.sex
        user.defaultLocation = NCMBUser.current().object(forKey: "DefaultLocation") as? String ?? suppleUser.defaultLocation
        user.defaultTimeTable = NCMBUser.current().object(forKey: "DefaultTimeTable") as? String ?? suppleUser.defaultTimeTable
        user.isActiveUser = NCMBUser.current().object(forKey: "isActiveUser") as? Bool ?? suppleUser.isActiveUser
        user.isManager = NCMBUser.current().object(forKey: "isManager") as? Bool ?? suppleUser.isManager
        //user.isLoggedIn = suppleUser.isLoggedIn
        user.isAuthenticated = NCMBUser.current().object(forKey: "mailAddressConfirm") as? Bool ?? false
        user.mailBK = NCMBUser.current().object(forKey: "mailBK") as? String ?? ""

        return user
        
    }

    /// Read user imformation from NCMBUser object
    ///
    /// - parameter :personalUserInfo
    /// - returns: personalUserInfo
    func getPersonalUserInfo(_ OldUser:personalUserInfo) -> personalUserInfo{
        // ログインユーザの情報を取得
        var user = OldUser
        if NCMBUser.current() != nil {
            user.mail = NCMBUser.current().mailAddress ?? ""
            user.name = NCMBUser.current().object(forKey: "userNameKanji") as? String ?? ""
            user.companyName = NCMBUser.current().object(forKey: "CompanyName") as? String ?? ""
            user.companyID = NCMBUser.current().object(forKey: "companyID") as? String ?? ""
            user.groupName = NCMBUser.current().object(forKey: "GroupName") as? String ?? ""
            user.IDNo = NCMBUser.current().object(forKey: "UserID") as? String ?? ""
            user.nameKana = NCMBUser.current().object(forKey: "userNameKana") as? String ?? ""
            user.sex = NCMBUser.current().object(forKey: "Sex") as? String ?? ""
            user.defaultLocation = NCMBUser.current().object(forKey: "DefaultLocation") as? String ?? ""
            user.defaultTimeTable = NCMBUser.current().object(forKey: "DefaultTimeTable") as? String ?? ""
            user.isLoggedIn = true
            user.isAuthenticated = NCMBUser.current().object(forKey: "mailAddressConfirm") as? Bool ?? false
            user.mailBK = NCMBUser.current().object(forKey: "mailBK") as? String ?? ""
        } else {
            user.isLoggedIn = false
        }
        
        return user
        
    }

    /// Read date of today and initiate todaysData
    ///
    /// - parameter company:companyInfo
    /// - returns: workDataOfDay
    func initTodaysData(_ company:companyInfo) -> workDataOfDay {
        var todaysData = workDataOfDay()
        let now = Date()
        let dateLineWk = dtc.getDateLine(company.dateLine)
        if dateLineWk.compare(now) == .orderedDescending { // dateLineWk > now
            todaysData.beginningOfDay = Date(timeInterval: -24*60*60, since: dateLineWk) //1日前.
            todaysData.endingOfDay = dateLineWk
        } else {
            todaysData.beginningOfDay = dateLineWk
            todaysData.endingOfDay = Date(timeInterval: 24*60*60, since: dateLineWk) //1日後.
        }
        //appDl.today = todaysData.beginningOfDay
        let date =  dtc.getDateMMDD(todaysData.beginningOfDay!) // MM/DD
        todaysData.dateLabel = date.date
        todaysData.weekdayLabel = date.weekday // (月)
        todaysData.weekdayNum = date.weekdayNum // 曜日の数値

        todaysData.startTimeOfWork = nil
        todaysData.endTimeOfWork = nil
        todaysData.isDayOff = false
        todaysData.location = ""
        todaysData.timeTable = ""
        todaysData.workHours = 0  // minutes
        todaysData.overtimeWorkHours = 0  // minutes
        todaysData.lateNightWorkHours = 0 // minutes
        todaysData.memo = ""
        todaysData.isStartManually = false
        todaysData.isEndManually = false
        todaysData.startLocation = ""
        todaysData.startAddress = ""
        todaysData.endLocation = ""
        todaysData.endAddress = ""
        return todaysData
    }
    
    /// Read date of today and initiate todaysData for PersonalUser
    ///
    /// - parameter workPlace:workPlaceInfo
    /// - returns: workDataOfDay
    func initTodaysData(_ workPlace:workPlaceInfo) -> workDataOfDay {
        var todaysData = workDataOfDay()
        let now = Date()
        let dateLineWk = dtc.getDateLine(workPlace.dateLine)
        if dateLineWk.compare(now) == .orderedDescending { // dateLineWk > now
            todaysData.beginningOfDay = Date(timeInterval: -24*60*60, since: dateLineWk) //1日前.
            todaysData.endingOfDay = dateLineWk
        } else {
            todaysData.beginningOfDay = dateLineWk
            todaysData.endingOfDay = Date(timeInterval: 24*60*60, since: dateLineWk) //1日後.
        }
        //appDl.today = todaysData.beginningOfDay
        let date =  dtc.getDateMMDD(todaysData.beginningOfDay!) // MM/DD
        todaysData.dateLabel = date.date
        todaysData.weekdayLabel = date.weekday // (月)
        todaysData.weekdayNum = date.weekdayNum // 曜日の数値

        todaysData.startTimeOfWork = nil
        todaysData.endTimeOfWork = nil
        todaysData.isDayOff = false
        todaysData.companyID = workPlace.workPlaceID
        todaysData.companyName = workPlace.workPlaceLabel
        todaysData.location = ""
        todaysData.locationID = ""
        todaysData.timeTable = ""
        todaysData.timeTableID = ""
        todaysData.workHours = 0  // minutes
        todaysData.overtimeWorkHours = 0  // minutes
        todaysData.lateNightWorkHours = 0 // minutes
        todaysData.memo = ""
        todaysData.isStartManually = false
        todaysData.isEndManually = false
        todaysData.startLocation = ""
        todaysData.startAddress = ""
        todaysData.endLocation = ""
        todaysData.endAddress = ""
        return todaysData
    }
    
    
    /// Create & set holiday info to appDl.thisMonthList & lastMonthList & ,nextMonthList
    ///
    /// - parameter company:companyInfo
    /// - returns: thisMonthList:[workDataOfDay],lastMonthList:[workDataOfDay],nextMonthList:[workDataOfDay]
    func createDaysWorkList(_ company:companyInfo,isWeekdayOff:[Bool]) -> (thisMonthList:[workDataOfDay],lastMonthList:[workDataOfDay],nextMonthList:[workDataOfDay]){
        // 締め日、日付変更時間の読み出し
        // 今日の始まりの時間を作る
        let beginingOfToday = dtc.getDateLine(company.dateLine)
        // 今月の１日を作る
        var firstDayOfThisMonth = dtc.get1stDay(beginingOfToday)
        ///print("firstDayOfThisMonth=\(firstDayOfThisMonth)")
        // 今月の締め日を作る
        var closeDayOfThisMonth = dtc.getCloseDay(beginingOfToday,closeDay:company.closeDate )
        // 先月の末日を作る
        let lastDayOfLastMonth = dtc.getPriorDate(firstDayOfThisMonth)
        // 先月の１日を作る
        let firstDayOfLastMonth = dtc.get1stDay(lastDayOfLastMonth)
        // 先月の締め日を作る
        var closeDayOfLastMonth = dtc.getCloseDay(firstDayOfLastMonth,closeDay:company.closeDate )
        // 今月の初日を作る
        var openDayOfThisMonth = dtc.getNextDate(closeDayOfLastMonth)
        // 先々月の末日を作る
        let lastDayOfBeforeMonth = dtc.getPriorDate(firstDayOfLastMonth)
        // 先々月の締め日を作る
        let closeDayOfBeforeMonth = dtc.getCloseDay(lastDayOfBeforeMonth,closeDay:company.closeDate )
        // 先月の初日を作る
        var openDayOfLastMonth = dtc.getNextDate(closeDayOfBeforeMonth)
        // 来月の初日を作る
        var openDayOfNextMonth = dtc.getNextDate(closeDayOfThisMonth)
        // 今月の末日を作る
        var lastDayOfThisMonth = dtc.getLastDay(firstDayOfThisMonth)
        // 来月の１日を作る
        var firstDayOfnextMonth = dtc.getNextDate(lastDayOfThisMonth)
        // 来月の締め日を作る
        var closeDayOfNextMonth = dtc.getCloseDay(firstDayOfnextMonth,closeDay:company.closeDate )
        // 今月の締め日を超えていたら月を１ヶ月ずらす
        if beginingOfToday.compare(closeDayOfThisMonth) == .orderedDescending {
            openDayOfLastMonth  = openDayOfThisMonth
            closeDayOfLastMonth = closeDayOfThisMonth
            openDayOfThisMonth  = openDayOfNextMonth
            closeDayOfThisMonth = closeDayOfNextMonth
            firstDayOfThisMonth = firstDayOfnextMonth
            // 来月の初日を作る
            openDayOfNextMonth = dtc.getNextDate(closeDayOfThisMonth)
            // 今月の末日を作る
            lastDayOfThisMonth = dtc.getLastDay(firstDayOfThisMonth)
            // 来月の１日を作る
            firstDayOfnextMonth = dtc.getNextDate(lastDayOfThisMonth)
            // 来月の締め日を作る
            closeDayOfNextMonth = dtc.getCloseDay(firstDayOfnextMonth,closeDay:company.closeDate )
        }
  
        let holidays = getHolidaysByID(company.companyID, isUsePublic: company.isUseNationalHolidays , dateFrom: openDayOfLastMonth)
        var thisMonthList:[workDataOfDay] = []
        var referenceDate = openDayOfThisMonth
        while true { // 当月の締め日前から締め日まで
            var wk = workDataOfDay()
            wk.beginningOfDay = referenceDate
            wk.endingOfDay = dtc.getNextDate(referenceDate)
            (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
            wk.isDayOff = isWeekdayOff[wk.weekdayNum]
            thisMonthList += [wk]
            referenceDate = dtc.getNextDate(referenceDate)
            if calendar.isDate(referenceDate, inSameDayAs: closeDayOfThisMonth) {
                wk.beginningOfDay = referenceDate
                wk.endingOfDay = dtc.getNextDate(referenceDate)
                (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
                wk.isDayOff = isWeekdayOff[wk.weekdayNum]
                thisMonthList += [wk]
                break
            }
        }
        var lastMonthList:[workDataOfDay] = []
        referenceDate = openDayOfLastMonth
        while true { // 当月の締め日前から締め日まで
            var wk = workDataOfDay()
            wk.beginningOfDay = referenceDate
            wk.endingOfDay = dtc.getNextDate(referenceDate)
            (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
            wk.isDayOff = isWeekdayOff[wk.weekdayNum]
            lastMonthList += [wk]
            referenceDate = dtc.getNextDate(referenceDate)
            if calendar.isDate(referenceDate, inSameDayAs: closeDayOfLastMonth) {
                wk.beginningOfDay = referenceDate
                wk.endingOfDay = dtc.getNextDate(referenceDate)
                (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
                wk.isDayOff = isWeekdayOff[wk.weekdayNum]
                lastMonthList += [wk]
                break
            }
        }
        
        var nextMonthList:[workDataOfDay] = []
        referenceDate = openDayOfNextMonth
        while true { // 当月の締め日前から締め日まで
            var wk = workDataOfDay()
            wk.beginningOfDay = referenceDate
            wk.endingOfDay = dtc.getNextDate(referenceDate)
            (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
            wk.isDayOff = isWeekdayOff[wk.weekdayNum]
            nextMonthList += [wk]
            referenceDate = dtc.getNextDate(referenceDate)
            if calendar.isDate(referenceDate, inSameDayAs: closeDayOfNextMonth) {
                wk.beginningOfDay = referenceDate
                wk.endingOfDay = dtc.getNextDate(referenceDate)
                (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
                wk.isDayOff = isWeekdayOff[wk.weekdayNum]
                nextMonthList += [wk]
                break
            }
        }
        

        for holiday in holidays {
            for i in 0 ..< thisMonthList.count {
                if calendar.isDate(holiday.date, inSameDayAs: thisMonthList[i].beginningOfDay!) {
                    thisMonthList[i].isDayOff = !holiday.isOnWork
                    thisMonthList[i].holidayName = holiday.holidayName
                    thisMonthList[i].additional = holiday.memo
                    break
                }
            }
            for i in 0 ..< lastMonthList.count {
                if calendar.isDate(holiday.date, inSameDayAs: lastMonthList[i].beginningOfDay!) {
                    lastMonthList[i].isDayOff = !holiday.isOnWork
                    lastMonthList[i].holidayName = holiday.holidayName
                    lastMonthList[i].additional = holiday.memo
                    break
                }
            }
            for i in 0 ..< nextMonthList.count {
                if calendar.isDate(holiday.date, inSameDayAs: nextMonthList[i].beginningOfDay!) {
                    nextMonthList[i].isDayOff = !holiday.isOnWork
                    nextMonthList[i].holidayName = holiday.holidayName
                    nextMonthList[i].additional = holiday.memo
                    break
                }
            }
        }
        return (thisMonthList,lastMonthList,nextMonthList)
        
    }

    /// Create & set holiday info to nextMonthList
    ///
    /// - parameter company:companyInfo
    /// - returns: nextMonthList:[workDataOfDay]
    func createNextMonthListsOW(_ company:companyInfo,isWeekdayOff:[Bool],LastDay:Date) -> [workDataOfDay]{
        // 来月の初日を作る
        let openDayOfNextMonth = dtc.getNextDate(LastDay)
        // 来月の締め日を作る
        // 初日＝1日（ついたち）か
        var closeDayOfNextMonth =  dtc.getLastDay(openDayOfNextMonth) // 月末
        if !(calendar.component(.day, from: openDayOfNextMonth) == 1) { // 1日ではない
            // 来月の１日を作る
            let firstDayOfnextMonth = dtc.getNextDate(closeDayOfNextMonth)
            // 翌月の締め日
            closeDayOfNextMonth = dtc.getCloseDay(firstDayOfnextMonth,closeDay:company.closeDate )
        }
        
        var nextMonthList:[workDataOfDay] = []

        let holidays = getHolidaysByID(company.companyID, isUsePublic: company.isUseNationalHolidays , dateFrom: openDayOfNextMonth)
        var referenceDate = openDayOfNextMonth
        while true { // 当月の締め日前から締め日まで
            var wk = workDataOfDay()
            wk.beginningOfDay = referenceDate
            wk.endingOfDay = dtc.getNextDate(referenceDate)
            (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
            wk.isDayOff = isWeekdayOff[wk.weekdayNum]
            nextMonthList += [wk]
            referenceDate = dtc.getNextDate(referenceDate)
            if calendar.isDate(referenceDate, inSameDayAs: closeDayOfNextMonth) {
                wk.beginningOfDay = referenceDate
                wk.endingOfDay = dtc.getNextDate(referenceDate)
                (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
                wk.isDayOff = isWeekdayOff[wk.weekdayNum]
                nextMonthList += [wk]
                break
            }
        }
        
        
        for holiday in holidays {
            for i in 0 ..< nextMonthList.count {
                if calendar.isDate(holiday.date, inSameDayAs: nextMonthList[i].beginningOfDay!) {
                    nextMonthList[i].isDayOff = !holiday.isOnWork
                    nextMonthList[i].holidayName = holiday.holidayName
                    nextMonthList[i].additional = holiday.memo
                    break
                }
            }
        }
        return nextMonthList
        
    }
    
    /// Create lastMonrhList
    ///
    /// - parameter company:companyInfo
    /// - returns: lastMonthList:[workDataOfDay]
    func createLastMonthListsOW(_ company:companyInfo,isWeekdayOff:[Bool],firstDay:Date) -> [workDataOfDay]{
        // 前月の末日を作る
        let closeDayOfLastMonth = dtc.getPriorDate(firstDay)
        // 来月の締め日を作る
        // 初日＝1日（ついたち）か
        var openDayOfLastMonth =  dtc.get1stDay(closeDayOfLastMonth) // 月初
        // 前月末
        if !(calendar.component(.day, from: firstDay) == 1) { // 1日ではない
            let lastDay = dtc.getPriorDate(openDayOfLastMonth)
            openDayOfLastMonth = dtc.getCloseDay(lastDay,closeDay:company.closeDate )
        }
        
        let holidays = getHolidaysByID(company.companyID, isUsePublic: company.isUseNationalHolidays , dateFrom: openDayOfLastMonth)

        var lastMonthList:[workDataOfDay] = []
        var referenceDate = openDayOfLastMonth
        while true { // 当月の締め日前から締め日まで
            var wk = workDataOfDay()
            wk.beginningOfDay = referenceDate
            wk.endingOfDay = dtc.getNextDate(referenceDate)
            (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
            wk.isDayOff = isWeekdayOff[wk.weekdayNum]
            lastMonthList += [wk]
            referenceDate = dtc.getNextDate(referenceDate)
            if calendar.isDate(referenceDate, inSameDayAs: closeDayOfLastMonth) {
                wk.beginningOfDay = referenceDate
                wk.endingOfDay = dtc.getNextDate(referenceDate)
                (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
                wk.isDayOff = isWeekdayOff[wk.weekdayNum]
                lastMonthList += [wk]
                break
            }
        }
        for holiday in holidays {
            for i in 0 ..< lastMonthList.count {
                if calendar.isDate(holiday.date, inSameDayAs: lastMonthList[i].beginningOfDay!) {
                    lastMonthList[i].isDayOff = !holiday.isOnWork
                    lastMonthList[i].holidayName = holiday.holidayName
                    lastMonthList[i].additional = holiday.memo
                    break
                }
            }
        }

        return lastMonthList
    }

    

    /// Create thisMonthList & lastMonthList & nextMonrhList for Personal User
    ///
    /// - parameter company:companyInfo
    /// - returns: thisMonthList:[workDataOfDay],lastMonthList:[workDataOfDay]
    func createWorkLists(_ workPlace:workPlaceInfo) -> (thisMonthList:[workDataOfDay],lastMonthList:[workDataOfDay],nextMonthList:[workDataOfDay]){
        // 締め日、日付変更時間の読み出し
        //appDl.company = getCompanyInfoByID(appDl.ownCD.user.companyID)
        // 今日の始まりの時間を作る
        let beginingOfToday = dtc.getDateLine(workPlace.dateLine)
        // 今月の１日を作る
        var firstDayOfThisMonth = dtc.get1stDay(beginingOfToday)
        // 今月の締め日を作る
        var closeDayOfThisMonth = dtc.getCloseDay(beginingOfToday,closeDay:workPlace.closeDate )
        // 先月の末日を作る
        let lastDayOfLastMonth = dtc.getPriorDate(firstDayOfThisMonth)
        // 先月の１日を作る
        let firstDayOfLastMonth = dtc.get1stDay(lastDayOfLastMonth)
        // 先月の締め日を作る
        var closeDayOfLastMonth = dtc.getCloseDay(firstDayOfLastMonth,closeDay:workPlace.closeDate )
        // 今月の初日を作る
        var openDayOfThisMonth = dtc.getNextDate(closeDayOfLastMonth)
        // 先々月の末日を作る
        let lastDayOfBeforeMonth = dtc.getPriorDate(firstDayOfLastMonth)
        // 先々月の締め日を作る
        let closeDayOfBeforeMonth = dtc.getCloseDay(lastDayOfBeforeMonth,closeDay:workPlace.closeDate )
        // 先月の初日を作る
        var openDayOfLastMonth = dtc.getNextDate(closeDayOfBeforeMonth)
        // 来月の初日を作る
        var openDayOfNextMonth = dtc.getNextDate(closeDayOfThisMonth)
        // 今月の末日を作る
        var lastDayOfThisMonth = dtc.getLastDay(firstDayOfThisMonth)
        // 来月の１日を作る
        var firstDayOfnextMonth = dtc.getNextDate(lastDayOfThisMonth)
        // 来月の締め日を作る
        var closeDayOfNextMonth = dtc.getCloseDay(firstDayOfnextMonth,closeDay:workPlace.closeDate )
        // 今月の締め日を超えていたら月を１ヶ月ずらす
        if beginingOfToday.compare(closeDayOfThisMonth) == .orderedDescending {
            openDayOfLastMonth  = openDayOfThisMonth
            closeDayOfLastMonth = closeDayOfThisMonth
            openDayOfThisMonth  = openDayOfNextMonth
            closeDayOfThisMonth = closeDayOfNextMonth
            firstDayOfThisMonth = firstDayOfnextMonth
            // 来月の初日を作る
            openDayOfNextMonth = dtc.getNextDate(closeDayOfThisMonth)
            // 今月の末日を作る
            lastDayOfThisMonth = dtc.getLastDay(firstDayOfThisMonth)
            // 来月の１日を作る
            firstDayOfnextMonth = dtc.getNextDate(lastDayOfThisMonth)
            // 来月の締め日を作る
            closeDayOfNextMonth = dtc.getCloseDay(firstDayOfnextMonth,closeDay:workPlace.closeDate )
        }

        
        var thisMonthList:[workDataOfDay] = []
        var referenceDate = openDayOfThisMonth
        while true { // 当月の締め日前から締め日まで
            var wk = workDataOfDay()
            wk.beginningOfDay = referenceDate
            wk.endingOfDay = dtc.getNextDate(referenceDate)
            (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
            wk.isDayOff = false
            thisMonthList += [wk]
            referenceDate = dtc.getNextDate(referenceDate)
            if calendar.isDate(referenceDate, inSameDayAs: closeDayOfThisMonth) {
                wk.beginningOfDay = referenceDate
                wk.endingOfDay = dtc.getNextDate(referenceDate)
                (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
                wk.isDayOff = false
                thisMonthList += [wk]
                break
            }
        }
        var lastMonthList:[workDataOfDay] = []
        referenceDate = openDayOfLastMonth
        while true { // 当月の締め日前から締め日まで
            var wk = workDataOfDay()
            wk.beginningOfDay = referenceDate
            wk.endingOfDay = dtc.getNextDate(referenceDate)
            (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
            wk.isDayOff = false
            lastMonthList += [wk]
            referenceDate = dtc.getNextDate(referenceDate)
            if calendar.isDate(referenceDate, inSameDayAs: closeDayOfLastMonth) {
                wk.beginningOfDay = referenceDate
                wk.endingOfDay = dtc.getNextDate(referenceDate)
                (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
                wk.isDayOff = false
                lastMonthList += [wk]
                break
            }
        }
        var nextMonthList:[workDataOfDay] = []
        referenceDate = openDayOfNextMonth
        while true { // 当月の締め日前から締め日まで
            var wk = workDataOfDay()
            wk.beginningOfDay = referenceDate
            wk.endingOfDay = dtc.getNextDate(referenceDate)
            (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
            wk.isDayOff = false
            nextMonthList += [wk]
            referenceDate = dtc.getNextDate(referenceDate)
            if calendar.isDate(referenceDate, inSameDayAs: closeDayOfNextMonth) {
                wk.beginningOfDay = referenceDate
                wk.endingOfDay = dtc.getNextDate(referenceDate)
                (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
                wk.isDayOff = false
                nextMonthList += [wk]
                break
            }
        }
        
        return (thisMonthList,lastMonthList,nextMonthList)
        
    }
 
    /// Create nextMonrhList
    ///
    /// - parameter company:companyInfo
    /// - returns: thisMonthList:[workDataOfDay],lastMonthList:[workDataOfDay]
    func createNextMonthLists(_ workPlace:workPlaceInfo,LastDay:Date) -> [workDataOfDay]{
        // 来月の初日を作る
        let openDayOfNextMonth = dtc.getNextDate(LastDay)
        // 来月の締め日を作る
        // 初日＝1日（ついたち）か
        var closeDayOfNextMonth =  dtc.getLastDay(openDayOfNextMonth) // 月末
        if !(calendar.component(.day, from: openDayOfNextMonth) == 1) { // 1日ではない
            // 来月の１日を作る
            let firstDayOfnextMonth = dtc.getNextDate(closeDayOfNextMonth)
            // 翌月の締め日
            closeDayOfNextMonth = dtc.getCloseDay(firstDayOfnextMonth,closeDay:workPlace.closeDate )
        }
        
        var nextMonthList:[workDataOfDay] = []
        var referenceDate = openDayOfNextMonth
        while true { // 当月の締め日前から締め日まで
            var wk = workDataOfDay()
            wk.beginningOfDay = referenceDate
            wk.endingOfDay = dtc.getNextDate(referenceDate)
            (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
            wk.isDayOff = false
            nextMonthList += [wk]
            referenceDate = dtc.getNextDate(referenceDate)
            if calendar.isDate(referenceDate, inSameDayAs: closeDayOfNextMonth) {
                wk.beginningOfDay = referenceDate
                wk.endingOfDay = dtc.getNextDate(referenceDate)
                (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
                wk.isDayOff = false
                nextMonthList += [wk]
                break
            }
        }
        
        return nextMonthList
        
    }

    /// Create lastMonrhList
    ///
    /// - parameter company:companyInfo
    /// - returns: thisMonthList:[workDataOfDay],lastMonthList:[workDataOfDay]
    func createLastMonthLists(_ workPlace:workPlaceInfo,firstDay:Date) -> [workDataOfDay]{
        // 前月の末日を作る
        let closeDayOfLastMonth = dtc.getPriorDate(firstDay)
        // 来月の締め日を作る
        // 初日＝1日（ついたち）か
        var openDayOfLastMonth =  dtc.get1stDay(closeDayOfLastMonth) // 月初
        // 前月末
        if !(calendar.component(.day, from: firstDay) == 1) { // 1日ではない
            let lastDay = dtc.getPriorDate(openDayOfLastMonth)
            openDayOfLastMonth = dtc.getCloseDay(lastDay,closeDay:workPlace.closeDate )
        }
        
        var lastMonthList:[workDataOfDay] = []
        var referenceDate = openDayOfLastMonth
        while true { // 当月の締め日前から締め日まで
            var wk = workDataOfDay()
            wk.beginningOfDay = referenceDate
            wk.endingOfDay = dtc.getNextDate(referenceDate)
            (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
            wk.isDayOff = false
            lastMonthList += [wk]
            referenceDate = dtc.getNextDate(referenceDate)
            if calendar.isDate(referenceDate, inSameDayAs: closeDayOfLastMonth) {
                wk.beginningOfDay = referenceDate
                wk.endingOfDay = dtc.getNextDate(referenceDate)
                (wk.dateLabel,wk.weekdayLabel,wk.weekdayNum) = dtc.getDateMMDD(referenceDate)
                wk.isDayOff = false
                lastMonthList += [wk]
                break
            }
        }
        
        return lastMonthList
    }

    /// Sprit TimeLabel with "HH:mm" to "HH" & "mm"
    ///
    /// - parameter String: "HH:mm"
    /// - returns: hour : "HH" with Int,
    ///            minute : "mm" with Int
    func spritTimeLabel(_ timeLabel:String)->(hour:Int,minute:Int) {
        let arTimes = timeLabel.components(separatedBy: ":")
        let hour = Int(arTimes[0]) ?? 0
        let minute = (arTimes.count > 1) ? (Int(arTimes[1]) ?? 0) : 0
        return (hour,minute)
        
    }
    
    /// Add minute data to time format with "HH:mm"
    ///
    /// - parameter timeLabel : "HH:mm"
    /// - parameter addMinute : minute value with Int
    /// - returns: label -> updated String with "HH:mm"
    ///            dayChanged -> time crossed over midnight or not
    func updateTimeLabel(_ timeLabel:String,addMinute:Int) -> (label:String,dayChanged:Bool) {
        let HM = spritTimeLabel(timeLabel)
        var hour = HM.hour
        var minute = HM.minute
        var dayChanged:Bool = false
        
        minute += addMinute
        if minute >= 60 {
            hour += 1
            minute -= 60
            if hour >= 24 {
                hour -= 24
                dayChanged = true
            }
        }
        if minute < 0 {
            hour -= 1
            minute += 60
            if hour < 0 {
                hour += 24
                dayChanged = true
            }
        }
        return (String(NSString(format: "%02d", hour)) + ":" + String(NSString(format: "%02d", minute)),dayChanged)
    }
    /// Adjusted time format with "HH:mm"
    ///
    /// - parameter timeLabel : "HH:mm"
    /// - parameter addMinute : minute value with Int
    /// - returns: adjusted String with "HH:mm"
    func adjustTimeLabel(_ timeLabel:String,unit:Int = 15) -> (timeStr:String, timeVal:Int){
        if timeLabel == "" { return ("",-1) }
        let HM = spritTimeLabel(timeLabel)
        var hour = HM.hour
        var minute = HM.minute
        
        // 分の刻みを調整
        let cnt:Int = minute / unit
        minute = cnt * unit
        // 時刻の調整 24時越えと数値がおかしい場合
        let overHour = minute / 60
        minute =  minute % 60
        hour += overHour
        hour = hour % 24
        var timeVal:Int = hour * 100 + minute
        if timeVal < 500 { // 5時前
            timeVal += 2400 // 24時を超える扱い
        }
        //return String(NSString(format: "%02d", hour)) + ":" + String(NSString(format: "%02d", minute))    }
        return (String(format: "%02d", hour) + ":" + String(format: "%02d", minute),timeVal)
    }

    func getLetterColor(_ color:UIColor) -> UIColor? {
        var hue:CGFloat = 0
        var saturation:CGFloat = 0
        var brightness:CGFloat = 0
        var alpha:CGFloat = 0
        if color.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha) {
            print("brightness",brightness)
            if brightness > 0.5 {
                return UIColor(hue: hue, saturation: saturation, brightness: brightness * 0.5 , alpha: alpha)
            } else {
                return UIColor(hue: hue, saturation: saturation, brightness: brightness * 2.0 , alpha: alpha)
            }
        } else {
            return nil
        }
        
    }
    
    /// その文字列がメールアドレスかどうか調べる
    func isValidEmail(_ string: String) -> Bool {
        //print("validate calendar: \(string)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: string)
        return result
    }


}
