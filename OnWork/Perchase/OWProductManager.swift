//
//  OWProductManager.swift
//  OnWork
//
//  Created by 細野浩明 on 2015/11/24.
//  Copyright © 2015年 Hiroaki Hosono. All rights reserved.
//

import UIKit
import StoreKit

private var productManagers : Set<OWProductManager> = Set()

class OWProductManager: NSObject, SKProductsRequestDelegate {
    fileprivate var completionForProductidentifiers : (([SKProduct]?,NSError?) -> Void)?
    
    /// 課金アイテム情報を取得
    class func productsWithProductIdentifiers(_ productIdentifiers : [String]!,completion:(([SKProduct]?,NSError?) -> Void)?){
        print("OWProductManager.productsWithProductIdentifiers")
        let productManager = OWProductManager()
        productManager.completionForProductidentifiers = completion
        let productRequest = SKProductsRequest(productIdentifiers: Set(productIdentifiers))
        productRequest.delegate = productManager
        productRequest.start()
        productManagers.insert(productManager)
    }
    
    // MARK: - SKProducts Request Delegate
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print("OWProductManager.productsRequest")
        var error : NSError? = nil
        if response.products.count == 0 {
            error = NSError(domain: "ProductsRequestErrorDomain", code: 0, userInfo: [NSLocalizedDescriptionKey:"プロダクトを取得できませんでした。"])
        }
        completionForProductidentifiers?(response.products, error)
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        print("OWProductManager.request")
        let error = NSError(domain: "ProductsRequestErrorDomain", code: 0, userInfo: [NSLocalizedDescriptionKey:"プロダクトを取得できませんでした。"])
        completionForProductidentifiers?(nil,error)
        productManagers.remove(self)
    }
    
    func requestDidFinish(_ request: SKRequest) {
        print("OWProductManager.requestDidFinish")
        productManagers.remove(self)
    }
    
    // MARK: - Utility
    /// 価格情報を抽出
    class func priceStringFromProduct(_ product: SKProduct!) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.formatterBehavior = .behavior10_4
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = product.priceLocale
        return numberFormatter.string(from: product.price)!
    }


}
