//
//  PerchaseCommon.swift
//  OnWorkPersonal
//
//  Created by 細野浩明 on 2018/06/16.
//  Copyright © 2018年 Melting Pot LLC. All rights reserved.
//

import Foundation
import StoreKit

//プロダクトID達
let LICENSE_PERMANENT = "LicensePermanent"
let LICENSE_BASIC = "LicenseBasic"
let LICENSE_BASIC_EXPIRE = "LicenseBasicExpire"
let PRODUCT_ID_APP = "jp.co.meltingpot.OnWorkPersonal"
let PRODUCT_ID_BASIC = "jp.co.meltingpot.OnWorkPersonal.Basic"
let PRODUCT_SECRET = "4d254d7f92454299a6856256752de8d6"


let productIdentifiers:[String] = [PRODUCT_ID_BASIC]
let productSecretKey:String = PRODUCT_SECRET

struct mylicense {
    var permanent:Bool = false // 有料購入者
    var basic:Bool = false
    var basicExpireDate:Date? = nil
    var basicExpired:Bool = false

    // ライセンス追加時にここに種類を追加
    
    func isBasicAvalable() -> licenseMode {
        if permanent {return .permanent}
        if basic {
            if basicExpired {
                return .expired
            } else {
                return .avalable
            }
        } else {
            return .none
        }
    }
}

enum licenseType:Int {
    case BASIC = 0
    case PERMANENT = 1
    case EXT         // ライセンス追加時にここに種類を追加
}
enum licenseMode {
    case none
    case avalable
    case expired
    case permanent
}


struct receiptInfo {
    var original_purchase_date: Date = Date()   // オリジナル購入日
    var original_transaction_id:String = ""   // 復元元トランザクションID
    var product_id:String = "" // Product ID = jp.co.meltingpot.XXXX
    var purchase_date: Date = Date() // 購入日または更新日
    var quantity:Int = 0          // 数量：購入したアイテム数
    var transaction_id:String = ""  // トランザクションID
    var expires_date:Date? = nil
    
    /// Parse RFC 3339 date string to NSDate
    func dateForPurchaseDateTimeString(_ purchaseDateTimeString: String) -> Date? {
        let enUSPOSIXLocale = Locale(identifier: "en_US_POSIX")
        
        let purchaseDateFormatter = DateFormatter()
        purchaseDateFormatter.locale = enUSPOSIXLocale
        purchaseDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss 'Etc/'ZZZ"
        purchaseDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let date:Date? = purchaseDateFormatter.date(from: purchaseDateTimeString)
        
        return date
    }
    func toBool(_ str:String) -> Bool {
        switch str {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return false
        }
    }
    ///サーバから読みだしたデータ(NSDictionary)から値を復元
    ///
    mutating func initWithDict (_ source:NSDictionary) {
        original_purchase_date = dateForPurchaseDateTimeString(source["original_purchase_date"] as! String)!
        original_transaction_id = source["original_transaction_id"] as! String
        product_id = source["product_id"] as! String
        purchase_date = dateForPurchaseDateTimeString(source["purchase_date"] as! String)!
        quantity = Int(source["quantity"] as! String)!
        transaction_id = source["transaction_id"] as! String
        expires_date = dateForPurchaseDateTimeString(source["expires_date"] as! String)

    }
    
}

class PerchaseProduct:UIResponder ,SKRequestDelegate, OWPurchaseManagerDelegate {
    // 呼び出し元の設定画面を設定する　画面更新が必要なところはこのVcにアクセスする
    var licenseVc:PersonalLicenseVC? = nil

    var license = mylicense()
    let receiptURL:URL = Bundle.main.appStoreReceiptURL ?? URL(fileURLWithPath: "http://error.com/")

    func saveLicenseLocal(){

        if license.permanent {
            license.basic = true
            license.basicExpireDate = dateTimeControll().getDateFromStr("9999/12/31 23:59:59")
        } else {
            if license.basic {
                if license.basicExpireDate == nil {
                    license.basicExpired = true
                } else if license.basicExpireDate! < Date() { // 期限切れ
                    license.basicExpired = true
                } else { // 期限内
                    license.basicExpired = false
                }
            } else {
                license.basicExpireDate = nil
                license.basicExpired = false // default値
            }
        }

        let userDefaults = UserDefaults.standard
        userDefaults.set(license.basic, forKey: LICENSE_BASIC)
        userDefaults.set(license.basicExpireDate, forKey: LICENSE_BASIC_EXPIRE)
        userDefaults.set(license.permanent, forKey: LICENSE_PERMANENT)
        
    }
    
    func restoreDisp() {
        if licenseVc != nil {
            licenseVc!.restoreStateLicensed() // 画面の更新
        }
    }
    
    func isOriginalVer(_ ver:String) -> Bool {
        if (ver == "1.0.0.2" ) || (ver == "1.1.0.0" ) { // 有料アプリで販売したビルドバージョン
//        if (ver == "1.0" ) || (ver == "1.1.0" ) { // SandBoxでは常に1.0
           return true
        }
        return false
    }
    
    func synchronousRequest(_ request:URLRequest) -> (JSON:Data?,resp:URLResponse?){
        let semaphore = DispatchSemaphore(value: 0)
        var result:Data? = nil
        var response: URLResponse? = nil
        
        let configuration = URLSessionConfiguration.ephemeral
        let session = URLSession(configuration: configuration)
        let task = session.dataTask(with: request, completionHandler: {resData,resp,error in
            if error != nil {
                semaphore.signal()
                return
            } else {
                result = resData
                response = resp
                semaphore.signal()
                return
            }
        })
        
        task.resume()
        _ = semaphore.wait(timeout: .distantFuture)
        
        return (result,response)
        
    }

    // appStoreに検証を投げる処理
    func verifyFromStore(_ receipt:String,url:URL) ->(isSandboxRequired:Bool, dict:NSDictionary?) {
        
        let requestContents: NSDictionary = ["receipt-data": receipt,"password":productSecretKey] as NSDictionary
        let requestData: Data
        do {
            requestData = try JSONSerialization.data(withJSONObject: requestContents, options: JSONSerialization.WritingOptions())
        } catch  let error1 as NSError  {
            print("\u{1b}[92mError！ \u{1b}[m\(#function)[\(#line)]","\(error1)")

            return (false,nil)
        }
        
        var request = URLRequest(url: url)
        
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField:"content-type")
        request.timeoutInterval = 5.0
        request.httpMethod = "POST"
        request.httpBody = requestData
        
        var resultJSON:Data? = nil
        var response: URLResponse? = nil
        
        (resultJSON,response) = synchronousRequest(request)
        if response == nil {
            return (false,nil)
        }
        
        let httpResponse = response as? HTTPURLResponse
//        print("httpResponse: \(#function)[\(#line)]",httpResponse?.description ?? "No httpResponse!")
        print("httpResponse.code:\(httpResponse?.statusCode ?? 0) \(#function)[\(#line)]")
        if httpResponse?.statusCode != 200 {
            return (false,nil)
        } else {
            // 帰ってきたデータを文字列に変換.
            var resultDict:[AnyHashable:Any] = [:]
            do{
                resultDict =  try JSONSerialization.jsonObject(with: resultJSON!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary as! [AnyHashable : Any]
            } catch  let error1 as NSError  {
                print("\u{1b}[92mError！ \u{1b}[m\(#function)[\(#line)]","\(error1)")
                return (true,nil)
            }
            let status = resultDict["status"] as? Int ?? -1
            if status == 0 { // Receipt is OK
                return (false,resultDict as NSDictionary?)
            } else if status == 21007 { // Sandbox Receipt
                return (true,nil)
                
            } else { // Other error
                return (false,nil)
            }
            
        }
        
    }

    
    func checkReceipt() {
        
        let receiptData:Data = try! Data(contentsOf: receiptURL)
        let receiptBase64Str: String = receiptData.base64EncodedString(options: NSData.Base64EncodingOptions())
        
        let appStoreUrl: URL = URL(string: "https://buy.itunes.apple.com/verifyReceipt")!
        let sandboxUrl: URL = URL(string: "https://sandbox.itunes.apple.com/verifyReceipt")!
        
        var data: NSDictionary?
        var result = verifyFromStore(receiptBase64Str,url: appStoreUrl)
        if result.isSandboxRequired {
            print("SandboxRequired \(#function)[\(#line)]")
            result = verifyFromStore(receiptBase64Str,url: sandboxUrl)
        }
        data = result.dict
        
        // レシートの内容確認
        if data == nil {
            print("Receipt Error! \(#function)[\(#line)]")
            return
        }
        let receipt = data!["receipt"] as! NSDictionary
        let original_application_version = receipt["original_application_version"] as! String
        print("original_application_version:\(original_application_version) \(#function)[\(#line)]")
        if isOriginalVer(original_application_version) {
            license.permanent = true
//            return
        }
        let inAppPurchaseArray = receipt["in_app"] as! NSArray
        var receipts:[receiptInfo] = []
        for i in 0 ..< inAppPurchaseArray.count {
            let inAppPurchase = inAppPurchaseArray[i] as! NSDictionary
            var receiptWk = receiptInfo()
            receiptWk.initWithDict(inAppPurchase)
            receipts.append(receiptWk)
        }
        // ログデータを期限の日付の降順にソート
        receipts.sort {
            ( dt1:receiptInfo , dt2:receiptInfo )->Bool in
            return dt1.expires_date!.timeIntervalSinceReferenceDate > dt2.expires_date!.timeIntervalSinceReferenceDate

        }
        // product ID で分ける
        let receiptsBasic:[receiptInfo] = receipts.filter({
            (log: receiptInfo) -> Bool in
            //フィルター結果として、残したい要素だけtrueを返す（検索条件）
            for id in productIdentifiers {
                if log.product_id == id {return true}
            }
            return false
            //return log.product_id == productIdentifiers[licenseType.BASIC.rawValue]
        })
        print("receipts=\(receiptsBasic.count) \(#function)[\(#line)]")
        if receiptsBasic.count > 0 {
            for receipt in receiptsBasic {
                if receipt.product_id == PRODUCT_ID_BASIC {
                    license.basic = true
//                    print("product_id:\(receipt.product_id),\(license.basic),",receipt.expires_date?.description ?? "Date:nil" )
                    if license.basicExpireDate == nil {
                       license.basicExpireDate = receipt.expires_date!
                    } else if receipt.expires_date! > license.basicExpireDate! { // 最近の状態を取得
                        license.basicExpireDate = receipt.expires_date
                  }

                }
                // 有料購入した場合
                if receipt.product_id == PRODUCT_ID_APP {
                    license.permanent = true
                    print("product_id:\(receipt.product_id),\(license.permanent) \(#function)[\(#line)]")
                }
            }
            print("basicExpireDate: \(license.basicExpireDate!) \(#function)[\(#line)]")

        }
    }

    // MARK: - OWPurchaseManager Delegate
    func purchaseManager(_ purchaseManager: OWPurchaseManager!, didFinishPurchaseWithTransaction transaction: SKPaymentTransaction!, decisionHandler: ((_ complete: Bool) -> Void)!) {
        //課金終了時に呼び出される
        print("課金終了　ID=",purchaseManager.productIdentifier ?? ""," \(#function)[\(#line)]")
        switch purchaseManager.productIdentifier ?? "" {
        case productIdentifiers[licenseType.BASIC.rawValue] :
            license.basic = true
        case productIdentifiers[licenseType.PERMANENT.rawValue] :
            license.permanent = true
            
        default:
            break
        }
        
        //        license = changedLicense
        checkReceipt()
        saveLicenseLocal()
        restoreDisp()
        
        //コンテンツ解放が終了したら、この処理を実行(true: 課金処理全部完了, false 課金処理中断)
        decisionHandler(true)
    }
    
    func purchaseManager(_ purchaseManager: OWPurchaseManager!, didFinishUntreatedPurchaseWithTransaction transaction: SKPaymentTransaction!, decisionHandler: ((_ complete: Bool) -> Void)!) {
        //課金終了時に呼び出される(startPurchaseで指定したプロダクトID以外のものが課金された時。)
        print("コンテンツ解放処理 \(#function)[\(#line)]")
        /*
         コンテンツ解放処理
         */
        //コンテンツ解放が終了したら、この処理を実行(true: 課金処理全部完了, false 課金処理中断)
        decisionHandler(true)
    }
    
    func purchaseManager(_ purchaseManager: OWPurchaseManager!, didFailWithError error: NSError!) {
        
        if licenseVc == nil { return }

        //課金失敗時に呼び出される
        print("課金失敗",error," \(#function)[\(#line)]")
        restoreDisp()
        
        let alertController = UIAlertController(
            title: NSLocalizedString("Purchase Failed!", tableName: "Message", comment: "課金失敗！"),
            message: NSLocalizedString("btnBuy-message-failed", tableName: "Message",value: "Please retry purchase.", comment: "課金処理が失敗しました。\n再度購入をお願いします。"),
            preferredStyle: .alert)
        
        let defaultAction:UIAlertAction = UIAlertAction(title: "OK",
                                                        style: UIAlertActionStyle.default,
                                                        handler: nil)
        alertController.addAction(defaultAction)
        alertController.popoverPresentationController?.sourceView = licenseVc!.view
        alertController.popoverPresentationController?.sourceRect = licenseVc!.view.frame
        licenseVc!.present(alertController, animated: true, completion: nil)

    }
    
    
    func purchaseManagerDidFinishRestore(_ purchaseManager: OWPurchaseManager!) {
        //リストア終了時に呼び出される(個々のトランザクションは”課金終了”で処理)
        print("リストア終了 \(#function)[\(#line)]")
        // レシートファイルがない場合があるので、SKReceiptRefreshRequestを実行する
        let fm = FileManager()
        // レシートがなかったら
        if !fm.fileExists(atPath: receiptURL.absoluteString) {
            print("レシートファイルを取得 \(#function)[\(#line)]")
            fetchReceipt()
        } else { // あったらそのまま確認
            print("レシート確認 \(#function)[\(#line)]")
            checkReceipt()
            print("状態更新 \(#function)[\(#line)]")
            saveLicenseLocal()
            restoreDisp()
        }
        
        
        /*
         インジケータなどを表示していたら非表示に
         */
    }
    
    func purchaseManagerDidDeferred(_ purchaseManager: OWPurchaseManager!) {
        //承認待ち状態時に呼び出される(ファミリー共有)
        print("承認待ち状態(ファミリー共有) \(#function)[\(#line)]")
        /*
         インジケータなどを表示していたら非表示に
         */
    }
    
    func readProductPurchase() {
        //プロダクト情報取得
        OWProductManager.productsWithProductIdentifiers(
            productIdentifiers,
            completion: { (products: [SKProduct]?, error : NSError?) -> Void in
                if error != nil {
                    print("\u{1b}[92m readProductPurchase Failed!\u{1b}[m",error!," \(#function)[\(#line)]")
                } else {
                    var lbString:[String] = [String] ( repeating: "" , count: productIdentifiers.count )
                    var productsWk:[SKProduct] = [SKProduct] ( repeating: SKProduct() , count: productIdentifiers.count )
                    print("products.count=",products!.count," \(#function)[\(#line)]")
                    if products != nil {
                        for product in products! {
                            for i in 0 ..< productIdentifiers.count {
                                if productIdentifiers[i] == product.productIdentifier {
                                    lbString[i] = OWProductManager.priceStringFromProduct(product)
                                    productsWk[i] = product
                                    break
                                }
                            }
                        }
                        if self.licenseVc != nil {
                            self.licenseVc!.lbBasicLicensePrice.text = lbString[0]
                            self.licenseVc!.myProducts = productsWk
                        }
                    } else {
                        print("\u{1b}[92m readProductPurchase Failed!\u{1b}[m"," \(#function)[\(#line)]")
                    }
                }
        })
    }
    


    // MARK: - SKRequestDelegate
    
    func fetchReceipt() {
        let request = SKReceiptRefreshRequest()
        request.delegate = self
        request.start()
    }
    
    func requestDidFinish(_ request: SKRequest) {
        // レシート取得完了時にしたい処理
        print("レシート確認 \(#function)[\(#line)]")
        checkReceipt()
        saveLicenseLocal()
        restoreDisp()
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        // レシート取得失敗時にしたい処理
        print("\u{1b}[92mレシート取得失敗 \u{1b}[m\(#function)[\(#line)]")

    }
    
    

}

