//
//  companyData.swift
//  OnWork
//
//  Created by 細野浩明 on 2016/02/12.
//  Copyright © 2016年 Hiroaki Hosono. All rights reserved.
//

import Foundation
import UIKit

class companyData {

    var isEnvironmentInitiated:Bool = false

    
    var user = userInfo()       // ログインユーザ
    var company = companyInfo() // ログインユーザの所属する会社情報
    var today:Date? = nil       // 本日の開始時刻
    var todaysData = workDataOfDay() // 本日の時刻情報

    var users:[userInfo] = []   // 所属会社のユーザ一覧
    
    var members:[workDataOfDay] = [] // 全社員のデータ＋今日の勤務情報
    var groups:[groupInfo] = []  // 所属会社のグループ一覧に当日のメンバー情報を振り分けしたもの
    var shifts:[shiftInfo] = []  // 所属会社のタイムテーブル一覧に当日のメンバー情報を振り分けしたもの
    var locations:[locationInfo] = [] // 所属会社の勤務地一覧に当日のメンバー情報を振り分けしたもの

    var userSummarys:[[userSummary]] = [[]]      // [grNo][userNo]
    var userDetails:[[[workDataOfDay]]] = [[[]]] // [grNo][userNo][days]
    var todaysMembers:[workDataOfDay] = []

    var timeTableDict:[String:timeTableDef] = [:] // 所属会社のタイムテーブル名の辞書
    var timeTableLists:[timeTableDef] = []  // 所属会社のタイムテーブル一覧
    var locationLists:[locationDef] = []  // 所属会社の勤務地一覧
    var selectedLocation = locationDef() // 端末のある勤務地
    
    var license:licenseDef = licenseDef() // ライセンス情報

    var lastMonthList:[workDataOfDay] = []    // 先月の実績：
    var thisMonthList:[workDataOfDay] = []    // 今月の実績と予定：
    var nextMonthList:[workDataOfDay] = []    // 来月の予定：
    var thisMonthHold:[workDataOfDay] = []    // 今月の実績と予定：アラート作成用
    var nextMonthHold:[workDataOfDay] = []    // 来月の予定：アラート作成用

    var lastMonthCal:[workDataOfDay] = []    // 先月のカレンダー：
    var thisMonthCal:[workDataOfDay] = []    // 今月のカレンダー：
    var nextMonthCal:[workDataOfDay] = []    // 来月のカレンダー：

    
    var weeklyCal:weeklyCalendar = weeklyCalendar()
    var isWeekdayOff:[Bool] = [true,true,true,true,true,true,true,true]
    var holidays:[holidayInfo] = [] // 先月から来月までの３ヶ月間の休日
    var timeTables:[String:timeTableDef] = [:]

    // 表示する月のデータ カレンダー表示のため前後の月のデータも含む
    var monthlyData:[workDataOfDay] = []
    var isStartMonday:Bool = true
    // カレンダーに表示できる月
    // 表示できる最初の月：先月　→　ライセンス購入したら　ライセンス登録した月 または １年前
    var startMonth:Date = Date()
    // 表示できる最後の月：３ヶ月後　→　ライセンス購入したら　１年後まで
    var endMonth:Date = Date()


    let dtc = dateTimeControll()
    let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
    let cf = commonFunction()

    
    func assortShifts() {
        shifts = []
        for shiftNo in  0 ..< timeTableLists.count {
            var shift = shiftInfo()
            shift.name = timeTableLists[shiftNo].tableName
            shift.memberCount = 0
            shift.members = []
            shifts += [shift]
        }
        
    }
    
    
    func selectLocation(_ locationName:String) {
        for loc in locationLists {
            if loc.LocationName == locationName {
                selectedLocation = loc
                break
            }
        }
    }
    
    // ▼▼▼▼　For Calendar View ver.3.0 ▼▼▼▼
    func setValidMonth() {
        startMonth = dtc.getLastYearStart(thisMonthList.first!.beginningOfDay!)
        endMonth = dtc.getNextYearEnd(thisMonthList.last!.beginningOfDay!) // 1年後
    }
    

    // カレンダー表示で使用するデータ
    func createTableCal() {
        monthlyData = []
        var targetDate = thisMonthList.first?.beginningOfDay ?? Date() // 1日目＝今月の初日
        var weekday:Int = calendar.component(.weekday, from: targetDate)
        var offsetPre:Int = 0
        
        if isStartMonday {
            weekday -= 1
            if weekday < 1 { weekday = 7}
        }
        
        if weekday != 1 { // 日曜日 or 月曜日まで巻き戻す
            for _ in 1 ..< weekday {
                targetDate = dtc.getPriorDate(targetDate)
                offsetPre += 1
            }
        }
        // 先頭から先月分のデータを埋める
        for i in (lastMonthList.count - offsetPre) ..< lastMonthList.count {
            var oneDay = lastMonthList[i]
            oneDay.isThisMonth = false
            monthlyData.append(oneDay)
        }
        // 今月のデータを埋める
        for i in 0 ..< thisMonthList.count {
            var oneDay = thisMonthList[i]
            oneDay.isThisMonth = true
            monthlyData.append(oneDay)
        }
        // 末尾に来月のデータを埋める
        weekday = calendar.component(.weekday, from: nextMonthList.first!.beginningOfDay!)
        if isStartMonday {
            weekday -= 1
            if weekday < 1 { weekday = 7}
        }
        if weekday != 1 { // 土曜日　or 日曜日まで埋める
            for i in 0 ... (7 - weekday)  {
                var oneDay = nextMonthList[i]
                oneDay.isThisMonth = false
                monthlyData.append(oneDay)
            }
        }
        
        print("Create monthlyData complete !",#function, #line)
    }
    
    func createMonthlyList() {
        lastMonthList =  getMonthlyLogList(user,monthlyCal:lastMonthCal)
        print("lastMonthList create complete !",#function, #line)
        thisMonthList =  getMonthlyLogList(user,monthlyCal:thisMonthCal)
        print("thisMonthList create complete !",#function, #line)
        nextMonthList =  getMonthlyLogList(user,monthlyCal:nextMonthCal)
        print("nextMonthList create complete !",#function, #line)
        // カレンダー表示で月を切り替えると変わってしまうため、保存しておく
        thisMonthHold = thisMonthList
        nextMonthHold = nextMonthList
        setValidMonth()
    }

    func FFMonthlyList() {
        lastMonthList = thisMonthList
        thisMonthList = nextMonthList
        // 翌月分を作る
        let nextMonthcal = cf.createNextMonthListsOW(company,isWeekdayOff: isWeekdayOff, LastDay:thisMonthList.last!.beginningOfDay!)
        nextMonthList = getMonthlyLogList(user,monthlyCal:nextMonthcal)
        print("nextMonthList create complete !",#function, #line)
    }
    
    func REWMonthlyList() {
        nextMonthList = thisMonthList
        thisMonthList = lastMonthList
        // 前月分を作る
        let lastMonthcal = cf.createLastMonthListsOW(company,isWeekdayOff: isWeekdayOff, firstDay:thisMonthList.first!.beginningOfDay!)
        lastMonthList = getMonthlyLogList(user,monthlyCal:lastMonthcal)
        print("lastMonthList create complete !",#function, #line)
        
    }
    // ▲▲▲▲　For Calendar View ver.3.0 ▲▲▲▲


}
