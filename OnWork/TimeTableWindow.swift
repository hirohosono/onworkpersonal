//
//  TimeTableWindow.swift
//  OnWork
//
//  Created by 細野浩明 on 2015/11/04.
//  Copyright © 2015年 Hiroaki Hosono. All rights reserved.
//

import Foundation
import UIKit

enum workTimeType {
    case plainTime   // 通常勤務時間
    case overTime    // 早出、残業
    case midNightPlainTime // 深夜(通常勤務)
    case midNightOverTime // 深夜(残業)
    case recessTime  // 休憩時間
}

enum dayKindType: String {
    case DayOn = "出勤日"
    case DayOff = "休日"
    case DayOnAtHoliday = "休日出勤"
    case NotSet = ""
}

struct timeUnit {
    var startTimeLabel:String
    var endTimeLabel:String
    var isWorkTime:workTimeType
    var startTime:Date
    var endTime:Date
    var isWorked:Bool = false
}


class cTimeTableWindow {
    var timeTableWindow:[timeUnit] = []
    var onedaysData = workDataOfDay()
    var dateLine:String = "05:00"
    var unitTime:Int = 15
    
    let cf = commonFunction()

    init (company:companyInfo) {
        self.dateLine = company.dateLine
        self.unitTime = company.unitTime
    }
    
    init (wp:workPlaceInfo) {
        self.dateLine = wp.dateLine
        self.unitTime = wp.unitTime
    }


    var oldTimeTableName:String = ""
    let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
    
    func checkTimePair(start:String, end:String, isStartFirst:Bool = true) -> (start:String,end:String) {
        
        let startTime = cf.adjustTimeLabel(start)
        let endTime = cf.adjustTimeLabel(end)
        if (startTime.timeStr == "") || (endTime.timeStr == "") {
            return ("","")
        } else {
            if startTime.timeVal >= endTime.timeVal {
                if isStartFirst {
                    return ( startTime.timeStr,
                             cf.updateTimeLabel(startTime.timeStr,addMinute: 15).label )
                } else {
                    return ( cf.updateTimeLabel(endTime.timeStr,addMinute: -15).label,
                             endTime.timeStr )
                }
            } else {
                return ( startTime.timeStr, endTime.timeStr )
            }
        }
    }

    
    func checkTimeTable(_ timetable:timeTableDef) -> timeTableDef {
        
        var rightTable:timeTableDef = timetable
        
        // 時刻は１５分刻みにする。　Start < Stopになるように修正 誤データが登録されている場合の対策
        (rightTable.startTime,rightTable.endTime) = checkTimePair(start:rightTable.startTime ,end:rightTable.endTime )
        (rightTable.recessStart1,rightTable.recessEnd1) = checkTimePair(start:rightTable.recessStart1 ,end:rightTable.recessEnd1 )
        (rightTable.recessStart2,rightTable.recessEnd2) = checkTimePair(start:rightTable.recessStart2 ,end:rightTable.recessEnd2 )
        (rightTable.recessStart3,rightTable.recessEnd3) = checkTimePair(start:rightTable.recessStart3 ,end:rightTable.recessEnd3 )
        (rightTable.recessStart4,rightTable.recessEnd4) = checkTimePair(start:rightTable.recessStart4 ,end:rightTable.recessEnd4 )
        (rightTable.recessStart5,rightTable.recessEnd5) = checkTimePair(start:rightTable.recessStart5 ,end:rightTable.recessEnd5 )

        
        return rightTable
    }

    /// Create self.timeTableWindow with timetable information
    ///
    /// - parameter timetable : timeTableDef
    /// - parameter oneday : date of self.timeTableWindow with workDataOfDay format
    /// - returns: nothing (self.timeTableWindow will be created )
    func createTimeTable(_ timetableOrg:timeTableDef,oneday:workDataOfDay,refresh:Bool = false) {
        if !refresh { // refresh == true の場合は同じ名前でも作り直す
            if oldTimeTableName ==  timetableOrg.tableName { return } // テーブル作成済み
        }
        
        let timetable = checkTimeTable(timetableOrg)
        // タイムテーブルの作成
        let tableCount:Int = 24 * (60 / self.unitTime)
        var timeFrom : String = self.dateLine
        var timeTo:String = cf.updateTimeLabel(timeFrom,addMinute: self.unitTime).label
        let unitTime:TimeInterval = Double(self.unitTime) * 60.0 // sec
        var startTimeWk:Date = oneday.beginningOfDay! as Date
        
        var endTimeWk:Date = Date(timeInterval: unitTime, since: startTimeWk)
        timeTableWindow = [] // 一旦初期化
        for _ in 0 ..< tableCount {
        //for var i = 0 ; i < tableCount ; i++ {
            
            timeTableWindow += [timeUnit(
                startTimeLabel:timeFrom,
                endTimeLabel:timeTo ,
                isWorkTime:.overTime , // デフォルトの時間は早出、残業扱い
                startTime:startTimeWk,
                endTime:endTimeWk,
                isWorked:false
                )]
            timeFrom = timeTo
            let updatedTime = cf.updateTimeLabel(timeFrom,addMinute: self.unitTime)
            timeTo = updatedTime.label
            startTimeWk = endTimeWk
            endTimeWk = Date(timeInterval: unitTime, since: startTimeWk)
            
        }
        print("TimeTable Created! Name = \(timetable.tableName)", startTimeWk)
        // 通常勤務の時間帯を設定
        if timetable.startTime != timetable.endTime {
            setWorkTimeType(workTimeType.plainTime,startPeriod:getTimePeriod(startLabel: timetable.startTime),endPeriod:getTimePeriod(endLabel: timetable.endTime))
        }
        // 深夜時間を設定
        
        let midNightStart = calendar.date(bySettingHour: 22, minute: 0, second: 0, of: timeTableWindow.first!.startTime)!
        
        let midNightEnd   = calendar.date( bySettingHour: 5, minute: 0, second: 0 , of: timeTableWindow.last!.endTime)!
        let startPeriod = getTimePeriod(time:midNightStart)
        let endPeriod = getTimePeriod(endTime:midNightEnd)
        
        setWorkTimeType(workTimeType.midNightOverTime,startPeriod:startPeriod,endPeriod:endPeriod)
        
        // 休み時間を設定
        if timetable.recessStart1 != timetable.recessEnd1 {
            setWorkTimeType(workTimeType.recessTime,startPeriod:getTimePeriod(startLabel: timetable.recessStart1),endPeriod:getTimePeriod(endLabel: timetable.recessEnd1))
        }
        if timetable.recessStart2 != timetable.recessEnd2 {
            setWorkTimeType(workTimeType.recessTime,startPeriod:getTimePeriod(startLabel: timetable.recessStart2),endPeriod:getTimePeriod(endLabel: timetable.recessEnd2))
        }
        if timetable.recessStart3 != timetable.recessEnd3 {
            setWorkTimeType(workTimeType.recessTime,startPeriod:getTimePeriod(startLabel: timetable.recessStart3),endPeriod:getTimePeriod(endLabel: timetable.recessEnd3))
        }
        if timetable.recessStart4 != timetable.recessEnd4 {
            setWorkTimeType(workTimeType.recessTime,startPeriod:getTimePeriod(startLabel: timetable.recessStart4),endPeriod:getTimePeriod(endLabel: timetable.recessEnd4))
        }
        if timetable.recessStart5 != timetable.recessEnd5 {
            setWorkTimeType(workTimeType.recessTime,startPeriod:getTimePeriod(startLabel: timetable.recessStart5),endPeriod:getTimePeriod(endLabel: timetable.recessEnd5))
        }
        
        oldTimeTableName = timetable.tableName //作成済みのタイムテーブル名を更新
        onedaysData = oneday
        
    }

    /// uodate the date value of self.timeTableWindow to today
    ///
    /// - parameter : nothing
    /// - returns: nothing (self.timeTableWindow will be updated )
    func refreshTimeTable() {
        
        // タイムテーブルの時刻を今日の日時に更新
        let tableCount:Int = 24 * (60 / self.unitTime)
        var timeFrom : String = self.dateLine
        var timeTo:String = cf.updateTimeLabel(timeFrom,addMinute: self.unitTime).label
        let unitTime:TimeInterval = Double(self.unitTime) * 60.0 // sec
        var startTimeWk:Date = onedaysData.beginningOfDay! as Date
        var endTimeWk:Date = Date(timeInterval: unitTime, since: startTimeWk)
        
        for i in 0  ..< tableCount  {
            
            timeTableWindow[i] = timeUnit(
                startTimeLabel:timeFrom,
                endTimeLabel:timeTo ,
                isWorkTime:timeTableWindow[i].isWorkTime , // 設定済みの値を引き継ぐ
                startTime:startTimeWk,
                endTime:endTimeWk,
                isWorked:false
            )
            timeFrom = timeTo
            let updatedTime = cf.updateTimeLabel(timeFrom,addMinute: self.unitTime)
            timeTo = updatedTime.label
            startTimeWk = endTimeWk
            endTimeWk = Date(timeInterval: unitTime, since: startTimeWk)
        }
        
    }
 /*
    /// Return color of workTimeType
    ///
    /// - parameter type : workTimeType
    /// - returns: UIColor
    func setWorkTimeColor(_ type:workTimeType)->UIColor {
        switch type {
        case .plainTime  : // 通常勤務時間
            return UIColor.black
        case .overTime   : // 早出、残業
            return myColorPastelGreen
        case .midNightPlainTime , .midNightOverTime : // 深夜
            return myColorPastelLightPink
        case .recessTime :  // 休憩時間
            return UIColor.green
            
        }
    }
*/
    /// Calicutate worked hours and set data to workDataOfDay using timeTableWindow
    ///
    /// - parameter oneday : one day data for calicutate with workDataOfDay
    /// - returns: updated data with workDataOfDay
    func setWorkedTime(_ oneday:workDataOfDay) -> workDataOfDay {
        var onedaysData = oneday
        if (oneday.startTimeOfWork == nil) || (oneday.endTimeOfWork == nil) {
            onedaysData.workHours = 0
            onedaysData.overtimeWorkHours = 0
            onedaysData.lateNightWorkHours = 0
            return onedaysData
        }
        var workHour:Int = 0
        var overworkHour:Int = 0
        var lateNightworkHour:Int = 0
        
        //onedaysData.startTimeOfWork = startTime
        //onedaysData.endTimeOfWork = endTime
        
        for i in 0  ..< timeTableWindow.count  {
            
            if (onedaysData.startTimeOfWork!.compare(timeTableWindow[i].startTime) != .orderedDescending /*-1 or 0 :比較対象より未来でない*/ )
                && (onedaysData.endTimeOfWork!.compare(timeTableWindow[i].endTime) != .orderedAscending /* 0 or +1 :比較対象より過去ではない*/) {
                    //print("Hit")
                    timeTableWindow[i].isWorked = true
                    switch timeTableWindow[i].isWorkTime {
                    case .plainTime :
                        workHour += 1
                    case .overTime :
                        overworkHour += 1
                    case .midNightPlainTime :
                        workHour += 1
                        lateNightworkHour += 1
                    case .midNightOverTime :
                        overworkHour += 1
                        lateNightworkHour += 1
                    case .recessTime :
                        break
                    }
            }
        }
        onedaysData.workHours = workHour * self.unitTime
        onedaysData.overtimeWorkHours = overworkHour * self.unitTime
        onedaysData.lateNightWorkHours = lateNightworkHour * self.unitTime
        
        return onedaysData
    }
    
    /// Set WorkTime type to timeTableWindow
    ///
    /// - parameter type : type to set
    /// - parameter startPeriod : start period of windows
    /// - parameter endPeriod : end period of windows
    /// - returns: nothing (self.timeTableWindow will be updated )
    func setWorkTimeType(_ type:workTimeType,startPeriod:Int,endPeriod:Int) {
        for i in startPeriod ... endPeriod {
        //for var i = startPeriod; i <= endPeriod; i++ {
            if (type == workTimeType.midNightOverTime)
                && (timeTableWindow[i].isWorkTime == workTimeType.plainTime ){
                    timeTableWindow[i].isWorkTime = workTimeType.midNightPlainTime
            } else {
                timeTableWindow[i].isWorkTime = type
            }
        }
    }
    
    /// Serch period of the time from timeTableWindow
    ///
    /// - parameter time : target start time with NSDate
    /// - returns: period number with Int (-1 : Not Hit)
    func getTimePeriod(time:Date)->Int {
        if timeTableWindow.count < 1  {
            
            //print("Not Hit in getTimePeriod with NO timeTableWindow")
            return  -1 // Not Hit
        }
        
        for i in 0 ..< timeTableWindow.count {
            
            if time.compare(timeTableWindow[i].endTime) == .orderedAscending /*endTimeより前*/ {
                if time.compare(timeTableWindow[i].startTime) != .orderedAscending/*startTime以後(=過去でない)*/ {
                    return i
                }
            }
        }
        
        print("Not Hit in getTimePeriod time:",time)
        return -1 // Not Hit
    }
    
    /// Serch period of the time from timeTableWindow
    ///
    /// - parameter endTime : target end time with NSDate
    /// - returns: period number with Int (-1 : Not Hit)
    func getTimePeriod(endTime time:Date)->Int {
        if timeTableWindow.count < 1  {
            return  -1 // Not Hit
        }
        
        for i in 0 ..< timeTableWindow.count {
            
            if time.compare(timeTableWindow[i].endTime) != .orderedDescending /*endTime以前（後でない）*/ {
                if time.compare(timeTableWindow[i].startTime) == .orderedDescending/*startTimeより後*/ {
                    return i
                }
            }
        }
        
        print("Not Hit in getTimePeriod endTime:",time)
        return -1 // Not Hit
    }
    
    /// Serch period of the time from timeTableWindow
    ///
    /// - parameter startLabel : target start time with String "HH:mm"
    /// - returns: period number with Int (-1 : Not Hit)
    func getTimePeriod(startLabel:String)->Int {
        
        // ラベルを範囲内に調整
        let timeLabel = cf.adjustTimeLabel(startLabel).timeStr
        
        for i in 0 ..< timeTableWindow.count {
            if timeTableWindow[i].startTimeLabel == timeLabel { return i }
        }
        print("Not Hit in getTimePeriod startLabel:",timeLabel)
        return -1 // Not Hit
    }

    /// Serch period of the time from timeTableWindow
    ///
    /// - parameter endLabel : target end time with String "HH:mm"
    /// - returns: period number with Int (-1 : Not Hit)
    func getTimePeriod(endLabel:String)->Int {
        
        // ラベルを範囲内に調整
        let timeLabel = cf.adjustTimeLabel(endLabel).timeStr

        for i in 0 ..< timeTableWindow.count {
            if timeTableWindow[i].endTimeLabel == timeLabel { return i }
        }
        print("Not Hit in getTimePeriod endLabel:",timeLabel)
        return -1 // Not Hit
    }
    


}
