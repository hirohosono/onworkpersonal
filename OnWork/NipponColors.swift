//
//  NipponColors.swift
//  RepeatAfterYou
//
//  Created by 細野浩明 on 2017/11/04.
//  Copyright © 2017年 Melting Pot LLC. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(rgb: UInt, alpha: CGFloat = 1.0) {
        let red: CGFloat = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
        let green: CGFloat = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
        let blue: CGFloat = CGFloat(rgb & 0x0000FF) / 255.0
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}

// NipponColors
let NC_001_NADESHIKO = UIColor(rgb:0xDC9FB4) //撫子, NADESHIKO
let NC_002_KOHBAI = UIColor(rgb:0xE16B8C) //紅梅, KOHBAI
let NC_003_SUOH = UIColor(rgb:0x8E354A) //蘇芳, SUOH
let NC_004_TAIKOH = UIColor(rgb:0xF8C3CD) //退紅, TAIKOH
let NC_005_IKKONZOME = UIColor(rgb:0xF4A7B9) //一斥染, IKKONZOME
let NC_006_KUWAZOME = UIColor(rgb:0x64363C) //桑染, KUWAZOME
let NC_007_MOMO = UIColor(rgb:0xF596AA) //桃, MOMO
let NC_007B_MOMO_B = UIColor(rgb:0xFFA0B6) //桃, MOMO+10 #FFA0B6
let NC_008_ICHIGO = UIColor(rgb:0xB5495B) //苺, ICHIGO
let NC_009_USUBENI = UIColor(rgb:0xE87A90) //薄紅, USUBENI
let NC_010_IMAYOH = UIColor(rgb:0xD05A6E) //今様, IMAYOH
let NC_011_NAKABENI = UIColor(rgb:0xDB4D6D) //中紅, NAKABENI
let NC_012_SAKURA = UIColor(rgb:0xFEDFE1) //桜, SAKURA
let NC_013_UMENEZUMI = UIColor(rgb:0x9E7A7A) //梅鼠, UMENEZUMI
let NC_014_KARAKURENAI = UIColor(rgb:0xD0104C) //韓紅花, KARAKURENAI
let NC_015_ENJI = UIColor(rgb:0x9F353A) //燕脂, ENJI
let NC_016_KURENAI = UIColor(rgb:0xCB1B45) //紅, KURENAI
let NC_017_TOKI = UIColor(rgb:0xEEA9A9) //鴇, TOKI
let NC_018_CYOHSYUN = UIColor(rgb:0xBF6766) //長春, CYOHSYUN
let NC_019_KOKIAKE = UIColor(rgb:0x86473F) //深緋, KOKIAKE
let NC_020_SAKURANEZUMI = UIColor(rgb:0xB19693) //桜鼠, SAKURANEZUMI
let NC_021_JINZAMOMI = UIColor(rgb:0xEB7A77) //甚三紅, JINZAMOMI
let NC_022_AZUKI = UIColor(rgb:0x954A45) //小豆, AZUKI
let NC_023_SUOHKOH = UIColor(rgb:0xA96360) //蘇芳香, SUOHKOH
let NC_024_AKABENI = UIColor(rgb:0xCB4042) //赤紅, AKABENI
let NC_025_SHINSYU = UIColor(rgb:0xAB3B3A) //真朱, SHINSYU
let NC_026_HAIZAKURA = UIColor(rgb:0xD7C4BB) //灰桜, HAIZAKURA
let NC_027_KURIUME = UIColor(rgb:0x904840) //栗梅, KURIUME
let NC_028_EBICHA = UIColor(rgb:0x734338) //海老茶, EBICHA
let NC_029_GINSYU = UIColor(rgb:0xC73E3A) //銀朱, GINSYU
let NC_030_KUROTOBI = UIColor(rgb:0x554236) //黒鳶, KUROTOBI
let NC_031_BENITOBI = UIColor(rgb:0x994639) //紅鳶, BENITOBI
let NC_032_AKEBONO = UIColor(rgb:0xF19483) //曙, AKEBONO
let NC_033_BENIKABA = UIColor(rgb:0xB54434) //紅樺, BENIKABA
let NC_034_MIZUGAKI = UIColor(rgb:0xB9887D) //水がき, MIZUGAKI
let NC_035_SANGOSYU = UIColor(rgb:0xF17C67) //珊瑚朱, SANGOSYU
let NC_036_BENIHIWADA = UIColor(rgb:0x884C3A) //紅檜皮, BENIHIWADA
let NC_037_SYOJYOHI = UIColor(rgb:0xE83015) //猩猩緋, SYOJYOHI
let NC_038_ENTAN = UIColor(rgb:0xD75455) //鉛丹, ENTAN
let NC_039_SHIKANCHA = UIColor(rgb:0xB55D4C) //芝翫茶, SHIKANCHA
let NC_040_HIWADA = UIColor(rgb:0x854836) //檜皮, HIWADA
let NC_041_KAKISHIBU = UIColor(rgb:0xA35E47) //柿渋, KAKISHIBU
let NC_042_AKE = UIColor(rgb:0xCC543A) //緋, AKE
let NC_043_TOBI = UIColor(rgb:0x724832) //鳶, TOBI
let NC_044_BENIHI = UIColor(rgb:0xF75C2F) //紅緋, BENIHI
let NC_045_KURIKAWACHA = UIColor(rgb:0x6A4028) //栗皮茶, KURIKAWACHA
let NC_046_BENGARA = UIColor(rgb:0x9A5034) //弁柄, BENGARA
let NC_047_TERIGAKI = UIColor(rgb:0xC46243) //照柿, TERIGAKI
let NC_048_EDOCHA = UIColor(rgb:0xAF5F3C) //江戸茶, EDOCHA
let NC_049_ARAISYU = UIColor(rgb:0xFB966E) //洗朱, ARAISYU
let NC_050_MOMOSHIOCHA = UIColor(rgb:0x724938) //百塩茶, MOMOSHIOCHA
let NC_051_KARACHA = UIColor(rgb:0xB47157) //唐茶, KARACHA
let NC_052_TOKIGARACHA = UIColor(rgb:0xDB8E71) //ときがら茶, TOKIGARACHA
let NC_053_OHNI = UIColor(rgb:0xF05E1C) //黄丹, OHNI
let NC_054_SOHI = UIColor(rgb:0xED784A) //纁, SOHI
let NC_055_ENSYUCHA = UIColor(rgb:0xCA7853) //遠州茶, ENSYUCHA
let NC_056_KABACHA = UIColor(rgb:0xB35C37) //樺茶, KABACHA
let NC_057_KOGECHA = UIColor(rgb:0x563F2E) //焦茶, KOGECHA
let NC_058_AKAKOH = UIColor(rgb:0xE3916E) //赤香, AKAKOH
let NC_059_SUZUMECHA = UIColor(rgb:0x8F5A3C) //雀茶, SUZUMECHA
let NC_060_SHISHI = UIColor(rgb:0xF0A986) //宍, SHISHI
let NC_061_SODENKARACHA = UIColor(rgb:0xA0674B) //宗伝唐茶, SODENKARACHA
let NC_062_KABA = UIColor(rgb:0xC1693C) //樺, KABA
let NC_063_KOKIKUCHINASHI = UIColor(rgb:0xFB9966) //深支子, KOKIKUCHINASHI
let NC_064_KURUMI = UIColor(rgb:0x947A6D) //胡桃, KURUMI
let NC_065_TAISYA = UIColor(rgb:0xA36336) //代赭, TAISYA
let NC_066_ARAIGAKI = UIColor(rgb:0xE79460) //洗柿, ARAIGAKI
let NC_067_KOHROZEN = UIColor(rgb:0x7D532C) //黄櫨染, KOHROZEN
let NC_068_AKAKUCHIBA = UIColor(rgb:0xC78550) //赤朽葉, AKAKUCHIBA
let NC_069_TONOCHA = UIColor(rgb:0x985F2A) //礪茶, TONOCHA
let NC_070_AKASHIROTSURUBAMI = UIColor(rgb:0xE1A679) //赤白橡, AKASHIROTSURUBAMI
let NC_071_SENCHA = UIColor(rgb:0x855B32) //煎茶, SENCHA
let NC_072_KANZO = UIColor(rgb:0xFC9F4D) //萱草, KANZO
let NC_073_SHAREGAKI = UIColor(rgb:0xFFBA84) //洒落柿, SHAREGAKI
let NC_074_BENIUKON = UIColor(rgb:0xE98B2A) //紅鬱金, BENIUKON
let NC_075_UMEZOME = UIColor(rgb:0xE9A368) //梅染, UMEZOME
let NC_076_BIWACHA = UIColor(rgb:0xB17844) //枇杷茶, BIWACHA
let NC_077_CHOJICHA = UIColor(rgb:0x96632E) //丁子茶, CHOJICHA
let NC_078_KENPOHZOME = UIColor(rgb:0x43341B) //憲法染, KENPOHZOME
let NC_079_KOHAKU = UIColor(rgb:0xCA7A2C) //琥珀, KOHAKU
let NC_080_USUGAKI = UIColor(rgb:0xECB88A) //薄柿, USUGAKI
let NC_081_KYARA = UIColor(rgb:0x78552B) //伽羅, KYARA
let NC_082_CHOJIZOME = UIColor(rgb:0xB07736) //丁子染, CHOJIZOME
let NC_083_FUSHIZOME = UIColor(rgb:0x967249) //柴染, FUSHIZOME
let NC_084_KUCHIBA = UIColor(rgb:0xE2943B) //朽葉, KUCHIBA
let NC_085_KINCHA = UIColor(rgb:0xC7802D) //金茶, KINCHA
let NC_086_KITSUNE = UIColor(rgb:0x9B6E23) //狐, KITSUNE
let NC_087_SUSUTAKE = UIColor(rgb:0x6E552F) //煤竹, SUSUTAKE
let NC_088_USUKOH = UIColor(rgb:0xEBB471) //薄香, USUKOH
let NC_089_TONOKO = UIColor(rgb:0xD7B98E) //砥粉, TONOKO
let NC_090_GINSUSUTAKE = UIColor(rgb:0x82663A) //銀煤竹, GINSUSUTAKE
let NC_091_OHDO = UIColor(rgb:0xB68E55) //黄土, OHDO
let NC_092_SHIRACHA = UIColor(rgb:0xBC9F77) //白茶, SHIRACHA
let NC_093_KOBICHA = UIColor(rgb:0x876633) //媚茶, KOBICHA
let NC_094_KIGARACHA = UIColor(rgb:0xC18A26) //黄唐茶, KIGARACHA
let NC_095_YAMABUKI = UIColor(rgb:0xFFB11B) //山吹, YAMABUKI
let NC_096_YAMABUKICHA = UIColor(rgb:0xD19826) //山吹茶, YAMABUKICHA
let NC_097_HAJIZOME = UIColor(rgb:0xDDA52D) //櫨染, HAJIZOME
let NC_098_KUWACHA = UIColor(rgb:0xC99833) //桑茶, KUWACHA
let NC_099_TAMAGO = UIColor(rgb:0xF9BF45) //玉子, TAMAGO
let NC_100_SHIROTSURUBAMI = UIColor(rgb:0xDCB879) //白橡, SHIROTSURUBAMI
let NC_101_KITSURUBAMI = UIColor(rgb:0xBA9132) //黄橡, KITSURUBAMI
let NC_102_TAMAMOROKOSHI = UIColor(rgb:0xE8B647) //玉蜀黍, TAMAMOROKOSHI
let NC_103_HANABA = UIColor(rgb:0xF7C242) //花葉, HANABA
let NC_104_NAMAKABE = UIColor(rgb:0x7D6C46) //生壁, NAMAKABE
let NC_105_TORINOKO = UIColor(rgb:0xDAC9A6) //鳥の子, TORINOKO
let NC_106_USUKI = UIColor(rgb:0xFAD689) //浅黄, USUKI
let NC_107_KIKUCHIBA = UIColor(rgb:0xD9AB42) //黄朽葉, KIKUCHIBA
let NC_108_KUCHINASHI = UIColor(rgb:0xF6C555) //梔子, KUCHINASHI
let NC_109_TOHOH = UIColor(rgb:0xFFC408) //籐黄, TOHOH
let NC_110_UKON = UIColor(rgb:0xEFBB24) //鬱金, UKON
let NC_111_KARASHI = UIColor(rgb:0xCAAD5F) //芥子, KARASHI
let NC_112_HIGOSUSUTAKE = UIColor(rgb:0x8D742A) //肥後煤竹, HIGOSUSUTAKE
let NC_113_RIKYUSHIRACHA = UIColor(rgb:0xB4A582) //利休白茶, RIKYUSHIRACHA
let NC_114_AKU = UIColor(rgb:0x877F6C) //灰汁, AKU
let NC_115_RIKYUCHA = UIColor(rgb:0x897D55) //利休茶, RIKYUCHA
let NC_116_ROKOHCHA = UIColor(rgb:0x74673E) //路考茶, ROKOHCHA
let NC_117_NATANEYU = UIColor(rgb:0xA28C37) //菜種油, NATANEYU
let NC_118_UGUISUCHA = UIColor(rgb:0x6C6024) //鶯茶, UGUISUCHA
let NC_119_KIMIRUCHA = UIColor(rgb:0x867835) //黄海松茶, KIMIRUCHA
let NC_120_MIRUCHA = UIColor(rgb:0x62592C) //海松茶, MIRUCHA
let NC_121_KARIYASU = UIColor(rgb:0xE9CD4C) //刈安, KARIYASU
let NC_122_NANOHANA = UIColor(rgb:0xF7D94C) //菜の花, NANOHANA
let NC_123_KIHADA = UIColor(rgb:0xFBE251) //黄蘗, KIHADA
let NC_124_MUSHIKURI = UIColor(rgb:0xD9CD90) //蒸栗, MUSHIKURI
let NC_125_AOKUCHIBA = UIColor(rgb:0xADA142) //青朽葉, AOKUCHIBA
let NC_126_OMINAESHI = UIColor(rgb:0xDDD23B) //女郎花, OMINAESHI
let NC_127_HIWACHA = UIColor(rgb:0xA5A051) //鶸茶, HIWACHA
let NC_128_HIWA = UIColor(rgb:0xBEC23F) //鶸, HIWA
let NC_129_UGUISU = UIColor(rgb:0x6C6A2D) //鶯, UGUISU
let NC_130_YANAGICHA = UIColor(rgb:0x939650) //柳茶, YANAGICHA
let NC_131_KOKE = UIColor(rgb:0x838A2D) //苔, KOKE
let NC_132_KIKUJIN = UIColor(rgb:0xB1B479) //麹塵, KIKUJIN
let NC_133_RIKANCHA = UIColor(rgb:0x616138) //璃寛茶, RIKANCHA
let NC_134_AIKOBICHA = UIColor(rgb:0x4B4E2A) //藍媚茶, AIKOBICHA
let NC_135_MIRU = UIColor(rgb:0x5B622E) //海松, MIRU
let NC_136_SENSAICHA = UIColor(rgb:0x4D5139) //千歳茶, SENSAICHA
let NC_137_BAIKOCHA = UIColor(rgb:0x89916B) //梅幸茶, BAIKOCHA
let NC_138_HIWAMOEGI = UIColor(rgb:0x90B44B) //鶸萌黄, HIWAMOEGI
let NC_139_YANAGIZOME = UIColor(rgb:0x91AD70) //柳染, YANAGIZOME
let NC_140_URAYANAGI = UIColor(rgb:0xB5CAA0) //裏柳, URAYANAGI
let NC_141_IWAICHA = UIColor(rgb:0x646A58) //岩井茶, IWAICHA
let NC_142_MOEGI = UIColor(rgb:0x7BA23F) //萌黄, MOEGI
let NC_143_NAE = UIColor(rgb:0x86C166) //苗, NAE
let NC_144_YANAGISUSUTAKE = UIColor(rgb:0x4A593D) //柳煤竹, YANAGISUSUTAKE
let NC_145_MATSUBA = UIColor(rgb:0x42602D) //松葉, MATSUBA
let NC_146_AONI = UIColor(rgb:0x516E41) //青丹, AONI
let NC_147_USUAO = UIColor(rgb:0x91B493) //薄青, USUAO
let NC_148_YANAGINEZUMI = UIColor(rgb:0x808F7C) //柳鼠, YANAGINEZUMI
let NC_149_TOKIWA = UIColor(rgb:0x1B813E) //常磐, TOKIWA
let NC_150_WAKATAKE = UIColor(rgb:0x5DAC81) //若竹, WAKATAKE
let NC_151_CHITOSEMIDORI = UIColor(rgb:0x36563C) //千歳緑, CHITOSEMIDORI
let NC_152_MIDORI = UIColor(rgb:0x227D51) //緑, MIDORI
let NC_153_BYAKUROKU = UIColor(rgb:0xA8D8B9) //白緑, BYAKUROKU
let NC_154_OITAKE = UIColor(rgb:0x6A8372) //老竹, OITAKE
let NC_155_TOKUSA = UIColor(rgb:0x2D6D4B) //木賊, TOKUSA
let NC_156_ONANDOCHA = UIColor(rgb:0x465D4C) //御納戸茶, ONANDOCHA
let NC_157_ROKUSYOH = UIColor(rgb:0x24936E) //緑青, ROKUSYOH
let NC_158_SABISEIJI = UIColor(rgb:0x86A697) //錆青磁, SABISEIJI
let NC_159_AOTAKE = UIColor(rgb:0x00896C) //青竹, AOTAKE
let NC_160_VELUDO = UIColor(rgb:0x096148) //ビロード, VELUDO
let NC_161_MUSHIAO = UIColor(rgb:0x20604F) //虫襖, MUSHIAO
let NC_162_AIMIRUCHA = UIColor(rgb:0x0F4C3A) //藍海松茶, AIMIRUCHA
let NC_163_TONOCHA2 = UIColor(rgb:0x4F726C) //沈香茶, TONOCHA2
let NC_164_AOMIDORI = UIColor(rgb:0x00AA90) //青緑, AOMIDORI
let NC_165_SEIJI = UIColor(rgb:0x69B0AC) //青磁, SEIJI
let NC_166_TETSU = UIColor(rgb:0x26453D) //鉄, TETSU
let NC_167_MIZUASAGI = UIColor(rgb:0x66BAB7) //水浅葱, MIZUASAGI
let NC_168_SEIHEKI = UIColor(rgb:0x268785) //青碧, SEIHEKI
let NC_169_SABITETSUONANDO = UIColor(rgb:0x405B55) //錆鉄御納戸, SABITETSUONANDO
let NC_170_KORAINANDO = UIColor(rgb:0x305A56) //高麗納戸, KORAINANDO
let NC_171_BYAKUGUN = UIColor(rgb:0x78C2C4) //白群, BYAKUGUN
let NC_172_OMESHICHA = UIColor(rgb:0x376B6D) //御召茶, OMESHICHA
let NC_173_KAMENOZOKI = UIColor(rgb:0xA5DEE4) //瓶覗, KAMENOZOKI
let NC_173B_KAMENOZOKI_B = UIColor(rgb:0xAFE8EE) //瓶覗, KAMENOZOKI+10 #AFE8EE
let NC_174_FUKAGAWANEZUMI = UIColor(rgb:0x77969A) //深川鼠, FUKAGAWANEZUMI
let NC_175_SABIASAGI = UIColor(rgb:0x6699A1) //錆浅葱, SABIASAGI
let NC_176_MIZU = UIColor(rgb:0x81C7D4) //水, MIZU
let NC_177_ASAGI = UIColor(rgb:0x33A6B8) //浅葱, ASAGI
let NC_178_ONANDO = UIColor(rgb:0x0C4842) //御納戸, ONANDO
let NC_179_AI = UIColor(rgb:0x0D5661) //藍, AI
let NC_180_SHINBASHI = UIColor(rgb:0x0089A7) //新橋, SHINBASHI
let NC_181_SABIONANDO = UIColor(rgb:0x336774) //錆御納戸, SABIONANDO
let NC_182_TETSUONANDO = UIColor(rgb:0x255359) //鉄御納戸, TETSUONANDO
let NC_183_HANAASAGI = UIColor(rgb:0x1E88A8) //花浅葱, HANAASAGI
let NC_184_AINEZUMI = UIColor(rgb:0x566C73) //藍鼠, AINEZUMI
let NC_185_MASUHANA = UIColor(rgb:0x577C8A) //舛花, MASUHANA
let NC_186_SORA = UIColor(rgb:0x58B2DC) //空, SORA
let NC_187_NOSHIMEHANA = UIColor(rgb:0x2B5F75) //熨斗目花, NOSHIMEHANA
let NC_188_CHIGUSA = UIColor(rgb:0x3A8FB7) //千草, CHIGUSA
let NC_189_OMESHIONANDO = UIColor(rgb:0x2E5C6E) //御召御納戸, OMESHIONANDO
let NC_190_HANADA = UIColor(rgb:0x006284) //縹, HANADA
let NC_191_WASURENAGUSA = UIColor(rgb:0x7DB9DE) //勿忘草, WASURENAGUSA
let NC_192_GUNJYO = UIColor(rgb:0x51A8DD) //群青, GUNJYO
let NC_193_TSUYUKUSA = UIColor(rgb:0x2EA9DF) //露草, TSUYUKUSA
let NC_194_KUROTSURUBAMI = UIColor(rgb:0x0B1013) //黒橡, KUROTSURUBAMI
let NC_195_KON = UIColor(rgb:0x0F2540) //紺, KON
let NC_196_KACHI = UIColor(rgb:0x08192D) //褐, KACHI
let NC_197_RURI = UIColor(rgb:0x005CAF) //瑠璃, RURI
let NC_198_RURIKON = UIColor(rgb:0x0B346E) //瑠璃紺, RURIKON
let NC_199_BENIMIDORI = UIColor(rgb:0x7B90D2) //紅碧, BENIMIDORI
let NC_200_FUJINEZUMI = UIColor(rgb:0x6E75A4) //藤鼠, FUJINEZUMI
let NC_201_TETSUKON = UIColor(rgb:0x261E47) //鉄紺, TETSUKON
let NC_202_KONJYO = UIColor(rgb:0x113285) //紺青, KONJYO
let NC_203_BENIKAKEHANA = UIColor(rgb:0x4E4F97) //紅掛花, BENIKAKEHANA
let NC_204_KONKIKYO = UIColor(rgb:0x211E55) //紺桔梗, KONKIKYO
let NC_205_FUJI = UIColor(rgb:0x8B81C3) //藤, FUJI
let NC_206_FUTAAI = UIColor(rgb:0x70649A) //二藍, FUTAAI
let NC_207_OUCHI = UIColor(rgb:0x9B90C2) //楝, OUCHI
let NC_208_FUJIMURASAKI = UIColor(rgb:0x8A6BBE) //藤紫, FUJIMURASAKI
let NC_209_KIKYO = UIColor(rgb:0x6A4C9C) //桔梗, KIKYO
let NC_210_SHION = UIColor(rgb:0x8F77B5) //紫苑, SHION
let NC_211_MESSHI = UIColor(rgb:0x533D5B) //滅紫, MESSHI
let NC_212_USU = UIColor(rgb:0xB28FCE) //薄, USU
let NC_213_HASHITA = UIColor(rgb:0x986DB2) //半, HASHITA
let NC_214_EDOMURASAKI = UIColor(rgb:0x77428D) //江戸紫, EDOMURASAKI
let NC_215_SHIKON = UIColor(rgb:0x3C2F41) //紫紺, SHIKON
let NC_216_KOKIMURASAKI = UIColor(rgb:0x4A225D) //深紫, KOKIMURASAKI
let NC_217_SUMIRE = UIColor(rgb:0x66327C) //菫, SUMIRE
let NC_218_MURASAKI = UIColor(rgb:0x592C63) //紫, MURASAKI
let NC_219_AYAME = UIColor(rgb:0x6F3381) //菖蒲, AYAME
let NC_220_FUJISUSUTAKE = UIColor(rgb:0x574C57) //藤煤竹, FUJISUSUTAKE
let NC_221_BENIFUJI = UIColor(rgb:0xB481BB) //紅藤, BENIFUJI
let NC_222_KUROBENI = UIColor(rgb:0x3F2B36) //黒紅, KUROBENI
let NC_223_NASUKON = UIColor(rgb:0x572A3F) //茄子紺, NASUKON
let NC_224_BUDOHNEZUMI = UIColor(rgb:0x5E3D50) //葡萄鼠, BUDOHNEZUMI
let NC_225_HATOBANEZUMI = UIColor(rgb:0x72636E) //鳩羽鼠, HATOBANEZUMI
let NC_226_KAKITSUBATA = UIColor(rgb:0x622954) //杜若, KAKITSUBATA
let NC_227_EBIZOME = UIColor(rgb:0x6D2E5B) //蒲葡, EBIZOME
let NC_228_BOTAN = UIColor(rgb:0xC1328E) //牡丹, BOTAN
let NC_229_UMEMURASAKI = UIColor(rgb:0xA8497A) //梅紫, UMEMURASAKI
let NC_230_NISEMURASAKI = UIColor(rgb:0x562E37) //似紫, NISEMURASAKI
let NC_231_TSUTSUJI = UIColor(rgb:0xE03C8A) //躑躅, TSUTSUJI
let NC_232_MURASAKITOBI = UIColor(rgb:0x60373E) //紫鳶, MURASAKITOBI
let NC_233_SHIRONERI = UIColor(rgb:0xFCFAF2) //白練, SHIRONERI
let NC_234_GOFUN = UIColor(rgb:0xFFFFFB) //胡粉, GOFUN
let NC_235_SHIRONEZUMI = UIColor(rgb:0xBDC0BA) //白鼠, SHIRONEZUMI
let NC_236_GINNEZUMI = UIColor(rgb:0x91989F) //銀鼠, GINNEZUMI
let NC_237_NAMARI = UIColor(rgb:0x787878) //鉛, NAMARI
let NC_238_HAI = UIColor(rgb:0x828282) //灰, HAI
let NC_239_SUNEZUMI = UIColor(rgb:0x787D7B) //素鼠, SUNEZUMI
let NC_240_RIKYUNEZUMI = UIColor(rgb:0x707C74) //利休鼠, RIKYUNEZUMI
let NC_241_NIBI = UIColor(rgb:0x656765) //鈍, NIBI
let NC_242_AONIBI = UIColor(rgb:0x535953) //青鈍, AONIBI
let NC_243_DOBUNEZUMI = UIColor(rgb:0x4F4F48) //溝鼠, DOBUNEZUMI
let NC_244_BENIKESHINEZUMI = UIColor(rgb:0x52433D) //紅消鼠, BENIKESHINEZUMI
let NC_245_AISUMICHA = UIColor(rgb:0x373C38) //藍墨茶, AISUMICHA
let NC_246_BINROJIZOME = UIColor(rgb:0x3A3226) //檳榔子染, BINROJIZOME
let NC_247_KESHIZUMI = UIColor(rgb:0x434343) //消炭, KESHIZUMI
let NC_248_SUMI = UIColor(rgb:0x1C1C1C) //墨, SUMI
let NC_249_KURO = UIColor(rgb:0x080808) //黒, KURO
let NC_250_RO = UIColor(rgb:0x0C0C0C) //呂, RO


