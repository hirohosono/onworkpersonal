//
//  TabVC.swift
//  OnWork
//
//  Created by 細野浩明 on 2017/11/10.
//  Copyright © 2017年 Hiroaki Hosono. All rights reserved.
//

import UIKit

class TabVC: UITabBarController {

    let appDl:AppDelegate = UIApplication.shared.delegate as! AppDelegate //AppDelegateのインスタンスを取得
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // fontの設定
        let fontFamily = UIFont.systemFont(ofSize: 10)
        
        // 選択時の設定
        let selectedColor:UIColor = NC_014_KARAKURENAI
        let selectedAttributes = [NSAttributedStringKey.font : fontFamily , NSAttributedStringKey.foregroundColor: selectedColor]
        
        /// タイトルテキストカラーの設定
        UITabBarItem.appearance().setTitleTextAttributes(selectedAttributes, for: UIControlState.selected)
        
        UITabBar.appearance().tintColor = NC_014_KARAKURENAI
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
