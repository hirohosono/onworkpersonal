//
//  appCommon.swift
//  OnWork
//
//  Created by 細野浩明 on 2015/08/30.
//  Copyright © 2015年 Hiroaki Hosono. All rights reserved.
//

import Foundation
import UIKit

let NO_SELECTION:String = " -- "

let bit0:UInt8 = 0b00000001
let bit1:UInt8 = 0b00000010
let bit2:UInt8 = 0b00000100
let bit3:UInt8 = 0b00001000
let bit4:UInt8 = 0b00010000
let bit5:UInt8 = 0b00100000
let bit6:UInt8 = 0b01000000
let bit7:UInt8 = 0b10000000

struct userInfo {
    var MBuserName:String = ""
    var name:String = ""
    var nameKana:String = ""
    var companyName:String = ""
    var groupName:String = ""
    var UserID:String = ""
    var sex:String = ""
    var defaultLocation:String = ""
    var defaultTimeTable:String = ""
    var currentLocation:String = ""
    var currentTimeTable:String = ""
    var XisLoggedIn:Bool = false  // 互換性のために残しておくけど使わない
    var isActiveUser:Bool = false
    var isManager:Bool = false
    var companyID:String = ""
    var mail:String = ""
    var password:String = ""
    // 内部管理用
    var isAuthenticated:Bool = false
    var mailBK:String = "" // 登録時のメールデータ（未認証時に見るためのデータ）

    
    /// カンマ区切りの文字列に変換
    ///
    func data2CSV() -> String {
        var rtnStr = MBuserName + "," + name + "," + nameKana +  "," + companyName +  "," + groupName
        rtnStr +=  "," + UserID  + "," + sex +  "," + defaultLocation + "," + defaultTimeTable
        rtnStr +=  "," + currentLocation + "," + currentTimeTable + ","
        if XisLoggedIn { rtnStr += "1" } else { rtnStr += "0" }
        if isActiveUser { rtnStr += ",1" } else { rtnStr += ",0" }
        if isManager { rtnStr += ",1" } else { rtnStr += ",0" }
        rtnStr += "," +  companyID + "," + mail + "," + password
        return rtnStr
    }
    ///カンマ区切りの文字列から値を復元
    ///
    mutating func CSV2Data (_ str:String) {
        let arItems = str.components(separatedBy: ",")
        for i in 0 ..< arItems.count {
            //print("i=\(i)",arItems[i])
            switch i {
            case 0:
                MBuserName = arItems[0]
            case 1:
                name = arItems[1]
            case 2:
                nameKana = arItems[2]
            case 3:
                companyName = arItems[3]
            case 4:
                groupName = arItems[4]
            case 5:
                UserID = arItems[5]
            case 6:
                sex = arItems[6]
            case 7:
                defaultLocation = arItems[7]
            case 8:
                defaultTimeTable = arItems[8]
            case 9:
                currentLocation = arItems[9]
            case 10:
                currentTimeTable = arItems[10]
            case 11:
                XisLoggedIn = (arItems[11] == "1")
            case 12:
                isActiveUser = (arItems[12] == "1")
            case 13:
                isManager = (arItems[13] == "1")
            case 14:
                companyID = arItems[14]
            case 15:
                mail = arItems[15]
            case 16:
                password = arItems[16]
            default:
                break;
            }
        }
    }
    
    ///データをクリア
    ///
    mutating func clear() {
        MBuserName = ""
        name = ""
        nameKana = ""
        companyName = ""
        groupName = ""
        UserID = ""
        sex = ""
        defaultLocation = ""
        defaultTimeTable = ""
        currentLocation = ""
        currentTimeTable = ""
        XisLoggedIn = false
        isActiveUser = false
        isManager = false
        companyID = ""
        mail = ""
        password = ""
    }

}


struct workDataOfDay {
    var name:String = ""
    var companyName:String = ""
    var companyID:String = ""
    var groupName:String = ""
    var UserID:String = ""
    var beginningOfDay:Date? = nil  // 一日の始まりの日時　＝　その日を表す
    var endingOfDay:Date? = nil  // 一日の終わりの日時　＝　次の日の始まり
    var dateLabel:String = "" // MM/DD
    var weekdayLabel:String = "" // (月)
    var holidayName:String = ""  //  xxの日
    var additional:String = ""  //  xxの日 備考
    var isDayOff:Bool = false
    var location:String = ""
    var locationID:String = ""
    var timeTable:String = ""
    var timeTableID:String = ""
    var startTimeOfWork:Date? = nil  // 業務の始まり
    var endTimeOfWork:Date? = nil  // 業務の終わり
    var workHours:Int = 0  // minutes
    var overtimeWorkHours:Int = 0  // minutes
    var lateNightWorkHours:Int = 0 // minutes
    var memo:String = ""
    var isStartManually:Bool = false
    var isEndManually:Bool = false
    var startLocation:String = ""
    var startAddress:String = ""
    var endLocation:String = ""
    var endAddress:String = ""
    var isThisMonth:Bool = false
    var weekdayNum:Int = 0
    
    // 出勤をクリア
    mutating func clearStart() {
        startTimeOfWork = nil  // 業務の始まり
        isStartManually = false
        startLocation = ""
        startAddress = ""
        workHours = 0  // minutes
        overtimeWorkHours = 0  // minutes
        lateNightWorkHours = 0 // minutes
    }
    // 退勤をクリア
    mutating func clearEnd() {
        endTimeOfWork = nil  // 業務の終わり
        isEndManually = false
        endLocation = ""
        endAddress = ""
        workHours = 0  // minutes
        overtimeWorkHours = 0  // minutes
        lateNightWorkHours = 0 // minutes
    }
}

struct dayOfCalendar {
    var date:Date? = nil  // 一日の始まりの日時　＝　その日を表す
    var dateLabel:String = "" // MM/DD
    var weekdayLabel:String = "" // (月)
    var holidayName:String = ""  //  xxの日
    var additional:String = ""  //  xxの日 備考
    var isPublic:Bool = false
    var isOnWork:Bool = false
    var weekdayNum:Int = 0
}

struct StampInfo {
    var workTime:Date? = nil
    var isManually:Bool = false
    var workPlace:String = ""
    var stampTime:Date? = nil
    var stampGeo:CLLocation? = nil
    var stampAddress:String = ""
    
}
struct dailyLogInfo {
    var date:Date? = nil
    var dayKind:String = ""
    var companyName:String = ""
    var companyID:String = ""
    var groupName:String = ""
    var UserID:String = ""
    var isDayOff:Bool = false
    var location:String = ""
    var locationID:String = ""
    var timeTable:String = ""
    var timeTableID:String = ""

    var startStamp:StampInfo? = nil
    var endStamp:StampInfo? = nil
    var workHours:Int = 0  // minutes
    var overtimeWorkHours:Int = 0  // minutes
    var lateNightWorkHours:Int = 0 // minutes
    var memo:String = ""
    
}


struct companyInfo {
    var companyID:String = ""
    var companyName:String = ""
    var dateLine:String = "05:00"
    var closeDate:Int = 31
    var unitTime:Int = 15
    var isUseNationalHolidays:Bool = true
    var companyLabel:String = ""
}

struct workPlaceInfo {
    var workPlaceID:String = ""
    var workPlaceLabel:String = ""
    var dateLine:String = "05:00"
    var closeDate:Int = 31
    var unitTime:Int = 15
    var isUseNationalHolidays:Bool = false
    var personID:String = ""
    var hourlyRate:Int = 1000
    var midNightRate:Int = 25 // % プラス分
    var overTimeRate:Int = 25 // % プラス分

}


struct workDataOfMonth {
    var workDays:Int = 0  // days
    var workHours:Int = 0  // minutes
    var overtimeWorkHours:Int = 0  // minutes
    var lateNightWorkHours:Int = 0 // minutes
}

struct weeklyCalendar {
    var companyName:String = ""
    var companyID:String = ""
    var calendarName:String = ""
    var isDayOffSunday:Bool = false
    var isDayOffMonday:Bool = false
    var isDayOffTuesday:Bool = false
    var isDayOffWednesday:Bool = false
    var isDayOffThursday:Bool = false
    var isDayOffFriday:Bool = false
    var isDayOffSaturday:Bool = false
}
struct holidayInfo {
    var date:Date = Date()
    var holidayName:String = ""
    var memo:String = ""
    var isPublic:Bool = false
    var isOnWork:Bool = false
}
struct holidayDef {
    var companyName:String = ""
    var companyID:String = ""
    var date:Date = Date()
    var holidayName:String = ""
    var calendarName:String = ""
    var isPublic:Bool = false
    var isOnWork:Bool = false
    var memo:String = ""
}

struct locationDef {
    var companyName:String = ""
    var companyID:String = ""
    var LocationName:String = ""
    var locationID:String = ""

    var address:String = ""
    var geo:NCMBGeoPoint = NCMBGeoPoint()
    var isUseBeacon:Bool = false
    var beaconUUID:String = ""
    var beaconMajor:String = ""
    var beaconMinor:String = ""
}

struct timeTableDef {
    var companyName:String = ""
    var companyID:String = ""
    var timeTableID:String = ""
    var tableName:String = ""
    var startTime:String = "09:00" //出勤時刻(定時)
    var endTime:String = "17:00" //退勤時刻(定時)
    var recessStart1:String = "12:00" // 昼休み開始
    var recessEnd1:String = "13:00" // 昼休み終了
    var recessStart2:String = ""
    var recessEnd2:String = ""
    var recessStart3:String = ""
    var recessEnd3:String = ""
    var recessStart4:String = ""
    var recessEnd4:String = ""
    var recessStart5:String = ""
    var recessEnd5:String = ""
    
    mutating func isDayOff() {
        timeTableID = "-1"
        tableName = "休日"
        startTime = "09:00" //出勤時刻(定時)
        endTime = "09:00" //退勤時刻(定時)
        recessStart1 = "" // 昼休み開始
        recessEnd1 = "" // 昼休み終了
        recessStart2 = ""
        recessEnd2 = ""
        recessStart3 = ""
        recessEnd3 = ""
        recessStart4 = ""
        recessEnd4 = ""
        recessStart5 = ""
        recessEnd5 = ""

    }
}

struct shiftInfo {
    var name:String = ""
    var timeTableID:String = ""
    var date:Date = Date(timeIntervalSince1970: 0)
    var holidayName:String = ""
    var isOnWork:Bool = false
    var startTime:String = ""
    var endTime:String = ""
    var memo:String = ""
    var memberCount:Int = 0
    var members:[workDataOfDay] = []
    
}

struct weeklySchedule {
    var scheduleID:String = ""
    var workPlaceID:String = ""
    var personID:String = ""
    var isOn_Mon:Bool = false
    var WP_Mon:String = ""
    var Loc_Mon:String = ""
    var TT_Mon:String = ""
    var isOn_Tue:Bool = false
    var WP_Tue:String = ""
    var Loc_Tue:String = ""
    var TT_Tue:String = ""
    var isOn_Wed:Bool = false
    var WP_Wed:String = ""
    var Loc_Wed:String = ""
    var TT_Wed:String = ""
    var isOn_Thu:Bool = false
    var WP_Thu:String = ""
    var Loc_Thu:String = ""
    var TT_Thu:String = ""
    var isOn_Fri:Bool = false
    var WP_Fri:String = ""
    var Loc_Fri:String = ""
    var TT_Fri:String = ""
    var isOn_Sat:Bool = false
    var WP_Sat:String = ""
    var Loc_Sat:String = ""
    var TT_Sat:String = ""
    var isOn_Sun:Bool = false
    var WP_Sun:String = ""
    var Loc_Sun:String = ""
    var TT_Sun:String = ""
    var isWeekdayOff:[Bool] = [true,true,true,true,true,true,true,true]
    
    mutating func upDateDayOff() {
        isWeekdayOff[1] = !isOn_Sun
        isWeekdayOff[2] = !isOn_Mon
        isWeekdayOff[3] = !isOn_Tue
        isWeekdayOff[4] = !isOn_Wed
        isWeekdayOff[5] = !isOn_Thu
        isWeekdayOff[6] = !isOn_Fri
        isWeekdayOff[7] = !isOn_Sat
   }
}



let freeLicenseCount:Int = 3
let smallLicenseCount:Int = 10
let mediumLicenseCount:Int = 25
let largeLicenseCount:Int = 40
let limitCountOfGroup:Int = 5
let limitCountOfLocation:Int = 5
let limitCountOfTimetable:Int = 5

struct licenseDef {
    var companyName:String = ""
    var companyID:String = ""
    var isLicensed:Bool = false
    var owner:String = ""
    var smallLicense:licenseDetail = licenseDetail(count:smallLicenseCount)
    var mediumLicense:licenseDetail = licenseDetail(count:mediumLicenseCount)
    var largeLicense:licenseDetail = licenseDetail(count:largeLicenseCount)
    var licenseCount:Int = freeLicenseCount
    var limitOfGroup:Int = 5
    var limitOfLocation:Int = 5
    var limitOfTimetable:Int = 5
}
struct licenseDetail {
    var isLicenseAvailable:Bool = false
    var term:Int = 0 // 0: Nothing,1: 1 Month,12: 1 Year
    var termRenew:Int = 0 // 0: Nothing,1: 1 Month,12: 1 Year
    var ExpireDate:Date? = nil
    var isRenewAutomatically:Bool? = nil
    var LicenseCount:Int
    init(count:Int) {
        LicenseCount = count
    }
}


struct groupInfo {
    var name:String = ""
    var ID:String = ""
    var memberCount:Int = 0
    var memberList:[userInfo] = []
    var members:[workDataOfDay] = []

}

struct userSummary {
    var workDays:Int = 0
    var workDaysOnDaysOff:Int = 0
    var workHours:Int = 0  // minutes
    var workHoursOnDaysOff:Int = 0  // minutes
    var overtimeWorkHours:Int = 0  // minutes
    var lateNightWorkHours:Int = 0 // minutes
    var user = userInfo()
}

struct locationInfo {
    var name:String = ""
    var id:String = ""
    var address:String = ""
    var geo:NCMBGeoPoint = NCMBGeoPoint()
    
    var memberCount:Int = 0
    var members:[workDataOfDay] = []

}

struct trialUserInfo {
    var isAvalable:Bool = false
    var companyLabel:String = ""
    var companyName:String = ""
    var groupName:String = ""
    var companyID:String = ""
    var IDNo:String = ""
    var name:String = ""
    var nameKana:String = ""
    var sex:String = ""
    var mail:String = ""
    var password:String = ""
}

struct personalUserInfo {
    var companyLabel:String = "勤務地１"
    var companyName:String = "勤務地１"
    var groupName:String = ""
    var companyID:String = ""
    var IDNo:String = ""
    var name:String = ""
    var nameKana:String = ""
    var sex:String = ""
    var mail:String = ""
    var defaultLocation:String = ""
    var defaultTimeTable:String = ""
    var currentLocation:String = ""
    var currentTimeTable:String = ""
    var password:String = ""
    var isRegistered:Bool = false
    var isAuthorized:Bool = false
    var isLoggedIn:Bool = false
    // 内部管理用
    var isAuthenticated:Bool = false
    var mailBK:String = "" // 登録時のメールデータ（未認証時に見るためのデータ）
    
}

struct userMonthlyDataCSV {
    var companyName:String = "" //企業名
    var UserID:String = "" //従業員コード
    var userName:String = "" //氏名
    var userNameKana:String = "" //氏名（フリガナ）
    var Sex:String = "" //性別　男・女
    var workTime:Int = 0 //所定労働時間（分）
    var overtimeWorkStatutory:Int = 0 //法定内残業時間（分） **未サポート　全て所定残業扱い
    var overtimeWork:Int = 0 //時間外労働時間（分）
    var lateNightWork:Int = 0 //深夜労働時間（分）
    var workHoursOnDaysOffStatutory:Int = 0 //法定休日労働時間（分） **未サポート　全て所定休日扱い
    var workHoursOnDaysOff:Int = 0 //所定休日労働時間（分）
    var totalWorkHours:Int = 0 //総労働時間（分）
    var totalWorkDays:Int = 0 //総労働日数
    var workDays:Int = 0 //所定労働出勤日数
    var workDaysOnDaysOff:Int = 0 //所定休日出勤日数
    var workDaysOnDaysOffStatutory:Int = 0 //法定休日出勤日数 **未サポート　全て所定休日扱い
    var tardyHours:Int = 0 //遅刻時間（分） **未サポート
    var earlyLeavingHours:Int = 0 //早退時間（分） **未サポート
    var absenceDays:Int = 0 //欠勤日数 **未サポート
    var tardyDays:Int = 0 //遅刻日数 **未サポート
    var earlyLeavingDays:Int = 0 //早退日数 **未サポート
    var paidAbsenceDays:Int = 0 //年休取得日数 **未サポート
    var initialDate:String = "" //集計開始日　ex. 2015/06/16(火)
    var finalDate:String = "" //集計終了日　ex. 2015/06/16(火)
    /// カンマ区切りの文字列に変換
    ///
    func data2CSV() -> String {
        var rtnStr = companyName + "," + UserID + "," + userName +  "," + userNameKana +  "," + Sex
        rtnStr +=  "," + workTime.description  + "," + overtimeWorkStatutory.description +  "," + overtimeWork.description + "," + lateNightWork.description
        rtnStr +=  "," + workHoursOnDaysOffStatutory.description + "," + workHoursOnDaysOff.description + "," + totalWorkHours.description
        rtnStr +=  "," + totalWorkDays.description + "," + workDays.description + "," + workDaysOnDaysOff.description
        rtnStr +=  "," + workDaysOnDaysOffStatutory.description + "," + tardyHours.description + "," + earlyLeavingHours.description
        rtnStr +=  "," + absenceDays.description + "," + tardyDays.description + "," + earlyLeavingDays.description
        rtnStr +=  "," + paidAbsenceDays.description
        rtnStr +=  "," + initialDate + "," + finalDate
        return rtnStr
    }
}


    let myColorPastelYellow = UIColor(red: 255.0/255.0, green: 245.0/255.0, blue: 200.0/255.0, alpha: 1.0) // PastelYellow     R255,G245,B200
    let myColorPastelBlue = UIColor(red: 180.0/255.0, green: 225.0/255.0, blue: 255.0/255.0, alpha: 1.0) // PastelBlue       R180,G225,B255
    let myColorPastelGreen = UIColor(red: 180.0/255.0, green: 255.0/255.0, blue: 180.0/255.0, alpha: 1.0) // PastelGreen      R180,G255,B180
    let myColorPastelLightPink = UIColor(red: 255.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0) // PastelLightPink  R255,G230,B230
    let myColorDarkPastelBlue = UIColor(red: 100.0/255.0, green: 145.0/255.0, blue: 175.0/255.0, alpha: 1.0) // DarkPastelBlue   R100,G145,B175
    let myColorDarkPastelOrange = UIColor(red: 165.0/255.0, green: 140.0/255.0, blue: 120.0/255.0, alpha: 1.0) // DarkPastelOrange R165,G140,B120
    let myColorDarkPastelPink = UIColor(red: 155.0/255.0, green: 120.0/255.0, blue: 120.0/255.0, alpha: 1.0) // DarkPastelPink   R155,G120,B120

let MYCOL_CLEAR_50      = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5) //黒 透過率50％
let MYCOL_CLEAR_70      = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.3) //黒 透過率70％
let MYCOL_CLEAR_90      = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.1) //黒 透過率90％
let MYCOL_YELLOW_50     = UIColor(red: 1.0, green: 177.0/255.0, blue:  27.0/255.0, alpha: 0.5) //山吹 透過率50％
let MYCOL_CLEAR_W_10    = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.9) //白 透過率10％
let MYCOL_CLEAR_W_90    = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.1) //白 透過率90％
let ICO_COL_CLEAR       = UIColor(rgb:0xFFFFFF, alpha: 0.0)
let ICO_COL_LOCKED      = UIColor(rgb:0xFF0000, alpha: 0.25)
let ICO_COL_UNLOCKED    = UIColor(rgb:0x00FF00, alpha: 0.25)


struct baseColor {
    var BASE:UIColor                = NC_234_GOFUN
    var TX_BASE:UIColor             = UIColor.clear
    var TX_LETTER:UIColor           = NC_194_KUROTSURUBAMI
    var ICO_BASE:UIColor            = MYCOL_YELLOW_50
    var TITLE_TX:UIColor            = NC_177_ASAGI
    var BTN_ADD:UIColor             = NC_177_ASAGI
    var BTN_SAVE:UIColor            = NC_014_KARAKURENAI
    var BTN_RESTORE:UIColor         = NC_095_YAMABUKI
    var CAL_BG:UIColor              = NC_235_SHIRONEZUMI
    var CAL_CELL_BG:UIColor         = NC_234_GOFUN
    var CAL_BG_WEEKDAY:UIColor      = NC_233_SHIRONERI
    var CAL_BG_HOLIDAY:UIColor      = myColorPastelLightPink
    var CAL_BG_WEEKDAY_PAST:UIColor = NC_173_KAMENOZOKI
    var CAL_BG_HOLIDAY_PAST:UIColor = NC_007_MOMO
    var CAL_BG_OUT_OF_RANGE:UIColor = NC_235_SHIRONEZUMI
    var CAL_TX_WEEKDAY:UIColor      = NC_194_KUROTSURUBAMI
    var CAL_TX_HOLIDAY:UIColor      = NC_014_KARAKURENAI
    var CAL_TX_OUT_OF_RANGE:UIColor = UIColor.lightGray
    var SECT_BG_NO_EVENT:UIColor    = NC_180_SHINBASHI
    var SECT_BG_NO_RSV:UIColor      = NC_014_KARAKURENAI
    var SECT_TX:UIColor             = NC_233_SHIRONERI
    var ROW_BG:UIColor              = NC_234_GOFUN
    var ROW_TX_NORMAL:UIColor       = NC_194_KUROTSURUBAMI
    var ROW_TX_ALART:UIColor        = NC_014_KARAKURENAI
    var SW_ITEM_TYPE_BG:UIColor     = NC_194_KUROTSURUBAMI
    var VIEW_GROUP_BG:UIColor       = NC_173B_KAMENOZOKI_B
    var VIEW_GROUP_TITLE:UIColor    = NC_177_ASAGI
    var MONEY:UIColor               = NC_014_KARAKURENAI
}

enum coinItem {
    case basicPlan          // 基本プラン
    case exportNormal       // １ヶ月分の集計エクスポート（標準形式CSV）
    case exportExtra        // １ヶ月分の集計エクスポート（追加形式CSV）
    case exportWithGPS      // １ヶ月分の集計エクスポート（GPSデータつきCSV）
    case printNormal        // １ヶ月分の集計印刷（標準形式）
    case savePastData6Month // データ保存（６ヶ月）
    case savePastData1Year  // データ保存（１年）
    case savePastData2Year  // データ保存（2年）
    case autoStamp          // 自動タイムスタンプ
}

enum coinDurableType : Int {
    case purchase = 0   // 購入
    case durable        // 耐久財
    case expendable     // 消耗品
}

struct PCoinPriceTable {
    var item : coinItem
    var price : Int
}

extension UserDefaults {
    
    func colorForKey(_ key: String) -> UIColor? {
        var color: UIColor?
        if let colorData = data(forKey: key) {
            color = NSKeyedUnarchiver.unarchiveObject(with: colorData) as? UIColor
        }
        return color
    }
    
    func setColor(_ color: UIColor?, forKey key: String) {
        var colorData: Data?
        if let color = color {
            colorData = NSKeyedArchiver.archivedData(withRootObject: color)
        }
        set(colorData, forKey: key)
    }
    
}

class BackgroundEnabledButton: UIButton {
    @IBInspectable var enabledBackgroundColor :UIColor?
    @IBInspectable var disabledBackgroundColor :UIColor?
    override var isEnabled :Bool {
        didSet {
            if isEnabled {
                self.backgroundColor = enabledBackgroundColor
            }
            else {
                self.backgroundColor = disabledBackgroundColor
            }
        }
    }
}

class RoundedView: UIView {
    @IBInspectable var cornerRadius :CGFloat = 10.0
    @IBInspectable var cornerTopLeft :Bool = false
    @IBInspectable var cornerTopRight :Bool = false
    @IBInspectable var cornerBottomLeft :Bool = false
    @IBInspectable var cornerBottomRight :Bool = false

    var corners: UIRectCorner = []
    
    override func layoutSubviews() {
        super.layoutSubviews()
        corners = []
        if cornerTopLeft {corners.insert(.topLeft)  }
        if cornerTopRight {corners.insert(.topRight)  }
        if cornerBottomLeft {corners.insert(.bottomLeft)  }
        if cornerBottomRight {corners.insert(.bottomRight)  }
       
        let maskPath = UIBezierPath(roundedRect: self.bounds,
                                    byRoundingCorners: self.corners,
                                    cornerRadii: CGSize(width: self.cornerRadius, height: self.cornerRadius))
        
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = maskPath.cgPath
        
        self.layer.mask = maskLayer
    }

}

/// CheckBox Button customized from UIButton
///
/// Using On/Off image named "CheckBoxOn.png"/"CheckBoxOff.png"
///
/// - parameter .Checked: : togle state true <-> false
///
class CheckBox : UIButton {
    let imgCheckBoxOn : UIImage? = UIImage( named:"CheckBoxOn64")
    let imgCheckBoxOff : UIImage? = UIImage( named:"CheckBoxOff64")
    
    var Checked : Bool = false {
        didSet {
            self.setState(self)
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setImage(imgCheckBoxOff, for: UIControlState() )
        self.setImage(imgCheckBoxOn, for: .selected )
        self.setImage(imgCheckBoxOn, for: .highlighted )
        self.setImage(imgCheckBoxOff, for: .disabled )
        // actionで指定する関数名の後に”:”が必要らしい
        self.addTarget(self, action: #selector(CheckBox.checkBoxClicked(_:)), for:UIControlEvents.touchUpInside)
        self.setState(self)
        //println("init CheckBox")
    }
    
    override init(frame: CGRect) {
        Checked = true
        super.init(frame: frame)
    }
    
    
    // 場合によっては先頭に @IBActionをつける必要あり
    @objc func checkBoxClicked (_ button:CheckBox) {
        button.Checked = !button.Checked
        // カスタムプロパティ更新時にValueChangedイベント発生させる
        sendActions(for: .valueChanged)
    }
    
    func setState (_ button:CheckBox) {
        if button.Checked {
            button.isSelected = true
        }else{
            button.isSelected = false
        }
    }
    
}

